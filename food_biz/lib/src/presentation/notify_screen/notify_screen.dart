import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/configs.dart';

import '../presentation.dart';

class NotifyScreen extends StatefulWidget {
  @override
  _NotifyScreenState createState() => _NotifyScreenState();
}

class _NotifyScreenState extends State<NotifyScreen> with ResponsiveWidget {
  late NotifyViewModel _viewModel;
  @override
  Widget build(BuildContext context) {
    return BaseWidget<NotifyViewModel>(
        viewModel: NotifyViewModel(),
        onViewModelReady: (viewModel) {
          _viewModel = viewModel..init();
        },
        builder: (context, viewModel, appbar) {
          return Scaffold(
              appBar: buildAppBar(
                  title: Text(
                'Thông báo',
                style: TextStyle(color: Colors.black),
                textAlign: TextAlign.center,
              )),
              backgroundColor: Colors.grey.shade300,
              body: buildUi(context));
        });
  }

  @override
  Widget buildDesktop(BuildContext context) {
    return _buildUi(context);
  }

  @override
  Widget buildMobile(BuildContext context) {
    return _buildUi(context);
  }

  @override
  Widget buildTablet(BuildContext context) {
    return _buildUi(context);
  }

  Widget _buildUi(BuildContext context) {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          children: [
            _notifyCard(),
            _notifyCard(),
            _notifyCard(),
            _notifyCard(),
            _notifyCard(),
            _notifyCard(),
          ],
        ),
      ),
    );
  }

  Widget _notifyCard() {
    return GestureDetector(
      onTap: () {
        _viewModel.onTapNotify();
      },
      child: Container(
          margin: EdgeInsets.only(top: 20, left: 20, right: 20),
          padding: EdgeInsets.all(10),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(10),
          ),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Image.asset(
                    AppImages.iconNotify,
                    scale: 4,
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Giao đơn hàng thành công',
                        style: TextStyle(fontSize: 16),
                      ),
                      Text('08:00 - 20/08/2021'),
                    ],
                  )
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                height: 167,
                decoration: BoxDecoration(
                  color: Colors.red,
                  borderRadius: BorderRadius.circular(10),
                  image: DecorationImage(
                    image: AssetImage(
                      AppImages.imgNotify,
                    ),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Text(
                'Lorem Ipsum is simply dummy text of the printing and typesetting industry. ',
                overflow: TextOverflow.clip,
                maxLines: 1,
              ),
              SizedBox(
                height: 20,
              ),
            ],
          )),
    );
  }
}
