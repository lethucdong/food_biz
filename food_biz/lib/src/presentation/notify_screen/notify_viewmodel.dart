import '../presentation.dart';
import 'package:get/get.dart';

class NotifyViewModel extends BaseViewModel {
  init() async {}
  onTapNotify() {
    Get.toNamed(Routers.notify_detail);
  }
}
