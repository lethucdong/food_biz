import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/configs.dart';

import '../../presentation.dart';

class NotifyDetailScreen extends StatefulWidget {
  @override
  _NotifyDetailScreenState createState() => _NotifyDetailScreenState();
}

class _NotifyDetailScreenState extends State<NotifyDetailScreen>
    with ResponsiveWidget {
  late NotifyDetailViewModel _viewModel;
  @override
  Widget build(BuildContext context) {
    return BaseWidget<NotifyDetailViewModel>(
        viewModel: NotifyDetailViewModel(),
        onViewModelReady: (viewModel) {
          _viewModel = viewModel..init();
        },
        builder: (context, viewModel, appbar) {
          return Scaffold(
              appBar: buildAppBar(
                  title: Text(
                'Khung giờ săn sale - Mua gì cũng có quà ',
                style: TextStyle(color: Colors.black),
                textAlign: TextAlign.center,
                overflow: TextOverflow.ellipsis,
              )),
              backgroundColor: Colors.grey.shade300,
              body: buildUi(context));
        });
  }

  @override
  Widget buildDesktop(BuildContext context) {
    return _buildUi(context);
  }

  @override
  Widget buildMobile(BuildContext context) {
    return _buildUi(context);
  }

  @override
  Widget buildTablet(BuildContext context) {
    return _buildUi(context);
  }

  Widget _buildUi(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 2),
      color: Colors.white,
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      height: MediaQuery.of(context).size.width * 9 / 16,
                      decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage(AppImages.imgNotify),
                            fit: BoxFit.fitWidth),
                      ),
                    ),
                    Text(
                      'Khung giờ săn sale - Mua gì cũng có quà ',
                      style: TextStyle(color: Colors.deepOrange, fontSize: 22),
                      softWrap: true,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text('08:00 - 20/08/2021'),
                    LineSpace(),
                    Text(
                      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also",
                      softWrap: true,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                      height: MediaQuery.of(context).size.width * 9 / 16,
                      decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage(AppImages.imgNotify),
                            fit: BoxFit.fitWidth),
                      ),
                    ),
                    Text(
                      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also",
                      softWrap: true,
                    ),
                  ],
                ),
              ),
            ),
          ),
          Container(
            height: 45,
            alignment: Alignment.center,
            color: Colors.deepOrange,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  'Đến bộ sưu tập ngay    ',
                  style: TextStyle(color: Colors.white, fontSize: 16),
                ),
                Image.asset(
                  AppImages.iconRightArrow,
                  color: Colors.white,
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
