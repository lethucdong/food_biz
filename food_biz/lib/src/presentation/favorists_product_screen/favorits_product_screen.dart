import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/configs.dart';

import '../presentation.dart';

class FavoritsProductScreen extends StatefulWidget {
  @override
  _FavoritsProductScreenState createState() => _FavoritsProductScreenState();
}

class _FavoritsProductScreenState extends State<FavoritsProductScreen>
    with ResponsiveWidget {
  late FavoritsProductViewModel _viewModel;
  @override
  Widget build(BuildContext context) {
    return BaseWidget<FavoritsProductViewModel>(
        viewModel: FavoritsProductViewModel(),
        onViewModelReady: (viewModel) {
          _viewModel = viewModel..init();
        },
        builder: (context, viewModel, appbar) {
          return Scaffold(
              appBar: _buildAppBar(),
              backgroundColor: Colors.grey.shade300,
              body: buildUi(context));
        });
  }

  @override
  Widget buildDesktop(BuildContext context) {
    // TODO: implement buildDesktop
    return _buildUi(context);
  }

  @override
  Widget buildMobile(BuildContext context) {
    // TODO: implement buildMobile
    return _buildUi(context);
  }

  @override
  Widget buildTablet(BuildContext context) {
    return _buildUi(context);
  }

  Widget _buildUi(BuildContext) {
    List<Widget> listWidget = [
      ProductCard(
          isHot: true,
          isNew: true,
          discount: 75,
          isLove: false,
          name: 'Tôm nõn hấp Hạ Long',
          price: 150000,
          profit: 50000,
          inventoryNum: null,
          isOrder: false),
      ProductCard(
          isHot: true,
          isNew: false,
          discount: 60,
          isLove: true,
          name: 'Cá cơm rim lạc vừng',
          price: 150000,
          profit: 50000,
          inventoryNum: 5,
          isOrder: false),
      ProductCard(
          isHot: false,
          isNew: true,
          discount: 70,
          isLove: false,
          name: 'Cá cơm rim lạc vừng',
          price: 150000,
          profit: 50000,
          inventoryNum: 5,
          isOrder: false),
      ProductCard(
          isHot: true,
          isNew: true,
          discount: 75,
          isLove: false,
          name: 'Tôm nõn hấp Hạ Long',
          price: 150000,
          profit: 50000,
          inventoryNum: null,
          isOrder: false),
      ProductCard(
          isHot: true,
          isNew: false,
          discount: 60,
          isLove: true,
          name: 'Cá cơm rim lạc vừng',
          price: 150000,
          profit: 50000,
          inventoryNum: 5,
          isOrder: false),
      ProductCard(
          isHot: false,
          isNew: true,
          discount: 70,
          isLove: false,
          name: 'Cá cơm rim lạc vừng',
          price: 150000,
          profit: 50000,
          inventoryNum: 5,
          isOrder: false),
      ProductCard(
          isHot: true,
          isNew: false,
          discount: 60,
          isLove: true,
          name: 'Cá cơm rim lạc vừng',
          price: 150000,
          profit: 50000,
          inventoryNum: 5,
          isOrder: false),
      ProductCard(
          isHot: false,
          isNew: true,
          discount: 70,
          isLove: false,
          name: 'Cá cơm rim lạc vừng',
          price: 150000,
          profit: 50000,
          inventoryNum: 5,
          isOrder: false)
    ];
    return Container(
      padding: EdgeInsets.only(top: 20, right: 20, left: 20),
      child: SingleChildScrollView(
        child: GridviewWidget(
          listWidget: listWidget,
          isShowMore: false,
          maxItemShow: 1000,
          functionShowmore: () {},
          childAspectRatio:
              ((MediaQuery.of(context).size.width - 1) * 0.65 / 392),
          heightChild: 280,
          crossAxisSpacing: 10,
          mainAxisSpacing: 10,
          crossAxisCount: 2,
        ),
      ),
    );
  }

  PreferredSizeWidget _buildAppBar() {
    return AppBar(
      elevation: 1,
      backgroundColor: Colors.white,
      shadowColor: Colors.grey,
      leading: IconButton(
        padding: EdgeInsets.only(top: 30),
        icon: Image.asset(AppImages.iconSlideMenu),
        color: Colors.black,
        onPressed: () {
          // Do something.
        },
      ),
      actions: [
        _buildIconAppbar(
            Image.asset(
              AppImages.iconSearch,
              color: Colors.black,
              scale: 2,
            ),
            0,
            () {}),
        _buildIconAppbar(Image.asset(AppImages.iconCart), 5, () {}),
      ],
      centerTitle: true,
      title: Padding(
        padding: const EdgeInsets.only(top: 20),
        child: Text(
          'Sản phẩm yêu thích',
          style: TextStyle(color: Colors.black, fontSize: 20),
        ),
      ),
      toolbarHeight: 80,
    );
  }

//#region buildIconAppbar
  _buildIconAppbar(Widget icon, int quantity, Function function) {
    return GestureDetector(
      onTap: function(),
      child: Container(
        padding: EdgeInsets.only(top: 30),
        child: Stack(
          children: [
            IconButton(
              icon: icon,
              color: Colors.black,
              onPressed: () {
                // Do something.
              },
            ),
            Positioned(
                top: 5,
                right: 5,
                width: 17,
                height: 17,
                child: quantity > 0
                    ? Container(
                        decoration: BoxDecoration(
                            shape: BoxShape.circle, color: Colors.red),
                        child: Text(
                          '$quantity' + '+',
                          style: TextStyle(fontSize: 12),
                          textAlign: TextAlign.center,
                        ),
                      )
                    : Container())
          ],
        ),
      ),
    );
  }
//#endregion
}
