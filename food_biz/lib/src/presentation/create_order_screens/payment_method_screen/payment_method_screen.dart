import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/constanst/constants.dart';

import '../../presentation.dart';

class PaymentMethodScreen extends StatefulWidget {
  const PaymentMethodScreen({Key? key}) : super(key: key);

  @override
  _PaymentMethodScreenState createState() => _PaymentMethodScreenState();
}

class _PaymentMethodScreenState extends State<PaymentMethodScreen>
    with ResponsiveWidget {
  late PaymentMethodViewModel _viewModel;
  @override
  Widget build(BuildContext context) {
    return BaseWidget<PaymentMethodViewModel>(
        viewModel: PaymentMethodViewModel(),
        onViewModelReady: (viewModel) {
          _viewModel = viewModel..init();
        },
        builder: (context, viewModel, appbar) {
          return Scaffold(
              appBar: buildAppBar(
                  title: Text(
                'Phương thức thanh toán',
                style: TextStyle(color: Colors.black),
                textAlign: TextAlign.center,
              )),
              backgroundColor: Colors.grey.shade300,
              body: buildUi(context));
        });
  }

  @override
  Widget buildDesktop(BuildContext context) {
    return _buildUi(context);
  }

  @override
  Widget buildMobile(BuildContext context) {
    return _buildUi(context);
  }

  @override
  Widget buildTablet(BuildContext context) {
    return _buildUi(context);
  }

  Widget _buildUi(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Column(
                    children: [
                      _buildListBank(),
                      _buildBanner(),
                      _buildChooseMethod(),
                    ],
                  ),
                ],
              ),
            ),
          ),
          GestureDetector(
            onTap: () {},
            child: Container(
              height: 45,
              alignment: Alignment.center,
              width: MediaQuery.of(context).size.width,
              color: Colors.deepOrange,
              child: Text(
                'Đồng ý',
                style: TextStyle(color: Colors.white, fontSize: 16),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildListBank() {
    return Container(
      color: Colors.white,
      margin: EdgeInsets.only(top: 5),
      padding: EdgeInsets.all(20),
      child: Column(
        children: [
          Row(
            children: [
              Image.asset(AppImages.iconBank),
              SizedBox(
                width: 10,
              ),
              Text(
                'Thanh toán khi chuyển khoản',
                style: TextStyle(fontSize: 16),
              ),
            ],
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: Column(
              children: [
                _buildItemBank(
                  image: Image.asset(AppImages.imgVcb),
                  label: 'Ngân hàng Vietcombank',
                  checked: true,
                ),
                _buildItemBank(
                  image: Image.asset(AppImages.imgTcb),
                  label: 'Ngân hàng Techcombank',
                  checked: false,
                ),
                _buildItemBank(
                  image: Image.asset(AppImages.imgVtb),
                  label: 'Ngân hàng Viettinbank',
                  checked: false,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildItemBank(
      {required Image image, required String label, required bool checked}) {
    return GestureDetector(
      onTap: () {
        _viewModel.onTapSelectDetail();
      },
      child: Container(
        height: 50,
        alignment: Alignment.center,
        padding: EdgeInsets.symmetric(vertical: 0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            LineSpace(),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                image,
                SizedBox(
                  width: 10,
                ),
                Expanded(child: Text(label)),
                WidgetCheckButton(onChange: () {}),
                // Container(
                //   alignment: Alignment.centerRight,
                //   width: 30,
                //   child: checked
                //       ? Icon(
                //           Icons.check_rounded,
                //           color: Colors.deepOrange,
                //         )
                //       : null,
                // ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildListLinkAccount() {
    return Column(
      children: [
        Row(
          children: [
            Image.asset(AppImages.iconCredit_orange),
            Text('Thanh toán qua tài khoản liên kết'),
          ],
        ),
        LineSpace(),
        Container(
          child: CarouselCommon(
            listWidget: [
              Container(
                height: 120,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Colors.amber,
                  image: DecorationImage(
                    image: AssetImage(AppImages.imgMomoBanner),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Container(
                height: 120,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Colors.amber,
                  image: DecorationImage(
                    image: AssetImage(AppImages.imgVnpBanner),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ],
            isShowPointSlide: false,
            isAutoScroll: true,
            width: MediaQuery.of(context).size.width,
            height: 130,
            viewportFraction: 1,
            pointPadingHeight: 10,
          ),
        ),
      ],
    );
  }

  Widget _buildBanner() {
    return Container(
      margin: EdgeInsets.only(top: 5),
      padding: EdgeInsets.all(20),
      color: Colors.white,
      child: Column(
        children: [
          Row(
            children: [
              Image.asset(AppImages.iconCredit_orange),
              SizedBox(
                width: 10,
              ),
              Text(
                'Thanh toán qua tài khoản liên kết',
                style: TextStyle(fontSize: 16),
              )
            ],
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Column(
              children: [
                SizedBox(
                  height: 10,
                ),
                LineSpace(),
                SizedBox(
                  height: 10,
                ),
                Container(
                  child: CarouselCommon(
                    listWidget: [
                      GridviewCard(
                          imageUrl: AppImages.imgMomoBanner, function: () {}),
                      GridviewCard(
                          imageUrl: AppImages.imgVnpBanner, function: () {}),
                    ],
                    isShowPointSlide: false,
                    isAutoScroll: true,
                    width: MediaQuery.of(context).size.width,
                    height: 120,
                    viewportFraction: 2 / 3,
                    pointPadingHeight: 10,
                    paddingPageview: 5,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildChooseMethod() {
    return Container(
      // height: 150,
      margin: EdgeInsets.only(top: 5),
      padding: EdgeInsets.all(20),
      color: Colors.white,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Image.asset(AppImages.iconGift),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Thanh toán khi nhận hàng',
                    style: TextStyle(fontSize: 16),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    'Phí thu hộ 0đ. Ưu đãi phí vận chuyển áp dụng với phí thu hộ',
                    overflow: TextOverflow.ellipsis,
                    softWrap: true,
                    maxLines: 3,
                  )
                ],
              ),
            ),
          ),
          // Image.asset(AppImages.iconChecked)
          WidgetCheckButton(onChange: () {}),
        ],
      ),
    );
  }
}
