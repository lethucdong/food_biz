import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/configs.dart';
import 'package:flutter_app/src/utils/app_utils.dart';

import '../../../presentation.dart';

class DetailPaymentScreen extends StatefulWidget {
  const DetailPaymentScreen({Key? key}) : super(key: key);

  @override
  _DetailPaymentScreenState createState() => _DetailPaymentScreenState();
}

class _DetailPaymentScreenState extends State<DetailPaymentScreen>
    with ResponsiveWidget {
  late DetailPaymentViewModel _viewModel;
  @override
  Widget build(BuildContext context) {
    return BaseWidget<DetailPaymentViewModel>(
        viewModel: DetailPaymentViewModel(),
        onViewModelReady: (viewModel) {
          _viewModel = viewModel..init();
        },
        builder: (context, viewModel, appbar) {
          return Scaffold(
              appBar: buildAppBar(
                  title: Text(
                'Thanh toán qua chuyển khoản',
                style: TextStyle(color: Colors.black),
                textAlign: TextAlign.center,
              )),
              backgroundColor: Colors.grey.shade300,
              body: buildUi(context));
        });
  }

  @override
  Widget buildDesktop(BuildContext context) {
    return _buildUi(context);
  }

  @override
  Widget buildMobile(BuildContext context) {
    return _buildUi(context);
  }

  @override
  Widget buildTablet(BuildContext context) {
    return _buildUi(context);
  }

  Widget _buildUi(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      child: SingleChildScrollView(
        child: Column(
          children: [
            _buildTutorial(),
            _buildInfo(),
            _buildDetailBill(),
            _buildNote(),
            _buildAction(),
          ],
        ),
      ),
    );
  }

  Widget _buildTutorial() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10),
      height: 85,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            width: 20,
            padding: EdgeInsets.only(top: 15),
            // margin: EdgeInsets.only(right: 15),
            child: Image.asset(AppImages.iconInfoRound),
          ),
          SizedBox(
            width: 10,
          ),
          Expanded(
            child: Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                      'Bạn sẽ thanh toán bằng cách chuyển khoản vào tài khoản ngân hàng của Foodbiz'),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    'Xem hướng dẫn',
                    style: TextStyle(
                      color: Colors.blue,
                      decoration: TextDecoration.underline,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildInfo() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10),
      color: Colors.white,
      child: Column(
        children: [
          SizedBox(
            height: 15,
          ),
          Row(
            children: [
              Container(
                width: 20,
                child: Image.asset(AppImages.iconPaymentName),
              ),
              SizedBox(
                width: 10,
              ),
              Expanded(child: Text('Tên ngân hàng')),
              Container(
                width: 80,
              ),
            ],
          ),
          SizedBox(
            height: 10,
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                width: 20,
              ),
              SizedBox(
                width: 10,
              ),
              Expanded(child: Text('Ngân hàng Vietcombank')),
              Container(
                width: 80,
                height: 20,
                child: Image.asset(
                  AppImages.iconLogoVcb,
                  scale: 4,
                ),
              ),
            ],
          ),
          SizedBox(
            height: 15,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 30),
            child: LineSpace(),
          ),
          SizedBox(
            height: 15,
          ),
          Row(
            children: [
              Container(
                width: 20,
              ),
              SizedBox(
                width: 10,
              ),
              Expanded(child: Text('Số tài khoản')),
              Container(
                width: 80,
              ),
            ],
          ),
          SizedBox(
            height: 10,
          ),
          Row(
            children: [
              Container(
                width: 20,
              ),
              SizedBox(
                width: 10,
              ),
              Expanded(child: Text('0031 000 123 456')),
              Container(
                width: 90,
                child: Row(
                  children: [
                    Image.asset(
                      AppImages.iconCopy,
                      color: Colors.blueAccent,
                      scale: 2,
                    ),
                    Text(
                      '  Sao chép',
                      style: TextStyle(color: Colors.blueAccent),
                    )
                  ],
                ),
              ),
            ],
          ),
          SizedBox(
            height: 10,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 30),
            child: LineSpace(),
          ),
          SizedBox(
            height: 15,
          ),
          Row(
            children: [
              Container(
                width: 20,
              ),
              SizedBox(
                width: 10,
              ),
              Expanded(child: Text('Chủ tài khoản')),
              Container(
                width: 80,
              ),
            ],
          ),
          SizedBox(
            height: 10,
          ),
          Row(
            children: [
              Container(
                width: 20,
              ),
              SizedBox(
                width: 10,
              ),
              Expanded(child: Text('Công ty cổ phần Foodbiz')),
              Container(
                width: 90,
                child: Row(
                  children: [
                    Image.asset(
                      AppImages.iconCopy,
                      color: Colors.blueAccent,
                      scale: 2,
                    ),
                    Text(
                      '  Sao chép',
                      style: TextStyle(color: Colors.blueAccent),
                    )
                  ],
                ),
              ),
            ],
          ),
          SizedBox(
            height: 15,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 30),
            child: LineSpace(),
          ),
          SizedBox(
            height: 15,
          ),
          Row(
            children: [
              Container(
                width: 20,
                child: Image.asset(AppImages.iconPaymentOther),
              ),
              SizedBox(
                width: 10,
              ),
              Expanded(child: Text('Số tiền chuyển')),
              Container(
                width: 80,
              ),
            ],
          ),
          SizedBox(
            height: 10,
          ),
          Row(
            children: [
              Container(
                width: 20,
              ),
              SizedBox(
                width: 10,
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      AppUtils.numToCurencyString(170000),
                      style: TextStyle(color: Colors.deepOrange),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text('Một trăm bảy mươi nghìn đồng')
                  ],
                ),
              ),
              Container(
                width: 90,
                padding: EdgeInsets.only(bottom: 20),
                child: Row(
                  children: [
                    Image.asset(
                      AppImages.iconCopy,
                      color: Colors.blueAccent,
                      scale: 2,
                    ),
                    Text(
                      '  Sao chép',
                      style: TextStyle(color: Colors.blueAccent),
                    )
                  ],
                ),
              ),
            ],
          ),
          SizedBox(
            height: 15,
          ),
          LineSpace(),
          SizedBox(
            height: 15,
          ),
          Row(
            children: [
              Container(
                width: 20,
                child: Image.asset(AppImages.iconPaymentOther),
              ),
              SizedBox(
                width: 10,
              ),
              Expanded(child: Text('Nội dung chuyển (bắt buộc)')),
              Container(
                width: 80,
              ),
            ],
          ),
          SizedBox(
            height: 10,
          ),
          Row(
            children: [
              Container(
                width: 20,
              ),
              SizedBox(
                width: 10,
              ),
              Expanded(
                child: Text(
                  'TT tien thu ho 21654a4987',
                  style: TextStyle(color: Colors.deepOrange),
                ),
              ),
              Container(
                width: 90,
                child: Row(
                  children: [
                    Image.asset(
                      AppImages.iconCopy,
                      color: Colors.blueAccent,
                      scale: 2,
                    ),
                    Text(
                      '  Sao chép',
                      style: TextStyle(color: Colors.blueAccent),
                    )
                  ],
                ),
              ),
            ],
          ),
          SizedBox(
            height: 15,
          ),
        ],
      ),
    );
  }

  Widget _buildDetailBill() {
    return Container(
      padding: EdgeInsets.all(10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text('Chi tiết đơn hàng'),
          SizedBox(
            height: 10,
          ),
          Container(
            height: 45,
            decoration: BoxDecoration(
              border: Border.all(
                color: Colors.grey.shade300,
              ),
              borderRadius: BorderRadius.circular(10),
              color: Colors.white,
            ),
            child: GestureDetector(
              onTap: () {},
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      '21654a4987',
                      style: TextStyle(color: Colors.blueAccent),
                    ),
                    Row(
                      children: [
                        Text(
                          AppUtils.numToCurencyString(170000),
                          textAlign: TextAlign.left,
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Image.asset(
                          AppImages.iconRightArrow,
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildNote() {
    return Container(
      padding: EdgeInsets.all(10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Text('Lưu ý'),
          SizedBox(
            height: 10,
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 20),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10),
            ),
            child: Container(
              child: Column(
                children: [
                  _buildNoteLine(
                    "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,",
                  ),
                  _buildNoteLine(
                    "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,",
                  ),
                  _buildNoteLine(
                    "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,",
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildNoteLine(String note) {
    return Container(
      height: 40,
      margin: EdgeInsets.symmetric(vertical: 20),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            width: 5,
            height: 5,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: Colors.deepOrange,
            ),
          ),
          SizedBox(
            width: 10,
          ),
          Expanded(
            child: Text(
              note,
              softWrap: true,
              overflow: TextOverflow.clip,
              maxLines: 2,
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildAction() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10),
      child: Column(
        children: [
          GestureDetector(
            onTap: () => {},
            child: Container(
              height: 45,
              width: MediaQuery.of(context).size.width,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Colors.deepOrange),
              child: Text(
                'Đã thanh toán',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 16,
                ),
              ),
            ),
          ),
          SizedBox(height: 10),
          GestureDetector(
            onTap: () => {},
            child: Container(
              height: 45,
              width: MediaQuery.of(context).size.width,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10), color: Colors.white),
              child: Text(
                'Hủy thanh toán',
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 16,
                ),
              ),
            ),
          ),
          SizedBox(height: 10),
          GestureDetector(
            onTap: () => {},
            child: Container(
              height: 45,
              width: MediaQuery.of(context).size.width,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10), color: Colors.grey),
              child: Text(
                'Trợ giúp',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 16,
                ),
              ),
            ),
          ),
          SizedBox(height: 10),
        ],
      ),
    );
  }
}
