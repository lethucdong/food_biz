import '../../presentation.dart';
import 'package:get/get.dart';

class PaymentMethodViewModel extends BaseViewModel {
  init() async {}
  onTapSelectDetail() {
    Get.toNamed(Routers.payment_detail);
  }
}
