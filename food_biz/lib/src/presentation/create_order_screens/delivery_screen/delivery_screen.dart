import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/constanst/constants.dart';
import 'package:flutter_app/src/utils/utils.dart';

import '../../presentation.dart';

class DeliveryScreen extends StatelessWidget with ResponsiveWidget {
  DeliveryScreen({Key? key}) : super(key: key);

  late DeliveryViewModel _viewModel;
  @override
  Widget build(BuildContext context) {
    return BaseWidget<DeliveryViewModel>(
        viewModel: DeliveryViewModel(),
        onViewModelReady: (viewModel) {
          _viewModel = viewModel..init();
        },
        builder: (context, viewModel, appbar) {
          return Scaffold(
              appBar: buildAppBar(
                  title: Text(
                'Phương thức vận chuyển',
                style: TextStyle(color: Colors.black),
                textAlign: TextAlign.center,
              )),
              backgroundColor: Colors.grey.shade300,
              body: buildUi(context));
        });
  }

  @override
  Widget buildDesktop(BuildContext context) {
    return _buildUi(context);
  }

  @override
  Widget buildMobile(BuildContext context) {
    return _buildUi(context);
  }

  @override
  Widget buildTablet(BuildContext context) {
    return _buildUi(context);
  }

  Widget _buildUi(BuildContext context) {
    return Column(
      children: [
        Expanded(
          child: SingleChildScrollView(
            child: Container(
              color: Colors.grey.shade200,
              margin: EdgeInsets.only(top: 5),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(20),
                    child: Wrap(
                      children: [
                        Text('Các phương thức vận chuyển của'),
                        SizedBox(
                          width: 20,
                        ),
                        Image.asset(
                          AppImages.imgLogo,
                          scale: 3,
                        )
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: Text(
                      'Bạn có thể Xem trạng thái đơn hàng trong lịch sử đơn hàng',
                      style: TextStyle(color: Colors.grey),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  _buildItemCard(),
                  Container(
                    margin: EdgeInsets.only(top: 2),
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    color: Colors.white,
                    height: 45,
                    child: Row(
                      children: [
                        Expanded(
                          child: Text(
                            'Thời gian giao hàng mong muốn',
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                        Expanded(
                          child: Text(
                            'Tất các các ngày trong tuần',
                            style: TextStyle(
                              color: Colors.deepOrange,
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 8,
                        ),
                        GestureDetector(
                            onTap: () => {
                                  showDialog(
                                      context: context,
                                      builder: (BuildContext context) {
                                        return SelectTimeScreen();
                                      }),
                                },
                            child: Image.asset(AppImages.iconRightArrow))
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
        GestureDetector(
          child: Container(
            height: 45,
            alignment: Alignment.center,
            width: MediaQuery.of(context).size.width,
            color: Colors.deepOrange,
            child: Text(
              'Đồng ý',
              style: TextStyle(color: Colors.white, fontSize: 16),
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildItemCard() {
    return Container(
      height: 91,
      color: Colors.white,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            width: 7,
            color: Colors.red,
          ),
          Expanded(
            child: Container(
              padding: EdgeInsets.all(13),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Giao hàng nhanh GOFAST',
                    style: TextStyle(fontSize: 16),
                  ),
                  RichText(
                    text: TextSpan(
                      text: AppUtils.numToCurencyString(30000) + '  ',
                      style: TextStyle(color: Colors.deepOrange),
                      children: <TextSpan>[
                        TextSpan(
                          text: AppUtils.numToCurencyString(40000),
                          style: TextStyle(
                              decoration: TextDecoration.lineThrough,
                              color: Colors.black),
                        ),
                      ],
                    ),
                  ),
                  RichText(
                    text: TextSpan(
                      text: 'Thời gian giao hàng dự kiến    ',
                      style: TextStyle(color: Colors.black),
                      children: <TextSpan>[
                        TextSpan(
                          text: '20/08 - 22/08',
                          style: TextStyle(color: Colors.blueAccent),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
