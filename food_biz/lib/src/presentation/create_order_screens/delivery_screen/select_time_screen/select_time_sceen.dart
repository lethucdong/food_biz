import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../../presentation.dart';

class SelectTimeScreen extends StatefulWidget {
  const SelectTimeScreen({Key? key}) : super(key: key);

  @override
  _SelectTimeScreenState createState() => _SelectTimeScreenState();
}

class _SelectTimeScreenState extends State<SelectTimeScreen>
    with ResponsiveWidget {
  late SelectTimeViewModel _viewModel;
  @override
  Widget build(BuildContext context) {
    return BaseWidget<SelectTimeViewModel>(
        viewModel: SelectTimeViewModel(),
        onViewModelReady: (viewModel) {
          _viewModel = viewModel..init();
        },
        builder: (context, viewModel, appbar) {
          return buildUi(context);
        });
  }

  @override
  Widget buildDesktop(BuildContext context) {
    return _buildUi(context);
  }

  @override
  Widget buildMobile(BuildContext context) {
    return _buildUi(context);
  }

  @override
  Widget buildTablet(BuildContext context) {
    return _buildUi(context);
  }

  Widget _buildUi(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Container(
        child: Column(
          children: [
            Expanded(
              child: GestureDetector(onTap: () {
                Navigator.pop(context);
              }
                  // color: Colors.transparent,
                  ),
            ),
            Column(
              children: [
                Container(
                  width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  height: 285,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10),
                      topRight: Radius.circular(10),
                    ),
                    border: Border.all(color: Colors.grey, width: 2),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 30),
                        child: Text(
                          'Thời gian giao hàng mong muốn',
                          style: TextStyle(fontSize: 16),
                        ),
                      ),
                      LineSpace(),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Transform.translate(
                            offset: Offset(-16, 0),
                            child: ListTile(
                              contentPadding: EdgeInsets.only(left: 0),
                              title: const Text('Tất cả các ngày trong tuần',
                                  style: TextStyle(fontSize: 14)),
                              horizontalTitleGap: 0,
                              leading: Radio<SingingCharacter>(
                                fillColor: MaterialStateProperty.all(
                                    Colors.deepOrange),
                                activeColor: Colors.deepOrange,
                                value: SingingCharacter.allday,
                                groupValue: _viewModel.character,
                                onChanged: (SingingCharacter? value) {
                                  Provider.of<SelectTimeViewModel>(context,
                                          listen: false)
                                      .OnChangeSelected(value!);
                                },
                              ),
                            ),
                          ),
                          Padding(
                            padding:
                                const EdgeInsets.only(left: 20, bottom: 20),
                            child: Text(
                                'Phù hợp với địa chỉ nhà riêng luôn có người nhận hàng'),
                          )
                        ],
                      ),
                      LineSpace(),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Transform.translate(
                            offset: Offset(-16, 0),
                            child: ListTile(
                              contentPadding: EdgeInsets.all(0),
                              title: const Text('Chỉ giao giờ hành chính',
                                  style: TextStyle(fontSize: 14)),
                              horizontalTitleGap: 0.0,
                              leading: Radio<SingingCharacter>(
                                activeColor: Colors.deepOrange,
                                fillColor: MaterialStateProperty.all(
                                    Colors.deepOrange),
                                value: SingingCharacter.none,
                                groupValue: _viewModel.character,
                                onChanged: (SingingCharacter? value) {
                                  Provider.of<SelectTimeViewModel>(context,
                                          listen: false)
                                      .OnChangeSelected(value!);
                                },
                              ),
                              minLeadingWidth: null,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 20),
                            child: Text(
                                'Phù hợp với địa chỉ công ty hoặc văn phòng'),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
                GestureDetector(
                  child: Container(
                    height: 45,
                    alignment: Alignment.center,
                    width: MediaQuery.of(context).size.width,
                    color: Colors.deepOrange,
                    child: Text(
                      'Đồng ý',
                      style: TextStyle(color: Colors.white, fontSize: 16),
                    ),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
