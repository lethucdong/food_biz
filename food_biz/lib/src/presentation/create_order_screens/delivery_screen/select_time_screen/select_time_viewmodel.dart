import '../../../presentation.dart';

enum SingingCharacter { allday, none }

class SelectTimeViewModel extends BaseViewModel {
  SingingCharacter? character = SingingCharacter.allday;
  init() async {}
  OnChangeSelected(SingingCharacter value) {
    character = value;
    notifyListeners();
  }
}
