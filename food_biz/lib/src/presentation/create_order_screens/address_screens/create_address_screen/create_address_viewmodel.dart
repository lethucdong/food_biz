import 'package:flutter_app/src/resource/model/model.dart';
import 'package:flutter_app/src/resource/repo/repo.dart';

import '../../../presentation.dart';
import 'package:get/get.dart';

class CreateAddressViewModel extends BaseViewModel {
  List<ProvinceModel> listProvince = List<ProvinceModel>.empty(growable: true);

  init() async {
    initListProvince();
  }

  initListProvince() async {
    try {
      await ProvinceRepository().getProvince().then((value) {
        if (value.isSuccess) {
          if (value.message == 'OK') {
            listProvince = value.data!;
          } else {
            print(value.message);
          }
        }
      });
      notifyListeners();
    } catch (e) {
      print("Exception: $e");
    }
  }

  onTapSave() {
    Get.back();
  }
}
