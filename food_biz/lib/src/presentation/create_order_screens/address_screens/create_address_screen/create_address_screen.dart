import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/configs.dart';
import 'package:flutter_app/src/resource/model/model.dart';

import '../../../presentation.dart';

class CreateAddressScreen extends StatefulWidget {
  final bool isEdit;
  const CreateAddressScreen({Key? key, this.isEdit = false}) : super(key: key);

  @override
  _CreateAddressScreenState createState() => _CreateAddressScreenState();
}

class _CreateAddressScreenState extends State<CreateAddressScreen>
    with ResponsiveWidget {
  late CreateAddressViewModel _viewModel;
  @override
  Widget build(BuildContext context) {
    return BaseWidget<CreateAddressViewModel>(
        viewModel: CreateAddressViewModel(),
        onViewModelReady: (viewModel) {
          _viewModel = viewModel..init();
        },
        builder: (context, viewModel, appbar) {
          return Scaffold(
              appBar: buildAppBar(
                  title: Text(
                widget.isEdit ? 'Sửa địa chỉ' : 'Thêm địa chỉ',
                style: TextStyle(color: Colors.black),
                textAlign: TextAlign.center,
              )),
              backgroundColor: Colors.grey.shade300,
              body: buildUi(context));
        });
  }

  @override
  Widget buildDesktop(BuildContext context) {
    return _buildUi(context);
  }

  @override
  Widget buildMobile(BuildContext context) {
    return _buildUi(context);
  }

  @override
  Widget buildTablet(BuildContext context) {
    return _buildUi(context);
  }

  Widget _buildUi(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 5),
      color: Colors.white,
      child: Column(
        children: [
          Expanded(
            child: SingleChildScrollView(
              child: Container(
                padding: EdgeInsets.all(20),
                child: Column(
                  children: [
                    buildTextInput(null, 'Họ và tên khách hàng',
                        'Vui lòng nhập', TextInputType.text),
                    SizedBox(height: 10),
                    buildTextInput(null, 'Số điện thoại', 'Vui lòng nhập',
                        TextInputType.text),
                    SizedBox(height: 10),
                    buildTextInput(null, 'Số nhà, tên đường', 'Vui lòng nhập',
                        TextInputType.text),
                    SizedBox(height: 10),
                    buildDroplistProvince('Tỉnh/ thành phố'),
                    SizedBox(height: 10),
                    buildDroplistProvince('Quận/ Huyện'),
                    SizedBox(height: 10),
                    buildDroplistProvince('Phường xã'),
                    SizedBox(height: 10),
                    Row(
                      children: [
                        // Image.asset(AppImages.iconChecked),
                        WidgetCheckButton(onChange: () {}),
                        SizedBox(
                          width: 10,
                        ),
                        Text(
                          'Đặt làm địa chỉ mặc định',
                          // style: TextStyle(color: Colors.deepOrange),
                        )
                      ],
                    )
                  ],
                ),
              ),
            ),
          ),
          !widget.isEdit
              ? GestureDetector(
                  onTap: () {
                    _viewModel.onTapSave();
                  },
                  child: Container(
                    height: 45,
                    alignment: Alignment.center,
                    width: MediaQuery.of(context).size.width,
                    color: Colors.deepOrange,
                    child: Text(
                      'Lưu lại',
                      style: TextStyle(color: Colors.white, fontSize: 16),
                    ),
                  ),
                )
              : Container(
                  decoration: BoxDecoration(boxShadow: [
                    BoxShadow(
                      color: Colors.grey,
                      offset: Offset(0.0, 0), //(x,y)
                      blurRadius: 1.0,
                    ),
                    BoxShadow(
                      color: Colors.white,
                      offset: Offset(0.0, 2), //(x,y)
                      blurRadius: 1.0,
                    ),
                  ]),
                  child: Row(
                    children: [
                      Expanded(
                        child: Container(
                          height: 45,
                          alignment: Alignment.center,
                          width: MediaQuery.of(context).size.width,
                          color: Colors.white,
                          child: Text(
                            'Xóa địa chỉ',
                            style: TextStyle(color: Colors.black, fontSize: 16),
                          ),
                        ),
                      ),
                      Expanded(
                        child: GestureDetector(
                          onTap: () {
                            _viewModel.onTapSave();
                          },
                          child: Container(
                            height: 45,
                            alignment: Alignment.center,
                            width: MediaQuery.of(context).size.width,
                            color: Colors.deepOrange,
                            child: Text(
                              'Lưu lại',
                              style:
                                  TextStyle(color: Colors.white, fontSize: 16),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                )
        ],
      ),
    );
  }

  Widget buildTextInput(TextEditingController? textEditingController,
      String label, String? hintText, TextInputType? textInputType) {
    return Container(
      padding: EdgeInsets.all(10),
      height: 75,
      width: MediaQuery.of(context).size.width,
      alignment: Alignment.topLeft,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          border: Border.all(color: Colors.grey)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(label),
          TextField(
              decoration: InputDecoration(
                  isDense: true,
                  hintText: hintText,
                  disabledBorder: InputBorder.none,
                  border: InputBorder.none),
              controller: textEditingController,
              keyboardType:
                  textInputType == null ? TextInputType.text : textInputType)
        ],
      ),
    );
  }

  Widget buildDroplistProvince(String title) {
    return Container(
        padding: EdgeInsets.all(10),
        height: 90,
        width: MediaQuery.of(context).size.width,
        alignment: Alignment.topLeft,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          border: Border.all(color: Colors.grey),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(title),
            Container(
              width: MediaQuery.of(context).size.width,
              child: DropdownButton<int>(
                underline: SizedBox(),
                hint: Text('Vui lòng chọn'),
                isExpanded: true,
                icon: Image.asset(AppImages.iconDropDown),
                items: _viewModel
                    .listProvince //_viewModel.registerController.listProvince.value
                    .map((ProvinceModel value) {
                  return DropdownMenuItem<int>(
                    value: value.code,
                    child: Text('${value.name}'),
                  );
                }).toList(),
                onChanged: (_) {},
              ),
            )
          ],
        ));
  }
}
