import '../../../presentation.dart';
import 'package:get/get.dart';

class AddressViewModel extends BaseViewModel {
  init() async {}
  onTapCreate() {
    Get.toNamed(Routers.create_address, arguments: [
      {"isEdit", false}
    ]);
  }

  onTapEdit() {
    Get.toNamed(Routers.edit_address);
  }
}
