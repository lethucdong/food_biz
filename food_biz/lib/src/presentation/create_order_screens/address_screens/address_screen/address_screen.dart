import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_app/src/configs/configs.dart';

import '../../../presentation.dart';

class AddressScreen extends StatefulWidget {
  @override
  _AddressScreenState createState() => _AddressScreenState();
}

class _AddressScreenState extends State<AddressScreen> with ResponsiveWidget {
  late AddressViewModel _viewModel;
  @override
  Widget build(BuildContext context) {
    return BaseWidget<AddressViewModel>(
        viewModel: AddressViewModel(),
        onViewModelReady: (viewModel) {
          _viewModel = viewModel..init();
        },
        builder: (context, viewModel, appbar) {
          return Scaffold(
              appBar: buildAppBar(
                  title: Text(
                'Địa chỉ',
                style: TextStyle(color: Colors.black),
                textAlign: TextAlign.center,
              )),
              backgroundColor: Colors.white,
              body: buildUi(context));
        });
  }

  @override
  Widget buildDesktop(BuildContext context) {
    return _buildUi(context);
  }

  @override
  Widget buildMobile(BuildContext context) {
    return _buildUi(context);
  }

  @override
  Widget buildTablet(BuildContext context) {
    return _buildUi(context);
  }

  Widget _buildUi(BuildContext) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      child: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Column(
          children: [
            Container(color: Colors.grey.shade300, height: 5),
            Container(
              child: Column(
                children: [
                  Container(
                    // height: 70,
                    color: Colors.white,
                    child: _buildSearch(),
                  ),
                  Container(color: Colors.grey.shade300, height: 5),
                  Column(
                    children: [
                      _buildList(),
                      Container(color: Colors.grey.shade300, height: 5),
                      GestureDetector(
                        onTap: () {
                          _viewModel.onTapCreate();
                        },
                        child: Container(
                          height: 45,
                          padding: EdgeInsets.symmetric(horizontal: 20),
                          color: Colors.white,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text('Thêm địa chỉ'),
                              Image.asset(AppImages.iconPlus),
                            ],
                          ),
                        ),
                      ),
                      Container(color: Colors.grey.shade300, height: 5),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  _buildSearch() {
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 20),
        alignment: Alignment.center,
        // color: Colors.red,
        decoration: BoxDecoration(
          color: Colors.grey.shade300,
          border: Border.all(color: Colors.grey.shade300, width: 1),
          borderRadius: BorderRadius.circular(12),
        ),
        height: 45,
        child: TextField(
          style: TextStyle(fontSize: 14),
          decoration: InputDecoration(
            isDense: true,
            hintText: 'Tìm kiếm',
            contentPadding: EdgeInsets.only(top: 15),
            suffixIcon: Image.asset(
              AppImages.iconSearch,
              scale: 2.2,
              color: Colors.black,
            ),
            border: InputBorder.none,
          ),
        ),
      ),
    );
  }

  Widget _buildList() {
    return Container(
      color: Colors.white,
      margin: EdgeInsets.symmetric(vertical: 5),
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        children: [
          for (int i = 0; i < 5; i++) _buildItemCard(selected: i == 0),
        ],
      ),
    );
  }

  Widget _buildItemCard({bool selected = false}) {
    return Column(
      children: [
        SizedBox(
          height: 10,
        ),
        Container(
          height: 80,
          child: Row(
            children: [
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    RichText(
                      text: TextSpan(
                        text: 'Trương Huyền    ',
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 14,
                        ),
                        children: <TextSpan>[
                          TextSpan(
                            text: selected ? '[mặc định]' : '',
                            style: TextStyle(
                              color: Colors.deepOrange,
                              fontSize: 13,
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 5),
                    Text('0364 111 999'),
                    SizedBox(height: 5),
                    Text('120 Nguyễn Trãi'),
                    SizedBox(height: 5),
                    Text('Thanh Xuân, Hà Nội')
                  ],
                ),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  GestureDetector(
                      onTap: () {
                        _viewModel.onTapEdit();
                      },
                      child: Text(
                        'Chỉnh sửa',
                        style: TextStyle(
                          color: Colors.blueAccent,
                          decoration: TextDecoration.underline,
                        ),
                      )),
                  Icon(selected ? Icons.check : null, color: Colors.red),
                  Image.asset(AppImages.iconLocation),
                ],
              ),
            ],
          ),
        ),
        SizedBox(
          height: 5,
        ),
        LineSpace(),
      ],
    );
  }
}
