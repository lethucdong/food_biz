import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_app/src/configs/constanst/constants.dart';
import 'package:flutter_app/src/presentation/presentation.dart';
import 'package:flutter_app/src/utils/utils.dart';

import 'create_order_viewmodel.dart';
import 'widget/group_product_info.dart';

class CreateOrderScreen extends StatefulWidget {
  @override
  _CreateOrderScreenState createState() => _CreateOrderScreenState();
}

class _CreateOrderScreenState extends State<CreateOrderScreen>
    with ResponsiveWidget {
  late Size _screenSize;
  late CreateOrderViewModel _viewModel;
  @override
  Widget build(BuildContext context) {
    _screenSize = MediaQuery.of(context).size;
    return BaseWidget<CreateOrderViewModel>(
        viewModel: CreateOrderViewModel(),
        onViewModelReady: (viewModel) {
          _viewModel = viewModel..init();
        },
        builder: (context, viewModel, appbar) {
          return Scaffold(
              appBar: buildAppBar(
                  title: Text(
                'Tạo đơn hàng',
                style: TextStyle(color: Colors.black),
                textAlign: TextAlign.center,
              )),
              backgroundColor: Colors.grey.shade300,
              body: buildUi(context));
        });
  }

  @override
  Widget buildDesktop(BuildContext context) {
    return buildBody(context);
  }

  @override
  Widget buildMobile(BuildContext context) {
    return buildBody(context);
  }

  @override
  Widget buildTablet(BuildContext context) {
    return buildBody(context);
  }

  Widget buildBody(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        child: Column(
          children: [
            _buildAddress(),
            SizedBox(
              height: 5,
            ),

            // _buildListCartItem(),
            GroupProductInfo(),
            SizedBox(
              height: 5,
            ),
            // _buildDeliveryNote(),
            // SizedBox(
            //   height: 5,
            // ),
            _builTypePayment(),
            SizedBox(
              height: 5,
            ),
            _buildInfoForCus(),
            SizedBox(
              height: 5,
            ),
            _buildVoucher(),
            SizedBox(
              height: 5,
            ),
            _buildYourInfor(),
            SizedBox(
              height: 5,
            ),
            _buildFooter(),
          ],
        ),
      ),
    );
  }

  Widget _lineSpace({required double height}) {
    return Container(
      height: height,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20), color: Colors.grey.shade300),
    );
  }

  Widget _buildLineInfo(
      {required Widget leftContent, required Widget rightContent}) {
    return Container(
      height: 50,
      width: _screenSize.width,
      alignment: Alignment.centerLeft,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [leftContent, Expanded(child: rightContent)],
      ),
    );
  }

  Widget _buildDeliveryNote() {
    return Container(
      padding: EdgeInsets.only(top: 20, right: 20, left: 20),
      color: Colors.white,
      child: Column(
        children: [
          GestureDetector(
            onTap: () {
              _viewModel.OnTapDelivery();
            },
            child: Row(
              children: [
                Image.asset(
                  AppImages.iconLocation,
                ),
                SizedBox(
                  width: 10,
                ),
                Text('Vận chuyển', style: TextStyle(fontSize: 16)),
              ],
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: RichText(
              text: TextSpan(
                text: 'Vui lòng nhập ',
                style: TextStyle(color: Colors.black),
                children: <TextSpan>[
                  TextSpan(
                    text: 'địa chỉ giao hàng ',
                    style: TextStyle(color: Colors.blueAccent),
                  ),
                  TextSpan(
                    text: 'để tỉnh phí vận chuyển',
                    style: TextStyle(color: Colors.black),
                  ),
                ],
              ),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          _lineSpace(height: 2),
          _buildLineInfo(
              leftContent: Text('Ghi chú vận chuyển'),
              rightContent: GestureDetector(
                child: Text(
                  'Thêm ghi chú đơn hàng',
                  textAlign: TextAlign.right,
                ),
              )),
        ],
      ),
    );
  }

  Widget _builTypePayment() {
    return Container(
      color: Colors.white,
      padding: EdgeInsets.all(20),
      child: Column(
        children: [
          Row(
            children: [
              Image.asset(
                AppImages.iconCreditCard,
                scale: 2,
              ),
              SizedBox(
                width: 10,
              ),
              Text('Phương thức thanh toán'),
            ],
          ),
          SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('Thanh toán khi nhận hàng'),
              GestureDetector(
                onTap: () {
                  _viewModel.onTapPayment();
                },
                child: Image.asset(AppImages.iconRightArrow),
              )
            ],
          ),
        ],
      ),
    );
  }

  Widget _buildInfoForCus() {
    return Container(
      color: Colors.white,
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        children: [
          _buildLineInfo(
              leftContent: Text(
                'Thông tin cho khách hàng của bạn',
                style: TextStyle(fontSize: 16),
              ),
              rightContent: SizedBox()),
          _lineSpace(height: 2),
          _buildLineInfo(
            leftContent: Text('Tổng tiền sản phẩm'),
            rightContent: Text(
              AppUtils.numToCurencyString(150000),
              textAlign: TextAlign.right,
              style: TextStyle(color: Colors.blueAccent),
            ),
          ),
          _lineSpace(height: 2),
          _buildLineInfo(
            leftContent: Text('Tổng phí vận chuyển'),
            rightContent: Text(
              AppUtils.numToCurencyString(30000),
              textAlign: TextAlign.right,
            ),
          ),
          _lineSpace(height: 2),
          _buildLineInfo(
            leftContent: Text('Giảm giá vận chuyển'),
            rightContent: Text(
              '-${AppUtils.numToCurencyString(20000)}',
              textAlign: TextAlign.right,
            ),
          ),
          _lineSpace(height: 2),
          _buildLineInfo(
            leftContent: Text('Tổng tiền thanh toán'),
            rightContent: Text(
              AppUtils.numToCurencyString(140000),
              textAlign: TextAlign.right,
              style: TextStyle(
                  color: Colors.deepOrange, fontWeight: FontWeight.bold),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildVoucher() {
    return Container(
      color: Colors.white,
      padding: EdgeInsets.only(top: 20, left: 20, right: 20),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  Image.asset(
                    AppImages.iconNavCoupon,
                    scale: 2,
                  ),
                  Text(' Voucher')
                ],
              ),
              Visibility(
                visible: true,
                child: Expanded(
                  child: Container(
                    alignment: Alignment.topRight,
                    child: Wrap(
                      direction: Axis.horizontal,
                      children: [
                        Stack(
                          children: [
                            Container(
                              child: Image.asset(
                                AppImages.imgUnionFrame1,
                                scale: 2,
                              ),
                            ),
                            Positioned(
                              child: Padding(
                                padding: const EdgeInsets.only(top: 2),
                                child: Text(
                                  '+ 15 xu',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontSize: 12, color: Colors.blueAccent),
                                ),
                              ),
                              left: 0,
                              right: 0,
                            )
                          ],
                        ),
                        Stack(
                          children: [
                            Container(
                              child: Image.asset(
                                AppImages.imgUnionFrame3,
                                scale: 2,
                              ),
                            ),
                            Positioned(
                              child: Padding(
                                padding: const EdgeInsets.only(top: 2),
                                child: Text(
                                  '-20k',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(fontSize: 12),
                                ),
                              ),
                              left: 0,
                              right: 0,
                            )
                          ],
                        ),
                        // Container(
                        //   margin: EdgeInsets.only(top: 10),
                        //   child: Stack(
                        //     children: [
                        //       Container(
                        //         child: Image.asset(
                        //           AppImages.imgUnionFrame2,
                        //           scale: 2,
                        //         ),
                        //       ),
                        //       Positioned(
                        //         child: Padding(
                        //           padding: const EdgeInsets.only(top: 2),
                        //           child: Text(
                        //             'Miễn phí vận chuyển',
                        //             textAlign: TextAlign.center,
                        //             style: TextStyle(fontSize: 12),
                        //           ),
                        //         ),
                        //         left: 0,
                        //         right: 0,
                        //       )
                        //     ],
                        //   ),
                        // ),
                      ],
                    ),
                  ),
                ),
              ),
              GestureDetector(
                onTap: () {
                  _viewModel.onTapVoucher();
                },
                child: Row(
                  children: [
                    Visibility(
                      visible: false,
                      child: Text(
                        'Chọn voucher ',
                      ),
                    ),
                    Padding(
                        padding: EdgeInsets.only(left: 10),
                        child: Image.asset(
                          AppImages.iconRightArrow,
                        ))
                  ],
                ),
              ),
            ],
          ),
          SizedBox(
            height: 10,
          ),
          _lineSpace(height: 2),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  Image.asset(AppImages.iconCoin),
                  Text(' Dùng 15 Fobi xu')
                ],
              ),
              Container(
                child: Row(
                  children: [
                    Text('(-15.000đ)'),
                    SwitchButton(),
                  ],
                ),
              ),
            ],
          ),
          // _lineSpace(height: 2),
        ],
      ),
    );
  }

  Widget _buildYourInfor() {
    return Container(
      color: Colors.white,
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        children: [
          _buildLineInfo(
              leftContent: Text(
                'Thông tin cho bạn',
                style: TextStyle(fontSize: 16),
              ),
              rightContent: SizedBox()),
          _lineSpace(height: 2),
          _buildLineInfo(
            leftContent: Text('Tổng Tiền Hàng'),
            rightContent: Text(
              AppUtils.numToCurencyString(170000),
              style: TextStyle(color: Colors.blueAccent),
              textAlign: TextAlign.right,
            ),
          ),
          _lineSpace(height: 2),
          _buildLineInfo(
            leftContent: Text('Tổng giá bán của bạn'),
            rightContent: Text(
              AppUtils.numToCurencyString(150000),
              textAlign: TextAlign.right,
            ),
          ),
          _lineSpace(height: 2),
          _buildLineInfo(
            leftContent: Row(
              children: [
                Text('Tổng thưởng hạng:'),
                Image.asset(AppImages.iconRank2, scale: 2),
              ],
            ),
            rightContent: Text(
              AppUtils.numToCurencyString(20000),
              textAlign: TextAlign.right,
            ),
          ),
          _lineSpace(height: 2),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 17),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    Text('Thưởng voucher'),
                    Image.asset(AppImages.iconInfo),
                  ],
                ),
                GestureDetector(
                  onTap: () {
                    _viewModel.onTapPromotionReward();
                  },
                  child: Row(
                    children: [
                      Text(
                        '${AppUtils.numToCurencyString(20000)}  ',
                      ),
                      SizedBox(width: 5),
                      Image.asset(AppImages.iconRightArrow),
                    ],
                  ),
                ),
              ],
            ),
          ),
          _lineSpace(height: 2),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 9),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('Đã dùng Fobi Xu:'),
                    Text(
                      '(quy đổi 1 xu = 1.000đ)',
                      style: TextStyle(color: Colors.grey),
                    )
                  ],
                ),
                Text(
                  '15 Xu',
                  textAlign: TextAlign.right,
                ),
              ],
            ),
          ),
          _lineSpace(height: 2),
          _buildLineInfo(
            leftContent: Text('Phí thu hộ COD'),
            rightContent: Text(
              '-${AppUtils.numToCurencyString(150000)}',
              textAlign: TextAlign.right,
            ),
          ),
          _lineSpace(height: 2),
          _buildLineInfo(
              leftContent: Text('Tổng lợi nhuận của bạn'),
              rightContent: Text(
                AppUtils.numToCurencyString(290000),
                textAlign: TextAlign.right,
                style: TextStyle(
                    color: Colors.deepOrange, fontWeight: FontWeight.bold),
              )),
          _lineSpace(height: 2),
          _buildLineInfo(
            leftContent: Text('15 Fobi Xu nhận được'),
            rightContent: Text(
              AppUtils.numToCurencyString(150000),
              textAlign: TextAlign.right,
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildFooter() {
    return Container(
      // height: 45,
      color: Colors.white,
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.all(20),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                WidgetCheckButton(onChange: () {}),
                SizedBox(
                  width: 10,
                ),
                Flexible(
                  child: RichText(
                    textAlign: TextAlign.left,
                    softWrap: true,
                    text: TextSpan(children: <TextSpan>[
                      TextSpan(
                          text:
                              "Nhấn đặt hàng đồng nghĩa bạn đã đồng ý tuân theo ",
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold)),
                      TextSpan(
                          text: "điều khoản Foodbiz",
                          style: TextStyle(color: Colors.black)),
                    ]),
                  ),
                ),
              ],
            ),
          ),
          Row(
            children: [
              Expanded(
                child: GestureDetector(
                  onTap: () {
                    _viewModel.onTapCreate();
                  },
                  child: Container(
                    height: 45,
                    // width: ,
                    color: Colors.grey,
                    alignment: Alignment.center,
                    child: Text(
                      'Tạo đơn',
                      style: TextStyle(color: Colors.white, fontSize: 16),
                    ),
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }

  Widget _buildAddress() {
    return Container(
      color: Colors.white,
      padding: EdgeInsets.all(20),
      child: Column(
        children: [
          Row(
            children: [
              Image.asset(
                AppImages.iconLocation,
              ),
              SizedBox(
                width: 10,
              ),
              Text(
                'Địa chỉ giao hàng',
                style: TextStyle(fontSize: 16),
              ),
            ],
          ),
          SizedBox(
            height: 10,
          ),
          Container(
            padding: EdgeInsets.all(20),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Colors.grey.shade200),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('Trương Huyền'),
                    GestureDetector(
                      onTap: () {
                        _viewModel.onTapAddress();
                      },
                      child: Text(
                        'Thay đổi',
                        style: TextStyle(color: Colors.blueAccent),
                      ),
                    )
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  '0364 111 999',
                  style: TextStyle(color: Colors.grey),
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  '120 Nguyễn Trãi',
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  'Thanh Xuân, Hà Nội',
                ),
              ],
            ),
          ),
          // GestureDetector(
          //   onTap: () {
          //     _viewModel.onTapAddress();
          //   },
          //   child: Container(
          //     height: 45,
          //     decoration: BoxDecoration(
          //       border: Border.all(
          //         color: Colors.grey.shade300,
          //       ),
          //       borderRadius: BorderRadius.circular(10),
          //       color: Colors.grey.shade200,
          //     ),
          //     child: Container(
          //       padding: EdgeInsets.symmetric(horizontal: 20),
          //       child: Row(
          //         mainAxisAlignment: MainAxisAlignment.spaceBetween,
          //         children: [
          //           Text('Nhập địa chỉ giao hàng'),
          //           Image.asset(
          //             AppImages.iconRightArrow,
          //           ),
          //         ],
          //       ),
          //     ),
          //   ),
          // ),
        ],
      ),
    );
  }
}
