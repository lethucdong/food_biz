import '../../presentation.dart';
import 'package:get/get.dart';

class CreateOrderViewModel extends BaseViewModel {
  init() async {}
  onTapVoucher() {
    Get.toNamed(Routers.voucher);
  }

  onTapAddress() {
    Get.toNamed(Routers.address);
  }

  OnTapDelivery() {
    Get.toNamed(Routers.delivery);
  }

  onTapPayment() {
    Get.toNamed(Routers.payment_method);
  }

  onTapCreate() {
    Get.toNamed(Routers.payment_detail);
  }

  onTapPromotionReward() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return PromotionRewardScreen();
        });
  }
}
