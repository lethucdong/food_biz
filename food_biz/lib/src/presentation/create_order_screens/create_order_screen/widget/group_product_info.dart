import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/configs.dart';
import 'package:flutter_app/src/presentation/widgets/widget_line.dart';
import 'package:flutter_app/src/utils/utils.dart';
import 'package:get/get.dart';

import '../../../presentation.dart';
import '../../../routers.dart';

class GroupProductInfo extends StatefulWidget {
  const GroupProductInfo({Key? key}) : super(key: key);

  @override
  _GroupProductInfoState createState() => _GroupProductInfoState();
}

class _GroupProductInfoState extends State<GroupProductInfo> {
  late Size _screenSize;
  @override
  Widget build(BuildContext context) {
    _screenSize = MediaQuery.of(context).size;
    return Container(
      child: _buildListCartItem(),
    );
  }

  Widget _buildListCartItem() {
    return Column(
      children: [
        Container(
          padding: EdgeInsets.symmetric(horizontal: 20),
          color: Colors.white,
          child: Column(
            children: [
              Container(
                padding: EdgeInsets.only(top: 10),
                width: _screenSize.width,
                child: Text(
                  'Elina Med',
                  style: TextStyle(
                    fontSize: 16,
                  ),
                  textAlign: TextAlign.left,
                ),
              ),
              for (int i = 0; i < 3; i++) _buildCartItem(i),
            ],
          ),
        ),
        LineSpace(),
        _buildDevivery()
      ],
    );
  }

  Widget _buildCartItem(int index) {
    return Container(
      padding: EdgeInsets.only(top: 10),
      child: Column(
        children: [
          Row(
            children: [
              Expanded(
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          width: 60,
                          height: 60,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: Colors.grey,
                            ),
                            image: DecorationImage(
                                image: AssetImage(
                                  AppImages.imgproductHaLong1,
                                ),
                                fit: BoxFit.cover),
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Text(
                                'Tôm nõn Hạ Long',
                                style: TextStyle(fontSize: 16),
                              ),
                              SizedBox(height: 5),
                              Container(
                                // width: 200,
                                child: RichText(
                                  softWrap: true,
                                  text: TextSpan(
                                    text: 'Giá bán nhà cung cấp:     ',
                                    style: TextStyle(color: Colors.black),
                                    children: <TextSpan>[
                                      TextSpan(
                                          text: AppUtils.numToCurencyString(
                                              5500000)),
                                    ],
                                  ),
                                ),
                              ),
                              SizedBox(height: 5),
                              RichText(
                                text: TextSpan(
                                  text: 'Giá bán Của bạn:     ',
                                  style: TextStyle(color: Colors.black),
                                  children: <TextSpan>[
                                    TextSpan(
                                      text:
                                          AppUtils.numToCurencyString(6000000),
                                      style:
                                          TextStyle(color: Colors.deepOrange),
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(height: 5),
                              // RichText(
                              //   text: TextSpan(
                              //     text: 'Nhà cung cấp:     ',
                              //     style: TextStyle(color: Colors.black),
                              //     children: <TextSpan>[
                              //       TextSpan(
                              //         text: 'Elina Med',
                              //       ),
                              //     ],
                              //   ),
                              // ),
                              SizedBox(height: 5),
                              Text('Số lượng: 1')
                            ],
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              )
            ],
          ),
          SizedBox(height: 5),
          index != 2 ? LineSpace() : Container(),
        ],
      ),
    );
  }

  Widget _buildDevivery() {
    return Container(
      padding: EdgeInsets.only(top: 20, right: 20, left: 20),
      color: Colors.white,
      child: Column(
        children: [
          GestureDetector(
            onTap: () {
              // _viewModel.OnTapDelivery();
              Get.toNamed(Routers.delivery);
            },
            child: Row(
              children: [
                Image.asset(
                  AppImages.iconLocation,
                ),
                SizedBox(
                  width: 10,
                ),
                Text('Vận chuyển', style: TextStyle(fontSize: 16)),
              ],
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Column(
            children: [
              Container(
                width: _screenSize.width,
                // height: 70,
                color: Colors.white,
                child: Container(
                  padding: EdgeInsets.symmetric(
                    horizontal: 20,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Expanded(
                            child: Text(
                              'Giao hàng  GOFAST',
                              style: TextStyle(fontSize: 16),
                              softWrap: true,
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          RichText(
                            textAlign: TextAlign.right,
                            text: TextSpan(
                              text: AppUtils.numToCurencyString(30000) + '  ',
                              style: TextStyle(
                                color: Colors.deepOrange,
                              ),
                              children: <TextSpan>[
                                TextSpan(
                                  text: AppUtils.numToCurencyString(40000),
                                  style: TextStyle(
                                      decoration: TextDecoration.lineThrough,
                                      color: Colors.black),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Expanded(child: Text('Thời gian giao hàng dự kiến')),
                          SizedBox(
                            width: 10,
                          ),
                          Text('20/08 - 22/08',
                              style: TextStyle(color: Colors.blueAccent),
                              textAlign: TextAlign.right),
                        ],
                      )
                      // RichText(
                      //   text: TextSpan(
                      //     text: 'Thời gian giao hàng dự kiến    ',
                      //     style: TextStyle(color: Colors.black),
                      //     children: <TextSpan>[
                      //       TextSpan(
                      //         text: '20/08 - 22/08',
                      //         style: TextStyle(color: Colors.blueAccent),
                      //       ),
                      //     ],
                      //   ),
                      // ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              LineSpace(),
              SizedBox(
                height: 10,
              ),
              Row(
                children: [
                  Expanded(
                    child: Text(
                      'Thời gian giao hàng mong muốn',
                      overflow: TextOverflow.ellipsis,
                      softWrap: true,
                    ),
                  ),
                  Expanded(
                    child: Text(
                      'Tất các các ngày trong tuần',
                      style: TextStyle(
                        color: Colors.deepOrange,
                        overflow: TextOverflow.ellipsis,
                      ),
                      softWrap: true,
                    ),
                  ),
                  SizedBox(
                    width: 8,
                  ),
                  GestureDetector(
                      onTap: () => {
                            showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return SelectTimeScreen();
                                }),
                          },
                      child: Image.asset(AppImages.iconRightArrow))
                ],
              ),
            ],
          ),
          SizedBox(
            height: 10,
          ),
          LineSpace(),
          _buildLineInfo(
              leftContent: Text('Ghi chú vận chuyển'),
              rightContent: GestureDetector(
                child: Padding(
                  padding: const EdgeInsets.only(left: 10),
                  child: Text(
                    'Lorem Ipsum is simpl Lorem Ipsum is simply... Lorem Ipsum is simply...',
                    textAlign: TextAlign.right,
                    overflow: TextOverflow.ellipsis,
                    softWrap: false,
                  ),
                ),
              )),
        ],
      ),
    );
  }

  Widget _buildLineInfo(
      {required Widget leftContent, required Widget rightContent}) {
    return Container(
      height: 50,
      // width: _screenSize.width,
      alignment: Alignment.centerLeft,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [leftContent, Expanded(child: rightContent)],
      ),
    );
  }
}
