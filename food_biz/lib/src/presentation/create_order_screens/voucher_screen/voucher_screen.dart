import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_app/src/configs/constanst/constants.dart';
import 'package:flutter_app/src/presentation/presentation.dart';
import 'package:flutter_app/src/utils/utils.dart';

class VoucherScreen extends StatefulWidget {
  @override
  _VoucherScreenState createState() => _VoucherScreenState();
}

class _VoucherScreenState extends State<VoucherScreen> with ResponsiveWidget {
  late VoucherViewModel _viewModel;
  @override
  Widget build(BuildContext context) {
    return BaseWidget<VoucherViewModel>(
        viewModel: VoucherViewModel(),
        onViewModelReady: (viewModel) {
          _viewModel = viewModel..init();
        },
        builder: (context, viewModel, appbar) {
          return Scaffold(
              appBar: buildAppBar(
                  title: Text(
                'Voucher',
                style: TextStyle(color: Colors.black),
                textAlign: TextAlign.center,
              )),
              backgroundColor: Colors.grey.shade300,
              body: buildUi(context));
        });
  }

  @override
  Widget buildDesktop(BuildContext context) {
    return _buildUi(context);
  }

  @override
  Widget buildMobile(BuildContext context) {
    return _buildUi(context);
  }

  @override
  Widget buildTablet(BuildContext context) {
    return _buildUi(context);
  }

  Widget _buildUi(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 5),
      color: Colors.white,
      // padding: EdgeInsets.all(20),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            child: SingleChildScrollView(
              child: Container(
                width: MediaQuery.of(context).size.width,
                padding: EdgeInsets.all(20),
                child: Column(
                  children: [
                    _buildInput(),
                    _buidVoucherContent(),
                  ],
                ),
              ),
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                    height: 45,
                    alignment: Alignment.centerLeft,
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                      '1 voucher đã được chọn',
                      textAlign: TextAlign.start,
                    ),
                    decoration: BoxDecoration(boxShadow: [
                      BoxShadow(
                        color: Colors.grey,
                        offset: Offset(0.0, 1), //(x,y)
                        blurRadius: 1.0,
                      ),
                      BoxShadow(
                        color: Colors.white,
                        offset: Offset(0.0, 1), //(x,y)
                        blurRadius: 3.0,
                      ),
                    ])),
                GestureDetector(
                  child: Container(
                    height: 45,
                    alignment: Alignment.center,
                    width: MediaQuery.of(context).size.width,
                    color: Colors.deepOrange,
                    child: Text(
                      'Đồng ý',
                      style: TextStyle(color: Colors.white, fontSize: 16),
                    ),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _buildInput() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Expanded(
          child: Container(
            width: 260,
            height: 44,
            child: TextField(
                decoration: InputDecoration(
                  isDense: true,
                  hintText: 'Nhập mã Fobi voucher',
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                  contentPadding: EdgeInsets.all(10),
                ),
                keyboardType: TextInputType.number),
          ),
        ),
        SizedBox(
          width: 10,
        ),
        Container(
            width: 79,
            height: 44,
            decoration: BoxDecoration(
                color: Colors.grey.shade200,
                borderRadius: BorderRadius.circular(10),
                border: Border.all(color: Colors.grey)),
            alignment: Alignment.center,
            child: Text('Áp dụng'))
      ],
    );
  }

  Widget _buidVoucherContent() {
    return Container(
      child: Column(
        children: [
          Row(
            children: [
              Expanded(
                child: Container(
                  height: 1,
                  color: Colors.grey,
                ),
              ),
              Text(
                'Voucher giảm giá',
                style: TextStyle(color: Colors.grey),
              ),
              Expanded(
                child: Container(
                  height: 1,
                  color: Colors.grey,
                ),
              ),
            ],
          ),
          _buildCardItem(),
          _buildCardItem(),
          GestureDetector(
            child: Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'Xem thêm   ',
                    style: TextStyle(fontSize: 16, color: Colors.blueAccent),
                  ),
                  Image.asset(AppImages.iconDropDown)
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _buildCardItem() {
    return GestureDetector(
      onTap: () {
        _viewModel.onTapDetail();
      },
      child: Container(
        padding: EdgeInsets.only(top: 10),
        child: Column(
          children: [
            Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      width: 60,
                      height: 60,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        border: Border.all(
                          color: Colors.grey,
                        ),
                        image: DecorationImage(
                            image: AssetImage(
                              AppImages.imgVoucherActive,
                            ),
                            fit: BoxFit.cover),
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Expanded(
                        child: Column(
                      // crossAxisAlignment: CrossAxisAlignment.start,
                      // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          // crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Expanded(
                              child: Text(
                                  'Giảm giá tới 50% cho đơn hàng 1 triệu đ. Tối đa 500.000 đ'),
                            ),
                            // Image.asset(
                            //   AppImages.iconChecked,
                            //   alignment: Alignment.bottomRight,
                            // )
                            WidgetCheckButton(onChange: () {}),
                          ],
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: Text('HSD: 20/8/2021'),
                            ),
                            GestureDetector(
                              child: Text(
                                'Điều kiện',
                                textAlign: TextAlign.end,
                              ),
                            )
                          ],
                        )
                      ],
                    ))
                  ],
                ),
              ],
            ),
            SizedBox(height: 5),
            _lineSpace(height: 2),
          ],
        ),
      ),
    );
  }

  Widget _lineSpace({required double height}) {
    return Container(
      height: height,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20), color: Colors.grey.shade300),
    );
  }
}
