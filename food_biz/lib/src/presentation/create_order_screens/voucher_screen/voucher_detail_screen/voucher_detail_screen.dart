export 'package:flutter/material.dart';

import 'package:flutter_app/src/configs/configs.dart';

import '../../../presentation.dart';

class VoucherDetailScreen extends StatelessWidget with ResponsiveWidget {
  VoucherDetailScreen({Key? key}) : super(key: key);
  late VoucherDetailViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<VoucherDetailViewModel>(
        viewModel: VoucherDetailViewModel(),
        onViewModelReady: (viewModel) {
          _viewModel = viewModel..init();
        },
        builder: (context, viewModel, appbar) {
          return Scaffold(
              appBar: buildAppBar(
                  title: Text(
                'Chi tiết voucher',
                style: TextStyle(color: Colors.black),
                textAlign: TextAlign.center,
              )),
              backgroundColor: Colors.grey.shade300,
              body: buildUi(context));
        });
  }

  @override
  Widget buildDesktop(BuildContext context) {
    return _buildUI(context);
  }

  @override
  Widget buildMobile(BuildContext context) {
    return _buildUI(context);
  }

  @override
  Widget buildTablet(BuildContext context) {
    return _buildUI(context);
  }

  Widget _buildUI(BuildContext context) {
    return Container(
      color: Colors.white,
      margin: EdgeInsets.only(top: 10),
      child: Column(
        children: [
          Expanded(
            child: Container(
              padding: EdgeInsets.all(20),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: 150,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      image: DecorationImage(
                          image: AssetImage(AppImages.imgVoucherUnBanner),
                          fit: BoxFit.cover),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    'Giảm giá tới 50% cho đơn hàng 1 triệu đ. Tối đa 500.000 đ',
                    overflow: TextOverflow.clip,
                    style: TextStyle(fontSize: 14),
                    softWrap: false,
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Text('HSD: 20/08/2021'),
                  SizedBox(
                    height: 10,
                  ),
                  LineSpace(),
                  SizedBox(
                    height: 10,
                  ),
                  Expanded(
                    child: Text(
                        "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."),
                  ),
                ],
              ),
            ),
          ),
          GestureDetector(
            child: Container(
              height: 45,
              alignment: Alignment.center,
              width: MediaQuery.of(context).size.width,
              color: Colors.deepOrange,
              child: Text(
                'Áp dụng',
                style: TextStyle(color: Colors.white, fontSize: 16),
              ),
            ),
          )
        ],
      ),
    );
  }
}
