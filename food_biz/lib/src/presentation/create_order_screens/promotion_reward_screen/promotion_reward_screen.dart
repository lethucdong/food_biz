import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/constanst/constants.dart';
import 'package:flutter_app/src/utils/app_utils.dart';
import '../../presentation.dart';

class PromotionRewardScreen extends StatefulWidget {
  const PromotionRewardScreen({Key? key}) : super(key: key);

  @override
  _PromotionRewardScreenState createState() => _PromotionRewardScreenState();
}

class _PromotionRewardScreenState extends State<PromotionRewardScreen>
    with ResponsiveWidget {
  late PromotionRewardViewModel _viewModel;
  @override
  Widget build(BuildContext context) {
    return BaseWidget<PromotionRewardViewModel>(
        viewModel: PromotionRewardViewModel(),
        onViewModelReady: (viewModel) {
          _viewModel = viewModel..init();
        },
        builder: (context, viewModel, appbar) {
          return buildUi(context);
        });
  }

  @override
  Widget buildDesktop(BuildContext context) {
    return _buildUi(context);
  }

  @override
  Widget buildMobile(BuildContext context) {
    return _buildUi(context);
  }

  @override
  Widget buildTablet(BuildContext context) {
    return _buildUi(context);
  }

  Widget _buildUi(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Container(
        child: Column(
          children: [
            Expanded(
              child: GestureDetector(onTap: () {
                Navigator.pop(context);
              }
                  // color: Colors.transparent,
                  ),
            ),
            Column(
              children: [
                Container(
                  width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  height: 285,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10),
                      topRight: Radius.circular(10),
                    ),
                    border: Border.all(color: Colors.grey, width: 2),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 30),
                        child: Text(
                          'Thưởng khuyến mại',
                          style: TextStyle(fontSize: 16),
                        ),
                      ),
                      LineSpace(),
                      Container(
                        padding: EdgeInsets.symmetric(vertical: 10),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Image.asset(
                              AppImages.iconTicket,
                              scale: 4,
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    // width: MediaQuery.of(context).size.width - 70,
                                    child: Text(
                                      'Thưởng thêm 5% hoa hồng từ kho ABCXYZ',
                                      softWrap: true,
                                    ),
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Text(
                                    AppUtils.numToCurencyString(20000),
                                    style: TextStyle(
                                      color: Colors.deepOrange,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Image.asset(AppImages.iconRightArrow),
                          ],
                        ),
                      ),
                      LineSpace(),
                      Container(
                        padding: EdgeInsets.symmetric(vertical: 10),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Image.asset(
                              AppImages.iconTicket,
                              scale: 4,
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text('Thưởng thêm 5% hoa hồng từ kho ABCXYZ'),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Text(
                                    AppUtils.numToCurencyString(20000),
                                    style: TextStyle(
                                      color: Colors.deepOrange,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Image.asset(AppImages.iconRightArrow),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    _viewModel.onTapAccept();
                  },
                  child: Container(
                    height: 45,
                    alignment: Alignment.center,
                    width: MediaQuery.of(context).size.width,
                    color: Colors.deepOrange,
                    child: Text(
                      'Đồng ý',
                      style: TextStyle(color: Colors.white, fontSize: 16),
                    ),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
