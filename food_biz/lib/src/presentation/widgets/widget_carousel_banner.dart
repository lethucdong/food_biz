import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/constanst/constants.dart';

List<String> listImage = [
  AppImages.imgBanner,
  AppImages.imgBanner,
  AppImages.imgBanner,
  AppImages.imgBanner,
  AppImages.imgBanner,
];

class CarouselBanner extends StatefulWidget {
  const CarouselBanner({Key? key}) : super(key: key);

  @override
  _CarouselBannerState createState() => _CarouselBannerState();
}

class _CarouselBannerState extends State<CarouselBanner> {
  late PageController controller;
  int currentpage = 0;

  @override
  initState() {
    super.initState();
    controller = PageController(
      initialPage: currentpage,
      keepPage: true,
      viewportFraction: 1,
    );
    Timer.periodic(Duration(seconds: 2), (Timer timer) {
      if (currentpage < listImage.length) {
        currentpage++;
      } else {
        currentpage = 0;
      }
      if (controller.hasClients && currentpage < listImage.length)
        controller.animateToPage(currentpage,
            duration: const Duration(milliseconds: 500), curve: Curves.ease);
    });
  }

  @override
  dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          width: MediaQuery.of(context).size.width,
          height: 155,
          child: PageView.builder(
              padEnds: false,
              onPageChanged: (value) {
                setState(() {
                  currentpage = value;
                });
              },
              itemCount: listImage.length,
              controller: controller,
              itemBuilder: (context, index) => builder(index)),
        ),
        Positioned(
          // left: MediaQuery.of(context).size.width / 2,
          // bottom: 10,
          child: Padding(
            padding: EdgeInsets.only(top: 135),
            child: _buildPointerIndex(),
          ),
        ),
      ],
    );
  }

  builder(int index) {
    return AnimatedBuilder(
      animation: controller,
      builder: (context, child) {
        double value = 1.0;
        if (controller.position.haveDimensions) {
          value = controller.page! - index;
          value = (1 - (value.abs() * .5)).clamp(0.0, 1.0);
        }

        return Center(
          child: SizedBox(
            // height: Curves.easeOut.transform(value) * 300,
            width: Curves.easeOut.transform(value) * 350,
            child: child,
          ),
        );
      },
      child: Container(
          decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        image: DecorationImage(
          image: AssetImage(listImage[index]),
          fit: BoxFit.cover,
        ),
      )),
    );
  }

  Widget _buildPointerIndex() {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          for (int i = 0; i < listImage.length; i++)
            GestureDetector(
              onTap: () {
                setState(() {
                  currentpage = i;
                  // controller.jumpToPage(currentpage);
                  controller.animateToPage(currentpage,
                      duration: const Duration(milliseconds: 500),
                      curve: Curves.ease);
                });
              },
              child: Container(
                margin: EdgeInsets.only(right: 10),
                width: 10,
                height: 10,
                decoration: BoxDecoration(
                  color: i == currentpage ? Colors.red : Colors.grey.shade300,
                  shape: BoxShape.circle,
                ),
              ),
            ),
        ],
      ),
    );
  }
}
