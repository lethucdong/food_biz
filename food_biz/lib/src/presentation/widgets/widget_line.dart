import 'package:flutter/material.dart';

class LineSpace extends StatelessWidget {
  const LineSpace({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 2,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20), color: Colors.grey.shade300),
    );
  }
}
