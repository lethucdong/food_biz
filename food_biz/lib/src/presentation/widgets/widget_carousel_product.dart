import 'dart:async';

import 'package:flutter/material.dart';

class CarouselProduct extends StatefulWidget {
  final List<Widget> listWidget;
  bool isShowPointSlide;
  bool isAutoScroll;
  double width;
  double height;
  double viewportFraction;
  CarouselProduct(
      {Key? key,
      required this.listWidget,
      required this.isShowPointSlide,
      required this.isAutoScroll,
      required this.width,
      required this.height,
      required this.viewportFraction})
      : super(key: key);

  @override
  _CarouselProductState createState() => _CarouselProductState();
}

class _CarouselProductState extends State<CarouselProduct> {
  late PageController controller;
  int currentpage = 0;

  @override
  initState() {
    super.initState();
    controller = PageController(
      initialPage: currentpage,
      keepPage: false,
      viewportFraction: widget.viewportFraction,
    );
    if (widget.isAutoScroll)
      Timer.periodic(Duration(seconds: 2), (Timer timer) {
        if (currentpage < widget.listWidget.length - 1) {
          currentpage++;
        } else {
          currentpage = 0;
        }
        if (controller.hasClients && currentpage < widget.listWidget.length - 1)
          controller.animateToPage(currentpage,
              duration: const Duration(milliseconds: 500), curve: Curves.ease);
      });
  }

  @override
  dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          width: widget.width,
          height: widget.height,
          child: PageView.builder(
              padEnds: false,
              onPageChanged: (value) {
                setState(() {
                  currentpage = value;
                });
              },
              itemCount: widget.listWidget.length,
              controller: controller,
              itemBuilder: (context, index) => builder(index)),
        ),
        SizedBox(
          height: 20,
        ),
        (widget.isShowPointSlide) ? _buildPointerIndex() : Container()
        // Positioned(
        //   // left: MediaQuery.of(context).size.width / 2,
        //   // bottom: 10,
        //   bottom: 20,
        //   // right: 0,
        //   left: 0,
        //   right: 0,
        //   child: _buildPointerIndex(),
        // ),
      ],
    );
  }

  builder(int index) {
    return AnimatedBuilder(
      animation: controller,
      builder: (context, child) {
        return Container(
          child: child,
        );
      },
      child: widget.listWidget[index],
    );
  }

  Widget _buildPointerIndex() {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          for (int i = 0; i < widget.listWidget.length; i++)
            GestureDetector(
              onTap: () {
                setState(() {
                  currentpage = i;
                  controller.animateToPage(currentpage,
                      duration: const Duration(milliseconds: 500),
                      curve: Curves.ease);
                });
              },
              child: Container(
                margin: EdgeInsets.only(right: 10),
                width: 10,
                height: 10,
                decoration: BoxDecoration(
                  color: i == currentpage ? Colors.red : Colors.grey.shade300,
                  shape: BoxShape.circle,
                ),
              ),
            ),
        ],
      ),
    );
  }
}
