import 'dart:async';

import 'package:flutter/material.dart';

class CarouselProductDetail extends StatefulWidget {
  final List<Widget> listWidget;
  bool isShowPointSlide;
  bool isAutoScroll;
  final double width;
  double height;
  double viewportFraction;
  double pointPadingHeight;
  CarouselProductDetail(
      {Key? key,
      required this.listWidget,
      required this.isShowPointSlide,
      required this.isAutoScroll,
      required this.width,
      required this.height,
      required this.viewportFraction,
      required this.pointPadingHeight})
      : super(key: key);

  @override
  _CarouselProductDetailState createState() => _CarouselProductDetailState();
}

class _CarouselProductDetailState extends State<CarouselProductDetail> {
  late PageController controller;
  int currentpage = 0;
  ScrollController _scrollController = ScrollController();

  @override
  initState() {
    super.initState();
    controller = PageController(
      initialPage: currentpage,
      keepPage: false,
      viewportFraction: widget.viewportFraction,
    );
    if (widget.isAutoScroll)
      Timer.periodic(Duration(seconds: 2), (Timer timer) {
        if (currentpage < widget.listWidget.length - 1) {
          currentpage++;
        } else {
          currentpage = 0;
        }
        if (controller.hasClients && currentpage < widget.listWidget.length - 1)
          // controller.animateToPage(currentpage,
          //     duration: const Duration(milliseconds: 500), curve: Curves.ease);
          controller.jumpToPage(currentpage);
      });
  }

  @override
  dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          width: widget.width,
          height: widget.height,
          child: PageView.builder(
              padEnds: false,
              onPageChanged: (value) {
                setState(() {
                  currentpage = value;
                });
              },
              itemCount: widget.listWidget.length,
              controller: controller,
              itemBuilder: (context, index) => builder(index)),
        ),
        SizedBox(
          height: widget.pointPadingHeight + 10,
        ),
        (widget.isShowPointSlide) ? _buildPointerIndex() : Container(),
        SizedBox(
          height: widget.pointPadingHeight + 10,
        ),
      ],
    );
  }

  builder(int index) {
    return AnimatedBuilder(
      animation: controller,
      builder: (context, child) {
        // return Container(
        //   width: widget.width,
        //   child: child,
        // );
        return Stack(
          children: [
            widget.listWidget[index],
            Positioned(
              child: Container(
                width: 39,
                height: 20,
                decoration: BoxDecoration(
                    color: Colors.white.withOpacity(0.7),
                    borderRadius: BorderRadius.circular(10)),
                alignment: Alignment.center,
                child: Text('${currentpage + 1}/${widget.listWidget.length}'),
              ),
              bottom: 20,
              right: 20,
            ),
          ],
        );
      },
      // child: widget.listWidget[index],
    );
  }

  Widget _buildPointerIndex() {
    // _scrollController.animateTo(currentpage.toDouble(),
    //     duration: Duration(milliseconds: 500), curve: Curves.ease);
    // _scrollController.jumpTo(currentpage.toDouble() * 56);
    return SingleChildScrollView(
      controller: _scrollController,
      scrollDirection: Axis.horizontal,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          for (int i = 0; i < widget.listWidget.length; i++)
            GestureDetector(
              onTap: () {
                setState(() {
                  currentpage = i;
                  // controller.animateToPage(currentpage,
                  //     duration: const Duration(milliseconds: 500),
                  //     curve: Curves.ease);
                  controller.jumpToPage(currentpage);
                });
              },
              // child: Container(
              //   margin: EdgeInsets.only(right: 10),
              //   width: 10,
              //   height: 10,
              //   decoration: BoxDecoration(
              //     color: i == currentpage ? Colors.red : Colors.grey.shade300,
              //     shape: BoxShape.circle,
              //   ),
              // ),
              child: Stack(children: [
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    border: i == currentpage
                        ? Border.all(
                            color: Colors.orange
                                .shade800, //                   <--- border color
                            width: 2.0,
                          )
                        : Border(),
                  ),
                  margin: EdgeInsets.symmetric(horizontal: 5),
                  width: 56,
                  height: 56,
                  child:
                      Transform.scale(scale: 1.1, child: widget.listWidget[i]),
                  // child: Column(
                  //   children: [
                  //     Expanded(
                  //       child: Transform.scale(
                  //           scale: 1.1, child: widget.listWidget[i]),
                  //     ),
                  //   ],
                  // ),
                ),
                Positioned(
                    child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    border: i == currentpage
                        ? Border.all(
                            color: Colors.orange
                                .shade800, //                   <--- border color
                            width: 2.0,
                          )
                        : Border(),
                  ),
                  margin: EdgeInsets.symmetric(horizontal: 5),
                  width: 56,
                  height: 56,
                ))
              ]),
            ),
        ],
      ),
    );
  }
}
