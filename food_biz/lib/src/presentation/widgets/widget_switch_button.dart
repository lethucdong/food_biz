import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:flutter_app/src/configs/constanst/constants.dart';

class SwitchButton extends StatefulWidget {
  late bool isActive;
  SwitchButton({
    Key? key,
    this.isActive = false,
  }) : super(key: key);

  @override
  _SwitchButtonState createState() => _SwitchButtonState();
}

class _SwitchButtonState extends State<SwitchButton> {
  void toggleSwitch(bool value) {
    if (widget.isActive == false) {
      setState(() {
        widget.isActive = true;
      });
    } else {
      setState(() {
        widget.isActive = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Transform.scale(
      scale: 0.7,
      child: CupertinoSwitch(
        onChanged: toggleSwitch,
        value: widget.isActive,
        activeColor: Colors.deepOrange,
        thumbColor: Colors.white,
        trackColor: Colors.grey,
      ),
    );
  }
}
