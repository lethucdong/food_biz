import 'dart:async';

import 'package:flutter/material.dart';

class CarouselCommon extends StatefulWidget {
  final List<Widget> listWidget;
  bool isShowPointSlide;
  bool isAutoScroll;
  final double width;
  double height;
  double viewportFraction;
  double pointPadingHeight;
  double? paddingPageview;
  bool? isGroupPoint = false;
  CarouselCommon(
      {Key? key,
      required this.listWidget,
      required this.isShowPointSlide,
      required this.isAutoScroll,
      required this.width,
      required this.height,
      required this.viewportFraction,
      required this.pointPadingHeight,
      this.paddingPageview = 0,
      this.isGroupPoint})
      : super(key: key);

  @override
  _CarouselCommonState createState() => _CarouselCommonState();
}

class _CarouselCommonState extends State<CarouselCommon> {
  late PageController controller;
  int currentpage = 0;

  @override
  initState() {
    super.initState();
    controller = PageController(
      initialPage: currentpage,
      keepPage: false,
      viewportFraction: widget.viewportFraction,
    );
    if (widget.isAutoScroll)
      Timer.periodic(Duration(seconds: 2), (Timer timer) {
        if (currentpage <
            (widget.listWidget.length + 1 - 1 / widget.viewportFraction)) {
          currentpage++;
        } else {
          currentpage = 0;
        }
        if (controller.hasClients &&
            currentpage <
                widget.listWidget.length + 1 - 1 / widget.viewportFraction)
          controller.animateToPage(currentpage,
              duration: const Duration(milliseconds: 500), curve: Curves.ease);
      });
  }

  @override
  dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      // width: widget.width + 20,
      height: widget.pointPadingHeight > widget.height
          ? widget.pointPadingHeight + 20
          : widget.height,
      child: Stack(
        children: [
          Container(
            width: widget.width,
            height: widget.height,
            child: PageView.builder(
                padEnds: false,
                onPageChanged: (value) {
                  setState(() {
                    currentpage = value;
                  });
                },
                itemCount: widget.listWidget.length,
                controller: controller,
                itemBuilder: (context, index) => builder(index)),
          ),
          Positioned(
            left: 0,
            right: 0,
            top: widget.pointPadingHeight,
            child:
                (widget.isShowPointSlide) ? _buildPointerIndex() : Container(),
          ),
        ],
      ),
    );
  }

  builder(int index) {
    return AnimatedBuilder(
      animation: controller,
      builder: (context, child) {
        return Padding(
          child: widget.listWidget[index],
          padding: EdgeInsets.symmetric(horizontal: widget.paddingPageview!),
        );
      },
      // child: widget.listWidget[index],
    );
  }

  Widget _buildPointerIndex() {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          for (int i = 0;
              i < widget.listWidget.length - (1 / widget.viewportFraction) + 1;
              i++)
            GestureDetector(
              onTap: () {
                setState(() {
                  currentpage = i;
                  controller.animateToPage(currentpage,
                      duration: const Duration(milliseconds: 500),
                      curve: Curves.ease);
                });
              },
              child: Container(
                margin: EdgeInsets.only(right: 10),
                width: i == currentpage && widget.isGroupPoint == true
                    ? (1 / widget.viewportFraction) * 10
                    : 10,
                height: 10,
                decoration: i != currentpage
                    ? BoxDecoration(
                        color: Colors.grey.shade300, shape: BoxShape.circle)
                    : BoxDecoration(
                        color: Colors.deepOrange,
                        borderRadius: BorderRadius.circular(10)),
              ),
            ),
        ],
      ),
    );
  }
}
