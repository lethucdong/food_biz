import 'package:flutter/material.dart';

class GridviewCard extends StatelessWidget {
  final String imageUrl;
  final Function function;
  const GridviewCard({Key? key, required this.imageUrl, required this.function})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: function(),
      child: Container(
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(10),
            image:
                DecorationImage(image: AssetImage(imageUrl), fit: BoxFit.fill)),
      ),
    );
  }
}
