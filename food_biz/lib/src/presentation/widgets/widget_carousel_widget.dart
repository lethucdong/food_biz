import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/configs.dart';

List<String> listImage1 = [
  AppImages.imgBanner,
  AppImages.imgBanner,
  AppImages.imgBanner,
  AppImages.imgBanner,
  AppImages.imgBanner,
];

class CarouselWidget extends StatefulWidget {
  const CarouselWidget({Key? key}) : super(key: key);

  @override
  _CarouselWidgetState createState() => _CarouselWidgetState();
}

class _CarouselWidgetState extends State<CarouselWidget> {
  late PageController controller;
  int currentpage = 1;

  @override
  initState() {
    super.initState();
    controller = PageController(
      initialPage: currentpage,
      keepPage: false,
      viewportFraction: 1 / 3,
    );
    Timer.periodic(Duration(seconds: 2), (Timer timer) {
      if (currentpage < listImage1.length - 1) {
        currentpage++;
      } else {
        currentpage = 1;
      }
      if (controller.hasClients && currentpage < listImage1.length - 1)
        controller.animateToPage(currentpage,
            duration: const Duration(milliseconds: 500), curve: Curves.ease);
    });
  }

  @override
  dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          padding: EdgeInsets.symmetric(horizontal: 10),
          width: MediaQuery.of(context).size.width,
          height: 60,
          child: PageView.builder(
              padEnds: false,
              onPageChanged: (value) {
                setState(() {
                  currentpage = value;
                });
              },
              itemCount: listImage1.length,
              controller: controller,
              itemBuilder: (context, index) => builder(index)),
        ),
        Positioned(
          // left: MediaQuery.of(context).size.width / 2,
          // bottom: 10,
          child: Padding(
            padding: EdgeInsets.only(top: 70),
            child: _buildPointerIndex(),
          ),
        ),
      ],
    );
  }

  builder(int index) {
    return AnimatedBuilder(
      animation: controller,
      builder: (context, child) {
        double value = 1.0;
        if (controller.position.haveDimensions) {
          value = controller.page! - index;
          value = (1 - (value.abs() * .5)).clamp(0.0, 1.0);
        }

        return Center(
          child: SizedBox(
            // height: Curves.easeOut.transform(value) * 200,
            // width: Curves.easeOut.transform(value) * 200,
            width: 110,
            child: child,
          ),
        );
      },
      child: Container(
          width: 160,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: Colors.red,
            image: DecorationImage(
              image: AssetImage(listImage1[index]),
              fit: BoxFit.cover,
            ),
          )),
    );
  }

  Widget _buildPointerIndex() {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          for (int i = 0; i < listImage1.length; i++)
            GestureDetector(
              onTap: () {
                setState(() {
                  currentpage = i;
                  controller.animateToPage(currentpage,
                      duration: const Duration(milliseconds: 500),
                      curve: Curves.ease);
                });
              },
              child: Container(
                margin: EdgeInsets.only(right: 10),
                width: i == currentpage ? 30 : 10,
                height: 10,
                decoration: i != currentpage
                    ? BoxDecoration(
                        color: Colors.grey.shade300, shape: BoxShape.circle)
                    : BoxDecoration(
                        color: Colors.deepOrange,
                        borderRadius: BorderRadius.circular(10)),
              ),
            ),
        ],
      ),
    );
  }
}
