import 'package:flutter/material.dart';

class WidgetCheckButton extends StatefulWidget {
  late bool? isChecked;
  late Function onChange;
  WidgetCheckButton({
    Key? key,
    this.isChecked = false,
    required this.onChange,
  }) : super(key: key);

  @override
  _WidgetCheckButtonState createState() => _WidgetCheckButtonState();
}

class _WidgetCheckButtonState extends State<WidgetCheckButton> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        widget.onChange();
        setState(() {
          widget.isChecked = !widget.isChecked!;
        });
      },
      child: Container(
        // margin: EdgeInsets.only(bottom: 41),
        width: 22,
        height: 22,
        alignment: Alignment.center,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
          border: Border.all(
              color: (widget.isChecked!) == true
                  ? Colors.deepOrange
                  : Colors.grey),
          color: (widget.isChecked!) == true ? Colors.deepOrange : Colors.white,
        ),
        child: (widget.isChecked!) == true
            ? Icon(
                Icons.check,
                color: Colors.white,
                size: 20,
              )
            : SizedBox(),
        // color: widget.isChecked! ? Colors.deepOrange : Colors.white
      ),
    );
  }
}
