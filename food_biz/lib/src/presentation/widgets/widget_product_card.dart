import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/configs.dart';
import 'package:flutter_app/src/presentation/presentation.dart';
import 'package:flutter_app/src/utils/utils.dart';
import 'package:get/get.dart';

class ProductCard extends StatefulWidget {
  bool isHot;
  bool isNew;
  int? discount;
  bool isLove;
  String name;
  double price;
  double profit;
  int? inventoryNum;
  bool isOrder;
  ProductCard(
      {Key? key,
      required this.isHot,
      required this.isNew,
      required this.discount,
      required this.isLove,
      required this.name,
      required this.price,
      required this.profit,
      required this.inventoryNum,
      required this.isOrder})
      : super(key: key);

  @override
  _ProductCardState createState() => _ProductCardState();
}

class _ProductCardState extends State<ProductCard> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Get.toNamed(Routers.product_detail);
      },
      child: Container(
        width: 162,
        height: 275,
        // margin: EdgeInsets.only(right: 10),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: Colors.white,
        ),
        alignment: Alignment.bottomLeft,
        child: Column(
          children: [
            Stack(
              children: [
                Container(
                  height: 140,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(10),
                        topRight: Radius.circular(10)),
                    image: DecorationImage(
                      image: AssetImage(AppImages.imgProduct_tom),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                Positioned(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 5),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            widget.isHot
                                ? Container(
                                    // margin: EdgeInsets.only(right: 2),
                                    padding: EdgeInsets.only(top: 5),
                                    width: 30,
                                    height: 30,
                                    alignment: Alignment.center,
                                    decoration: BoxDecoration(
                                      image: DecorationImage(
                                          image: AssetImage(
                                            AppImages.iconHotSale,
                                          ),
                                          fit: BoxFit.none,
                                          scale: 2),
                                    ),
                                    child: Text(
                                      'Hot',
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 9),
                                    ),
                                  )
                                : Container(),
                            widget.isNew
                                ? Container(
                                    width: 30,
                                    height: 30,
                                    alignment: Alignment.center,
                                    decoration: BoxDecoration(
                                        image: DecorationImage(
                                            image: AssetImage(
                                              AppImages.iconNew,
                                            ),
                                            fit: BoxFit.none,
                                            scale: 1.8)),
                                  )
                                : Container()
                          ],
                        ),
                        (widget.discount != null && widget.discount != 0)
                            ? Container(
                                padding: EdgeInsets.symmetric(horizontal: 5),
                                width: 52,
                                height: 23,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(5),
                                    color: Colors.red.shade300),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Image.asset(
                                      AppImages.iconFlash,
                                      scale: 2,
                                    ),
                                    Text(
                                      '-${widget.discount}%',
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 13),
                                    )
                                  ],
                                ),
                              )
                            : Container()
                      ],
                    ),
                  ),
                ),
                Positioned(
                  child: GestureDetector(
                    onTap: () {
                      setState(() {
                        widget.isLove = !widget.isLove;
                      });
                    },
                    child: Container(
                      width: 40,
                      height: 40,
                      decoration: BoxDecoration(
                        color: Colors.grey.shade300.withOpacity(0.5),
                        borderRadius:
                            BorderRadius.circular(widget.isOrder ? 10 : 30),
                      ),
                      child: Image.asset(
                        widget.isLove == true
                            ? AppImages.iconRedHeart
                            : AppImages.iconEmptyHeart,
                        scale: 1.4,
                      ),
                    ),
                  ),
                  bottom: 0,
                  right: 0,
                )
              ],
            ),
            Container(
              alignment: Alignment.topLeft,
              padding: EdgeInsets.all(10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    widget.name,
                    style: TextStyle(fontSize: 17),
                    softWrap: false,
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Row(
                    children: [
                      Text('Giá từ: '),
                      Flexible(
                        child: Text(
                          AppUtils.numToCurencyString(widget.price),
                          style: TextStyle(color: Colors.deepOrange),
                          overflow: TextOverflow.ellipsis,
                          maxLines: 1,
                        ),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Row(
                    children: [
                      Text('Lợi nhuận: '),
                      Flexible(
                        child: Text(
                          AppUtils.numToCurencyString(widget.profit),
                          style: TextStyle(color: Colors.grey),
                          overflow: TextOverflow.ellipsis,
                          maxLines: 1,
                        ),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  !widget.isOrder
                      ? (widget.inventoryNum != null &&
                              widget.inventoryNum != 0)
                          ? Text(
                              'Hàng còn: ${widget.inventoryNum}',
                              style: TextStyle(
                                  fontSize: 15, color: Colors.blueAccent),
                            )
                          : Text(
                              'Còn hàng',
                              style: TextStyle(
                                  fontSize: 15, color: Colors.blueAccent),
                            )
                      : Text('Có hàng từ ',
                          style:
                              TextStyle(fontSize: 15, color: Colors.blueAccent))
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
