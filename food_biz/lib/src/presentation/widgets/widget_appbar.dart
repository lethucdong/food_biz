import 'package:flutter/material.dart';

PreferredSizeWidget buildAppBar(
    {Widget? title, double? elevation = 0, Widget? action}) {
  return AppBar(
    backgroundColor: Colors.white,
    centerTitle: true,
    title: Padding(
      padding: EdgeInsets.only(top: 30),
      child: title,
    ),
    leading: IconButton(
      padding: EdgeInsets.only(top: 30),
      icon: Icon(Icons.arrow_back),
      color: Colors.black,
      onPressed: () {
        // Do something.
      },
    ),
    actions: [
      Container(
        padding: EdgeInsets.only(top: 50, right: 20),
        child: action,
      )
    ],
    elevation: elevation,
    toolbarHeight: 80,
  );
}
