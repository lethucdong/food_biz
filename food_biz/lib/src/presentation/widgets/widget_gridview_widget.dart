import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/configs.dart';

class GridviewWidget extends StatefulWidget {
  List<Widget> listWidget;
  bool isShowMore;
  int maxItemShow;
  Function? functionShowmore;
  double childAspectRatio;
  double heightChild;
  double crossAxisSpacing;
  double mainAxisSpacing;
  int crossAxisCount;
  GridviewWidget(
      {Key? key,
      required this.listWidget,
      required this.isShowMore,
      required this.maxItemShow,
      required this.childAspectRatio,
      required this.heightChild,
      this.functionShowmore,
      required this.crossAxisSpacing,
      required this.mainAxisSpacing,
      required this.crossAxisCount})
      : super(key: key);

  @override
  _GridviewWidgetState createState() => _GridviewWidgetState();
}

class _GridviewWidgetState extends State<GridviewWidget> {
  @override
  Widget build(BuildContext context) {
    if (widget.maxItemShow > widget.listWidget.length)
      widget.maxItemShow = widget.listWidget.length;

    return Container(
      height: (widget.maxItemShow % 2 == 0)
          ? ((widget.maxItemShow / 2).floor() * widget.heightChild)
          : ((1 + (widget.maxItemShow / 2).floor()) * widget.heightChild),
      // padding: EdgeInsets.only(top: 10),
      child: GridView.builder(
        physics: NeverScrollableScrollPhysics(),
        itemCount: widget.maxItemShow,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            childAspectRatio: widget.childAspectRatio,
            crossAxisCount: widget.crossAxisCount,
            crossAxisSpacing: widget.crossAxisSpacing,
            mainAxisSpacing: widget.mainAxisSpacing),
        itemBuilder: (BuildContext context, int index) {
          if (index == widget.maxItemShow - 1 && widget.isShowMore)
            return GestureDetector(
              onTap: () {},
              child: Container(
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Container(
                    // alignment: Alignment.center,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image.asset(
                          AppImages.iconMenu,
                          scale: 2,
                        ),
                        Text('Xem tất cả >')
                      ],
                    ),
                  )),
            );
          return widget.listWidget[index];
        },
      ),
    );
  }
}
