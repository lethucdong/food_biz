import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/configs.dart';
import 'package:flutter_app/src/utils/app_utils.dart';
import 'package:provider/provider.dart';

import '../presentation.dart';

class OrderScreen extends StatefulWidget {
  const OrderScreen({Key? key}) : super(key: key);
  @override
  _OrderScreenState createState() => _OrderScreenState();
}

class _OrderScreenState extends State<OrderScreen> with ResponsiveWidget {
  late OrderViewModel _viewModel;
  late Size _screenSize;
  @override
  Widget build(BuildContext context) {
    return BaseWidget<OrderViewModel>(
        viewModel: OrderViewModel(),
        onViewModelReady: (viewModel) {
          _viewModel = viewModel..init();
        },
        builder: (context, viewModel, appbar) {
          return Scaffold(
              backgroundColor: Colors.grey.shade200, body: buildUi(context));
        });
  }

  @override
  Widget buildDesktop(BuildContext context) {
    return _buildUi(context);
  }

  @override
  Widget buildMobile(BuildContext context) {
    return _buildUi(context);
  }

  @override
  Widget buildTablet(BuildContext context) {
    return _buildUi(context);
  }

  Widget _buildUi(BuildContext context) {
    return Container(
      child: Column(
        children: [
          _buildMenu(context),
          _buildPageView(context),
        ],
      ),
    );
  }

  Widget _buildMenu(BuildContext context) {
    List<String> listItemMenu = [
      'Tất cả',
      'Đơn hàng nháp',
      'Đơn hàng mới',
      'Đơn hàng đang xử lý',
      'Đơn hàng đã giao',
      'Đơn hàng sau khi giao',
      'Đơn hàng hủy',
    ];

    return Stack(
      children: [
        Container(
          height: 80,
          width: double.infinity,
          color: Colors.white,
        ),
        Positioned(
          child: Container(
            padding: EdgeInsets.only(top: 50),
            alignment: Alignment.bottomLeft,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Container(
                  height: 40,
                  child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: listItemMenu.length,
                      itemBuilder: (BuildContext context, int index) {
                        return GestureDetector(
                          onTap: () {
                            Provider.of<OrderViewModel>(context, listen: false)
                                .onTapMenu(index);
                          },
                          child: Column(
                            children: [
                              Container(
                                // color: Colors.redAccent,
                                child: FittedBox(
                                  fit: BoxFit.scaleDown,
                                  child: Container(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: index == 0 ||
                                                index == listItemMenu.length - 1
                                            ? 20
                                            : 10),
                                    child: Text(
                                      listItemMenu[index],
                                      style: TextStyle(fontSize: 16),
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Container(
                                width: 30,
                                height: 5,
                                color: index ==
                                        context
                                            .watch<OrderViewModel>()
                                            .menuSelectedIndex
                                    ? Colors.deepOrange
                                    : null,
                              ),
                            ],
                          ),
                        );
                      }),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }

  _buildSearch() {
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 20),
        alignment: Alignment.center,
        // color: Colors.red,
        decoration: BoxDecoration(
          color: Colors.grey.shade300,
          border: Border.all(color: Colors.grey.shade300, width: 1),
          borderRadius: BorderRadius.circular(12),
        ),
        height: 45,
        child: TextField(
          style: TextStyle(fontSize: 21),
          decoration: InputDecoration(
            isDense: true,
            hintText: 'Tìm kiếm',
            contentPadding: EdgeInsets.only(top: 10),
            suffixIcon: Image.asset(
              AppImages.iconSearch,
              scale: 2,
              color: Colors.black,
            ),
            border: InputBorder.none,
          ),
        ),
      ),
    );
  }

  _buildOrverView() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          SizedBox(
            height: 10,
          ),
          Text('Tổng quan', style: TextStyle(fontSize: 16)),
          SizedBox(
            height: 20,
          ),
          _buildInfo(),
          _buildListProduct(),
        ],
      ),
    );
  }

  Widget _buildProductItem() {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: Colors.white,
      ),
      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
      margin: EdgeInsets.symmetric(vertical: 5),
      child: Column(
        children: [
          Row(
            children: [
              Expanded(
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          width: 60,
                          height: 60,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: Colors.grey,
                            ),
                            image: DecorationImage(
                                image: AssetImage(
                                  AppImages.imgproductHaLong1,
                                ),
                                fit: BoxFit.cover),
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Text(
                                'Tôm nõn Hạ Long',
                                style: TextStyle(fontSize: 16),
                              ),
                              SizedBox(height: 5),
                              Text(
                                AppUtils.numToCurencyString(6000000),
                                style: TextStyle(
                                    color: Colors.deepOrange, fontSize: 16),
                              ),
                              SizedBox(height: 5),
                              Container(
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text('SL: 11',
                                        style: TextStyle(fontSize: 16)),
                                    Text(
                                      'Đang thanh toán',
                                      style: TextStyle(
                                          color: Colors.deepOrange,
                                          fontSize: 16),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              )
            ],
          ),
          // LineSpace(),
        ],
      ),
    );
  }

  _buildInfo() {
    return Container(
      padding: EdgeInsets.all(20),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
      ),
      child: Column(
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: Text(
                  'Tổng số đơn thành công',
                  style: TextStyle(fontSize: 16),
                  softWrap: true,
                ),
              ),
              SizedBox(
                width: 5,
              ),
              Text(
                '0',
                style: TextStyle(color: Colors.deepOrange, fontSize: 16),
              ),
            ],
          ),
          SizedBox(
            height: 20,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: Text(
                  'Tổng giá trị đơn hàng thành công',
                  style: TextStyle(fontSize: 16),
                  overflow: TextOverflow.clip,
                  softWrap: true,
                ),
              ),
              SizedBox(
                width: 5,
              ),
              Text(
                AppUtils.numToCurencyString(0),
                style: TextStyle(color: Colors.deepOrange, fontSize: 16),
              ),
            ],
          ),
          SizedBox(
            height: 20,
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: Text(
                  'Hoa hồng đã nhận',
                  style: TextStyle(fontSize: 16),
                  softWrap: true,
                ),
              ),
              SizedBox(
                width: 5,
              ),
              Text(
                AppUtils.numToCurencyString(0),
                style: TextStyle(color: Colors.deepOrange, fontSize: 16),
              ),
            ],
          ),
        ],
      ),
    );
  }

  _buildListProduct() {
    return Container(
      padding: EdgeInsets.only(top: 50),
      child: Column(
        children: [_buildProductItem()],
      ),
    );
  }

  _buildPageView(BuildContext context) {
    return Expanded(
      child: PageView(
        /// [PageView.scrollDirection] defaults to [Axis.horizontal].
        /// Use [Axis.vertical] to scroll vertically.
        controller: _viewModel.pageController,
        physics: NeverScrollableScrollPhysics(),
        children: <Widget>[
          _buildShowAll(),
          _buildRawOrder(),
          _buildNewOrder(),
          _buildPendingOrder(),
          _buildFinishOrder(),
          _buildAfterFinishOrder(),
          _buildCancelOrder(),
        ],
      ),
    );
  }

  _buildShowAll() {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          children: [
            _buildSearch(),
            _buildOrverView(),
          ],
        ),
      ),
    );
  }

  _buildRawOrder() {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          children: [
            _buildOrderItem(type: TypeOrder.RAW),
            _buildOrderItem(type: TypeOrder.RAW),
            _buildOrderItem(type: TypeOrder.RAW),
            _buildOrderItem(type: TypeOrder.RAW),
          ],
        ),
      ),
    );
  }

  _buildOrderItem({required TypeOrder type}) {
    return Padding(
      padding: const EdgeInsets.symmetric(
        horizontal: 20,
        // vertical: 10,
      ),
      child: GestureDetector(
        child: Container(
          padding: EdgeInsets.all(10),
          margin: EdgeInsets.only(bottom: 10),
          height: 95,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(10),
          ),
          child: Column(
            children: [
              Row(
                children: [
                  Text(
                    type == TypeOrder.RAW
                        ? 'Đơn hàng nháp'
                        : type == TypeOrder.PENDING
                            ? 'Đơn hàng đang xử lý'
                            : type == TypeOrder.FINISH
                                ? 'Đơn hàng đã giao'
                                : type == TypeOrder.AFTER_FINISH
                                    ? 'Đơn hàng sau khi giao'
                                    : type == TypeOrder.CANCEL
                                        ? 'Đơn hàng hủy'
                                        : 'Đơn hàng mới',
                    style: TextStyle(color: Colors.deepOrange),
                  ),
                  SizedBox(
                    width: 15,
                  ),
                  Container(
                    width: 5,
                    height: 5,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Colors.deepOrange,
                    ),
                  ),
                  SizedBox(
                    width: 7,
                  ),
                  Text(
                    '28/7/2021',
                    style: TextStyle(color: Colors.grey),
                  ),
                ],
              ),
              SizedBox(height: 10),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Mã đơn hàng: A123456',
                    style: TextStyle(fontSize: 16),
                  ),
                  GestureDetector(
                      onTap: () {
                        _viewModel.onTapOrderDetail(type);
                      },
                      child: Image.asset(AppImages.iconRightAround))
                ],
              ),
              SizedBox(height: 10),
              Row(
                children: [
                  Text(
                    AppUtils.numToCurencyString(340000),
                    style: TextStyle(color: Colors.blueAccent),
                  ),
                  SizedBox(
                    width: 15,
                  ),
                  Container(
                    width: 5,
                    height: 5,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Colors.blueAccent,
                    ),
                  ),
                  SizedBox(
                    width: 7,
                  ),
                  Text(
                    '4 món',
                    style: TextStyle(color: Colors.grey),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  _buildNewOrder() {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          children: [
            _buildOrderItem(type: TypeOrder.NEW),
            _buildOrderItem(type: TypeOrder.NEW),
            _buildOrderItem(type: TypeOrder.NEW),
            _buildOrderItem(type: TypeOrder.NEW),
          ],
        ),
      ),
    );
  }

  _buildPendingOrder() {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          children: [
            _buildOrderItem(type: TypeOrder.PENDING),
            _buildOrderItem(type: TypeOrder.PENDING),
            _buildOrderItem(type: TypeOrder.PENDING),
            _buildOrderItem(type: TypeOrder.PENDING),
          ],
        ),
      ),
    );
  }

  _buildFinishOrder() {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          children: [
            _buildOrderItem(type: TypeOrder.FINISH),
            _buildOrderItem(type: TypeOrder.FINISH),
            _buildOrderItem(type: TypeOrder.FINISH),
            _buildOrderItem(type: TypeOrder.FINISH),
          ],
        ),
      ),
    );
  }

  _buildAfterFinishOrder() {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          children: [
            _buildOrderItem(type: TypeOrder.AFTER_FINISH),
            _buildOrderItem(type: TypeOrder.AFTER_FINISH),
            _buildOrderItem(type: TypeOrder.AFTER_FINISH),
            _buildOrderItem(type: TypeOrder.AFTER_FINISH),
          ],
        ),
      ),
    );
  }

  _buildCancelOrder() {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          children: [
            _buildOrderItem(type: TypeOrder.CANCEL),
            _buildOrderItem(type: TypeOrder.CANCEL),
            _buildOrderItem(type: TypeOrder.CANCEL),
            _buildOrderItem(type: TypeOrder.CANCEL),
          ],
        ),
      ),
    );
  }
}
