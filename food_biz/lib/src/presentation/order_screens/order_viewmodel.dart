import '../presentation.dart';
import 'package:get/get.dart';

enum TypeOrder { RAW, NEW, PENDING, FINISH, AFTER_FINISH, CANCEL }

class OrderViewModel extends BaseViewModel {
  int menuSelectedIndex = 0;
  late PageController pageController;
  init() async {
    pageController =
        PageController(initialPage: menuSelectedIndex, keepPage: true);
  }

  onTapMenu(int tabIndex) {
    pageController.jumpToPage(tabIndex);
    menuSelectedIndex = tabIndex;

    notifyListeners();
  }

  onTapOrderDetail(TypeOrder typeOrder) {
    Get.toNamed(Routers.order_detail, arguments: {'typeOrder': typeOrder});
  }
}
