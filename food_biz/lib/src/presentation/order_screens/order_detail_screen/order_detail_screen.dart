import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/configs.dart';
import 'package:flutter_app/src/utils/app_utils.dart';
import 'package:provider/provider.dart';

import '../../presentation.dart';

class OrderDetailScreen extends StatefulWidget {
  final TypeOrder typeOrder;
  const OrderDetailScreen({Key? key, required this.typeOrder})
      : super(key: key);
  @override
  _OrderDetailScreenState createState() => _OrderDetailScreenState();
}

class _OrderDetailScreenState extends State<OrderDetailScreen>
    with ResponsiveWidget {
  late OrderDetailViewModel _viewModel;
  late Size _screenSize;
  @override
  Widget build(BuildContext context) {
    return BaseWidget<OrderDetailViewModel>(
        viewModel: OrderDetailViewModel(),
        onViewModelReady: (viewModel) {
          _viewModel = viewModel..init();
        },
        builder: (context, viewModel, appbar) {
          return Scaffold(
              appBar: buildAppBar(
                  title: Text(
                'Chi tiết đơn hàng',
                style:
                    TextStyle(color: Colors.black, fontStyle: FontStyle.normal),
                textAlign: TextAlign.center,
              )),
              backgroundColor: Colors.grey.shade300,
              body: buildUi(context));
        });
  }

  @override
  Widget buildDesktop(BuildContext context) {
    return _buildUi(context);
  }

  @override
  Widget buildMobile(BuildContext context) {
    return _buildUi(context);
  }

  @override
  Widget buildTablet(BuildContext context) {
    return _buildUi(context);
  }

  Widget _buildUi(BuildContext context) {
    _screenSize = MediaQuery.of(context).size;
    return Column(
      children: [
        Expanded(
            child: Container(
          child: SingleChildScrollView(
            child: Column(
              children: [
                _buildHeader(),
                _buildListProduct(),
                _buildDevivery(),
                _buildPaymentMethod(),
                _buildInfoForCus(),
                _buildYourInfor(),
                _buildReward(),
              ],
            ),
          ),
        )),
        GestureDetector(
          onTap: () {},
          child: widget.typeOrder == TypeOrder.RAW
              ? Container(
                  height: 45,
                  color: Colors.deepOrange,
                  alignment: Alignment.center,
                  child: Text(
                    'Đặt đơn',
                    style: TextStyle(color: Colors.white, fontSize: 16),
                  ),
                )
              : widget.typeOrder == TypeOrder.FINISH ||
                      widget.typeOrder == TypeOrder.AFTER_FINISH
                  ? Container(
                      height: 45,
                      color: Colors.deepOrange,
                      alignment: Alignment.center,
                      child: Text(
                        'Đặt lại',
                        style: TextStyle(color: Colors.white, fontSize: 16),
                      ),
                    )
                  : widget.typeOrder == TypeOrder.CANCEL
                      ? Container()
                      : Row(
                          children: [
                            Expanded(
                              flex: 1,
                              child: GestureDetector(
                                onTap: () {
                                  showDialog(
                                    context: context,
                                    builder: (BuildContext context) {
                                      return Scaffold(
                                        backgroundColor: Colors.transparent,
                                        body: Center(
                                          child: Container(
                                            margin: EdgeInsets.symmetric(
                                                horizontal: 20),
                                            height: 150,
                                            decoration: BoxDecoration(
                                              color: Colors.white,
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                            ),
                                            child: Column(
                                              children: [
                                                Expanded(
                                                  child: Center(
                                                      child: Padding(
                                                    padding: const EdgeInsets
                                                            .symmetric(
                                                        horizontal: 30),
                                                    child: Text(
                                                      'Bạn cần gọi về hệ thống để xử lý việc hủy đơn hàng',
                                                      textAlign:
                                                          TextAlign.center,
                                                    ),
                                                  )),
                                                ),
                                                Container(
                                                  child: Row(
                                                    children: [
                                                      Expanded(
                                                        child: GestureDetector(
                                                          onTap: () {
                                                            Navigator.pop(
                                                                context);
                                                          },
                                                          child: Container(
                                                            height: 45,
                                                            decoration:
                                                                BoxDecoration(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .only(
                                                                bottomLeft: Radius
                                                                    .circular(
                                                                        10),
                                                              ),
                                                              color: Colors
                                                                  .orange
                                                                  .shade800,
                                                            ),
                                                            alignment: Alignment
                                                                .center,
                                                            child: Text(
                                                              'Hủy bỏ',
                                                              style: TextStyle(
                                                                  color: Colors
                                                                      .white),
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                      SizedBox(width: 1),
                                                      Expanded(
                                                        child: GestureDetector(
                                                          onTap: () {},
                                                          child: Container(
                                                            height: 45,
                                                            decoration:
                                                                BoxDecoration(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .only(
                                                                bottomRight:
                                                                    Radius
                                                                        .circular(
                                                                            10),
                                                              ),
                                                              color: Colors
                                                                  .orange
                                                                  .shade800,
                                                            ),
                                                            alignment: Alignment
                                                                .center,
                                                            child: Text(
                                                              'Đồng ý',
                                                              style: TextStyle(
                                                                  color: Colors
                                                                      .white),
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      );
                                    },
                                  );
                                },
                                child: Container(
                                  // width: MediaQuery.of(context).size.width,
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    border: Border(
                                      top: BorderSide(color: Colors.grey),
                                      left: BorderSide(color: Colors.grey),
                                      bottom: BorderSide(color: Colors.grey),
                                    ),
                                  ),
                                  height: 45,
                                  alignment: Alignment.center,
                                  child: Text(
                                    'Hủy đơn hàng',
                                    style: TextStyle(
                                        color: Colors.black,
                                        // fontWeight: FontWeight.bold,
                                        fontSize: 16),
                                  ),
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 1,
                              child: GestureDetector(
                                child: Container(
                                  // width: MediaQuery.of(context).size.width,
                                  height: 45,
                                  color: Colors.deepOrange,
                                  alignment: Alignment.center,
                                  child: Text(
                                    'Đặt lại',
                                    style: TextStyle(
                                        color: Colors.white,
                                        // fontWeight: FontWeight.bold,
                                        fontSize: 16),
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
        )
      ],
    );
    // return Container(
    //   child: SingleChildScrollView(
    //     child: Column(
    //       children: [
    //         _buildHeader(),
    //         _buildListProduct(),
    //         _buildDevivery(),
    //         _buildPaymentMethod(),
    //         _buildInfoForCus(),
    //         _buildYourInfor(),
    //         _buildReward(),
    //       ],
    //     ),
    //   ),
    // );
  }

  _buildHeader() {
    return Container(
        padding: EdgeInsets.all(20),
        margin: EdgeInsets.only(top: 5),
        color: Colors.white,
        child: Column(
          children: [
            _buildInfo(),
            _buildAddress(),
          ],
        ));
  }

  _buildInfo() {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Mã đơn hàng:',
              style: TextStyle(color: Colors.grey, fontSize: 16),
            ),
            Text(
              '102949HG',
              style: TextStyle(color: Colors.grey, fontSize: 16),
            ),
          ],
        ),
        SizedBox(height: 5),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Ngày đặt hàng:',
              style: TextStyle(color: Colors.grey, fontSize: 16),
            ),
            Text(
              '4:30 29/10/2020',
              style: TextStyle(color: Colors.grey, fontSize: 16),
            ),
          ],
        ),
        SizedBox(height: 5),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Đơn hàng:',
              style: TextStyle(color: Colors.grey, fontSize: 16),
            ),
            Text(
              widget.typeOrder == TypeOrder.RAW
                  ? 'Đơn hàng nháp'
                  : widget.typeOrder == TypeOrder.PENDING
                      ? 'Đơn hàng đang xử lý'
                      : widget.typeOrder == TypeOrder.FINISH
                          ? 'Đơn hàng đã giao'
                          : widget.typeOrder == TypeOrder.AFTER_FINISH
                              ? 'Đơn hàng sau khi giao'
                              : widget.typeOrder == TypeOrder.CANCEL
                                  ? 'Đơn hàng hủy'
                                  : 'Đơn hàng mới',
              style: TextStyle(color: Colors.deepOrange, fontSize: 16),
            ),
          ],
        )
      ],
    );
  }

  Widget _buildAddress() {
    return Container(
      // width: _screenSize.width,
      // padding: EdgeInsets.all(20),
      child: Column(
        children: [
          Row(
            children: [
              Image.asset(
                AppImages.iconLocation,
              ),
              SizedBox(
                width: 10,
              ),
              Text(
                'Địa chỉ giao hàng',
                style: TextStyle(fontSize: 16),
              ),
            ],
          ),
          SizedBox(
            height: 10,
          ),
          Container(
            width: _screenSize.width,
            padding: EdgeInsets.all(20),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Colors.grey.shade200),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('Trương Huyền'),
                SizedBox(
                  height: 10,
                ),
                Text(
                  '0364 111 999',
                  style: TextStyle(color: Colors.grey),
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  '120 Nguyễn Trãi',
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  'Thanh Xuân, Hà Nội',
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  _buildListProduct() {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.only(top: 5),
          padding: EdgeInsets.only(left: 20, right: 20, top: 10, bottom: 50),
          color: Colors.white,
          child: Column(
            children: [
              _buildProductItem(),
              _buildProductItem(),
              _buildProductItem(),
            ],
          ),
        ),
      ],
    );
  }

  Widget _buildProductItem() {
    return Container(
      padding: EdgeInsets.only(top: 10),
      child: Column(
        children: [
          Row(
            children: [
              Expanded(
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          width: 60,
                          height: 60,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: Colors.grey,
                            ),
                            image: DecorationImage(
                                image: AssetImage(
                                  AppImages.imgproductHaLong1,
                                ),
                                fit: BoxFit.cover),
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Text(
                                'Tôm nõn Hạ Long',
                                style: TextStyle(fontSize: 16),
                              ),
                              SizedBox(height: 5),
                              Container(
                                // width: 200,
                                child: RichText(
                                  softWrap: true,
                                  text: TextSpan(
                                    text: 'Giá bán nhà cung cấp:     ',
                                    style: TextStyle(color: Colors.black),
                                    children: <TextSpan>[
                                      TextSpan(
                                          text: AppUtils.numToCurencyString(
                                              5500000)),
                                    ],
                                  ),
                                ),
                              ),
                              SizedBox(height: 5),
                              RichText(
                                text: TextSpan(
                                  text: 'Giá bán Của bạn:     ',
                                  style: TextStyle(color: Colors.black),
                                  children: <TextSpan>[
                                    TextSpan(
                                      text:
                                          AppUtils.numToCurencyString(6000000),
                                      style:
                                          TextStyle(color: Colors.deepOrange),
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(height: 5),
                              Text('Số lượng: 1')
                            ],
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              )
            ],
          ),
          SizedBox(height: 5),
          LineSpace()
        ],
      ),
    );
  }

  Widget _buildDevivery() {
    return Container(
      margin: EdgeInsets.only(top: 5),
      padding: EdgeInsets.only(top: 20, right: 20, left: 20),
      color: Colors.white,
      child: Column(
        children: [
          Row(
            children: [
              Image.asset(
                AppImages.iconLocation,
              ),
              SizedBox(
                width: 10,
              ),
              Text('Vận chuyển', style: TextStyle(fontSize: 16)),
            ],
          ),
          SizedBox(
            height: 10,
          ),
          Column(
            children: [
              Container(
                width: _screenSize.width,
                // height: 70,
                color: Colors.white,
                child: Container(
                  padding: EdgeInsets.symmetric(
                    horizontal: 20,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Expanded(
                            child: Text(
                              'Giao hàng  GOFAST',
                              style: TextStyle(fontSize: 16),
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          RichText(
                            textAlign: TextAlign.right,
                            text: TextSpan(
                              text: AppUtils.numToCurencyString(30000) + '  ',
                              style: TextStyle(
                                color: Colors.deepOrange,
                              ),
                              children: <TextSpan>[
                                TextSpan(
                                  text: AppUtils.numToCurencyString(40000),
                                  style: TextStyle(
                                      decoration: TextDecoration.lineThrough,
                                      color: Colors.black),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Expanded(child: Text('Thời gian giao hàng dự kiến')),
                          SizedBox(
                            width: 10,
                          ),
                          Text('20/08 - 22/08',
                              style: TextStyle(color: Colors.blueAccent),
                              textAlign: TextAlign.right),
                        ],
                      )
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              LineSpace(),
              SizedBox(
                height: 10,
              ),
              Row(
                children: [
                  Expanded(
                    child: Text(
                      'Thời gian giao hàng mong muốn',
                      overflow: TextOverflow.ellipsis,
                      softWrap: true,
                    ),
                  ),
                  Expanded(
                    child: Text(
                      'Tất các các ngày trong tuần',
                      style: TextStyle(
                        color: Colors.deepOrange,
                        overflow: TextOverflow.ellipsis,
                      ),
                      softWrap: true,
                    ),
                  ),
                ],
              ),
            ],
          ),
          SizedBox(
            height: 10,
          ),
          LineSpace(),
          _buildLineInfo(
              leftContent: Text('Ghi chú vận chuyển'),
              rightContent: GestureDetector(
                child: Padding(
                  padding: const EdgeInsets.only(left: 10),
                  child: Text(
                    'Lorem Ipsum is simpl Lorem Ipsum is simply... Lorem Ipsum is simply...',
                    textAlign: TextAlign.right,
                    overflow: TextOverflow.ellipsis,
                    softWrap: false,
                  ),
                ),
              )),
        ],
      ),
    );
  }

  Widget _buildLineInfo(
      {required Widget leftContent, required Widget rightContent}) {
    return Container(
      height: 50,
      // width: _screenSize.width,
      alignment: Alignment.centerLeft,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [leftContent, Expanded(child: rightContent)],
      ),
    );
  }

  Widget _buildPaymentMethod() {
    return Container(
      margin: EdgeInsets.only(top: 5),
      color: Colors.white,
      padding: EdgeInsets.all(20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Image.asset(
                AppImages.iconCreditCard,
                scale: 2,
              ),
              SizedBox(
                width: 10,
              ),
              Text('Phương thức thanh toán'),
            ],
          ),
          SizedBox(
            height: 10,
          ),
          Text(
            'Thanh toán qua chuyển khoản',
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 10),
            child: LineSpace(),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text('Chọn ngân hàng'),
              Wrap(
                children: [
                  Text('Vietcombank'),
                  Image.asset(AppImages.imgVcb),
                ],
              )
            ],
          )
        ],
      ),
    );
  }

  Widget _buildInfoForCus() {
    return Container(
      color: Colors.white,
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        children: [
          _buildLineInfo(
              leftContent: Text(
                'Thông tin cho khách hàng của bạn',
                style: TextStyle(fontSize: 16),
              ),
              rightContent: SizedBox()),
          LineSpace(),
          _buildLineInfo(
            leftContent: Text('Tổng tiền sản phẩm'),
            rightContent: Text(
              AppUtils.numToCurencyString(150000),
              textAlign: TextAlign.right,
              style: TextStyle(color: Colors.blueAccent),
            ),
          ),
          LineSpace(),
          _buildLineInfo(
            leftContent: Text('Tổng phí vận chuyển'),
            rightContent: Text(
              AppUtils.numToCurencyString(30000),
              textAlign: TextAlign.right,
            ),
          ),
          LineSpace(),
          _buildLineInfo(
            leftContent: Text('Giảm giá vận chuyển'),
            rightContent: Text(
              '-${AppUtils.numToCurencyString(20000)}',
              textAlign: TextAlign.right,
            ),
          ),
          LineSpace(),
          _buildLineInfo(
            leftContent: Text('Tổng tiền thanh toán'),
            rightContent: Text(
              AppUtils.numToCurencyString(140000),
              textAlign: TextAlign.right,
              style: TextStyle(
                  color: Colors.deepOrange, fontWeight: FontWeight.bold),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildYourInfor() {
    return Container(
      margin: EdgeInsets.only(top: 5),
      color: Colors.white,
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        children: [
          _buildLineInfo(
              leftContent: Text(
                'Thông tin cho bạn',
                style: TextStyle(fontSize: 16),
              ),
              rightContent: SizedBox()),
          LineSpace(),
          _buildLineInfo(
            leftContent: Text('Tổng Tiền Hàng'),
            rightContent: Text(
              AppUtils.numToCurencyString(170000),
              style: TextStyle(color: Colors.blueAccent),
              textAlign: TextAlign.right,
            ),
          ),
          LineSpace(),
          _buildLineInfo(
            leftContent: Text('Tổng giá bán của bạn'),
            rightContent: Text(
              AppUtils.numToCurencyString(150000),
              textAlign: TextAlign.right,
            ),
          ),
          LineSpace(),
          _buildLineInfo(
            leftContent: Row(
              children: [
                Text('Tổng thưởng hạng:'),
                Image.asset(AppImages.iconRank2, scale: 2),
              ],
            ),
            rightContent: Text(
              AppUtils.numToCurencyString(20000),
              textAlign: TextAlign.right,
            ),
          ),
          LineSpace(),
          _buildLineInfo(
            leftContent: Text('Voucher của bạn'),
            rightContent: Text(
              AppUtils.numToCurencyString(20000),
              textAlign: TextAlign.right,
            ),
          ),
          LineSpace(),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 10),
            child: Row(
              children: [
                Text('Fobi Xu'),
                Expanded(child: Container()),
                Container(
                    // width: 173,

                    height: 30,
                    alignment: Alignment.centerRight,
                    padding: EdgeInsets.only(right: 10, left: 30),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10),
                      border: Border.all(color: Colors.deepOrange, width: 1),
                    ),
                    child: Text(AppUtils.numToCurencyString(35000),
                        style: TextStyle(fontSize: 14))),
              ],
            ),
          ),
          LineSpace(),
          _buildLineInfo(
            leftContent: Text('Tổng Tiền Ship'),
            rightContent: Text(
              '-${AppUtils.numToCurencyString(1000)}',
              textAlign: TextAlign.right,
            ),
          ),
          LineSpace(),
          _buildLineInfo(
              leftContent: Text('Tổng lợi nhuận của bạn'),
              rightContent: Text(
                AppUtils.numToCurencyString(290000),
                textAlign: TextAlign.right,
                style: TextStyle(
                    color: Colors.deepOrange, fontWeight: FontWeight.bold),
              )),
        ],
      ),
    );
  }

  _buildReward() {
    return Container(
      margin: EdgeInsets.only(top: 5),
      color: Colors.white,
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        children: [
          _buildLineInfo(
            leftContent: Text('Số Fobi Xu nhận được'),
            rightContent: Text(
              AppUtils.numToCurencyString(10000).replaceAll('đ', ' Xu'),
              textAlign: TextAlign.right,
            ),
          ),
          LineSpace(),
          _buildLineInfo(
            leftContent: Text('Đã sử dụng Voucher'),
            rightContent: Text(
              AppUtils.numToCurencyString(10000).replaceAll('đ', ' Xu'),
              textAlign: TextAlign.right,
            ),
          ),
        ],
      ),
    );
  }
}
