import '../../presentation.dart';

class OrderDetailViewModel extends BaseViewModel {
  int menuSelectedIndex = 0;
  late PageController pageController;
  init() async {
    pageController =
        PageController(initialPage: menuSelectedIndex, keepPage: true);
  }

  onTapMenu(int tabIndex) {
    pageController.jumpToPage(tabIndex);
    menuSelectedIndex = tabIndex;

    notifyListeners();
  }
}
