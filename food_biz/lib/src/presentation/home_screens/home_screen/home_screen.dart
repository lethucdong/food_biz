import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/constanst/app_images.dart';

import '../../presentation.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> with ResponsiveWidget {
  late HomeViewModel _viewModel;
  var _scaffoldKey = GlobalKey<ScaffoldState>();
  late Size _screenSize;
  @override
  Widget build(BuildContext context) {
    _screenSize = MediaQuery.of(context).size;

    return BaseWidget<HomeViewModel>(
        viewModel: HomeViewModel(),
        onViewModelReady: (viewModel) {
          _viewModel = viewModel..init();
        },
        builder: (context, viewModel, appbar) {
          return Scaffold(
            key: _scaffoldKey,
            appBar: _buildAppBar(),
            body: buildUi(context),
            drawer: sideDrawer(context),
          );
        });
  }

  @override
  Widget buildDesktop(BuildContext context) {
    return _buildBody(context);
  }

  @override
  Widget buildMobile(BuildContext context) {
    return _buildBody(context);
  }

  @override
  Widget buildTablet(BuildContext context) {
    return _buildBody(context);
  }

  Widget _buildBody(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        // padding: EdgeInsets.symmetric(horizontal: 20),
        color: Colors.grey.shade300,
        alignment: Alignment.center,
        child: Column(
          children: [
            _buildSlideImage(),
            SizedBox(height: 10),
            _buildCategories(context),
            SizedBox(height: 10),
            // Container(
            //   padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
            //   decoration: BoxDecoration(
            //     borderRadius: BorderRadius.circular(10),
            //     color: Colors.grey.shade200,
            //   ),
            //   child: Column(
            //     children: [
            //       _buildBestSell(),
            //       SizedBox(height: 10),
            //       _buildPreOder(),
            //       SizedBox(height: 10),
            //     ],
            //   ),
            // ),
            _buildBestSell(),
            SizedBox(height: 10),
            _buildPreOder(),
            SizedBox(height: 10),
            _buildBestSeller(),
            SizedBox(height: 10),
            _buildBlog(),
            SizedBox(height: 10),
            _buildPromotion(),
            SizedBox(height: 10),
            _buildSpecialFood(),
            SizedBox(height: 10),
            _buildRecommendFood(),
            SizedBox(height: 10),
            _buildComboProduct(),
            SizedBox(height: 10),
            _buildRecommendProduct(context),
          ],
        ),
      ),
    );
  }

  PreferredSizeWidget _buildAppBar() {
    return AppBar(
      elevation: 1,
      backgroundColor: Colors.white,
      shadowColor: Colors.grey,
      leading: IconButton(
        padding: EdgeInsets.only(top: 30),
        icon: Image.asset(AppImages.iconSlideMenu),
        color: Colors.black,
        onPressed: () {
          _scaffoldKey.currentState!.openDrawer();
        },
      ),
      actions: [
        _buildIconAppbar(Image.asset(AppImages.iconBell), 5, () {
          _viewModel.onTapNotify();
        }),
        _buildIconAppbar(Image.asset(AppImages.iconCart), 0, () {}),
      ],
      title: Padding(
        padding: const EdgeInsets.only(top: 30),
        child: Container(
          // color: Colors.red,
          decoration: BoxDecoration(
            color: Colors.grey.shade300,
            border: Border.all(color: Colors.grey.shade300, width: 1),
            borderRadius: BorderRadius.circular(12),
          ),
          height: 35,
          child: TextField(
            decoration: InputDecoration(
              isDense: true,
              hintText: 'Tìm kiếm',
              // contentPadding: EdgeInsets.only(bottom: 0),
              prefixIcon: Icon(Icons.search_outlined),
              border: InputBorder.none,
            ),
          ),
        ),
      ),
      toolbarHeight: 80,
    );
  }

//#region buildIconAppbar
  _buildIconAppbar(Widget icon, int quantity, Function function) {
    return Container(
      padding: EdgeInsets.only(top: 30),
      child: Stack(
        children: [
          IconButton(
            icon: icon,
            color: Colors.black,
            onPressed: () {
              function();
              // Do something.
            },
          ),
          Positioned(
              top: 5,
              right: 5,
              width: 17,
              height: 17,
              child: quantity > 0
                  ? Container(
                      decoration: BoxDecoration(
                          shape: BoxShape.circle, color: Colors.red),
                      child: Text(
                        '$quantity' + '+',
                        style: TextStyle(fontSize: 12),
                        textAlign: TextAlign.center,
                      ),
                    )
                  : Container())
        ],
      ),
    );
  }
//#endregion

//#region buildSlideImage
  _buildSlideImage() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10),
      decoration: BoxDecoration(
          color: Colors.grey.shade200,
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(10),
              bottomRight: Radius.circular(10))),
      child: Column(
        children: [
          SizedBox(
            height: 10,
          ),
          // CarouselBan
          // ner(),
          CarouselCommon(
            paddingPageview: 5,
            isShowPointSlide: true,
            isAutoScroll: true,
            width: _screenSize.width,
            height: 155,
            viewportFraction: 1,
            pointPadingHeight: 140,
            listWidget: [
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  image: DecorationImage(
                    image: AssetImage(AppImages.imgBanner),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  image: DecorationImage(
                    image: AssetImage(AppImages.imgBanner),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  image: DecorationImage(
                    image: AssetImage(AppImages.imgBanner),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ],
          ),
          SizedBox(
            height: 5,
          ),
          CarouselCommon(
            paddingPageview: 5,
            isShowPointSlide: true,
            isAutoScroll: true,
            width: _screenSize.width - 20,
            height: 60,
            viewportFraction: 1 / 3,
            pointPadingHeight: 70,
            isGroupPoint: true,
            listWidget: [
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  image: DecorationImage(
                    image: AssetImage(AppImages.imgBanner),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  image: DecorationImage(
                    image: AssetImage(AppImages.imgBanner),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  image: DecorationImage(
                    image: AssetImage(AppImages.imgBanner),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  image: DecorationImage(
                    image: AssetImage(AppImages.imgBanner),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  image: DecorationImage(
                    image: AssetImage(AppImages.imgBanner),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  image: DecorationImage(
                    image: AssetImage(AppImages.imgBanner),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

//#endregion
  _buildCategories(BuildContext context) {
    return Container(
      width: _screenSize.width,
      alignment: Alignment.center,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: Colors.grey.shade200,
      ),
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Container(
          // width:_screenSize.width,
          padding: EdgeInsets.symmetric(vertical: 10),

          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              categoryWidget(
                Image.asset(
                  AppImages.iconCategories,
                  scale: 1.5,
                ),
                'Mẹ & bé',
                () {},
                Colors.blue,
                Colors.blue.shade300,
              ),
              categoryWidget(
                  Image.asset(
                    AppImages.iconCategories,
                    scale: 1.5,
                  ),
                  'Làm đẹp & chăm sóc cá nhân',
                  () {},
                  Colors.yellow,
                  Colors.yellow.shade300),
              categoryWidget(
                  Image.asset(
                    AppImages.iconCategories,
                    scale: 1.5,
                  ),
                  'Hàng tiêu dung & thực phẩm',
                  () {},
                  Colors.purple,
                  Colors.purple.shade300),
              categoryWidget(
                  Image.asset(
                    AppImages.iconCategories,
                    scale: 1.5,
                  ),
                  'Sức khỏe - đời sống',
                  () {},
                  Colors.red,
                  Colors.red.shade300),
              categoryWidget(
                  Image.asset(
                    AppImages.iconCategories,
                    scale: 1.5,
                  ),
                  'Thiết bị gia dụng',
                  () {},
                  Colors.green,
                  Colors.green.shade300),
            ],
          ),
        ),
      ),
    );
  }

  Widget categoryWidget(
    Image imageIcon,
    String title,
    Function function,
    Color startColor,
    Color endColor,
  ) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 2),
      height: 100,
      width: 70,
      child: Column(
        children: [
          GestureDetector(
            onTap: () {
              function();
            },
            child: Container(
              width: 50,
              height: 50,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                    colors: [
                      startColor,
                      endColor,
                    ]),
                shape: BoxShape.circle,
              ),
              child: imageIcon,
            ),
          ),
          Flexible(
            child: Text(
              title,
              softWrap: true,
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 12),
            ),
          )
        ],
      ),
    );
  }

  _buildBestSell() {
    List<Widget> listWidget = [
      ProductCard(
          isHot: true,
          isNew: true,
          discount: 75,
          isLove: false,
          name: 'Tôm nõn hấp Hạ Long',
          price: 150000,
          profit: 50000,
          inventoryNum: null,
          isOrder: false),
      ProductCard(
          isHot: true,
          isNew: false,
          discount: 60,
          isLove: true,
          name: 'Cá cơm rim lạc vừng',
          price: 150000,
          profit: 50000,
          inventoryNum: 5,
          isOrder: false),
      ProductCard(
          isHot: false,
          isNew: true,
          discount: 70,
          isLove: false,
          name: 'Cá cơm rim lạc vừng',
          price: 150000,
          profit: 50000,
          inventoryNum: 5,
          isOrder: false),
    ];

    return Container(
      padding: EdgeInsets.symmetric(vertical: 0, horizontal: 20),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: Colors.grey.shade200,
      ),
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('SẢN PHẨM BÁN CHẠY'),
                GestureDetector(
                  onTap: () {
                    _viewModel.onTapShowMoreBetSell(
                        listWidget: listWidget, title: 'Sản Phẩm Bán Chạy');
                  },
                  child: Text('Xem thêm'),
                )
              ],
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            // crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              CarouselCommon(
                listWidget: [
                  ProductCard(
                      isHot: true,
                      isNew: true,
                      discount: 75,
                      isLove: false,
                      name: 'Tôm nõn hấp Hạ Long',
                      price: 150000,
                      profit: 50000,
                      inventoryNum: null,
                      isOrder: false),
                  ProductCard(
                      isHot: true,
                      isNew: false,
                      discount: 60,
                      isLove: true,
                      name: 'Cá cơm rim lạc vừng',
                      price: 150000,
                      profit: 50000,
                      inventoryNum: 5,
                      isOrder: false),
                  ProductCard(
                      isHot: false,
                      isNew: true,
                      discount: 70,
                      isLove: false,
                      name: 'Cá cơm rim lạc vừng',
                      price: 150000,
                      profit: 50000,
                      inventoryNum: 5,
                      isOrder: false),
                ],
                isShowPointSlide: true,
                isAutoScroll: true,
                width: _screenSize.width - 40,
                height: 255,
                viewportFraction: 1 / 2,
                pointPadingHeight: 265,
                paddingPageview: 5,
              ),
            ],
          )
        ],
      ),
    );
  }

  Widget _buildPreOder() {
    List<Widget> listWidget = <Widget>[
      ProductCard(
          isHot: true,
          isNew: true,
          discount: 75,
          isLove: false,
          name: 'Tôm nõn hấp Hạ Long',
          price: 150000,
          profit: 50000,
          inventoryNum: null,
          isOrder: true),
      ProductCard(
          isHot: true,
          isNew: false,
          discount: 60,
          isLove: true,
          name: 'Cá cơm rim lạc vừng',
          price: 150000,
          profit: 50000,
          inventoryNum: 5,
          isOrder: true),
      ProductCard(
          isHot: false,
          isNew: true,
          discount: 70,
          isLove: false,
          name: 'Cá cơm rim lạc vừng',
          price: 150000,
          profit: 50000,
          inventoryNum: 5,
          isOrder: true),
    ];
    return Container(
      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: Colors.grey.shade200,
      ),
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('SẢN PHẨM ORDER'),
                GestureDetector(
                  onTap: () {
                    _viewModel.onTapShowMoreBetSell(
                        listWidget: listWidget, title: 'Sản Phẩm Order');
                  },
                  child: Text('Xem thêm'),
                )
              ],
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              CarouselCommon(
                listWidget: listWidget,
                isAutoScroll: false,
                isShowPointSlide: false,
                width: _screenSize.width - 40,
                height: 256,
                viewportFraction: 0.5,
                pointPadingHeight: 10,
                paddingPageview: 5,
              ),
            ],
          )
        ],
      ),
    );
  }

  _buildBestSeller() {
    return Container(
      height: 530,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: Colors.grey.shade200,
      ),
      child: Stack(
        children: [
          Container(
            alignment: Alignment.topRight,
            width: _screenSize.width,
            height: 114,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(10),
                    topRight: Radius.circular(10)),
                image: DecorationImage(
                    image: AssetImage(AppImages.imgBestSeller),
                    fit: BoxFit.fill)),
          ),
          Positioned(
            child: Container(
              // width: 200,
              // height: 200,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
              ),
              // child: Expanded(
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                ),
                child: CarouselCommon(
                  listWidget: [
                    _buildBestSellerCard(),
                    _buildBestSellerCard(),
                    _buildBestSellerCard()
                  ],
                  isShowPointSlide: true,
                  isAutoScroll: true,
                  width: _screenSize.width - 40,
                  height: 410,
                  viewportFraction: 1,
                  pointPadingHeight: 420,
                  paddingPageview: 5,
                ),
              ),
              // ),
            ),
            right: 20,
            left: 20,
            top: 90,
            // bottom: 30,
          ),
        ],
      ),
    );
  }

  Widget _buildBestSellerCard() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Container(
          child: Column(
            children: [
              Container(
                height: 50,
                width: double.infinity,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10),
                      topRight: Radius.circular(10)),
                  color: Colors.deepOrange,
                ),
                child: Text(
                  'BEST SELLER THÁNG 6',
                  style: TextStyle(color: Colors.white, fontSize: 16),
                ),
              ),
              Container(
                color: Colors.white,
                margin: EdgeInsets.only(bottom: 10),
                padding: EdgeInsets.only(top: 10),
                child: Row(
                  children: [
                    Expanded(
                      flex: 3,
                      child: Container(
                        height: 90,
                        child: Container(
                          margin:
                              EdgeInsets.only(top: 10, bottom: 10, right: 10),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              image: DecorationImage(
                                  image: AssetImage(AppImages.imgProduct_1),
                                  fit: BoxFit.fill)),
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 10,
                      child: Container(
                        height: 90,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text('CHÀ BÔNG TÔM'),
                            SizedBox(
                              height: 10,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                // Image.asset(AppImages.iconCertificatePortait),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Image.asset(
                                        AppImages.iconCertificatePortait),
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text('Doanh số:'),
                                        Text(
                                          '30 000 000 đ',
                                          style: TextStyle(
                                              color: Colors.blueAccent),
                                        )
                                      ],
                                    ),
                                  ],
                                ),
                                Container(
                                  // padding: EdgeInsets.only(right: 20),
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Image.asset(
                                          AppImages.iconCertificatePortait),
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text('Số đơn hàng:'),
                                          Text('1 000 đơn',
                                              style: TextStyle(
                                                  color: Colors.blueAccent))
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                                SizedBox(
                                  height: 30,
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Image.asset(AppImages.iconCertificate),
                                RichText(
                                  text: TextSpan(
                                    text: 'Thưởng:  ',
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 14,
                                    ),
                                    children: <TextSpan>[
                                      TextSpan(
                                        text: '10% giá trị',
                                        style: TextStyle(
                                          color: Colors.deepOrange,
                                          fontSize: 13,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
        // SizedBox(
        //   height: 10,
        // ),
        Container(
          child: Column(
            children: [
              for (int i = 0; i < 5; i++)
                Container(
                  height: 50,
                  decoration: BoxDecoration(
                    borderRadius: i == 4
                        ? BorderRadius.only(
                            bottomLeft: Radius.circular(10),
                            bottomRight: Radius.circular(10))
                        : BorderRadius.zero,
                    color: i % 2 == 0 ? Colors.white : Colors.grey.shade200,
                  ),
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                              width: 40,
                              height: 40,
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: Colors.black,
                                  image: DecorationImage(
                                      image: AssetImage(AppImages.iconAvatar),
                                      fit: BoxFit.scaleDown)),
                            ),
                            SizedBox(width: 10),
                            Container(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text('User name',
                                      style: TextStyle(fontSize: 16)),
                                  Text(
                                    '35 000 000 đ',
                                    style: TextStyle(
                                        color: Colors.deepOrange, fontSize: 13),
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      Text(
                        '${i + 1}',
                        style: TextStyle(
                            color: i == 0
                                ? Colors.yellow[700]
                                : i == 1
                                    ? Colors.grey
                                    : i == 2
                                        ? Colors.deepOrange
                                        : Colors.black),
                      )
                    ],
                  ),
                )
            ],
          ),
        ),
      ],
    );
  }

  Widget _buildBlog() {
    return Container(
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: Colors.grey.shade200,
      ),
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('TIN TỨC'),
                GestureDetector(
                  onTap: () {},
                  child: Text('Xem thêm'),
                )
              ],
            ),
          ),
          Container(
            child: CarouselCommon(
              paddingPageview: 5,
              isShowPointSlide: true,
              isAutoScroll: true,
              width: _screenSize.width - 40,
              height: 210,
              viewportFraction: 1,
              pointPadingHeight: 220,
              listWidget: [
                _buildBlogCard(),
                _buildBlogCard(),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _buildBlogCard() {
    return Column(
      children: [
        Container(
          height: 120,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: Colors.amber,
            image: DecorationImage(
              image: AssetImage(AppImages.imgBlog1),
              fit: BoxFit.cover,
            ),
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Container(
          height: 76,
          child: Text(
              'Giá trị dinh dưỡng tuyệt vời của trái cây đối với sức khỏe\nĂn trái cây một giờ trước bữa ăn có tác dụng giảm béo và giúp tiêu hóa có hiệu quả nhất. Trong tr...',
              // style: TextStyle(fontSize: 1),
              maxLines: 4,
              overflow: TextOverflow.ellipsis),
        )
      ],
    );
  }

  Widget _buildPromotion() {
    return _buildGridviewContent(
        listWidgetBanner: [
          Container(
            height: 120,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: Colors.amber,
              image: DecorationImage(
                image: AssetImage(AppImages.imgBannerPromotion),
                fit: BoxFit.cover,
              ),
            ),
          ),
        ],
        title: 'CHƯƠNG TRÌNH KHUYẾN MẠI',
        functionShowMore: () {},
        listWidget: [
          GridviewCard(imageUrl: AppImages.imgPromotionEvent1, function: () {}),
          GridviewCard(imageUrl: AppImages.imgPromotionEvent2, function: () {}),
          GridviewCard(imageUrl: AppImages.imgPromotionEvent3, function: () {}),
          GridviewCard(imageUrl: AppImages.imgPromotionEvent4, function: () {}),
          GridviewCard(imageUrl: AppImages.imgPromotionEvent5, function: () {}),
          GridviewCard(imageUrl: AppImages.imgPromotionEvent4, function: () {}),
          GridviewCard(imageUrl: AppImages.imgPromotionEvent3, function: () {}),
        ],
        isShowMoreGrv: true,
        maxItemShowGrv: 6,
        functionShowMoreGrv: () {},
        disableShowMore: false);
  }

  Widget _buildGridviewContent(
      {required List<Widget> listWidgetBanner,
      required String title,
      required Function functionShowMore,
      required bool disableShowMore,
      required List<Widget> listWidget,
      required bool isShowMoreGrv,
      required int maxItemShowGrv,
      required Function functionShowMoreGrv}) {
    return Container(
      padding: EdgeInsets.only(top: 20, left: 20, right: 20),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: Colors.grey.shade200,
      ),
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(bottom: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  title,
                  style: TextStyle(fontSize: 16),
                ),
                !disableShowMore
                    ? GestureDetector(
                        onTap: functionShowMore(),
                        child: Text('Xem thêm'),
                      )
                    : Container()
              ],
            ),
          ),
          Container(
            child: CarouselCommon(
              listWidget: listWidgetBanner,
              isShowPointSlide: false,
              isAutoScroll: true,
              width: _screenSize.width,
              height: 130,
              viewportFraction: 1,
              pointPadingHeight: 10,
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Container(
            child: GridviewWidget(
              listWidget: listWidget,
              isShowMore: isShowMoreGrv,
              maxItemShow: maxItemShowGrv,
              functionShowmore: functionShowMoreGrv,
              childAspectRatio: 1.5,
              heightChild: (MediaQuery.of(context).size.width * 125 / 392),
              crossAxisSpacing: 10,
              mainAxisSpacing: 10,
              crossAxisCount: 2,
            ),
          )
        ],
      ),
    );
  }

  Widget _buildSpecialFood() {
    return _buildGridviewContent(
        listWidgetBanner: [
          Container(
            height: 120,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: Colors.amber,
              image: DecorationImage(
                image: AssetImage(AppImages.imgBannerHaLong),
                fit: BoxFit.fill,
              ),
            ),
          ),
        ],
        title: 'ĐẶC SẢN HẠ LONG',
        functionShowMore: () {},
        listWidget: [
          GridviewCard(imageUrl: AppImages.imgproductHaLong1, function: () {}),
          GridviewCard(imageUrl: AppImages.imgproductHaLong2, function: () {}),
          GridviewCard(imageUrl: AppImages.imgproductHaLong3, function: () {}),
          GridviewCard(imageUrl: AppImages.imgproductHaLong4, function: () {}),
        ],
        isShowMoreGrv: false,
        maxItemShowGrv: 4,
        functionShowMoreGrv: () {},
        disableShowMore: true);
  }

  Widget _buildRecommendFood() {
    return _buildGridviewContent(
        listWidgetBanner: [
          Container(
            height: 120,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: Colors.amber,
              image: DecorationImage(
                image: AssetImage(AppImages.imgBannerChaNgon),
                fit: BoxFit.cover,
              ),
            ),
          ),
        ],
        title: 'CHẢ NGON BA MIỀN',
        functionShowMore: () {},
        listWidget: [
          GridviewCard(imageUrl: AppImages.imgproductChaNgon1, function: () {}),
          GridviewCard(imageUrl: AppImages.imgproductChaNgon2, function: () {}),
          GridviewCard(imageUrl: AppImages.imgproductChaNgon3, function: () {}),
          GridviewCard(imageUrl: AppImages.imgproductChaNgon4, function: () {}),
        ],
        isShowMoreGrv: false,
        maxItemShowGrv: 4,
        functionShowMoreGrv: () {},
        disableShowMore: true);
  }

  _buildComboProduct() {
    List<Widget> listWidget = [
      ProductCard(
          isHot: true,
          isNew: true,
          discount: 75,
          isLove: false,
          name: 'Tôm nõn hấp Hạ Long',
          price: 150000,
          profit: 50000,
          inventoryNum: null,
          isOrder: false),
      ProductCard(
          isHot: true,
          isNew: false,
          discount: 60,
          isLove: true,
          name: 'Cá cơm rim lạc vừng',
          price: 150000,
          profit: 50000,
          inventoryNum: 5,
          isOrder: false),
      ProductCard(
          isHot: false,
          isNew: true,
          discount: 70,
          isLove: false,
          name: 'Cá cơm rim lạc vừng',
          price: 150000,
          profit: 50000,
          inventoryNum: 5,
          isOrder: false),
    ];
    return Container(
      padding: EdgeInsets.symmetric(vertical: 0, horizontal: 20),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: Colors.grey.shade200,
      ),
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('COMBO SẢN PHẨM'),
                GestureDetector(
                  onTap: () {
                    _viewModel.onTapShowMoreBetSell(
                        listWidget: listWidget, title: 'Sản Phẩm Order');
                  },
                  child: Text('Xem thêm'),
                )
              ],
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              CarouselCommon(
                listWidget: listWidget,
                isShowPointSlide: true,
                isAutoScroll: true,
                width: _screenSize.width - 40,
                height: 255,
                viewportFraction: 1 / 2,
                pointPadingHeight: 265,
                paddingPageview: 5,
              ),
            ],
          )
        ],
      ),
    );
  }

  Widget _buildRecommendProduct(BuildContext context) {
    final List<String> entries = <String>[
      'Sản phẩm gợi ý',
      'Sản phẩm mới',
      'Mới về',
      'Hot',
      'Chuẩn'
    ];
    List<Widget> listWidget = [
      ProductCard(
          isHot: true,
          isNew: true,
          discount: 75,
          isLove: false,
          name: 'Tôm nõn hấp Hạ Long',
          price: 150000,
          profit: 50000,
          inventoryNum: null,
          isOrder: false),
      ProductCard(
          isHot: true,
          isNew: false,
          discount: 60,
          isLove: true,
          name: 'Cá cơm rim lạc vừng',
          price: 150000,
          profit: 50000,
          inventoryNum: 5,
          isOrder: false),
      ProductCard(
          isHot: false,
          isNew: true,
          discount: 70,
          isLove: false,
          name: 'Cá cơm rim lạc vừng',
          price: 150000,
          profit: 50000,
          inventoryNum: 5,
          isOrder: false),
      ProductCard(
          isHot: true,
          isNew: true,
          discount: 75,
          isLove: false,
          name: 'Tôm nõn hấp Hạ Long',
          price: 150000,
          profit: 50000,
          inventoryNum: null,
          isOrder: false),
      ProductCard(
          isHot: true,
          isNew: false,
          discount: 60,
          isLove: true,
          name: 'Cá cơm rim lạc vừng',
          price: 150000,
          profit: 50000,
          inventoryNum: 5,
          isOrder: false),
      ProductCard(
          isHot: false,
          isNew: true,
          discount: 70,
          isLove: false,
          name: 'Cá cơm rim lạc vừng',
          price: 150000,
          profit: 50000,
          inventoryNum: 5,
          isOrder: false),
      ProductCard(
          isHot: true,
          isNew: false,
          discount: 60,
          isLove: true,
          name: 'Cá cơm rim lạc vừng',
          price: 150000,
          profit: 50000,
          inventoryNum: 5,
          isOrder: false),
      ProductCard(
          isHot: false,
          isNew: true,
          discount: 70,
          isLove: false,
          name: 'Cá cơm rim lạc vừng',
          price: 150000,
          profit: 50000,
          inventoryNum: 5,
          isOrder: false)
    ];

    return Container(
      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 25),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: Colors.grey.shade200,
      ),
      child: Column(
        children: [
          Container(
            height: 50,
            width: _screenSize.width,
            child: ListView.builder(
                scrollDirection: Axis.horizontal,
                padding: const EdgeInsets.all(8),
                itemCount: entries.length,
                itemBuilder: (BuildContext context, int index) {
                  return GestureDetector(
                    child: Container(
                      height: 30,
                      margin: EdgeInsets.only(right: 30),
                      child: Text(entries[index]),
                    ),
                  );
                }),
          ),
          Container(
            child: GridviewWidget(
              listWidget: listWidget,
              isShowMore: false,
              maxItemShow: 1000,
              functionShowmore: () {},
              childAspectRatio:
                  (MediaQuery.of(context).size.width * 0.65 / 392) - 0.025,
              heightChild: 280,
              crossAxisSpacing: 10,
              mainAxisSpacing: 10,
              crossAxisCount: 2,
            ),
          )
        ],
      ),
    );
  }

  Widget sideDrawer(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          color: Colors.white,
          width: _screenSize.width - 40,
          child: Drawer(
            child: Column(
              children: <Widget>[
                Container(
                  height: 100,
                  child: DrawerHeader(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Expanded(child: Container()),
                        Text(
                          'Danh mục sản phẩm',
                          textAlign: TextAlign.left,
                          style: TextStyle(color: Colors.black, fontSize: 16),
                        ),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  child: Container(
                    // color: Colors.red,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          width: 70,
                          child: SingleChildScrollView(
                            child: Column(
                              children: [
                                GestureDetector(
                                  onTap: () {},
                                  child: Container(
                                    margin: EdgeInsets.only(bottom: 2),
                                    padding: EdgeInsets.all(5),
                                    alignment: Alignment.center,
                                    color: Colors.deepOrange,
                                    height: 80,
                                    width: 70,
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Container(
                                          height: 40,
                                          width: 40,
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            image: DecorationImage(
                                              image: AssetImage(
                                                  AppImages.iconDrwRecommend),
                                            ),
                                          ),
                                        ),
                                        Flexible(
                                          child: Text(
                                            'Gợi ý cho bạn',
                                            overflow: TextOverflow.ellipsis,
                                            softWrap: true,
                                            maxLines: 2,
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                                fontSize: 12,
                                                color: Colors.white),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                drawerCard(() {}, AppImages.imgBannerHaLong,
                                    'Đặc sản Hạ Long', Colors.grey.shade200),
                                drawerCard(() {}, AppImages.imgBannerChaNgon,
                                    'Chả ngon 3 miền', Colors.grey.shade200),
                                drawerCard(() {}, AppImages.imgBannerHaLong,
                                    'Đặc sản Hạ Long', Colors.grey.shade200),
                                drawerCard(() {}, AppImages.imgBannerChaNgon,
                                    'Chả ngon 3 miền', Colors.grey.shade200),
                                drawerCard(() {}, AppImages.imgBannerHaLong,
                                    'Đặc sản Hạ Long', Colors.grey.shade200),
                                drawerCard(() {}, AppImages.imgBannerChaNgon,
                                    'Chả ngon 3 miền', Colors.grey.shade200),
                                drawerCard(() {}, AppImages.imgBannerHaLong,
                                    'Đặc sản Hạ Long', Colors.grey.shade200),
                                drawerCard(() {}, AppImages.imgBannerChaNgon,
                                    'Chả ngon 3 miền', Colors.grey.shade200),
                              ],
                            ),
                          ),
                        ),
                        Expanded(
                          child: Container(
                            child: Column(
                              children: [
                                Container(
                                  color: Colors.blue,
                                  child: Column(
                                    children: [
                                      Container(
                                          height: 50,
                                          color: Colors.white,
                                          child: GestureDetector(
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: [
                                                Text('Gợi ý cho bạn'),
                                                Container(
                                                    transform:
                                                        Matrix4.rotationY(180),
                                                    child: Icon(
                                                        Icons.arrow_back_ios))
                                              ],
                                            ),
                                          ))
                                    ],
                                  ),
                                ),
                                Expanded(
                                  child: Container(
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 0),
                                    child: SingleChildScrollView(
                                      child: GridviewWidget(
                                        listWidget: [
                                          drawerCard(
                                              () {},
                                              AppImages.imgDrawerCard1,
                                              'Sản phẩm bán chạy',
                                              Colors.white),
                                          drawerCard(
                                              () {},
                                              AppImages.imgDrawerCard2,
                                              'Sản phẩm order',
                                              Colors.white),
                                          drawerCard(
                                              () {},
                                              AppImages.imgDrawerCard3,
                                              'Combo sản phẩm',
                                              Colors.white),
                                          drawerCard(
                                              () {},
                                              AppImages.imgDrawerCard4,
                                              'Sản phẩm gợi ý',
                                              Colors.white),
                                          drawerCard(
                                              () {},
                                              AppImages.imgDrawerCard1,
                                              'Sản phẩm bán chạy',
                                              Colors.white),
                                          drawerCard(
                                              () {},
                                              AppImages.imgDrawerCard2,
                                              'Sản phẩm order',
                                              Colors.white),
                                          drawerCard(
                                              () {},
                                              AppImages.imgDrawerCard3,
                                              'Combo sản phẩm',
                                              Colors.white),
                                          drawerCard(
                                              () {},
                                              AppImages.imgDrawerCard4,
                                              'Sản phẩm gợi ý',
                                              Colors.white),
                                        ],
                                        isShowMore: false,
                                        maxItemShow: 100,
                                        functionShowmore: () {},
                                        childAspectRatio:
                                            (MediaQuery.of(context).size.width *
                                                    0.87 /
                                                    392) -
                                                0.025,
                                        //0.85,
                                        heightChild: 85,
                                        crossAxisSpacing: 0,
                                        mainAxisSpacing: 0,
                                        crossAxisCount: 3,
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        GestureDetector(
          onTap: () {
            _scaffoldKey.currentState!.openEndDrawer();
          },
          child: Container(
              alignment: Alignment.center,
              width: 30,
              height: 30,
              margin: EdgeInsets.only(top: 70),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  topRight: Radius.circular(5),
                  bottomRight: Radius.circular(5),
                ),
                color: Colors.deepOrange,
              ),
              child: Text(
                'X',
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 20,
                ),
                textAlign: TextAlign.center,
              )),
        )
      ],
    );
  } //#endregion

  Widget drawerCard(
      Function function, String imageUrl, String label, Color bgColor) {
    return GestureDetector(
      onTap: function(),
      child: Container(
        margin: EdgeInsets.only(bottom: 2),
        padding: EdgeInsets.all(5),
        alignment: Alignment.center,
        color: bgColor,
        height: 100,
        width: 70,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              height: 60,
              width: 70,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                image: DecorationImage(
                    image: AssetImage(imageUrl), fit: BoxFit.cover),
              ),
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 5),
              child: Text(
                label,
                overflow: TextOverflow.ellipsis,
                softWrap: true,
                maxLines: 2,
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 12),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
