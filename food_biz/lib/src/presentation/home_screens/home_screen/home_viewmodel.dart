import 'package:flutter/material.dart';
import 'package:flutter_app/src/resource/model/model.dart';
import 'package:flutter_app/src/resource/repo/repo.dart';

import '../../presentation.dart';
import 'package:get/get.dart';

class HomeViewModel extends BaseViewModel {
  late List<BannerModel> listBanner;
  init() async {
    getListBaner();
  }

  onTapNotify() {
    Get.toNamed(Routers.notify);
  }

  onTapShowMoreBetSell(
      {required List<Widget> listWidget, required String title}) {
    Get.toNamed(Routers.show_more,
        arguments: {'listItem': listWidget, 'title': title});
  }

  getListBaner() async {
    try {
      await BannerRepository().getListBanner().then((value) {
        if (value.isSuccess) {
          listBanner = value.data!;
          notifyListeners();

          print('${listBanner.map((e) => print(e.toJson()))}');
          // if (value.message == 'OK') {
          //   listBanner = value.data!;
          // } else {
          //   print(value.message);
          // }
        }
      });
    } catch (e) {
      print("Exception: $e");
    }
  }
}
