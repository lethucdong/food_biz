import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/configs.dart';

import '../../presentation.dart';

class ShowMoreScreen extends StatefulWidget {
  late List<Widget> listItem;
  late String title;
  ShowMoreScreen({required this.listItem, required this.title});
  @override
  _ShowMoreScreenState createState() => _ShowMoreScreenState();
}

class _ShowMoreScreenState extends State<ShowMoreScreen> with ResponsiveWidget {
  late ShowMoreViewModel _viewModel;
  @override
  Widget build(BuildContext context) {
    return BaseWidget<ShowMoreViewModel>(
        viewModel: ShowMoreViewModel(),
        onViewModelReady: (viewModel) {
          _viewModel = viewModel..init();
        },
        builder: (context, viewModel, appbar) {
          return Scaffold(
              appBar: _buildAppBar(),
              backgroundColor: Colors.grey.shade300,
              body: buildUi(context));
        });
  }

  @override
  Widget buildDesktop(BuildContext context) {
    // TODO: implement buildDesktop
    return _buildUi(context);
  }

  @override
  Widget buildMobile(BuildContext context) {
    // TODO: implement buildMobile
    return _buildUi(context);
  }

  @override
  Widget buildTablet(BuildContext context) {
    return _buildUi(context);
  }

  Widget _buildUi(BuildContext) {
    return Container(
      padding: EdgeInsets.only(top: 20, right: 20, left: 20),
      child: SingleChildScrollView(
        child: GridviewWidget(
          listWidget: widget.listItem,
          isShowMore: false,
          maxItemShow: 1000,
          functionShowmore: () {},
          childAspectRatio:
              (MediaQuery.of(context).size.width * 0.65 / 392) - 0.02,
          heightChild: 280,
          crossAxisSpacing: 10,
          mainAxisSpacing: 10,
          crossAxisCount: 2,
        ),
      ),
    );
  }

  PreferredSizeWidget _buildAppBar() {
    return AppBar(
      elevation: 1,
      backgroundColor: Colors.white,
      shadowColor: Colors.grey,
      leading: IconButton(
        padding: EdgeInsets.only(top: 30),
        icon: Image.asset(AppImages.iconSlideMenu),
        color: Colors.black,
        onPressed: () {
          // Do something.
        },
      ),
      actions: [
        _buildIconAppbar(
            Image.asset(
              AppImages.iconSearch,
              color: Colors.black,
              scale: 2,
            ),
            0,
            () {}),
        _buildIconAppbar(Image.asset(AppImages.iconCart), 5, () {}),
      ],
      centerTitle: true,
      title: Padding(
        padding: const EdgeInsets.only(top: 20),
        child: Text(
          // 'Sản phẩm bán chạy',
          widget.title,
          style: TextStyle(color: Colors.black, fontSize: 20),
        ),
      ),
      toolbarHeight: 80,
    );
  }

//#region buildIconAppbar
  _buildIconAppbar(Widget icon, int quantity, Function function) {
    return GestureDetector(
      onTap: function(),
      child: Container(
        padding: EdgeInsets.only(top: 30),
        child: Stack(
          children: [
            IconButton(
              icon: icon,
              color: Colors.black,
              onPressed: () {
                // Do something.
              },
            ),
            Positioned(
                top: 5,
                right: 5,
                width: 17,
                height: 17,
                child: quantity > 0
                    ? Container(
                        decoration: BoxDecoration(
                            shape: BoxShape.circle, color: Colors.red),
                        child: Text(
                          '$quantity' + '+',
                          style: TextStyle(fontSize: 12),
                          textAlign: TextAlign.center,
                        ),
                      )
                    : Container())
          ],
        ),
      ),
    );
  }
//#endregion
}
