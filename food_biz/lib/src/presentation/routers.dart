import 'package:flutter/material.dart';
import 'package:flutter_app/src/presentation/presentation.dart';

import 'navigation/navigation_screen.dart';

class Routers {
  static const String navigation = "/navigation";
  static const String login = "/login";
  static const String confirm_login = "/confirm_login";
  static const String register = "/register";
  static const String home = "/home";
  static const String product_detail = "/product_detail";
  static const String cart = '/cart';
  static const String create_order = 'create_order';
  static const String voucher = 'voucher';
  static const String voucher_detail = 'voucher_detail';
  static const String address = 'address';
  static const String create_address = 'create_address';
  static const String edit_address = 'edit_address';
  static const String delivery = 'delivery';
  static const String select_time = 'select_time';
  static const String payment_method = 'payment_method';
  static const String payment_detail = 'payment_detail';
  static const String promotion_reward = 'promotion_reward';
  static const String notify_detail = 'notify_detail';
  static const String show_more = 'show_more';
  static const String notify = 'notify';
  static const String profile = 'profile';
  static const String edit_profile = 'edit_profile';
  static const String credit = 'credit';
  static const String add_credit = 'add_credit';
  static const String product_pr = 'product_pr';
  static const String product_review = 'product_review';
  static const String create_review = 'create_review';
  static const String setting = 'setting';
  static const String support = 'support';
  static const String policy = 'policy';
  static const String feedback = 'feedback';
  static const String community_standard = 'community_standard';
  static const String rank_account = 'rank_account';
  static const String rank_detail = 'rank_detail';
  static const String reconciliation = 'reconciliation';
  static const String withdrawal = 'withdrawal';
  static const String withdrawal_success = 'withdrawal_success';
  static const String withdrawal_history = 'withdrawal_history';
  static const String fobi_coin = 'fobi_coin';
  static const String statistical = 'statistical';
  static const String best_sell = 'best_sell';
  static const String sale = 'sale';
  static const String debt = 'debt';
  static const String customers = 'customers';
  static const String voucher_storage = 'voucher_storage';
  static const String voucher_history = 'voucher_history';
  static const String download_image = 'download_image';
  static const String order_detail = 'order_detail';

  static Route<dynamic> generateRoute(RouteSettings settings) {
    var arguments = settings.arguments;
    switch (settings.name) {
      case navigation:
        return animRoute(NavigationScreen(), name: navigation);
      case login:
        return animRoute(LoginScreen(), name: login);
      case confirm_login:
        return animRoute(ConfrimLoginScreen(), name: confirm_login);
      case register:
        return animRoute(RegisterScreen(), name: register);
      case home:
        return animRoute(HomeScreen(), name: home);
      case product_detail:
        return animRoute(ProductDetailScreen(), name: product_detail);
      case cart:
        return animRoute(CartScreen(), name: cart);
      case create_order:
        return animRoute(CreateOrderScreen(), name: create_order);
      case voucher:
        return animRoute(VoucherScreen(), name: voucher);
      case voucher_detail:
        return animRoute(VoucherDetailScreen(), name: voucher_detail);
      case address:
        return animRoute(AddressScreen(), name: address);
      case create_address:
        return animRoute(CreateAddressScreen(), name: create_address);
      case edit_address:
        return animRoute(
            CreateAddressScreen(
              isEdit: true,
            ),
            name: edit_address);
      case delivery:
        return animRoute(DeliveryScreen(), name: delivery);
      case select_time:
        return animRoute(SelectTimeScreen(), name: select_time);
      case payment_method:
        return animRoute(PaymentMethodScreen(), name: payment_method);
      case payment_detail:
        return animRoute(DetailPaymentScreen(), name: payment_detail);
      case promotion_reward:
        return animRoute(PromotionRewardScreen(), name: promotion_reward);
      case notify:
        return animRoute(NotifyScreen(), name: notify);
      case notify_detail:
        return animRoute(NotifyDetailScreen(), name: notify_detail);
      case profile:
        return animRoute(ProfileScreen(), name: profile);
      case edit_profile:
        return animRoute(EditProfileScreen(), name: edit_profile);
      case credit:
        return animRoute(CreditScreen(), name: credit);
      case add_credit:
        return animRoute(AddCreditScreen(), name: add_credit);
      case product_pr:
        return animRoute(ProductPrScreen(title: (arguments as Map)['title']),
            name: product_pr);
      case product_review:
        return animRoute(ProductReviewScreen(), name: product_review);
      case create_review:
        return animRoute(CreateReviewScreen(), name: create_review);
      case setting:
        return animRoute(SettingScreen(), name: setting);
      case support:
        return animRoute(SupportScreen(), name: support);
      case policy:
        return animRoute(PolicyScreen(), name: policy);
      case feedback:
        return animRoute(FeedbackScreen(), name: feedback);
      case community_standard:
        return animRoute(CommunityStandardScreen(), name: community_standard);
      case rank_account:
        return animRoute(RankAccountScreen(), name: rank_account);
      case reconciliation:
        return animRoute(ReconciliationScreen(), name: reconciliation);
      case withdrawal:
        return animRoute(WithdrawalScreen(), name: withdrawal);
      case withdrawal_success:
        return animRoute(WithdrawalSuccessScreen(), name: withdrawal_success);
      case withdrawal_history:
        return animRoute(WithdrawalHistoryScreen(), name: withdrawal_history);
      case fobi_coin:
        return animRoute(FobiCoinScreen(), name: fobi_coin);
      case statistical:
        return animRoute(StatisticalScreen(), name: statistical);
      case customers:
        return animRoute(CustomersScreen(), name: customers);
      case sale:
        return animRoute(SaleScreen(), name: sale);
      case debt:
        return animRoute(DebtScreen(), name: debt);
      case voucher_storage:
        return animRoute(VoucherStorageScreen(), name: voucher_storage);
      case voucher_history:
        return animRoute(VoucherHistoryScreen(), name: voucher_history);
      case download_image:
        return animRoute(DownloadImageScreen(), name: download_image);
      case order_detail:
        return animRoute(
            OrderDetailScreen(
              typeOrder: (arguments! as Map)['typeOrder'],
            ),
            name: order_detail);
      case best_sell:
        return animRoute(
            BestSellScreen(
              listItem: (arguments! as Map)['listItem'],
            ),
            name: best_sell);
      case rank_detail:
        return animRoute(
            RankDetailScreen(
              rankItemWidget: (arguments! as Map)['rankItemWidget'],
              rankBonusDetailWidget:
                  (arguments as Map)['rankBonusDetailWidget'],
              rankName: (arguments as Map)['rankName'],
            ),
            name: rank_detail);
      case show_more:
        return animRoute(
            ShowMoreScreen(
              listItem: (arguments! as Map)['listItem'],
              title: (arguments as Map)['title'],
            ),
            name: show_more);
      default:
        return animRoute(
            Container(
                child: Center(
                    child: Text('No route defined for ${settings.name}'))),
            name: "/error");
    }
  }

  static Route animRoute(Widget page,
      {Offset? beginOffset, required String name, Object? arguments}) {
    return PageRouteBuilder(
      settings: RouteSettings(name: name, arguments: arguments),
      pageBuilder: (context, animation, secondaryAnimation) => page,
      transitionsBuilder: (context, animation, secondaryAnimation, child) {
        var begin = beginOffset ?? Offset(0.0, 0.0);
        var end = Offset.zero;
        var curve = Curves.ease;
        var tween = Tween(begin: begin, end: end).chain(
          CurveTween(curve: curve),
        );

        return SlideTransition(
          position: animation.drive(tween),
          child: child,
        );
      },
    );
  }

  static Offset _center = Offset(0.0, 0.0);
  static Offset _top = Offset(0.0, 1.0);
  static Offset _bottom = Offset(0.0, -1.0);
  static Offset _left = Offset(-1.0, 0.0);
  static Offset _right = Offset(1.0, 0.0);
}
