import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/configs.dart';

import '../presentation.dart';

class EndowScreen extends StatefulWidget {
  @override
  _EndowScreenState createState() => _EndowScreenState();
}

class _EndowScreenState extends State<EndowScreen> with ResponsiveWidget {
  late EndowViewModel _viewModel;
  @override
  Widget build(BuildContext context) {
    return BaseWidget<EndowViewModel>(
        viewModel: EndowViewModel(),
        onViewModelReady: (viewModel) {
          _viewModel = viewModel..init();
        },
        builder: (context, viewModel, appbar) {
          return Scaffold(
              appBar: _buildAppBar(),
              backgroundColor: Colors.grey.shade300,
              body: buildUi(context));
        });
  }

  @override
  Widget buildDesktop(BuildContext context) {
    return _buildUi(context);
  }

  @override
  Widget buildMobile(BuildContext context) {
    return _buildUi(context);
  }

  @override
  Widget buildTablet(BuildContext context) {
    return _buildUi(context);
  }

  Widget _buildUi(BuildContext context) {
    List<Widget> listWidget = [
      GridviewCard(imageUrl: AppImages.imgPromotionEvent1, function: () {}),
      GridviewCard(imageUrl: AppImages.imgPromotionEvent2, function: () {}),
      GridviewCard(imageUrl: AppImages.imgPromotionEvent3, function: () {}),
      GridviewCard(imageUrl: AppImages.imgPromotionEvent4, function: () {}),
      GridviewCard(imageUrl: AppImages.imgPromotionEvent5, function: () {}),
      GridviewCard(imageUrl: AppImages.imgPromotionEvent4, function: () {}),
      GridviewCard(imageUrl: AppImages.imgPromotionEvent3, function: () {}),
      GridviewCard(imageUrl: AppImages.imgPromotionEvent1, function: () {}),
      GridviewCard(imageUrl: AppImages.imgPromotionEvent2, function: () {}),
      GridviewCard(imageUrl: AppImages.imgPromotionEvent3, function: () {}),
      GridviewCard(imageUrl: AppImages.imgPromotionEvent4, function: () {}),
      GridviewCard(imageUrl: AppImages.imgPromotionEvent5, function: () {}),
      GridviewCard(imageUrl: AppImages.imgPromotionEvent4, function: () {}),
      GridviewCard(imageUrl: AppImages.imgPromotionEvent3, function: () {}),
    ];

    return Container(
      padding: EdgeInsets.only(top: 20, right: 20, left: 20),
      child: SingleChildScrollView(
        child: GridviewWidget(
          listWidget: listWidget,
          isShowMore: false,
          maxItemShow: 1000,
          functionShowmore: () {},
          childAspectRatio: 1.5,
          heightChild: 125,
          crossAxisSpacing: 10,
          mainAxisSpacing: 10,
          crossAxisCount: 2,
        ),
      ),
    );
  }

  PreferredSizeWidget _buildAppBar() {
    return AppBar(
      elevation: 1,
      backgroundColor: Colors.white,
      shadowColor: Colors.grey,
      centerTitle: true,
      title: Text(
        'Ưu đãi',
        style: TextStyle(color: Colors.black, fontSize: 20),
      ),
      toolbarHeight: 80,
    );
  }
}
