import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../configs/configs.dart';
import '../presentation.dart';
import 'package:get/get.dart';

class NavigationScreen extends StatefulWidget {
  @override
  _NavigationScreenState createState() => _NavigationScreenState();
}

class _NavigationScreenState extends State<NavigationScreen>
    with ResponsiveWidget {
  late NavigationViewModel _viewModel;
  var _scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return BaseWidget<NavigationViewModel>(
        viewModel: NavigationViewModel(),
        onViewModelReady: (viewModel) {
          _viewModel = viewModel..init();
        },
        builder: (context, viewModel, appbar) {
          return Scaffold(
            key: _scaffoldKey,
            // appBar:
            //     _viewModel.curtentPageIndex == 4 ? null : _buildAppBar(context),
            body: buildUi(context),
            drawer: sideDrawer(context),
          );
        });
  }

  @override
  Widget buildDesktop(BuildContext context) {
    return _buildBody(context);
  }

  @override
  Widget buildMobile(BuildContext context) {
    return _buildBody(context);
  }

  @override
  Widget buildTablet(BuildContext context) {
    return _buildBody(context);
  }

  Widget _buildBody(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      child: Column(
        children: [
          Expanded(
            child: PageView(
              controller: _viewModel.pageController,
              scrollDirection: Axis.horizontal,
              physics: NeverScrollableScrollPhysics(),
              children: [
                HomeScreen(),
                FavoritsProductScreen(),
                OrderScreen(),
                EndowScreen(),
                UserScreen(),
              ],
              onPageChanged: (value) {
                setState(() {});
              },
            ),
          ),
          _buildNav(context),
        ],
      ),
    );
  }

  PreferredSizeWidget _buildAppBar(BuildContext context) {
    return AppBar(
      elevation: 1,
      backgroundColor: Colors.white,
      shadowColor: Colors.grey,
      leading: IconButton(
        padding: EdgeInsets.only(top: 30),
        icon: Image.asset(AppImages.iconSlideMenu),
        color: Colors.black,
        onPressed: () {
          // Scaffold.of(context).openDrawer();
          _scaffoldKey.currentState!.openDrawer();
          // Do something.
        },
      ),
      actions: [
        _buildIconAppbar(Image.asset(AppImages.iconBell), 5, () {}),
        _buildIconAppbar(Image.asset(AppImages.iconCart), 0, () {}),
      ],
      title: Padding(
        padding: const EdgeInsets.only(top: 30),
        child: Container(
          // color: Colors.red,
          decoration: BoxDecoration(
            color: Colors.grey.shade300,
            border: Border.all(color: Colors.grey.shade300, width: 1),
            borderRadius: BorderRadius.circular(12),
          ),
          height: 35,
          child: TextField(
            decoration: InputDecoration(
              isDense: true,
              hintText: 'Tìm kiếm',
              // contentPadding: EdgeInsets.only(bottom: 0),
              prefixIcon: Icon(Icons.search_outlined),
              border: InputBorder.none,
            ),
          ),
        ),
      ),
      toolbarHeight: 80,
    );
  }

//#region buildIconAppbar
  _buildIconAppbar(Widget icon, int quantity, Function function) {
    return GestureDetector(
      onTap: function(),
      child: Container(
        padding: EdgeInsets.only(top: 30),
        child: Stack(
          children: [
            IconButton(
              icon: icon,
              color: Colors.black,
              onPressed: () {
                // Do something.
              },
            ),
            Positioned(
                top: 5,
                right: 5,
                width: 17,
                height: 17,
                child: quantity > 0
                    ? Container(
                        decoration: BoxDecoration(
                            shape: BoxShape.circle, color: Colors.red),
                        child: Text(
                          '$quantity' + '+',
                          style: TextStyle(fontSize: 12),
                          textAlign: TextAlign.center,
                        ),
                      )
                    : Container())
          ],
        ),
      ),
    );
  }

  Widget _buildNav(BuildContext context) {
    return Container(
      height: 60,
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.grey,
            spreadRadius: 5,
            blurRadius: 7,
            offset: Offset(0, 3), // changes position of shadow
          ),
        ],
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          buildNavItem(
              icon: Image.asset(
                AppImages.iconNavHome,
                scale: 1.5,
                color:
                    context.watch<NavigationViewModel>().curtentPageIndex == 0
                        ? Colors.deepOrange
                        : null,
              ),
              label: Image.asset(
                AppImages.imgLogo,
                scale: 5,
              ),
              pageIndex: 0,
              onTap: () {
                Provider.of<NavigationViewModel>(context, listen: false)
                    .onTapNav(0);
              }),
          buildNavItem(
              icon: Image.asset(
                AppImages.iconNavLike,
                scale: 1.5,
                color:
                    context.watch<NavigationViewModel>().curtentPageIndex == 1
                        ? Colors.deepOrange
                        : null,
              ),
              label: Text(
                'Yêu thích',
                style: TextStyle(
                  color:
                      context.watch<NavigationViewModel>().curtentPageIndex == 1
                          ? Colors.deepOrange
                          : null,
                ),
              ),
              pageIndex: 1,
              onTap: () {
                Provider.of<NavigationViewModel>(context, listen: false)
                    .onTapNav(1);
              }),
          buildNavItem(
              icon: Image.asset(
                AppImages.iconNavOrder,
                scale: 1.5,
                color:
                    context.watch<NavigationViewModel>().curtentPageIndex == 2
                        ? Colors.deepOrange
                        : null,
              ),
              label: Text(
                'Đơn hàng',
                style: TextStyle(
                  color:
                      context.watch<NavigationViewModel>().curtentPageIndex == 2
                          ? Colors.deepOrange
                          : null,
                ),
              ),
              pageIndex: 2,
              onTap: () {
                Provider.of<NavigationViewModel>(context, listen: false)
                    .onTapNav(2);
              }),
          buildNavItem(
              icon: Image.asset(
                AppImages.iconNavCoupon,
                scale: 1.5,
                color:
                    context.watch<NavigationViewModel>().curtentPageIndex == 3
                        ? Colors.deepOrange
                        : null,
              ),
              label: Text(
                'Ưu đãi',
                style: TextStyle(
                  color:
                      context.watch<NavigationViewModel>().curtentPageIndex == 3
                          ? Colors.deepOrange
                          : null,
                ),
              ),
              pageIndex: 3,
              onTap: () {
                Provider.of<NavigationViewModel>(context, listen: false)
                    .onTapNav(3);
              }),
          buildNavItem(
              icon: Container(
                width: 25,
                height: 25,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  image: DecorationImage(
                    image: AssetImage(
                      AppImages.iconAvatar,
                    ),
                  ),
                  border: Border.all(
                      color: context
                                  .watch<NavigationViewModel>()
                                  .curtentPageIndex ==
                              4
                          ? Colors.deepOrange
                          : Colors.grey,
                      width: 1),
                ),
              ),
              label: Text(
                'Tài khoản',
                style: TextStyle(
                  color:
                      context.watch<NavigationViewModel>().curtentPageIndex == 4
                          ? Colors.deepOrange
                          : null,
                ),
              ),
              pageIndex: 4,
              onTap: () {
                Provider.of<NavigationViewModel>(context, listen: false)
                    .onTapNav(4);
              }),
        ],
      ),
    );
  }

  Widget buildNavItem(
      {required Widget icon,
      required Widget label,
      required int pageIndex,
      required Function onTap}) {
    return GestureDetector(
      onTap: () {
        onTap();
      },
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            width: 25,
            height: 25,
            child: icon,
          ),
          label
        ],
      ),
    );
  }

  Widget sideDrawer(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          color: Colors.white,
          width: MediaQuery.of(context).size.width - 40,
          child: Drawer(
            child: Column(
              children: <Widget>[
                Container(
                  height: 100,
                  child: DrawerHeader(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Expanded(child: Container()),
                        Text(
                          'Danh mục sản phẩm',
                          textAlign: TextAlign.left,
                          style: TextStyle(color: Colors.black, fontSize: 16),
                        ),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  child: Container(
                    // color: Colors.red,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          width: 70,
                          child: SingleChildScrollView(
                            child: Column(
                              children: [
                                GestureDetector(
                                  onTap: () {},
                                  child: Container(
                                    margin: EdgeInsets.only(bottom: 2),
                                    padding: EdgeInsets.all(5),
                                    alignment: Alignment.center,
                                    color: Colors.deepOrange,
                                    height: 80,
                                    width: 70,
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Container(
                                          height: 40,
                                          width: 40,
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            image: DecorationImage(
                                              image: AssetImage(
                                                  AppImages.iconDrwRecommend),
                                            ),
                                          ),
                                        ),
                                        Flexible(
                                          child: Text(
                                            'Gợi ý cho bạn',
                                            overflow: TextOverflow.ellipsis,
                                            softWrap: true,
                                            maxLines: 2,
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                                fontSize: 12,
                                                color: Colors.white),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                drawerCard(() {}, AppImages.imgBannerHaLong,
                                    'Đặc sản Hạ Long', Colors.grey.shade200),
                                drawerCard(() {}, AppImages.imgBannerChaNgon,
                                    'Chả ngon 3 miền', Colors.grey.shade200),
                                drawerCard(() {}, AppImages.imgBannerHaLong,
                                    'Đặc sản Hạ Long', Colors.grey.shade200),
                                drawerCard(() {}, AppImages.imgBannerChaNgon,
                                    'Chả ngon 3 miền', Colors.grey.shade200),
                                drawerCard(() {}, AppImages.imgBannerHaLong,
                                    'Đặc sản Hạ Long', Colors.grey.shade200),
                                drawerCard(() {}, AppImages.imgBannerChaNgon,
                                    'Chả ngon 3 miền', Colors.grey.shade200),
                                drawerCard(() {}, AppImages.imgBannerHaLong,
                                    'Đặc sản Hạ Long', Colors.grey.shade200),
                                drawerCard(() {}, AppImages.imgBannerChaNgon,
                                    'Chả ngon 3 miền', Colors.grey.shade200),
                              ],
                            ),
                          ),
                        ),
                        Expanded(
                          child: Container(
                            child: Column(
                              children: [
                                Container(
                                  color: Colors.blue,
                                  child: Column(
                                    children: [
                                      Container(
                                          height: 50,
                                          color: Colors.white,
                                          child: GestureDetector(
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: [
                                                Text('Gợi ý cho bạn'),
                                                Container(
                                                    transform:
                                                        Matrix4.rotationY(180),
                                                    child: Icon(
                                                        Icons.arrow_back_ios))
                                              ],
                                            ),
                                          ))
                                    ],
                                  ),
                                ),
                                Expanded(
                                  child: Container(
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 0),
                                    child: SingleChildScrollView(
                                      child: GridviewWidget(
                                        listWidget: [
                                          drawerCard(
                                              () {},
                                              AppImages.imgDrawerCard1,
                                              'Sản phẩm bán chạy',
                                              Colors.white),
                                          drawerCard(
                                              () {},
                                              AppImages.imgDrawerCard2,
                                              'Sản phẩm order',
                                              Colors.white),
                                          drawerCard(
                                              () {},
                                              AppImages.imgDrawerCard3,
                                              'Combo sản phẩm',
                                              Colors.white),
                                          drawerCard(
                                              () {},
                                              AppImages.imgDrawerCard4,
                                              'Sản phẩm gợi ý',
                                              Colors.white),
                                          drawerCard(
                                              () {},
                                              AppImages.imgDrawerCard1,
                                              'Sản phẩm bán chạy',
                                              Colors.white),
                                          drawerCard(
                                              () {},
                                              AppImages.imgDrawerCard2,
                                              'Sản phẩm order',
                                              Colors.white),
                                          drawerCard(
                                              () {},
                                              AppImages.imgDrawerCard3,
                                              'Combo sản phẩm',
                                              Colors.white),
                                          drawerCard(
                                              () {},
                                              AppImages.imgDrawerCard4,
                                              'Sản phẩm gợi ý',
                                              Colors.white),
                                        ],
                                        isShowMore: false,
                                        maxItemShow: 100,
                                        functionShowmore: () {},
                                        childAspectRatio: 0.1,
                                        heightChild: 100,
                                        crossAxisSpacing: 0,
                                        mainAxisSpacing: 0,
                                        crossAxisCount: 3,
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        GestureDetector(
          onTap: () {
            _scaffoldKey.currentState!.openEndDrawer();
          },
          child: Container(
              alignment: Alignment.center,
              width: 30,
              height: 30,
              margin: EdgeInsets.only(top: 70),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  topRight: Radius.circular(5),
                  bottomRight: Radius.circular(5),
                ),
                color: Colors.deepOrange,
              ),
              child: Text(
                'X',
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 20,
                ),
                textAlign: TextAlign.center,
              )),
        )
      ],
    );
  } //#endregion

  Widget drawerCard(
      Function function, String imageUrl, String label, Color bgColor) {
    return GestureDetector(
      onTap: function(),
      child: Container(
        margin: EdgeInsets.only(bottom: 2),
        padding: EdgeInsets.all(5),
        alignment: Alignment.center,
        color: bgColor,
        height: 100,
        width: 70,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              height: 60,
              width: 70,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                image: DecorationImage(
                    image: AssetImage(imageUrl), fit: BoxFit.cover),
              ),
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 5),
              child: Text(
                label,
                overflow: TextOverflow.ellipsis,
                softWrap: true,
                maxLines: 2,
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 12),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
