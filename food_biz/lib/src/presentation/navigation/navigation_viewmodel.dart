import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../resource/resource.dart';
import '../presentation.dart';

class NavigationViewModel extends BaseViewModel {
  late final PageController pageController;
  int curtentPageIndex = 0;
  init() async {
    pageController =
        PageController(initialPage: curtentPageIndex, keepPage: true);
  }

  onTapNav(int pageIndex) {
    // curtentPageIndex = pageIndex;
    // notifyListeners();
    curtentPageIndex = pageIndex;
    pageController.jumpToPage(pageIndex);
    notifyListeners();
  }
}
