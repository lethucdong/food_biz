import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/configs.dart';
import 'package:provider/provider.dart';

import '../../presentation.dart';

class DownloadImageScreen extends StatefulWidget {
  const DownloadImageScreen({Key? key}) : super(key: key);
  @override
  _DownloadImageScreenState createState() => _DownloadImageScreenState();
}

class _DownloadImageScreenState extends State<DownloadImageScreen>
    with ResponsiveWidget {
  late DownloadImageViewModel _viewModel;
  late Size _screenSize;
  @override
  Widget build(BuildContext context) {
    _screenSize = MediaQuery.of(context).size;
    return BaseWidget<DownloadImageViewModel>(
        viewModel: DownloadImageViewModel(),
        onViewModelReady: (viewModel) {
          _viewModel = viewModel..init();
        },
        builder: (context, viewModel, appbar) {
          return Scaffold(
              appBar: buildAppBar(
                  title: Text(
                'Tải hình ảnh',
                style: TextStyle(
                    color: Colors.black, fontWeight: FontWeight.normal),
                textAlign: TextAlign.center,
                overflow: TextOverflow.ellipsis,
              )),
              backgroundColor: Colors.white,
              body: buildUi(context));
        });
  }

  @override
  Widget buildDesktop(BuildContext context) {
    return _buildUi(context);
  }

  @override
  Widget buildMobile(BuildContext context) {
    return _buildUi(context);
  }

  @override
  Widget buildTablet(BuildContext context) {
    return _buildUi(context);
  }

  Widget _buildUi(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      child: Column(
        children: [
          Container(
            height: 5,
            color: Colors.grey.shade300,
          ),
          Expanded(child: _buildContent(context)),
          _buildFooter(context),
        ],
      ),
    );
  }

  _buildFooter(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border(
          top: BorderSide(width: 2.0, color: Colors.grey.shade200),
        ),
      ),
      child: Row(
        children: [
          Expanded(
            child: Container(
              height: 45,
              alignment: Alignment.center,
              width: MediaQuery.of(context).size.width,
              color: Colors.white,
              child: Row(
                // mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                    width: 20,
                  ),
                  WidgetCheckButton(
                    isChecked:
                        context.watch<DownloadImageViewModel>().isSelectAll,
                    onChange: () {
                      Provider.of<DownloadImageViewModel>(context,
                              listen: false)
                          .onTapChooseAll();
                    },
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    'Chọn tất cả',
                    style: TextStyle(color: Colors.black, fontSize: 16),
                  ),
                ],
              ),
            ),
          ),
          Expanded(
            child: GestureDetector(
              onTap: () {},
              child: Container(
                height: 45,
                alignment: Alignment.center,
                width: MediaQuery.of(context).size.width,
                color: Colors.deepOrange,
                child: Text(
                  'Lưu lại',
                  style: TextStyle(color: Colors.white, fontSize: 16),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  _buildContent(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      child: SingleChildScrollView(
        child: Wrap(
          children: <Widget>[
            for (int i = 0; i < 10; i++)
              Container(
                alignment: Alignment.topRight,
                width: (_screenSize.width / 2) - 20,
                height: (_screenSize.width / 2) - 20,
                margin: EdgeInsets.all(5),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  image: DecorationImage(
                    image: AssetImage(AppImages.imgDownload),
                    fit: BoxFit.cover,
                  ),
                ),
                child: Container(
                  margin: EdgeInsets.only(top: 10, right: 10),
                  child: WidgetCheckButton(
                    onChange: () {},
                    isChecked:
                        context.watch<DownloadImageViewModel>().isSelectAll,
                  ),
                ),
              ),
          ],
        ),
      ),
    );
  }
}
