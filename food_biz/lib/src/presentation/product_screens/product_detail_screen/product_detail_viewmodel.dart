import 'package:flutter_app/src/presentation/cart_screen/widget/add_to_cart_widget.dart';

import '../../presentation.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';

class ProductDetailViewModel extends BaseViewModel {
  var rating = 0.0;
  bool isLike = false;
  init() async {}
  onTapAddtoCart() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AddToCartWidget(
            onTap: () {
              Get.back();
              Get.toNamed(Routers.cart);
            },
          );
        });
    // Get.toNamed(Routers.cart);
  }

  updateRating(double ratingIndex) {
    if (rating != ratingIndex) {
      rating = ratingIndex;
      notifyListeners();
    }
  }

  void onTapProductPr() {
    Get.toNamed(Routers.product_pr, arguments: {'title': 'Mẫu PR sản phẩm'});
  }

  void onTapReview() {
    Get.toNamed(Routers.product_review);
  }

  void onTapDescription() {
    Get.toNamed(Routers.product_pr, arguments: {'title': 'Mô tả sản phẩm'});
  }

  void onTapLike() {
    isLike = !isLike;
    notifyListeners();
  }

  void onTapDownloadImage() {
    Get.back();
    Get.toNamed(Routers.download_image);
  }
}
