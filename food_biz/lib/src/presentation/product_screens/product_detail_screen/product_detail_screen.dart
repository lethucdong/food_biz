import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/constanst/constants.dart';
import 'package:flutter_app/src/presentation/cart_screen/widget/add_to_cart_widget.dart';
import 'package:flutter_app/src/utils/app_utils.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:get/get.dart';
import '../../presentation.dart';
import 'package:provider/provider.dart';

class ProductDetailScreen extends StatelessWidget with ResponsiveWidget {
  late ProductDetailViewModel _viewModel;
  @override
  Widget build(BuildContext context) {
    return BaseWidget<ProductDetailViewModel>(
        viewModel: ProductDetailViewModel(),
        onViewModelReady: (viewModel) {
          _viewModel = viewModel..init();
        },
        builder: (context, viewModel, appbar) {
          return Scaffold(body: buildUi(context));
        });
  }

  @override
  Widget buildDesktop(BuildContext context) {
    return _buildBody(context);
  }

  @override
  Widget buildMobile(BuildContext context) {
    return _buildBody(context);
  }

  @override
  Widget buildTablet(BuildContext context) {
    return _buildBody(context);
  }

  Widget _buildBody(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Expanded(
            child: Container(
              // color: Colors.grey.shade300,
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    _buildContentSlide(context),
                    _lineSpace(height: 10),
                    // SizedBox(height: 10),
                    _buildSelectSize(context),
                    _lineSpace(height: 10),
                    _buildRankedReward(context),
                    _lineSpace(height: 10),
                    // SizedBox(height: 50)
                    _builVoucher(context),
                    _lineSpace(height: 10),
                    _buildDelivery(context),
                    // _lineSpace(),
                    _productBundle(context),
                    _buildProductInfo(context),
                    _lineSpace(height: 10),
                    _buildDescription(context),
                    _lineSpace(height: 10),
                    _buildProductPR(context),
                    _lineSpace(height: 10),
                    _buildReviewProduct(context),
                    _buildSameProduct(context),
                  ],
                ),
              ),
            ),
          ),
          Container(
            color: Colors.white,
            height: 70,
            child: Column(
              children: [
                SizedBox(height: 5),
                _lineSpace(height: 2),
                SizedBox(height: 5),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Expanded(
                      flex: 4,
                      child: GestureDetector(
                        onTap: () {
                          this._viewModel.onTapAddtoCart();
                        },
                        child: Container(
                          height: 52,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Image.asset(
                                AppImages.iconCart,
                                scale: 1.2,
                              ),
                              Text(
                                'Thêm vào giỏ hàng',
                                overflow: TextOverflow.clip,
                                maxLines: 2,
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontSize: 12,
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 6,
                      child: Container(
                        child: Row(
                          children: [
                            Expanded(
                              child: Container(
                                width: 110,
                                height: 52,
                                decoration: BoxDecoration(
                                  color: Colors.blue.shade800,
                                  borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(10),
                                    bottomLeft: Radius.circular(10),
                                  ),
                                ),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Image.asset(AppImages.iconReply,
                                        scale: 1.7),
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Text(
                                      'Đăng bán',
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 12,
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                            Expanded(
                              child: Container(
                                width: 110,
                                height: 52,
                                decoration: BoxDecoration(
                                  color: Colors.deepOrange,
                                  borderRadius: BorderRadius.only(
                                    topRight: Radius.circular(10),
                                    bottomRight: Radius.circular(10),
                                  ),
                                ),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Image.asset(AppImages.iconBag, scale: 1.7),
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Text(
                                      'Đăt hàng',
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 12,
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _iconButton(
      {required double size,
      required double boderCir,
      required Image image,
      required Function function}) {
    return GestureDetector(
      onTap: () => {function()},
      child: Container(
        width: size,
        height: size,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(boderCir),
          color: Colors.grey,
        ),
        child: image,
      ),
    );
  }

  Widget _buildContentSlide(BuildContext context) {
    return Stack(
      children: [
        Column(
          children: [
            Container(
              decoration: BoxDecoration(
                // borderRadius: BorderRadius.only(
                //   bottomLeft: Radius.circular(10),
                //   bottomRight: Radius.circular(10),
                // ),
                color: Colors.white,
              ),
              child: Column(
                children: [
                  Container(
                    color: Colors.white,
                    child: CarouselProductDetail(
                      listWidget: [
                        Container(
                          alignment: Alignment.center,
                          child: Image.asset(
                            AppImages.imgProductDetail,
                            scale: 0.5,
                          ),
                        ),
                        Container(
                          child: Image.asset(
                            AppImages.imgBannerHaLong,
                            scale: 0.5,
                          ),
                        ),
                        Container(
                          child: Image.asset(
                            AppImages.imgProductDetail,
                            scale: 0.5,
                          ),
                        ),
                        Container(
                          child: Image.asset(
                            AppImages.imgProductDetail,
                            scale: 0.5,
                          ),
                        ),
                        Container(
                          child: Image.asset(
                            AppImages.imgProductDetail,
                            scale: 0.5,
                          ),
                        ),
                        Container(
                          child: Image.asset(
                            AppImages.imgProductDetail,
                            scale: 0.5,
                          ),
                        ),
                        Container(
                          child: Image.asset(
                            AppImages.imgProductDetail,
                            scale: 0.5,
                          ),
                        ),
                        Container(
                          child: Image.asset(
                            AppImages.imgProductDetail,
                            scale: 0.5,
                          ),
                        )
                      ],
                      isShowPointSlide: true,
                      isAutoScroll: true,
                      height: 425,
                      width: MediaQuery.of(context).size.width,
                      viewportFraction: 1,
                      pointPadingHeight: 0,
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(15),
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text('TÔM NÕN HẠ LONG',
                                style: TextStyle(fontSize: 16)),
                            Container(
                              padding: EdgeInsets.symmetric(horizontal: 10),
                              width: 80,
                              height: 40,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: Colors.red.shade400),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Image.asset(AppImages.iconFlash),
                                  Text(
                                    '-70%',
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 16),
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                        SizedBox(height: 20),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              child: Row(
                                children: [
                                  Text(
                                    '5.000.000đ',
                                    style: TextStyle(
                                        color: Colors.deepOrange, fontSize: 16),
                                  ),
                                  SizedBox(width: 10),
                                  Text('(Giá thị trường)'),
                                ],
                              ),
                            ),
                            Container(
                              child: Row(
                                // mainAxisAlignment: MainAxisAlignment.spaceBetween,

                                children: [
                                  _iconButton(
                                    boderCir: 21,
                                    image: Image.asset(
                                      AppImages.iconDownload,
                                      scale: 2,
                                    ),
                                    function: () {
                                      showDialog(
                                        context: context,
                                        builder: (BuildContext context) {
                                          return Scaffold(
                                            backgroundColor: Colors.transparent,
                                            body: Center(
                                              child: Container(
                                                margin: EdgeInsets.symmetric(
                                                    horizontal: 20),
                                                height: 150,
                                                decoration: BoxDecoration(
                                                  color: Colors.white,
                                                  borderRadius:
                                                      BorderRadius.circular(10),
                                                ),
                                                child: Column(
                                                  children: [
                                                    Expanded(
                                                      child: Center(
                                                          child: Text(
                                                              'Xác nhận tải hình ảnh về thiết bị')),
                                                    ),
                                                    Container(
                                                      child: Row(
                                                        children: [
                                                          Expanded(
                                                            child:
                                                                GestureDetector(
                                                              onTap: () {
                                                                Navigator.pop(
                                                                    context);
                                                              },
                                                              child: Container(
                                                                height: 45,
                                                                decoration:
                                                                    BoxDecoration(
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .only(
                                                                    bottomLeft:
                                                                        Radius.circular(
                                                                            10),
                                                                  ),
                                                                  color: Colors
                                                                      .orange
                                                                      .shade800,
                                                                ),
                                                                alignment:
                                                                    Alignment
                                                                        .center,
                                                                child: Text(
                                                                  'Hủy bỏ',
                                                                  style: TextStyle(
                                                                      color: Colors
                                                                          .white),
                                                                ),
                                                              ),
                                                            ),
                                                          ),
                                                          SizedBox(width: 1),
                                                          Expanded(
                                                            child:
                                                                GestureDetector(
                                                              onTap: () {
                                                                _viewModel
                                                                    .onTapDownloadImage();
                                                              },
                                                              child: Container(
                                                                height: 45,
                                                                decoration:
                                                                    BoxDecoration(
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .only(
                                                                    bottomRight:
                                                                        Radius.circular(
                                                                            10),
                                                                  ),
                                                                  color: Colors
                                                                      .orange
                                                                      .shade800,
                                                                ),
                                                                alignment:
                                                                    Alignment
                                                                        .center,
                                                                child: Text(
                                                                  'Đồng ý',
                                                                  style: TextStyle(
                                                                      color: Colors
                                                                          .white),
                                                                ),
                                                              ),
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          );
                                        },
                                      );
                                    },
                                    size: 31,
                                  ),
                                  SizedBox(width: 10),
                                  _iconButton(
                                    boderCir: 21,
                                    image: Image.asset(
                                      context
                                              .watch<ProductDetailViewModel>()
                                              .isLike
                                          ? AppImages.iconRedHeart
                                          : AppImages.iconEmptyHeart,
                                      scale: 2,
                                      color: context
                                              .watch<ProductDetailViewModel>()
                                              .isLike
                                          ? Colors.red
                                          : Colors.white,
                                    ),
                                    function: () {
                                      Provider.of<ProductDetailViewModel>(
                                              context,
                                              listen: false)
                                          .onTapLike();
                                    },
                                    size: 31,
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: 10),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Text('Lợi nhuận '),
                            Image.asset(AppImages.iconProfit),
                            Text(': '),
                            Text(AppUtils.numToCurencyString(500000))
                          ],
                        ),
                        SizedBox(height: 10),
                        Row(
                          children: [
                            RatingBar(
                              initialRating: context
                                  .watch<ProductDetailViewModel>()
                                  .rating,
                              direction: Axis.horizontal,
                              allowHalfRating: false,
                              itemCount: 5,
                              itemSize: 15,
                              itemPadding:
                                  EdgeInsets.symmetric(horizontal: 0.0),
                              ratingWidget: RatingWidget(
                                empty: Image.asset(
                                  AppImages.iconStarEmpty,
                                  scale: 2,
                                ),
                                half: Image.asset(
                                  AppImages.iconStarFull,
                                  scale: 2,
                                ),
                                full: Image.asset(
                                  AppImages.iconStarFull,
                                  scale: 2,
                                ),
                              ),
                              onRatingUpdate: (rating) {
                                print(rating);
                                Provider.of<ProductDetailViewModel>(context,
                                        listen: false)
                                    .updateRating(rating);
                              },
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Text('${_viewModel.rating}'),
                            Text('   |   '),
                            Image.asset(
                              AppImages.iconHotSale,
                              scale: 3,
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Text('Còn 20 sản phẩm'),
                          ],
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  )
                ],
              ),
            ),
          ],
        ),
        Positioned(
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 20),
            width: MediaQuery.of(context).size.width,
            child: Row(
              // crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                _iconButton(
                  boderCir: 10,
                  image: Image.asset(
                    AppImages.iconLeftArrow,
                    scale: 2,
                  ),
                  function: () {},
                  size: 42,
                ),
                Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      _iconButton(
                        boderCir: 10,
                        image: Image.asset(
                          AppImages.iconSearch,
                          scale: 2,
                        ),
                        function: () {},
                        size: 42,
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      _iconButton(
                        boderCir: 10,
                        image: Image.asset(
                          AppImages.iconUnion,
                          scale: 2,
                        ),
                        function: () {},
                        size: 42,
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      _iconButton(
                        boderCir: 10,
                        image: Image.asset(
                          AppImages.iconShopingCart,
                          scale: 2,
                        ),
                        function: () {},
                        size: 42,
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
          top: 10,
          left: 0,
        ),
      ],
    );
  }

  Widget _buildSelectSize(BuildContext context) {
    List<dynamic> listItem = [
      {'key': 1, 'value': 'Khối lượng: 1kg - kích thước: Size nhỏ'},
      {'key': 2, 'value': 'Khối lượng: 5kg - kích thước: Size vừa'},
      {'key': 3, 'value': 'Khối lượng: 10kg - kích thước: Size lớn'}
    ];
    return Container(
      color: Colors.white,
      padding: EdgeInsets.all(10),
      height: 80,
      width: MediaQuery.of(context).size.width,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            padding: EdgeInsets.symmetric(horizontal: 10),
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              border: Border.all(color: Colors.grey),
            ),
            child: DropdownButton<int>(
              underline: SizedBox(),
              hint: Text('${listItem[0]['value']}'),
              isExpanded: true,
              icon: Image.asset(AppImages.iconDropDown),
              items: listItem.map((dynamic value) {
                return DropdownMenuItem<int>(
                  value: value['key'],
                  child: Text(
                    '${value['value']}',
                    softWrap: true,
                    overflow: TextOverflow.ellipsis,
                  ),
                );
              }).toList(),
              onChanged: (_) {},
            ),
          )
        ],
      ),
    );
  }

  Widget _lineSpace({required double height}) {
    return Container(
      height: height,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20), color: Colors.grey.shade300),
    );
  }

  _buildRankedReward(BuildContext context) {
    List<dynamic> listItem = [
      {'key': 0, 'value': '0'},
      {'key': 1, 'value': '10k'},
      {'key': 2, 'value': '20k'},
      {'key': 3, 'value': '30k'},
      {'key': 4, 'value': '40k'},
    ];
    return Container(
      padding: EdgeInsets.all(20),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('Thưởng thêm hạng'),
              GestureDetector(
                onTap: () {},
                child: Text('Xem chi tiết'),
              )
            ],
          ),
          SizedBox(height: 20),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              for (int i = 0; i < listItem.length; i++)
                Container(
                  width: 30,
                  child: Text(
                    '${listItem[i]['value']}',
                    textAlign: TextAlign.center,
                  ),
                )
            ],
          ),
          SizedBox(height: 5),
          Stack(
            children: [
              Container(
                // padding: EdgeInsets.only(top: 100),
                height: 30,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      height: 3,
                      // width: 100,
                      color: Colors.grey.shade300,
                    )
                  ],
                ),
              ),
              Positioned(
                // top: 0,
                // left: 0,
                bottom: 0,
                left: 0,
                right: 0,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    for (int i = 0; i < listItem.length; i++)
                      GestureDetector(
                        child: Container(
                          width: 30,
                          height: 30,
                          decoration: BoxDecoration(
                              color: listItem[i]['key'] == 2
                                  ? Colors.red
                                  : Colors.white,
                              shape: BoxShape.circle,
                              border: Border.all(
                                  color: Colors.grey.shade300, width: 3)),
                        ),
                      ),
                  ],
                ),
              ),
            ],
          ),
          SizedBox(height: 5),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              for (int i = 0; i < listItem.length; i++)
                Container(
                    width: 30,
                    child: Image.asset(listItem[i]['key'] == 0
                        ? AppImages.iconRank0
                        : listItem[i]['key'] == 1
                            ? AppImages.iconRank1
                            : listItem[i]['key'] == 2
                                ? AppImages.iconRank2
                                : listItem[i]['key'] == 3
                                    ? AppImages.iconRank3
                                    : AppImages.iconRank4))
            ],
          ),
        ],
      ),
    );
  }

  _builVoucher(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(20),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('Voucher'),
              Container(
                alignment: Alignment.center,
                padding: EdgeInsets.symmetric(horizontal: 10),
                width: 100,
                height: 40,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.deepOrange),
                child: Text(
                  'Giảm 10%',
                  style: TextStyle(color: Colors.white, fontSize: 16),
                ),
              ),
            ],
          ),
          SizedBox(height: 10),
          TextField(
            decoration: InputDecoration(
              isDense: true,
              hintText:
                  'Áp dung voucher giảm giá 10% cho đơn hàng Áp dung voucher giảm giá 10% cho đơn hàng...',
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
              ),
              contentPadding: EdgeInsets.all(10),
            ),
            // keyboardType: TextInputType.number,
          ),
          // SizedBox(
          //   height: 10,
          // ),
        ],
      ),
    );
  }

  Widget _buildDelivery(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(20),
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              GestureDetector(
                onTap: () {
                  print('Free ship');
                },
                child: Container(
                  child: Row(
                    children: [
                      Image.asset(
                        AppImages.iconDeliveryTruck,
                        scale: 2,
                      ),
                      SizedBox(width: 10),
                      Text('Free ship'),
                    ],
                  ),
                ),
              ),
              SizedBox(width: 10),
              Container(
                child: Row(
                  children: [
                    Image.asset(
                      AppImages.iconCertificateT,
                      scale: 2,
                    ),
                    SizedBox(width: 10),
                    Text('Hàng chất'),
                  ],
                ),
              ),
              SizedBox(width: 10),
              Container(
                child: Row(
                  children: [
                    Image.asset(
                      AppImages.iconDeliveryFast,
                      scale: 2,
                    ),
                    SizedBox(width: 10),
                    Text('Giao hàng'),
                  ],
                ),
              ),
            ]),
      ),
    );
  }

  Widget _productBundle(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(20),
      alignment: Alignment.center,
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
          color: Colors.grey.shade200, borderRadius: BorderRadius.circular(10)),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('Sản phẩm mua kèm'),
              GestureDetector(
                child: Text('Xem tất cả'),
              ),
            ],
          ),
          SizedBox(height: 20),
          SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: CarouselCommon(
              listWidget: <Widget>[
                _productBundleCard(),
                _productBundleCard(),
                _productBundleCard(),
              ],
              isAutoScroll: false,
              isShowPointSlide: false,
              width: MediaQuery.of(context).size.width - 40,
              height: 245,
              viewportFraction: 0.5,
              pointPadingHeight: 10,
              paddingPageview: 5,
            ),
          ),
          SizedBox(height: 20),
          _lineSpace(height: 3),
          SizedBox(height: 20),
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      RichText(
                        text: TextSpan(
                          text: 'Tổng: ',
                          style: DefaultTextStyle.of(context).style,
                          children: <TextSpan>[
                            TextSpan(
                              text: AppUtils.numToCurencyString(1950000),
                              style: TextStyle(
                                color: Colors.grey,
                                decoration: TextDecoration.lineThrough,
                                fontSize: 16,
                              ),
                            ),
                            TextSpan(
                              text: '  ' + AppUtils.numToCurencyString(1650000),
                              style: TextStyle(
                                fontSize: 16,
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(height: 10),
                      RichText(
                        text: TextSpan(
                          text: 'Tiết kiệm:  ',
                          style: DefaultTextStyle.of(context).style,
                          children: <TextSpan>[
                            TextSpan(
                              text: AppUtils.numToCurencyString(3000000),
                              style: TextStyle(
                                color: Colors.deepOrange,
                                decoration: TextDecoration.lineThrough,
                                fontSize: 16,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  width: 122,
                  height: 50,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    color: Colors.deepOrange,
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Text(
                    'Thêm hàng',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 16,
                        fontWeight: FontWeight.bold),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _productBundleCard() {
    return GestureDetector(
      onTap: () {
        Get.toNamed(Routers.product_detail);
      },
      child: Container(
        width: 162,
        height: 242,
        // margin: EdgeInsets.only(right: 10),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: Colors.white,
        ),
        alignment: Alignment.bottomLeft,
        child: Column(
          children: [
            Stack(
              children: [
                Container(
                  height: 140,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(10),
                        topRight: Radius.circular(10)),
                    image: DecorationImage(
                      image: AssetImage(AppImages.imgProduct_tom),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ],
            ),
            Container(
              alignment: Alignment.topLeft,
              padding: EdgeInsets.all(10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Tôm nõn hấp hạ long',
                    style: TextStyle(fontSize: 17),
                    softWrap: false,
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Row(
                    children: [
                      Text(
                        AppUtils.numToCurencyString(6500000),
                        style: TextStyle(
                            color: Colors.grey,
                            decoration: TextDecoration.lineThrough),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Text(
                        AppUtils.numToCurencyString(5500000),
                        style: TextStyle(
                          color: Colors.deepOrange,
                          decoration: TextDecoration.lineThrough,
                          fontSize: 16,
                        ),
                      ),
                      WidgetCheckButton(onChange: () {}),
                      // Container(
                      //   width: 24,
                      //   height: 24,
                      //   decoration: BoxDecoration(
                      //     color: Colors.white,
                      //     borderRadius: BorderRadius.circular(5),
                      //     border: Border.all(color: Colors.grey),
                      //   ),
                      // )
                    ],
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _buildProductInfo(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(20),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _buildLineInfo(
            title: 'Thông tin sản phẩm',
            type: 0,
          ),
          _lineSpace(height: 2),
          _buildLineInfo(
            title: 'Giá thị trường:',
            price: 110000,
            type: 0,
          ),
          _lineSpace(height: 2),
          _buildLineInfo(
            title: 'Giá tiền hàng:',
            price: 80000,
            type: 0,
          ),
          _lineSpace(height: 2),
          _buildLineInfo(
            title: 'Lợi nhuận hạng:',
            price: 110000,
            imageIcon: Image.asset(AppImages.iconRank0, scale: 2),
            type: 0,
          ),
          _lineSpace(height: 2),
          Container(
            padding: EdgeInsets.only(left: 20),
            child: Column(
              children: [
                _buildLineInfo(
                  title: 'Lợi nhuận sản phẩm:',
                  price: 20000,
                  type: 0,
                ),
                _lineSpace(height: 2),
                _buildLineInfo(
                  title: 'Thưởng thêm hạng:',
                  price: 20000,
                  imageIcon: Image.asset(AppImages.iconRank0, scale: 2),
                  type: 0,
                ),
                _lineSpace(height: 2),
              ],
            ),
          ),
          _buildLineInfo(
              title: 'Mua số lượng sỉ:',
              type: 1,
              function: () {},
              text: 'Liên hệ'),
          _lineSpace(height: 2),
          _buildLineInfo(
              title: 'Nhà cung cấp:',
              type: 1,
              function: () {},
              text: 'Elina Med'),
          _lineSpace(height: 2),
          _buildLineInfo(title: 'Gửi từ:', type: 2, text: 'Hà Nội'),
          _lineSpace(height: 2),
          _buildLineInfo(
              title: 'Danh mục:', type: 1, function: () {}, text: 'Mẹ và bé'),
        ],
      ),
    );
  }

  Widget _buildLineInfo(
      {required String title,
      double price = 0,
      Image? imageIcon,
      required int type,
      Function? function,
      String? text /*0: price, 1: link, 2: text*/}) {
    return Container(
      height: 50,
      alignment: Alignment.centerLeft,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            children: [
              Text(title + '  '),
              if (imageIcon != null) imageIcon,
            ],
          ),
          (price != 0 && type == 0)
              ? Text(
                  AppUtils.numToCurencyString(price),
                )
              : (function != null && type == 1)
                  ? GestureDetector(child: Text(text! + '  >'))
                  : (type == 2)
                      ? Text(text!)
                      : Container(),
        ],
      ),
    );
  }

  Widget _buildDescription(BuildContext context) {
    return Column(
      children: [
        Container(
          padding: EdgeInsets.all(20),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text('Mô tả sản phẩm'),
                  GestureDetector(
                    onTap: () {
                      _viewModel.onTapDescription();
                    },
                    child: Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.symmetric(horizontal: 10),
                        width: 100,
                        height: 40,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: Colors.grey.shade100),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Image.asset(
                              AppImages.iconCopy,
                              scale: 2,
                            ),
                            Text(
                              'Sao chép',
                              style: TextStyle(
                                  color: Colors.deepOrange, fontSize: 14),
                            ),
                          ],
                        )),
                  ),
                ],
              ),
              SizedBox(height: 20),
              Container(
                height: 400,
                child: Text(
                  'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. ',
                  maxLines: 15,
                  overflow: TextOverflow.ellipsis,
                ),
              )
            ],
          ),
        ),
        _lineSpace(height: 2),
        Container(
          alignment: Alignment.center,
          height: 50,
          child: GestureDetector(
            child: Text(
              'Xem thêm  >',
              style: TextStyle(
                fontSize: 16,
                color: Colors.deepOrange,
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildProductPR(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
      ),
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('Mẫu PR sản phẩm'),
                GestureDetector(
                  onTap: () {},
                  child: Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    width: 100,
                    height: 40,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: Colors.grey.shade100),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Image.asset(
                          AppImages.iconCopy,
                          scale: 2,
                        ),
                        Text(
                          'Sao chép',
                          style:
                              TextStyle(color: Colors.deepOrange, fontSize: 14),
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
          Container(
            child: CarouselCommon(
              paddingPageview: 5,
              isShowPointSlide: true,
              isAutoScroll: true,
              width: MediaQuery.of(context).size.width - 40,
              height: 350,
              viewportFraction: 1,
              pointPadingHeight: 370,
              listWidget: [
                _buildBlogCard(),
                _buildBlogCard(),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildBlogCard() {
    return Container(
      decoration: BoxDecoration(
        border: Border.all(
          color: Colors.grey.shade300,
        ),
        borderRadius: BorderRadius.circular(10),
        color: Colors.white,
      ),
      child: Column(
        children: [
          Container(
            height: 120,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: Colors.amber,
              image: DecorationImage(
                image: AssetImage(AppImages.imgBlog1),
                fit: BoxFit.cover,
              ),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Container(
            height: 165,
            padding: EdgeInsets.symmetric(horizontal: 10),
            child: Text(
                'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. ',
                // style: TextStyle(fontSize: 1),
                maxLines: 8,
                overflow: TextOverflow.ellipsis),
          ),
          _lineSpace(height: 2),
          Container(
            alignment: Alignment.center,
            height: 50,
            child: GestureDetector(
              onTap: () {
                _viewModel.onTapProductPr();
              },
              child: Container(
                alignment: Alignment.center,
                width: 134,
                height: 32,
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.deepOrange),
                  borderRadius: BorderRadius.circular(5),
                ),
                child: Text(
                  'Xem đầy đủ  >',
                  style: TextStyle(
                    fontSize: 16,
                    color: Colors.deepOrange,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildReviewProduct(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 20, left: 20, right: 20),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('Đánh giá sản phẩm'),
              GestureDetector(
                onTap: () {
                  _viewModel.onTapReview();
                },
                child: Text('Xem tất cả'),
              ),
            ],
          ),
          SizedBox(height: 10),
          Row(
            children: [
              RatingBar(
                initialRating: 4,
                direction: Axis.horizontal,
                allowHalfRating: false,
                itemCount: 5,
                itemSize: 15,
                itemPadding: EdgeInsets.symmetric(horizontal: 0.0),
                ratingWidget: RatingWidget(
                  empty: Image.asset(
                    AppImages.iconStarEmpty,
                    scale: 2,
                  ),
                  half: Image.asset(
                    AppImages.iconStarFull,
                    scale: 2,
                  ),
                  full: Image.asset(
                    AppImages.iconStarFull,
                    scale: 2,
                  ),
                ),
                onRatingUpdate: (rating) {
                  print(rating);
                  Provider.of<ProductDetailViewModel>(context, listen: false)
                      .updateRating(rating);
                },
              ),
              SizedBox(width: 10),
              Text('  4.0/5  (5.2k đánh giá)'),
            ],
          ),
          SizedBox(height: 10),
          _lineSpace(height: 2),
          _buildReviewerCard(),
          _buildReviewerCard(),
          _buildReviewerCard(),
          Container(
            alignment: Alignment.center,
            height: 50,
            child: GestureDetector(
              child: Text(
                'Xem tất cả  >',
                style: TextStyle(
                  fontSize: 16,
                  color: Colors.deepOrange,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildReviewerCard() {
    return Container(
      padding: EdgeInsets.only(top: 10),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                width: 34,
                height: 34,
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(
                        image: AssetImage(AppImages.iconAvatar))),
              ),
              SizedBox(width: 10),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('Trương Huyền'),
                  RatingBar(
                    initialRating: 4,
                    direction: Axis.horizontal,
                    allowHalfRating: false,
                    itemCount: 5,
                    itemSize: 15,
                    itemPadding: EdgeInsets.symmetric(horizontal: 0.0),
                    ratingWidget: RatingWidget(
                      empty: Image.asset(
                        AppImages.iconStarEmpty,
                        scale: 2,
                      ),
                      half: Image.asset(
                        AppImages.iconStarFull,
                        scale: 2,
                      ),
                      full: Image.asset(
                        AppImages.iconStarFull,
                        scale: 2,
                      ),
                    ),
                    onRatingUpdate: (rating) {},
                  ),
                ],
              )
            ],
          ),
          SizedBox(height: 10),
          Text(
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever ',
            maxLines: 15,
            overflow: TextOverflow.ellipsis,
          ),
          SizedBox(height: 10),
          _lineSpace(height: 2)
        ],
      ),
    );
  }

  _buildSameProduct(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: Colors.grey.shade200,
      ),
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Sản phẩm tương tự',
                  style: TextStyle(fontSize: 16),
                ),
                // GestureDetector(
                //   onTap: () {},
                //   child: Text('Xem thêm'),
                // )
              ],
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            // crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              CarouselCommon(
                listWidget: [
                  ProductCard(
                      isHot: true,
                      isNew: true,
                      discount: 75,
                      isLove: false,
                      name: 'Tôm nõn hấp Hạ Long',
                      price: 150000,
                      profit: 50000,
                      inventoryNum: null,
                      isOrder: false),
                  ProductCard(
                      isHot: true,
                      isNew: false,
                      discount: 60,
                      isLove: true,
                      name: 'Cá cơm rim lạc vừng',
                      price: 150000,
                      profit: 50000,
                      inventoryNum: 5,
                      isOrder: false),
                  ProductCard(
                      isHot: false,
                      isNew: true,
                      discount: 70,
                      isLove: false,
                      name: 'Cá cơm rim lạc vừng',
                      price: 150000,
                      profit: 50000,
                      inventoryNum: 5,
                      isOrder: false),
                ],
                isShowPointSlide: false,
                isAutoScroll: true,
                width: MediaQuery.of(context).size.width - 40,
                height: 255,
                viewportFraction: 1 / 2,
                pointPadingHeight: 10,
                paddingPageview: 5,
              ),
            ],
          )
        ],
      ),
    );
  }
}
