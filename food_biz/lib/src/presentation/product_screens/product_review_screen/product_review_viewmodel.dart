import '../../presentation.dart';
import 'package:get/get.dart';

class ProductReviewViewModel extends BaseViewModel {
  init() async {}

  void onTapCreateReview() {
    Get.toNamed(Routers.create_review);
  }
}
