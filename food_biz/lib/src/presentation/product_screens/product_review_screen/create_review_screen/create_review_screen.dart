import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/configs.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

import '../../../presentation.dart';

class CreateReviewScreen extends StatefulWidget {
  @override
  _CreateReviewScreenState createState() => _CreateReviewScreenState();
}

class _CreateReviewScreenState extends State<CreateReviewScreen>
    with ResponsiveWidget {
  late CreateReviewViewModel _viewModel;
  @override
  Widget build(BuildContext context) {
    return BaseWidget<CreateReviewViewModel>(
        viewModel: CreateReviewViewModel(),
        onViewModelReady: (viewModel) {
          _viewModel = viewModel..init();
        },
        builder: (context, viewModel, appbar) {
          return Scaffold(
              appBar: buildAppBar(
                title: Text(
                  'Viết đánh giá',
                  style: TextStyle(
                      color: Colors.black, fontWeight: FontWeight.normal),
                  textAlign: TextAlign.center,
                  overflow: TextOverflow.ellipsis,
                ),
                elevation: 2,
              ),
              backgroundColor: Colors.white,
              body: buildUi(context));
        });
  }

  @override
  Widget buildDesktop(BuildContext context) {
    return _buildUi(context);
  }

  @override
  Widget buildMobile(BuildContext context) {
    return _buildUi(context);
  }

  @override
  Widget buildTablet(BuildContext context) {
    return _buildUi(context);
  }

  Widget _buildUi(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      child: Column(
        children: [
          Expanded(
            child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
              ),
              child: SingleChildScrollView(
                child: Container(
                  padding: EdgeInsets.all(20),
                  child: Column(
                    children: [
                      Image.asset(AppImages.imgBannerReview),
                      SizedBox(
                        height: 20,
                      ),
                      Container(
                        height: 280,
                        padding: EdgeInsets.only(top: 20, right: 20, left: 20),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Color.fromRGBO(255, 244, 239, 1),
                        ),
                        alignment: Alignment.center,
                        child: Column(
                          children: [
                            Text(
                              'Cho sao để đánh giá',
                              style: TextStyle(
                                  color: Colors.deepOrange, fontSize: 18),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            RatingBar(
                              initialRating: 4,
                              direction: Axis.horizontal,
                              allowHalfRating: false,
                              itemCount: 5,
                              itemSize: 30,
                              itemPadding:
                                  EdgeInsets.symmetric(horizontal: 5.0),
                              ratingWidget: RatingWidget(
                                empty: Container(
                                  child: Image.asset(
                                    AppImages.iconStarEmpty,
                                    scale: 2,
                                  ),
                                ),
                                half: Image.asset(
                                  AppImages.iconStarFull,
                                  scale: 2,
                                ),
                                full: Image.asset(
                                  AppImages.iconStarFull,
                                  scale: 2,
                                ),
                              ),
                              onRatingUpdate: (rating) {
                                print(rating);
                                // Provider.of<ProductDetailViewModel>(context, listen: false)
                                //     .updateRating(rating);
                              },
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Text(
                              'Đánh giá của bạn',
                              style: TextStyle(
                                  color: Colors.deepOrange, fontSize: 18),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Container(
                              height: 140,
                              padding: EdgeInsets.all(10),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                border: Border.all(
                                  color: Colors.deepOrange,
                                ),
                                color: Colors.white,
                              ),
                              child: Container(
                                // color: Colors.white,
                                // width: 200,
                                constraints: BoxConstraints(maxHeight: 100),
                                child: SingleChildScrollView(
                                  child: TextField(
                                    maxLines: null,
                                    decoration: const InputDecoration(
                                      isDense: true,
                                      border: InputBorder.none,
                                      hintText: 'Nhập đánh giá của bạn...',
                                    ),
                                    style: TextStyle(color: Colors.grey),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
          GestureDetector(
            onTap: () {
              _viewModel.onTapCreateReview();
            },
            child: Container(
              margin: EdgeInsets.only(top: 5),
              color: Colors.deepOrange,
              height: 45,
              alignment: Alignment.center,
              child: Text(
                'Gửi đánh giá',
                style: TextStyle(color: Colors.white, fontSize: 16),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
