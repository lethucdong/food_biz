import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/configs.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

import '../../presentation.dart';

class ProductReviewScreen extends StatefulWidget {
  @override
  _ProductReviewScreenState createState() => _ProductReviewScreenState();
}

class _ProductReviewScreenState extends State<ProductReviewScreen>
    with ResponsiveWidget {
  late ProductReviewViewModel _viewModel;
  @override
  Widget build(BuildContext context) {
    return BaseWidget<ProductReviewViewModel>(
        viewModel: ProductReviewViewModel(),
        onViewModelReady: (viewModel) {
          _viewModel = viewModel..init();
        },
        builder: (context, viewModel, appbar) {
          return Scaffold(
              appBar: buildAppBar(
                title: Text(
                  'Đánh giá sản phẩm',
                  style: TextStyle(
                      color: Colors.black, fontWeight: FontWeight.normal),
                  textAlign: TextAlign.center,
                  overflow: TextOverflow.ellipsis,
                ),
                elevation: 2,
              ),
              backgroundColor: Colors.white,
              body: buildUi(context));
        });
  }

  @override
  Widget buildDesktop(BuildContext context) {
    return _buildUi(context);
  }

  @override
  Widget buildMobile(BuildContext context) {
    return _buildUi(context);
  }

  @override
  Widget buildTablet(BuildContext context) {
    return _buildUi(context);
  }

  Widget _buildUi(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      child: Column(
        children: [
          Expanded(
            child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
              ),
              child: SingleChildScrollView(
                child: Container(
                  padding: EdgeInsets.only(top: 20),
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text('Đánh giá sản phẩm'),
                            GestureDetector(
                              onTap: () {},
                              child: Text('Xem tất cả'),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(height: 10),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20),
                        child: Row(
                          children: [
                            RatingBar(
                              initialRating: 4,
                              direction: Axis.horizontal,
                              allowHalfRating: false,
                              itemCount: 5,
                              itemSize: 15,
                              itemPadding:
                                  EdgeInsets.symmetric(horizontal: 0.0),
                              ratingWidget: RatingWidget(
                                empty: Image.asset(
                                  AppImages.iconStarEmpty,
                                  scale: 2,
                                ),
                                half: Image.asset(
                                  AppImages.iconStarFull,
                                  scale: 2,
                                ),
                                full: Image.asset(
                                  AppImages.iconStarFull,
                                  scale: 2,
                                ),
                              ),
                              onRatingUpdate: (rating) {
                                print(rating);
                                // Provider.of<ProductDetailViewModel>(context, listen: false)
                                //     .updateRating(rating);
                              },
                            ),
                            SizedBox(width: 10),
                            Text('  4.0/5  (5.2k đánh giá)'),
                          ],
                        ),
                      ),
                      SizedBox(height: 10),
                      LineSpace(),
                      Column(
                        children: [
                          _buildReviewerCard(),
                          _buildReviewerCard(),
                          _buildReviewerCard(),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          GestureDetector(
            onTap: () {
              _viewModel.onTapCreateReview();
            },
            child: Container(
              margin: EdgeInsets.only(top: 5),
              color: Colors.deepOrange,
              height: 45,
              alignment: Alignment.center,
              child: Text(
                '  Viết đánh giá',
                style: TextStyle(color: Colors.white, fontSize: 16),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildReviewerCard() {
    return Container(
      padding: EdgeInsets.only(top: 10),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Container(
                  width: 34,
                  height: 34,
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      image: DecorationImage(
                          image: AssetImage(AppImages.iconAvatar))),
                ),
              ),
              SizedBox(width: 10),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('Trương Huyền'),
                  RatingBar(
                    initialRating: 4,
                    direction: Axis.horizontal,
                    allowHalfRating: false,
                    itemCount: 5,
                    itemSize: 15,
                    itemPadding: EdgeInsets.symmetric(horizontal: 0.0),
                    ratingWidget: RatingWidget(
                      empty: Image.asset(
                        AppImages.iconStarEmpty,
                        scale: 2,
                      ),
                      half: Image.asset(
                        AppImages.iconStarFull,
                        scale: 2,
                      ),
                      full: Image.asset(
                        AppImages.iconStarFull,
                        scale: 2,
                      ),
                    ),
                    onRatingUpdate: (rating) {},
                  ),
                ],
              )
            ],
          ),
          SizedBox(height: 10),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Text(
              'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever ',
              maxLines: 15,
              overflow: TextOverflow.ellipsis,
            ),
          ),
          SizedBox(height: 10),
          LineSpace(),
        ],
      ),
    );
  }
}
