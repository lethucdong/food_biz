import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/configs.dart';

import '../../presentation.dart';

class ProductPrScreen extends StatefulWidget {
  final String title;
  const ProductPrScreen({Key? key, required this.title}) : super(key: key);
  @override
  _ProductPrScreenState createState() => _ProductPrScreenState();
}

class _ProductPrScreenState extends State<ProductPrScreen>
    with ResponsiveWidget {
  late ProductPrViewModel _viewModel;
  @override
  Widget build(BuildContext context) {
    return BaseWidget<ProductPrViewModel>(
        viewModel: ProductPrViewModel(),
        onViewModelReady: (viewModel) {
          _viewModel = viewModel..init();
        },
        builder: (context, viewModel, appbar) {
          return Scaffold(
              appBar: buildAppBar(
                  title: Text(
                widget.title,
                style: TextStyle(
                    color: Colors.black, fontWeight: FontWeight.normal),
                textAlign: TextAlign.center,
                overflow: TextOverflow.ellipsis,
              )),
              backgroundColor: Colors.white,
              body: buildUi(context));
        });
  }

  @override
  Widget buildDesktop(BuildContext context) {
    return _buildUi(context);
  }

  @override
  Widget buildMobile(BuildContext context) {
    return _buildUi(context);
  }

  @override
  Widget buildTablet(BuildContext context) {
    return _buildUi(context);
  }

  Widget _buildUi(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      child: Column(
        children: [
          Expanded(
            child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey,
                    spreadRadius: 0.1,
                    blurRadius: 5,
                    offset: Offset(0, 1), // changes position of shadow
                  ),
                ],
              ),
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    Container(
                      child: Image.asset(AppImages.imgProductPr),
                    ),
                    Container(
                      padding: EdgeInsets.all(20),
                      child: Column(
                        children: [
                          Text(
                            'Tôm nõn hấp Hạ Long, giá cao nhưng được đặt dsfsdfmua ầm ầm',
                            maxLines: 1,
                            style: TextStyle(fontSize: 16),
                          ),
                          SizedBox(
                            height: 30,
                          ),
                          Text(
                            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. ",
                            style: TextStyle(fontSize: 16),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 5),
            // color: Colors.white,
            height: 45,
            alignment: Alignment.center,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset(
                  AppImages.iconCopy,
                  color: Colors.blueAccent,
                  scale: 2,
                ),
                Text(
                  '  Sao chép',
                  style: TextStyle(color: Colors.blueAccent, fontSize: 16),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
