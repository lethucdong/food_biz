import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/configs.dart';

import '../../presentation.dart';

class ProfileScreen extends StatefulWidget {
  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> with ResponsiveWidget {
  late ProfileViewModel _viewModel;
  @override
  Widget build(BuildContext context) {
    return BaseWidget<ProfileViewModel>(
        viewModel: ProfileViewModel(),
        onViewModelReady: (viewModel) {
          _viewModel = viewModel..init();
        },
        builder: (context, viewModel, appbar) {
          return Scaffold(
              appBar: buildAppBar(
                  title: Text(
                'Thông tin cá nhân',
                style: TextStyle(color: Colors.black),
                textAlign: TextAlign.center,
              )),
              backgroundColor: Colors.grey.shade300,
              body: buildUi(context));
        });
  }

  @override
  Widget buildDesktop(BuildContext context) {
    return _buildUi(context);
  }

  @override
  Widget buildMobile(BuildContext context) {
    return _buildUi(context);
  }

  @override
  Widget buildTablet(BuildContext context) {
    return _buildUi(context);
  }

  Widget _buildUi(BuildContext context) {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          children: [
            _buildAvatar(),
            _buildEditInfo(),
            _buildListTool(),
          ],
        ),
      ),
    );
  }

  Widget _buildAvatar() {
    return Column(
      children: [
        Container(
          height: 150,
          child: Stack(
            children: [
              Container(
                height: 75,
                decoration: BoxDecoration(
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 5,
                      blurRadius: 5,
                      offset: Offset(0, 0), // changes position of shadow
                    ),
                  ],
                ),
              ),
              Positioned(
                left: 0,
                right: 0,
                top: 10,
                child: Container(
                  height: 130,
                  width: 130,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.white,
                    border: Border.all(color: Colors.white, width: 7),
                    image: DecorationImage(
                      image: AssetImage(AppImages.iconAvatar),
                      fit: BoxFit.contain,
                    ),
                  ),
                ),
              ),
              Positioned(
                left: 110,
                right: 0,
                bottom: 10,
                child: Container(
                  height: 30,
                  width: 30,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.deepOrange,
                    border: Border.all(color: Colors.white, width: 2),
                    image: DecorationImage(
                      image: AssetImage(AppImages.iconCamera),
                      fit: BoxFit.none,
                      // scale: 0,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
        Text('Trương Huyền'),
        SizedBox(
          height: 20,
        ),
      ],
    );
  }

  Widget _buildEditInfo() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        children: [
          _buildEditInfoCard(
            title: 'Chỉnh sửa thông tin cá nhân',
            desc:
                'Đại lý nên hạn chế thay đổi thông tin của mình trừ khi sai sót trong quá trình đăng ký.',
            imgUrl: AppImages.imgEditProfile,
            function: () {
              _viewModel.onTapEditProfile();
            },
          ),
          _buildEditInfoCard(
            title: 'Thông tin tài khoản ngân hàng',
            desc:
                'Cập nhật thông tin tài khoản của bạn để thuận tiện cho thanh toán',
            imgUrl: AppImages.imgEditCredit,
            function: () {
              _viewModel.onTapCredit();
            },
          ),
        ],
      ),
    );
  }

  Widget _buildEditInfoCard(
      {required String title,
      required String desc,
      required String imgUrl,
      required Function function}) {
    return GestureDetector(
      onTap: () {
        function();
      },
      child: Container(
        padding: EdgeInsets.all(10),
        margin: EdgeInsets.only(bottom: 10),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
        ),
        child: Column(
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  // color: Colors.red,
                  height: 16,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                        flex: 4,
                        child: Padding(
                          padding: const EdgeInsets.only(left: 10),
                          child: Text(
                            title,
                            style: TextStyle(
                                color: Colors.deepOrange, fontSize: 16),
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 2,
                        child: GestureDetector(
                          child: Container(
                            alignment: Alignment.topRight,
                            child: Image.asset(
                              AppImages.iconRightArrow,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  height: 50,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                        flex: 4,
                        child: Text(
                          desc,
                        ),
                      ),
                      Expanded(
                        flex: 2,
                        child: GestureDetector(
                          child: Container(
                            alignment: Alignment.topRight,
                            child: Image.asset(imgUrl),
                          ),
                        ),
                      ),
                    ],
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }

  Widget _buildListTool() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        children: [
          _buildToolCard(
              imgUrl: AppImages.iconSetting,
              title: 'Cài đặt',
              onTap: () {
                _viewModel.onTapSetting();
              }),
          _buildToolCard(
              imgUrl: AppImages.iconInterrogation,
              title: 'Hỗ trợ',
              onTap: () {
                _viewModel.onTapSupport();
              }),
          _buildToolCard(imgUrl: AppImages.iconExit, title: 'Đăng xuất'),
        ],
      ),
    );
  }

  Widget _buildToolCard(
      {required String imgUrl, required String title, Function? onTap}) {
    return GestureDetector(
      onTap: () {
        onTap!();
      },
      child: Container(
        margin: EdgeInsets.only(bottom: 10),
        padding: EdgeInsets.symmetric(horizontal: 10),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10), color: Colors.white),
        height: 67,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                Image.asset(
                  imgUrl,
                  color: Colors.deepOrange,
                ),
                SizedBox(
                  width: 15,
                ),
                Text(
                  title,
                  style: TextStyle(fontSize: 16),
                ),
              ],
            ),
            Image.asset(AppImages.iconRightArrow),
          ],
        ),
      ),
    );
  }
}
