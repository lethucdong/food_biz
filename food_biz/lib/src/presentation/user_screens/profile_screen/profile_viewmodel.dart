import '../../presentation.dart';
import 'package:get/get.dart';

class ProfileViewModel extends BaseViewModel {
  init() async {}

  void onTapEditProfile() {
    Get.toNamed(Routers.edit_profile);
  }

  void onTapCredit() {
    Get.toNamed(Routers.credit);
  }

  void onTapSetting() {
    Get.toNamed(Routers.setting);
  }

  void onTapSupport() {
    Get.toNamed(Routers.support);
  }
}
