import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/configs.dart';

import '../../presentation.dart';

class SettingScreen extends StatefulWidget {
  @override
  _SettingScreenState createState() => _SettingScreenState();
}

class _SettingScreenState extends State<SettingScreen> with ResponsiveWidget {
  late SettingViewModel _viewModel;
  @override
  Widget build(BuildContext context) {
    return BaseWidget<SettingViewModel>(
        viewModel: SettingViewModel(),
        onViewModelReady: (viewModel) {
          _viewModel = viewModel..init();
        },
        builder: (context, viewModel, appbar) {
          return Scaffold(
              appBar: buildAppBar(
                title: Text(
                  'Cài đặt',
                  style: TextStyle(color: Colors.black),
                  textAlign: TextAlign.center,
                ),
                elevation: 2,
              ),
              backgroundColor: Colors.white,
              body: buildUi(context));
        });
  }

  @override
  Widget buildDesktop(BuildContext context) {
    return _buildUi(context);
  }

  @override
  Widget buildMobile(BuildContext context) {
    return _buildUi(context);
  }

  @override
  Widget buildTablet(BuildContext context) {
    return _buildUi(context);
  }

  Widget _buildUi(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      child: Column(
        children: [
          _buildItem(
            title: 'Ưu đãi',
            isActive: true,
            description:
                "Hãy là người đầu tiên được cập nhật các thông tin khuyến mãi sắp diễn ra",
          ),
          _buildItem(
            title: 'Đơn hàng',
            isActive: false,
            description: "Cập nhật mới nhất về tình trạng đơn hàng của bạn",
          ),
          _buildItem(
            title: 'Thông báo & nhắc nhở',
            isActive: false,
            description: "Cập nhật về các tin tức khác trên ứng dụng",
          ),
        ],
      ),
    );
  }

  _buildItem(
      {required String title,
      required String description,
      bool isActive = false}) {
    return Container(
      height: 150,
      color: Colors.white,
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.symmetric(horizontal: 20),
            width: double.infinity,
            height: 50,
            color: Colors.grey.shade200,
            alignment: Alignment.centerLeft,
            child: Text(
              title,
              style: TextStyle(fontSize: 14, color: Colors.grey.shade700),
            ),
          ),
          Expanded(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: Text(
                      description,
                      softWrap: true,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 2,
                      style: TextStyle(fontSize: 16),
                    ),
                  ),
                  SwitchButton(
                    isActive: isActive,
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
