import '../../presentation.dart';
import 'package:get/get.dart';

class CreditViewModel extends BaseViewModel {
  init() async {}

  void onTapAddCredit() {
    Get.toNamed(Routers.add_credit);
  }
}
