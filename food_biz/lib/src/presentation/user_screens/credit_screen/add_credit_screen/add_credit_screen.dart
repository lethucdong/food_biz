import 'package:flutter/material.dart';

import '../../../presentation.dart';

class AddCreditScreen extends StatefulWidget {
  final bool isEdit;
  const AddCreditScreen({Key? key, this.isEdit = false}) : super(key: key);

  @override
  _AddCreditScreenState createState() => _AddCreditScreenState();
}

class _AddCreditScreenState extends State<AddCreditScreen>
    with ResponsiveWidget {
  late AddCreditViewModel _viewModel;
  @override
  Widget build(BuildContext context) {
    return BaseWidget<AddCreditViewModel>(
        viewModel: AddCreditViewModel(),
        onViewModelReady: (viewModel) {
          _viewModel = viewModel..init();
        },
        builder: (context, viewModel, appbar) {
          return Scaffold(
              appBar: buildAppBar(
                  title: Text(
                'Thông tin cá nhân',
                style: TextStyle(color: Colors.black),
                textAlign: TextAlign.center,
              )),
              backgroundColor: Colors.grey.shade300,
              body: buildUi(context));
        });
  }

  @override
  Widget buildDesktop(BuildContext context) {
    return _buildUi(context);
  }

  @override
  Widget buildMobile(BuildContext context) {
    return _buildUi(context);
  }

  @override
  Widget buildTablet(BuildContext context) {
    return _buildUi(context);
  }

  Widget _buildUi(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 2),
      padding: EdgeInsets.symmetric(horizontal: 20),
      height: MediaQuery.of(context).size.height - 80,
      color: Colors.white,
      child: Column(
        children: [
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  SizedBox(height: 10),
                  buildTextInput(null, 'Ngân hàng', 'Chọn ngân hàng của bạn',
                      TextInputType.text),
                  SizedBox(height: 20),
                  buildTextInput(
                      null, 'Chi nhánh', 'Nhập chi nhánh', TextInputType.text),
                  SizedBox(height: 20),
                  buildTextInput(null, 'Chủ tài khoản',
                      'Nhập tên chủ tài khoản ngân hàng', TextInputType.text),
                  SizedBox(height: 20),
                  buildTextInput(null, 'số tài khoản',
                      'Nhập số tài khoản của bạn', TextInputType.text),
                  // Expanded(child: Container()),
                ],
              ),
            ),
          ),
          Column(
            children: [
              Container(
                decoration: BoxDecoration(
                  color: Colors.deepOrange,
                  borderRadius: BorderRadius.circular(10),
                ),
                height: 45,
                alignment: Alignment.center,
                child: Text(
                  'Lưu lại',
                  style: TextStyle(color: Colors.white, fontSize: 16),
                ),
              ),
              SizedBox(height: 20),
            ],
          ),
        ],
      ),
    );
  }

  Widget buildTextInput(TextEditingController? textEditingController,
      String label, String? hintText, TextInputType? textInputType) {
    return Container(
      padding: EdgeInsets.all(10),
      height: 75,
      width: MediaQuery.of(context).size.width,
      alignment: Alignment.topLeft,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          border: Border.all(color: Colors.grey)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(label),
          TextField(
              decoration: InputDecoration(
                  isDense: true,
                  hintText: hintText,
                  disabledBorder: InputBorder.none,
                  border: InputBorder.none),
              controller: textEditingController,
              keyboardType:
                  textInputType == null ? TextInputType.text : textInputType)
        ],
      ),
    );
  }
}
