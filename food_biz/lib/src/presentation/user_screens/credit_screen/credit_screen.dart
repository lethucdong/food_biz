import 'package:flutter_app/src/configs/configs.dart';

import '../../presentation.dart';

class CreditScreen extends StatefulWidget {
  final bool isEmpty;
  const CreditScreen({Key? key, this.isEmpty = false}) : super(key: key);

  @override
  _CreditScreenState createState() => _CreditScreenState();
}

class _CreditScreenState extends State<CreditScreen> with ResponsiveWidget {
  late CreditViewModel _viewModel;
  @override
  Widget build(BuildContext context) {
    return BaseWidget<CreditViewModel>(
        viewModel: CreditViewModel(),
        onViewModelReady: (viewModel) {
          _viewModel = viewModel..init();
        },
        builder: (context, viewModel, appbar) {
          return Scaffold(
              appBar: buildAppBar(
                  title: Text(
                'Thông tin tài khoản',
                style: TextStyle(color: Colors.black),
                textAlign: TextAlign.center,
              )),
              backgroundColor: Colors.grey.shade300,
              body: buildUi(context));
        });
  }

  @override
  Widget buildDesktop(BuildContext context) {
    return _buildUi(context);
  }

  @override
  Widget buildMobile(BuildContext context) {
    return _buildUi(context);
  }

  @override
  Widget buildTablet(BuildContext context) {
    return _buildUi(context);
  }

  Widget _buildUi(BuildContext context) {
    return widget.isEmpty ? _builEmptyCredit() : _builCredit();
  }

  Widget _builEmptyCredit() {
    return Container(
      margin: EdgeInsets.only(top: 2),
      color: Colors.white,
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              height: 60,
            ),
            Image.asset(AppImages.imgBannerCredit, scale: 2),
            SizedBox(
              height: 10,
            ),
            Text(
              'Bạn chưa có tài khoản nào.',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              'Bạn cần cập nhật thông tin tài khoản của \nmình để thuận lợi trong quá trình thanh toán',
              softWrap: true,
              textAlign: TextAlign.center,
            ),
            SizedBox(
              height: 20,
            ),
            GestureDetector(
              onTap: () {
                _viewModel.onTapAddCredit();
              },
              child: Container(
                width: 253,
                height: 50,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  color: Colors.deepOrange,
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Text(
                  'Thêm tài khoản',
                  style: TextStyle(color: Colors.white, fontSize: 16),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _builCredit() {
    return Container(
      margin: EdgeInsets.only(top: 2),
      // color: Colors.white,
      child: Column(
        children: [
          Column(
            children: [
              _buildList(),
              Container(
                height: 45,
                padding: EdgeInsets.symmetric(horizontal: 20),
                color: Colors.white,
                child: GestureDetector(
                  onTap: () {
                    _viewModel.onTapAddCredit();
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Thêm tài khoản'),
                      Image.asset(AppImages.iconPlus),
                    ],
                  ),
                ),
              )
            ],
          ),
          Expanded(
              child: Container(
            color: Colors.white,
            margin: EdgeInsets.only(top: 5),
          ))
        ],
      ),
    );
  }

  Widget _buildList() {
    return Container(
      color: Colors.white,
      margin: EdgeInsets.symmetric(vertical: 5),
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        children: [
          _buildItemCard(selected: true),
          _buildItemCard(),
          _buildItemCard(),
          _buildItemCard(),
        ],
      ),
    );
  }

  Widget _buildItemCard({bool selected = false}) {
    return Column(
      children: [
        SizedBox(
          height: 10,
        ),
        Container(
          height: 80,
          child: Row(
            children: [
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    RichText(
                      text: TextSpan(
                        text: 'Vietcombank    ',
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 14,
                        ),
                        children: <TextSpan>[
                          TextSpan(
                            text: selected ? '[mặc định]' : '',
                            style: TextStyle(
                              color: Colors.deepOrange,
                              fontSize: 13,
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 5),
                    Text('031000123456'),
                    SizedBox(height: 5),
                    Text('Truong thi huyen'),
                    SizedBox(height: 5),
                    Text('Chi nhánh Hà Nội')
                  ],
                ),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  GestureDetector(
                      onTap: () {
                        _viewModel.onTapAddCredit();
                      },
                      child: Text(
                        'Chỉnh sửa',
                        style: TextStyle(
                          color: Colors.blueAccent,
                          decoration: TextDecoration.underline,
                        ),
                      )),
                  // Icon(selected ? Icons.check : null, color: Colors.red),
                  // Image.asset(AppImages.iconLocation, scale: 2),
                ],
              ),
            ],
          ),
        ),
        SizedBox(
          height: 5,
        ),
        LineSpace(),
      ],
    );
  }
}
