import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/configs.dart';

import '../../../presentation.dart';

class RankDetailScreen extends StatefulWidget {
  final RankItemWidget rankItemWidget;
  final RankBonusDetailWidget rankBonusDetailWidget;
  final String rankName;
  const RankDetailScreen({
    Key? key,
    required this.rankItemWidget,
    required this.rankBonusDetailWidget,
    required this.rankName,
  }) : super(key: key);

  @override
  _RankDetailScreenState createState() => _RankDetailScreenState();
}

class _RankDetailScreenState extends State<RankDetailScreen>
    with ResponsiveWidget {
  late RankDetailViewModel _viewModel;
  @override
  Widget build(BuildContext context) {
    return BaseWidget<RankDetailViewModel>(
        viewModel: RankDetailViewModel(),
        onViewModelReady: (viewModel) {
          _viewModel = viewModel..init();
        },
        builder: (context, viewModel, appbar) {
          return Scaffold(
              appBar: buildAppBar(
                title: Text(
                  widget.rankName.replaceAll('Thăng', 'Thông tin'),
                  style: TextStyle(color: Colors.black),
                  textAlign: TextAlign.center,
                ),
                elevation: 2,
              ),
              backgroundColor: Colors.grey.shade300,
              body: buildUi(context));
        });
  }

  @override
  Widget buildDesktop(BuildContext context) {
    return _buildUi(context);
  }

  @override
  Widget buildMobile(BuildContext context) {
    return _buildUi(context);
  }

  @override
  Widget buildTablet(BuildContext context) {
    return _buildUi(context);
  }

  Widget _buildUi(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.all(20),
            child: AbsorbPointer(
              absorbing: true,
              child: widget.rankItemWidget,
            ),
          ),
          _builRankdContent(),
        ],
      ),
    );
  }

  Widget _builRankdContent() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: widget.rankBonusDetailWidget,
    );
  }
}
