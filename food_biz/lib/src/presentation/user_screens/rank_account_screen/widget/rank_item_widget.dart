import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/configs.dart';
import 'package:get/get.dart';

import '../../../presentation.dart';

class RankItemWidget extends StatelessWidget {
  String urlIcon;
  String rankName;
  String description;
  double? currentProgress;
  double? maxProgress;
  bool? isProgressTab;
  RankItemWidget(
      {Key? key,
      required this.urlIcon,
      required this.rankName,
      required this.description,
      this.currentProgress,
      this.maxProgress,
      this.isProgressTab = false})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Get.toNamed(Routers.rank_detail, arguments: {
          'rankItemWidget': this,
          'rankName': this.rankName,
          'rankBonusDetailWidget': RankBonusDetailWidget(
            rank: urlIcon == AppImages.iconRank0 ? 0 : 1,
          )
        });
      },
      child: Container(
        padding: EdgeInsets.all(20),
        // height: 131, //_screenSize.width * 0.4,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
        ),
        child: Column(
          children: [
            Row(
              children: [
                Expanded(
                  child: Row(
                    children: [
                      Image.asset(
                        urlIcon,
                        scale: 2,
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      Text(
                        rankName,
                        style: TextStyle(fontSize: 16),
                      ),
                    ],
                  ),
                ),
                isProgressTab == true
                    ? Container()
                    : Image.asset(AppImages.iconRightArrow)
              ],
            ),
            SizedBox(
              height: 5,
            ),
            Container(
              padding: EdgeInsets.only(left: 30),
              child: Column(
                children: [
                  Text(
                    description,
                    softWrap: true,
                    overflow: TextOverflow.clip,
                    maxLines: 1,
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  currentProgress != null && maxProgress != null
                      ? Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text('Tiến độ:'),
                            Text(
                              '$currentProgress/$maxProgress tr đ',
                              style: TextStyle(color: Colors.blueAccent),
                            ),
                          ],
                        )
                      : Container(),
                  SizedBox(
                    height: 5,
                  ),
                  currentProgress != null && maxProgress != null
                      ? Container(
                          height: 6,
                          decoration: BoxDecoration(
                            color: Colors.grey,
                            borderRadius: BorderRadius.circular(5),
                          ),
                          child: Row(
                            children: [
                              Expanded(
                                flex: (currentProgress! * 100).toInt(),
                                child: Container(
                                  height: 6,
                                  decoration: BoxDecoration(
                                    color: Colors.deepOrange,
                                    borderRadius: BorderRadius.circular(5),
                                  ),
                                ),
                              ),
                              Expanded(
                                flex: ((maxProgress! - currentProgress!) * 100)
                                    .toInt(),
                                child: Container(),
                              ),
                            ],
                          ),
                        )
                      : Container()
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
