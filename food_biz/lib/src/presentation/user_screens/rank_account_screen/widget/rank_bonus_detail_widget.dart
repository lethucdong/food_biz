import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/configs.dart';
import 'package:flutter_app/src/configs/constanst/app_values.dart';

import '../../../presentation.dart';

class RankBonusDetailWidget extends StatelessWidget {
  late int rank;
  RankBonusDetailWidget({Key? key, required this.rank}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<Widget> listItem =
        rank == 0 ? AppValues.LIST_RANK_0_INFOR : AppValues.LIST_RANK_2_INFOR;
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20),
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: 10,
          ),
          Text(
            'Ưu đãi hạng hiện tại',
            style: TextStyle(fontSize: 16),
          ),
          SizedBox(
            height: 10,
          ),
          for (int i = 0; i < listItem.length; i++)
            Container(
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 5),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Image.asset(
                      AppImages.iconRoundTicked,
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Expanded(
                      child: listItem[i],
                    ),
                  ],
                ),
              ),
            ),
        ],
      ),
    );
  }
}
