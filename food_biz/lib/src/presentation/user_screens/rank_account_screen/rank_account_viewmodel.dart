import '../../presentation.dart';

class RankAccountViewModel extends BaseViewModel {
  late PageController pageController;
  int curentPageIndex = 0;

  init() async {
    pageController = PageController(
      initialPage: 0,
      viewportFraction: 1,
      keepPage: true,
    );
  }

  updateTabController(int tab) {
    pageController.jumpToPage(tab);
    curentPageIndex = tab;
    notifyListeners();
  }
}
