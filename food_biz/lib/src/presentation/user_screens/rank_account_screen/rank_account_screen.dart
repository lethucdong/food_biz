import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/configs.dart';
import 'package:provider/src/provider.dart';

import '../../presentation.dart';

class RankAccountScreen extends StatefulWidget {
  const RankAccountScreen({Key? key}) : super(key: key);

  @override
  _RankAccountScreenState createState() => _RankAccountScreenState();
}

class _RankAccountScreenState extends State<RankAccountScreen>
    with ResponsiveWidget {
  late RankAccountViewModel _viewModel;
  late Size _screenSize;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _screenSize = MediaQuery.of(context).size;
    return BaseWidget<RankAccountViewModel>(
        viewModel: RankAccountViewModel(),
        onViewModelReady: (viewModel) {
          _viewModel = viewModel..init();
        },
        builder: (context, viewModel, appbar) {
          return Scaffold(
              appBar: buildAppBar(
                title: Text(
                  'Hạng tài khoản',
                  style: TextStyle(color: Colors.black),
                  textAlign: TextAlign.center,
                ),
                elevation: 2,
              ),
              backgroundColor: Colors.grey.shade300,
              body: buildUi(context));
        });
  }

  @override
  Widget buildDesktop(BuildContext context) {
    return _buildUi(context);
  }

  @override
  Widget buildMobile(BuildContext context) {
    return _buildUi(context);
  }

  @override
  Widget buildTablet(BuildContext context) {
    return _buildUi(context);
  }

  Widget _buildUi(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        height: _screenSize.height - 80,
        child: Column(
          children: [
            Container(
              color: Colors.white,
              child: Column(
                children: [
                  _buildHeader(context),
                  _buildTabControl(context),
                ],
              ),
            ),
            Expanded(
              child: Container(
                  padding: EdgeInsets.all(20),
                  child: _buildTabContent(context)),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget _buildHeader(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 20, left: 20, right: 20),
      width: _screenSize.width,
      // height: _screenSize.width * 0.45,
      decoration: BoxDecoration(
        color: Colors.red,
        borderRadius: BorderRadius.circular(10),
        image: DecorationImage(
          image: AssetImage(AppImages.imgRankBg),
          fit: BoxFit.cover,
        ),
      ),
      // alignment: Alignment.center,
      child: Stack(
        children: [
          Positioned(
            child: Center(
              child: Image.asset(AppImages.imgLogoTransparent),
            ),
          ),
          Container(
            padding: EdgeInsets.all(20),
            child: Column(
              // mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Image.asset(
                      AppImages.imgLogoWhite,
                      color: Colors.white,
                    ),
                    Text(
                      'ĐẠI LÝ',
                      style: TextStyle(color: Colors.white),
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.all(20),
                  child: Text(
                    'TÀI KHOẢN VÀNG',
                    style: TextStyle(color: Colors.white, fontSize: 18),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Text(
                      'Tên tài khoản',
                      style: TextStyle(color: Colors.white),
                    ),
                    Text(
                      'Hiệu lực',
                      style: TextStyle(color: Colors.white),
                    ),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 10),
                      child: Text(
                        'Trương Huyền',
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                    Text(
                      '---',
                      style: TextStyle(color: Colors.white),
                    ),
                  ],
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _buildTabControl(BuildContext context) {
    return Container(
      height: 40,
      child: Row(
        children: [
          Expanded(
            child: GestureDetector(
              onTap: () {
                // _viewModel.pageController.jumpToPage(0);

                Provider.of<RankAccountViewModel>(context, listen: false)
                    .updateTabController(0);
              },
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(),
                  Container(
                    // color: Colors.red,
                    alignment: Alignment.center,
                    child: Text('Tiến độ'),
                  ),
                  Container(
                    height: 2,
                    color:
                        context.watch<RankAccountViewModel>().curentPageIndex ==
                                0
                            ? Colors.deepOrange
                            : Colors.grey,
                  ),
                ],
              ),
            ),
          ),
          Expanded(
            child: GestureDetector(
              onTap: () {
                // _viewModel.pageController.jumpToPage(1);
                Provider.of<RankAccountViewModel>(context, listen: false)
                    .updateTabController(1);
              },
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(),
                  Container(
                    // color: Colors.red,
                    alignment: Alignment.center,
                    child: Text('Thưởng hạng'),
                  ),
                  Container(
                    height: 2,
                    color:
                        context.watch<RankAccountViewModel>().curentPageIndex ==
                                1
                            ? Colors.deepOrange
                            : Colors.grey,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildTabContent(BuildContext context) {
    return PageView(
      physics: NeverScrollableScrollPhysics(),
      scrollDirection: Axis.horizontal,
      onPageChanged: (value) {
        // _viewModel.updateTabController(value);

        Provider.of<RankAccountViewModel>(context, listen: false)
            .updateTabController(value);
      },
      controller: _viewModel.pageController,
      children: <Widget>[
        SingleChildScrollView(child: _buildProgress()),
        SingleChildScrollView(child: _buildRewardBonus()),
      ],
    );
  }

  Widget _buildProgress() {
    return Container(
      width: _screenSize.width,
      child: Column(
        children: [
          // _buildProgressRankInfo(
          //     description: 'Tiền hàng gốc đại lý thanh toán cho Foodbiz',
          //     rankName: 'Thăng hạng bạch kim',
          //     urlIcon: AppImages.iconRank3,
          //     currentProgress: 7.5,
          //     maxProgress: 15),
          RankItemWidget(
            description: 'Tiền hàng gốc đại lý thanh toán cho Foodbiz',
            rankName: 'Thăng hạng bạch kim',
            urlIcon: AppImages.iconRank3,
            currentProgress: 7.5,
            maxProgress: 15,
            isProgressTab: _viewModel.curentPageIndex == 0 ? true : false,
          ),
          SizedBox(
            height: 20,
          ),
          _buildProgressCheckList(),
        ],
      ),
    );
  }

  Widget _buildProgressCheckList() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20),
      width: _screenSize.width,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: 10,
          ),
          Text(
            'Ưu đãi hạng hiện tại',
            style: TextStyle(fontSize: 16),
          ),
          SizedBox(
            height: 10,
          ),
          for (int i = 0; i < AppValues.LIST_RANK_0_INFOR.length; i++)
            Container(
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 5),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Image.asset(
                      AppImages.iconRoundTicked,
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Expanded(
                      child: AppValues.LIST_RANK_0_INFOR[i],
                    ),
                  ],
                ),
              ),
            ),
        ],
      ),
    );
  }

  Widget _buildRewardBonus() {
    return Container(
      child: Column(
        children: [
          RankItemWidget(
            description: 'Tài khoản đại lý đã đăng ký trên hệ thống của Foobiz',
            rankName: 'Thăng hạng đồng',
            urlIcon: AppImages.iconRank0,
            isProgressTab: _viewModel.curentPageIndex == 0 ? true : false,
          ),
          SizedBox(height: 20),
          RankItemWidget(
            description: 'Tiền hàng gốc đại lý thanh toán cho Foodbiz',
            rankName: 'Thăng hạng bạc',
            urlIcon: AppImages.iconRank1,
            currentProgress: 1,
            maxProgress: 3,
            isProgressTab: _viewModel.curentPageIndex == 0 ? true : false,
          ),
          SizedBox(height: 20),
          RankItemWidget(
            description: 'Tiền hàng gốc đại lý thanh toán cho Foodbiz',
            rankName: 'Thăng hạng vàng',
            urlIcon: AppImages.iconRank2,
            currentProgress: 3.5,
            maxProgress: 7,
            isProgressTab: _viewModel.curentPageIndex == 0 ? true : false,
          ),
          SizedBox(height: 20),
          RankItemWidget(
            description: 'Tiền hàng gốc đại lý thanh toán cho Foodbiz',
            rankName: 'Thăng hạng bạch kim',
            urlIcon: AppImages.iconRank3,
            currentProgress: 7.5,
            maxProgress: 15,
            isProgressTab: _viewModel.curentPageIndex == 0 ? true : false,
          ),
          SizedBox(height: 20),
          RankItemWidget(
            description: 'Tiền hàng gốc đại lý thanh toán cho Foodbiz',
            rankName: 'Thăng hạng kim cương',
            urlIcon: AppImages.iconRank4,
            currentProgress: 20.5,
            maxProgress: 30,
            isProgressTab: _viewModel.curentPageIndex == 0 ? true : false,
          ),
        ],
      ),
    );
  }
}
