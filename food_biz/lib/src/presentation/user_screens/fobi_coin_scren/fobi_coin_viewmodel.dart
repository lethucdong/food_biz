import '../../presentation.dart';
import 'package:get/get.dart';

class FobiCoinViewModel extends BaseViewModel {
  bool isShowPopup = false;
  bool isShowAll = true;

  int monthSelected = DateTime.now().month;
  int yearSelected = DateTime.now().year;

  init() async {}
  onTapGoToHome() {
    Get.offAndToNamed(Routers.navigation);
  }

  onTapTriggerPopup() {
    isShowPopup = !isShowPopup;
    notifyListeners();
  }

  onPickupMonth({required int monthSelected, required int yearSelected}) {
    this.monthSelected = monthSelected;
    this.yearSelected = yearSelected;
    isShowAll = false;
    onTapTriggerPopup();
    notifyListeners();
  }
}
