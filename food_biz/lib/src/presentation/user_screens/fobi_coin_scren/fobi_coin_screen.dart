import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/configs.dart';
import 'package:flutter_app/src/presentation/user_screens/user_widget/month_picker_widget.dart';
import 'package:get/get.dart';
import 'package:provider/src/provider.dart';
import '../../presentation.dart';

class FobiCoinScreen extends StatefulWidget {
  const FobiCoinScreen({Key? key}) : super(key: key);

  @override
  _FobiCoinScreenState createState() => _FobiCoinScreenState();
}

class _FobiCoinScreenState extends State<FobiCoinScreen> with ResponsiveWidget {
  late FobiCoinViewModel _viewModel;
  late Size _screenSize;
  @override
  Widget build(BuildContext context) {
    _screenSize = MediaQuery.of(context).size;
    return BaseWidget<FobiCoinViewModel>(
        viewModel: FobiCoinViewModel(),
        onViewModelReady: (viewModel) {
          _viewModel = viewModel..init();
        },
        builder: (context, viewModel, appbar) {
          return Scaffold(
              appBar: buildAppBar(
                title: Text(
                  'Fobi Xu',
                  style: TextStyle(color: Colors.black),
                  textAlign: TextAlign.center,
                ),
                elevation: 2,
              ),
              backgroundColor: Colors.grey.shade300,
              body: buildUi(context));
        });
  }

  @override
  Widget buildDesktop(BuildContext context) {
    return _buildUi(context);
  }

  @override
  Widget buildMobile(BuildContext context) {
    return _buildUi(context);
  }

  @override
  Widget buildTablet(BuildContext context) {
    return _buildUi(context);
  }

  Widget _buildUi(BuildContext context) {
    return Container(
      width: _screenSize.width,
      height: _screenSize.height,
      child: Column(
        children: [
          _buildHeader(),
          Expanded(
            child: Stack(
              children: [
                Column(
                  children: [
                    Container(
                      width: _screenSize.width,
                      decoration: BoxDecoration(color: Colors.white),
                      height: 35,
                      padding: const EdgeInsets.symmetric(
                          horizontal: 30, vertical: 10),
                      child: Text('Xu đang chờ về'),
                    ),
                    Expanded(
                      child: Container(
                        padding: EdgeInsets.all(20),
                        child: SingleChildScrollView(
                          child: Column(
                            children: [
                              _buildTotal(),
                              _buildListItem(),
                            ],
                          ),
                          scrollDirection: Axis.vertical,
                        ),
                      ),
                    ),
                  ],
                ),
                Visibility(
                  visible: context.watch<FobiCoinViewModel>().isShowPopup,
                  child: Stack(
                    children: [
                      Container(
                        color: Colors.black.withOpacity(0.5),
                        // child: _buildMonthPicker(),
                      ),
                      Positioned(
                        child: MonthPickerWidget(
                          monthSelected: _viewModel.monthSelected,
                          yearSelected: _viewModel.yearSelected,
                          onChange: (int monthSelected, int yearSelected) {
                            Provider.of<FobiCoinViewModel>(context,
                                    listen: false)
                                .onPickupMonth(
                                    monthSelected: monthSelected,
                                    yearSelected: yearSelected);
                          },
                        ),
                        // child: _buildMonthPicker(),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildItem() {
    return Container(
      padding: EdgeInsets.all(15),
      margin: EdgeInsets.only(bottom: 10),
      // height: 95,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Image.asset(AppImages.iconFobiCoinAround),
          SizedBox(
            width: 10,
          ),
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Giảm giá đơn hàng',
                      style: TextStyle(fontSize: 16),
                    ),
                    Text(
                      '-15 xu',
                      style: TextStyle(color: Colors.blueAccent, fontSize: 16),
                    ),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                Text('Sử dụng Fobi Xu để giảm giá đơn hàng : 123546'),
                SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      '08:00 - 20/10/2010',
                      style: TextStyle(color: Colors.grey),
                    ),
                    Text(
                      'Hoàn thành',
                      style: TextStyle(color: Colors.deepOrange),
                    ),
                  ],
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildHeader() {
    return Container(
      color: Colors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: 122,
            // color: Colors.red,
            decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage(AppImages.imgBgOrange), fit: BoxFit.cover),
            ),
            child: Stack(
              children: [
                Container(
                  // color: Colors.blue,\
                  padding: EdgeInsets.all(10),
                  margin: EdgeInsets.all(20),
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.white),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'Số Fobi xu hiện tại',
                        style: TextStyle(
                          fontSize: 12,
                          color: Colors.white,
                        ),
                        textAlign: TextAlign.left,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            '15 Xu',
                            style: TextStyle(fontSize: 26, color: Colors.white),
                          ),
                          GestureDetector(
                            onTap: () {
                              _viewModel.onTapTriggerPopup();
                            },
                            child: Row(
                              children: [
                                Text(
                                  _viewModel.isShowAll
                                      ? 'Tất cả'
                                      : 'Tháng ${_viewModel.monthSelected}/ ${_viewModel.yearSelected}',
                                  style: TextStyle(
                                      fontSize: 18, color: Colors.white),
                                ),
                                Image.asset(
                                  AppImages.iconRightArrow,
                                  color: Colors.white,
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildListItem() {
    return Column(
      children: [
        _buildItem(),
        _buildItem(),
        _buildItem(),
        _buildItem(),
        _buildItem(),
        _buildItem(),
        _buildItem(),
        _buildItem(),
        _buildItem(),
      ],
    );
  }

  Widget _buildTotal() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      height: 55,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: Colors.white,
      ),
      child: Row(
        children: [
          Expanded(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Số xu đã nhận',
                  ),
                  Text(
                    '10.000 xu',
                    style: TextStyle(
                      color: Colors.deepOrange,
                      fontSize: 16,
                    ),
                  ),
                ],
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.symmetric(vertical: 5),
            width: 1,
            color: Colors.grey.shade100,
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Số xu đã sử dụng',
                  ),
                  Text(
                    '10.000 xu',
                    style: TextStyle(
                      color: Colors.blueAccent,
                      fontSize: 16,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
