import '../../presentation.dart';
import 'package:get/get.dart';

class StatisticalViewModel extends BaseViewModel {
  init() async {}

  void onTapBestSell() {
    List<Widget> listWidget = [
      ProductCard(
          isHot: true,
          isNew: true,
          discount: 75,
          isLove: false,
          name: 'Tôm nõn hấp Hạ Long',
          price: 150000,
          profit: 50000,
          inventoryNum: null,
          isOrder: false),
      ProductCard(
          isHot: true,
          isNew: false,
          discount: 60,
          isLove: true,
          name: 'Cá cơm rim lạc vừng',
          price: 150000,
          profit: 50000,
          inventoryNum: 5,
          isOrder: false),
      ProductCard(
          isHot: false,
          isNew: true,
          discount: 70,
          isLove: false,
          name: 'Cá cơm rim lạc vừng',
          price: 150000,
          profit: 50000,
          inventoryNum: 5,
          isOrder: false),
    ];
    Get.toNamed(Routers.best_sell, arguments: {'listItem': listWidget});
  }

  void onTapSale() {
    Get.toNamed(Routers.sale);
  }

  void onTapDebt() {
    Get.toNamed(Routers.debt);
  }

  void onTapCustomers() {
    Get.toNamed(Routers.customers);
  }
}
