import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/configs.dart';
import 'package:flutter_app/src/utils/utils.dart';

import '../../presentation.dart';

class StatisticalScreen extends StatefulWidget {
  @override
  _StatisticalScreenState createState() => _StatisticalScreenState();
}

class _StatisticalScreenState extends State<StatisticalScreen>
    with ResponsiveWidget {
  late StatisticalViewModel _viewModel;
  @override
  Widget build(BuildContext context) {
    return BaseWidget<StatisticalViewModel>(
        viewModel: StatisticalViewModel(),
        onViewModelReady: (viewModel) {
          _viewModel = viewModel..init();
        },
        builder: (context, viewModel, appbar) {
          return Scaffold(
              appBar: buildAppBar(
                  title: Text(
                    'Thống kê',
                    style: TextStyle(color: Colors.black),
                    textAlign: TextAlign.center,
                  ),
                  elevation: 4),
              backgroundColor: Colors.grey.shade300,
              body: buildUi(context));
        });
  }

  @override
  Widget buildDesktop(BuildContext context) {
    return _buildUi(context);
  }

  @override
  Widget buildMobile(BuildContext context) {
    return _buildUi(context);
  }

  @override
  Widget buildTablet(BuildContext context) {
    return _buildUi(context);
  }

  Widget _buildUi(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(20),
      child: Column(
        children: [
          _buildToolCard(
              imgUrl: AppImages.iconCredit1,
              title: 'Sản phẩm bán chạy',
              quantity: '10 sản phẩm',
              onTap: () {
                _viewModel.onTapBestSell();
              }),
          _buildToolCard(
              imgUrl: AppImages.iconDollar1,
              title: 'Doanh số',
              onTap: () {
                _viewModel.onTapSale();
              },
              quantity: AppUtils.numToCurencyString(0)),
          _buildToolCard(
            imgUrl: AppImages.iconCredit1,
            title: 'Công nợ, thanh toán',
            onTap: () {
              _viewModel.onTapDebt();
            },
            quantity: AppUtils.numToCurencyString(0),
          ),
          _buildToolCard(
            imgUrl: AppImages.iconUser1,
            title: 'Số lượng khách hàng',
            quantity: '0 khách hàng',
            onTap: () {
              _viewModel.onTapCustomers();
            },
          ),
        ],
      ),
    );
  }

  Widget _buildToolCard(
      {required String imgUrl,
      required String title,
      Function? onTap,
      required String quantity}) {
    return GestureDetector(
      onTap: () {
        onTap!();
      },
      child: Container(
        margin: EdgeInsets.only(bottom: 10),
        padding: EdgeInsets.symmetric(horizontal: 10),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10), color: Colors.white),
        height: 67,
        child: GestureDetector(
          onTap: () {
            onTap!();
          },
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  Image.asset(
                    imgUrl,
                  ),
                  SizedBox(
                    width: 15,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        title,
                        style: TextStyle(fontSize: 16),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Text(
                        quantity,
                        style: TextStyle(
                          color: Colors.blueAccent,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              Image.asset(AppImages.iconRightArrow),
            ],
          ),
        ),
      ),
    );
  }
}
