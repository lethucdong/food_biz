import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/configs.dart';
import 'package:flutter_app/src/utils/utils.dart';

import '../../../presentation.dart';

class BestSellScreen extends StatefulWidget {
  late List<Widget> listItem;
  BestSellScreen({required this.listItem});
  @override
  _BestSellScreenState createState() => _BestSellScreenState();
}

class _BestSellScreenState extends State<BestSellScreen> with ResponsiveWidget {
  late BestSellViewModel _viewModel;
  @override
  Widget build(BuildContext context) {
    return BaseWidget<BestSellViewModel>(
        viewModel: BestSellViewModel(),
        onViewModelReady: (viewModel) {
          _viewModel = viewModel..init();
        },
        builder: (context, viewModel, appbar) {
          return Scaffold(
              appBar: buildAppBar(
                  title: Text(
                    'Sản phẩm bán chạy',
                    style: TextStyle(color: Colors.black),
                    textAlign: TextAlign.center,
                  ),
                  elevation: 4),
              backgroundColor: Colors.grey.shade300,
              body: buildUi(context));
        });
  }

  @override
  Widget buildDesktop(BuildContext context) {
    return _buildUi(context);
  }

  @override
  Widget buildMobile(BuildContext context) {
    return _buildUi(context);
  }

  @override
  Widget buildTablet(BuildContext context) {
    return _buildUi(context);
  }

  Widget _buildUi(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 20, right: 20, left: 20),
      child: SingleChildScrollView(
        child: GridviewWidget(
          listWidget: widget.listItem,
          isShowMore: false,
          maxItemShow: 1000,
          functionShowmore: () {},
          childAspectRatio:
              (MediaQuery.of(context).size.width * 0.65 / 392) - 0.02,
          heightChild: 280,
          crossAxisSpacing: 10,
          mainAxisSpacing: 10,
          crossAxisCount: 2,
        ),
      ),
    );
  }
}
