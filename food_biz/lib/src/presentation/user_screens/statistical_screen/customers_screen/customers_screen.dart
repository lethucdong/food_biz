import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/configs.dart';
import 'package:flutter_app/src/utils/utils.dart';

import '../../../presentation.dart';

class CustomersScreen extends StatefulWidget {
  @override
  _CustomersScreenState createState() => _CustomersScreenState();
}

class _CustomersScreenState extends State<CustomersScreen>
    with ResponsiveWidget {
  late CustomersViewModel _viewModel;
  @override
  Widget build(BuildContext context) {
    return BaseWidget<CustomersViewModel>(
        viewModel: CustomersViewModel(),
        onViewModelReady: (viewModel) {
          _viewModel = viewModel..init();
        },
        builder: (context, viewModel, appbar) {
          return Scaffold(
              appBar: buildAppBar(
                  title: Text(
                    'Thống kê khách hàng',
                    style: TextStyle(color: Colors.black),
                    textAlign: TextAlign.center,
                  ),
                  elevation: 4),
              backgroundColor: Colors.grey.shade300,
              body: buildUi(context));
        });
  }

  @override
  Widget buildDesktop(BuildContext context) {
    return _buildUi(context);
  }

  @override
  Widget buildMobile(BuildContext context) {
    return _buildUi(context);
  }

  @override
  Widget buildTablet(BuildContext context) {
    return _buildUi(context);
  }

  Widget _buildUi(BuildContext context) {
    return Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Column(
          children: [
            Container(
              height: 50,
              color: Colors.white,
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text('Tổng số khách hàng'),
                  Text(
                    '10 khách hàng',
                    style: TextStyle(
                      color: Colors.deepOrange,
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              child: Container(
                width: double.infinity,
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 20),
                      child: Text('Thông tin khách hàng'),
                    ),
                    Expanded(
                      child: SingleChildScrollView(
                        child: _buildListCustomer(),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ));
  }

  Widget _buildListCustomer() {
    return Column(
      children: [
        _buildCustomerCard(),
        _buildCustomerCard(),
        _buildCustomerCard(),
        _buildCustomerCard(),
        _buildCustomerCard(),
        _buildCustomerCard(),
        _buildCustomerCard(),
        _buildCustomerCard(),
      ],
    );
  }

  Widget _buildCustomerCard() {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.only(bottom: 20),
      height: 120,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
      ),
      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text('Trương Huyền'),
          Text(
            '0364 111 999',
            style: TextStyle(color: Colors.grey.shade500),
          ),
          Text('120 Nguyễn Trãi'),
          Text('Thanh Xuân, Hà Nội'),
        ],
      ),
    );
  }
}
