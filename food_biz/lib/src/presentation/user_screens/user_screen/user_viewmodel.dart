import '../../presentation.dart';
import 'package:get/get.dart';

class UserViewModel extends BaseViewModel {
  init() async {}

  void onTapSetting() {
    Get.toNamed(Routers.profile);
  }

  void onTapRankAccount() {
    Get.toNamed(Routers.rank_account);
  }

  void onTapReconciliation() {
    Get.toNamed(Routers.reconciliation);
  }

  void onTapFobiCoin() {
    Get.toNamed(Routers.fobi_coin);
  }

  void onTapStatistical() {
    Get.toNamed(Routers.statistical);
  }

  void onTapVouchersStorage() {
    Get.toNamed(Routers.voucher_storage);
  }
}
