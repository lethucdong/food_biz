import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_app/src/configs/configs.dart';
import 'package:flutter_app/src/utils/app_utils.dart';

import '../../presentation.dart';

class UserScreen extends StatefulWidget {
  @override
  _UserScreenState createState() => _UserScreenState();
}

class _UserScreenState extends State<UserScreen> with ResponsiveWidget {
  late UserViewModel _viewModel;
  @override
  @override
  Widget build(BuildContext context) {
    return BaseWidget<UserViewModel>(
        viewModel: UserViewModel(),
        onViewModelReady: (viewModel) {
          _viewModel = viewModel..init();
        },
        builder: (context, viewModel, appbar) {
          return Scaffold(
            backgroundColor: Colors.grey.shade300,
            body: buildUi(context),
          );
        });
  }

  @override
  Widget buildDesktop(BuildContext context) {
    return _buildUi(context);
  }

  @override
  Widget buildMobile(BuildContext context) {
    return _buildUi(context);
  }

  @override
  Widget buildTablet(BuildContext context) {
    return _buildUi(context);
  }

  Widget _buildUi(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          _buildInfo(),
          _buildListCate(),
          _buildListItem(),
          _buildContact(),
          Text('Phiên bản 0.0.1'),
          SizedBox(height: 10),
        ],
      ),
    );
  }

  Widget _buildInfo() {
    return Container(
      padding: EdgeInsets.only(top: 50, left: 20, right: 20),
      height: 250,
      decoration: BoxDecoration(
          color: Colors.red,
          image: DecorationImage(
              image: AssetImage(AppImages.imgBgOrange), fit: BoxFit.cover)),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Image.asset(
                AppImages.iconSlideMenu,
                color: Colors.white,
              ),
              Text(
                'Tài khoản',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 18,
                ),
              ),
              GestureDetector(
                onTap: () {
                  _viewModel.onTapSetting();
                },
                child: Image.asset(AppImages.iconSetting),
              ),
            ],
          ),
          SizedBox(height: 30),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                child: Row(
                  children: [
                    Container(
                      width: 40,
                      height: 40,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: Colors.black,
                        image: DecorationImage(
                            image: AssetImage(AppImages.iconAvatar),
                            fit: BoxFit.scaleDown),
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Column(
                      children: [
                        Text(
                          'Trương Huyền',
                          style: TextStyle(color: Colors.white, fontSize: 16),
                        ),
                        Container(
                          padding: EdgeInsets.all(5),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            color: Colors.white,
                          ),
                          child: Row(
                            children: [
                              Image.asset(
                                AppImages.iconRank2,
                                scale: 3,
                              ),
                              SizedBox(width: 15),
                              Text('Rank Vàng'),
                              SizedBox(width: 15),
                              Image.asset(AppImages.iconRightArrow),
                            ],
                          ),
                        )
                      ],
                    )
                  ],
                ),
              ),
              Text(
                'Đại lý',
                style: TextStyle(color: Colors.white),
              )
            ],
          ),
          SizedBox(
            height: 10,
          ),
          Expanded(
            child: Container(
              margin: EdgeInsets.only(bottom: 10),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
              ),
              child: Column(
                children: [
                  Expanded(
                    child: Container(
                      padding: EdgeInsets.only(left: 10),
                      width: double.infinity,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Text('Doanh thu'),
                          Text(
                            AppUtils.numToCurencyString(0),
                            style: TextStyle(color: Colors.deepOrange),
                          ),
                        ],
                      ),
                    ),
                  ),
                  LineSpace(),
                  Expanded(
                    child: Row(
                      children: [
                        Expanded(
                          child: Container(
                            padding: EdgeInsets.symmetric(horizontal: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        Image.asset(
                                          AppImages.iconNavOrder,
                                          scale: 3,
                                          color: Colors.deepOrange,
                                        ),
                                        Text(
                                          '   Đơn hàng',
                                          style: TextStyle(
                                              color: Colors.deepOrange),
                                        )
                                      ],
                                    ),
                                    Text(
                                      AppUtils.numToCurencyString(0),
                                    ),
                                  ],
                                ),
                                Image.asset(AppImages.iconRightArrow),
                              ],
                            ),
                          ),
                        ),
                        Container(
                          width: 2,
                          color: Colors.grey.shade300,
                        ),
                        Expanded(
                          child: Container(
                            padding: EdgeInsets.symmetric(horizontal: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        Image.asset(
                                          AppImages.iconDollar,
                                        ),
                                        Text(
                                          '   Thu nhập',
                                          style: TextStyle(
                                              color: Colors.deepOrange),
                                        )
                                      ],
                                    ),
                                    Text(
                                      AppUtils.numToCurencyString(0),
                                    ),
                                  ],
                                ),
                                Image.asset(AppImages.iconRightArrow),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _buildListCate() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 10, top: 10, bottom: 20),
            child: Text('Tài khoản của tôi'),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              _listCateCard(
                imgUrl: AppImages.iconRedHeart,
                firstLabel: 'Đối soát',
                secondLabel: '0 đ',
                bgColor: Color.fromRGBO(242, 206, 203, 1),
                onTap: () {
                  _viewModel.onTapReconciliation();
                },
              ),
              _listCateCard(
                imgUrl: AppImages.iconCoin3d,
                firstLabel: 'Fobi Xu',
                secondLabel: '0 đXu',
                bgColor: Color.fromRGBO(244, 228, 206, 1),
                onTap: () {
                  _viewModel.onTapFobiCoin();
                },
              ),
              _listCateCard(
                  imgUrl: AppImages.iconThreeup,
                  firstLabel: 'Hạng',
                  secondLabel: 'Vàng',
                  bgColor: Color.fromRGBO(194, 222, 244, 1),
                  onTap: () {
                    _viewModel.onTapRankAccount();
                  }),
              _listCateCard(
                imgUrl: AppImages.iconPortrait,
                firstLabel: 'Loại TK',
                secondLabel: 'Đại lý',
                bgColor: Color.fromRGBO(202, 231, 214, 1),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget _listCateCard(
      {required String imgUrl,
      required String firstLabel,
      required String secondLabel,
      required Color bgColor,
      Function? onTap}) {
    return GestureDetector(
      onTap: () {
        onTap!();
      },
      child: Container(
        height: 94,
        width: 60,
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.all(10),
              width: 46,
              height: 46,
              decoration: BoxDecoration(
                color: bgColor,
                borderRadius: BorderRadius.circular(10),
              ),
              child: Image.asset(imgUrl),
            ),
            Text(
              firstLabel,
              overflow: TextOverflow.clip,
              softWrap: true,
              style: TextStyle(fontSize: 14),
            ),
            Text(
              secondLabel,
              overflow: TextOverflow.clip,
              softWrap: true,
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildListItem() {
    return Container(
      margin: EdgeInsets.all(20),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
      ),
      child: Column(
        children: [
          buidListItemCard(
            title: 'Báo cáo bán hàng',
            imgUrl: AppImages.iconMonthReport,
            function: () {
              _viewModel.onTapStatistical();
            },
          ),
          buidListItemCard(
            title: 'Danh sách khách hàng',
            imgUrl: AppImages.iconListCustomer,
            function: () {},
          ),
          buidListItemCard(
            title: 'Kho voucher',
            imgUrl: AppImages.iconVoucher,
            function: () {
              _viewModel.onTapVouchersStorage();
            },
          ),
          buidListItemCard(
            title: 'Hợp đồng với đại lý hệ thống',
            imgUrl: AppImages.iconListCustomer,
            function: () {},
          ),
          buidListItemCard(
            title: 'Đánh giá của tôi',
            imgUrl: AppImages.iconListCustomer,
            function: () {},
          ),
          buidListItemCard(
            title: 'Chính sách Foodbiz',
            imgUrl: AppImages.iconListCustomer,
            function: () {},
          ),
          buidListItemCard(
            title: 'Trợ giúp',
            imgUrl: AppImages.iconListCustomer,
            function: () {},
          ),
        ],
      ),
    );
  }

  Widget buidListItemCard(
      {required String imgUrl,
      required String title,
      required Function function}) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: GestureDetector(
            onTap: () {
              function();
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  padding: EdgeInsets.symmetric(vertical: 20),
                  child: Row(
                    children: [
                      Image.asset(imgUrl),
                      SizedBox(width: 10),
                      Text(
                        title,
                        style: TextStyle(fontSize: 16),
                      ),
                    ],
                  ),
                ),
                Image.asset(AppImages.iconRightArrow)
              ],
            ),
          ),
        ),
        LineSpace(),
      ],
    );
  }

  Widget _buildContact() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        children: [
          _buildContactCard(
            function: () {},
            title: 'Hotline: 1900 1900',
            description:
                'Mọi thông tin chi tiết xin vui lòng liên hệ vào hotline của chúng tôi.',
            imgUrl: AppImages.imgCenter_bro,
            toDo: 'Gọi ngay',
            bgColor: Colors.green.shade800,
          ),
          _buildContactCard(
              function: () {},
              title: 'Chat với chúng tôi',
              description: 'Chat với chúng tôi qua Zalo hoặc messenger',
              imgUrl: AppImages.imgChat_bro,
              toDo: 'Liên hệ',
              bgColor: Colors.purple.shade800),
        ],
      ),
    );
  }

  Widget _buildContactCard({
    required Function function,
    required String title,
    required String description,
    required String toDo,
    required String imgUrl,
    required Color bgColor,
  }) {
    return Container(
      height: 155,
      width: double.infinity,
      padding: EdgeInsets.only(left: 10, right: 20),
      margin: EdgeInsets.symmetric(vertical: 10),
      decoration: BoxDecoration(
        color: bgColor,
        borderRadius: BorderRadius.circular(10),
        image: DecorationImage(
          image: AssetImage(AppImages.imgEffect),
          fit: BoxFit.cover,
        ),
      ),
      child: Row(
        children: [
          Expanded(
            flex: 5,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 20,
                ),
                Text(
                  title,
                  style: TextStyle(fontSize: 20, color: Colors.white),
                ),
                SizedBox(
                  height: 5,
                ),
                Expanded(
                  child: Text(
                    description,
                    softWrap: true,
                    overflow: TextOverflow.clip,
                    style: TextStyle(fontSize: 14, color: Colors.white),
                  ),
                ),
                Container(
                  height: 40,
                  margin: EdgeInsets.only(right: 20),
                  width: double.infinity,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.white),
                  alignment: Alignment.center,
                  child: Text(
                    toDo,
                    style: TextStyle(fontSize: 20, color: Colors.black),
                  ),
                ),
                SizedBox(
                  height: 5,
                ),
              ],
            ),
          ),
          Expanded(
            flex: 3,
            child: Image.asset(imgUrl),
          ),
        ],
      ),
    );
  }
}
