import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/configs.dart';
import 'package:provider/src/provider.dart';

import '../../../presentation.dart';

class VoucherHistoryScreen extends StatefulWidget {
  const VoucherHistoryScreen({Key? key}) : super(key: key);

  @override
  _VoucherHistoryScreenState createState() => _VoucherHistoryScreenState();
}

class _VoucherHistoryScreenState extends State<VoucherHistoryScreen>
    with ResponsiveWidget {
  late VoucherHistoryViewModel _viewModel;
  late Size _screenSize;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _screenSize = MediaQuery.of(context).size;
    return BaseWidget<VoucherHistoryViewModel>(
        viewModel: VoucherHistoryViewModel(),
        onViewModelReady: (viewModel) {
          _viewModel = viewModel..init();
        },
        builder: (context, viewModel, appbar) {
          return Scaffold(
              appBar: buildAppBar(
                title: Text(
                  'Lịch sử',
                  style: TextStyle(color: Colors.black),
                  textAlign: TextAlign.center,
                ),
                elevation: 2,
              ),
              backgroundColor: Colors.grey.shade300,
              body: buildUi(context));
        });
  }

  @override
  Widget buildDesktop(BuildContext context) {
    return _buildUi(context);
  }

  @override
  Widget buildMobile(BuildContext context) {
    return _buildUi(context);
  }

  @override
  Widget buildTablet(BuildContext context) {
    return _buildUi(context);
  }

  Widget _buildUi(BuildContext context) {
    return Container(
      height: _screenSize.height - 80,
      child: Column(
        children: [
          _buildTabControl(context),
          Expanded(
            child: Container(
                color: Colors.white, child: _buildTabContent(context)),
          ),
        ],
      ),
    );
  }

  Widget _buildTabControl(BuildContext context) {
    return Container(
      height: 50,
      padding: EdgeInsets.only(top: 10),
      color: Colors.white,
      child: Row(
        children: [
          Expanded(
            child: GestureDetector(
              onTap: () {
                // _viewModel.pageController.jumpToPage(0);

                Provider.of<VoucherHistoryViewModel>(context, listen: false)
                    .updateTabController(0);
              },
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(),
                  Container(
                    // color: Colors.red,
                    alignment: Alignment.center,
                    child: Text('Hết hiệu lực'),
                  ),
                  Container(
                    height: 2,
                    color: context
                                .watch<VoucherHistoryViewModel>()
                                .curentPageIndex ==
                            0
                        ? Colors.deepOrange
                        : Colors.grey.shade400,
                  ),
                ],
              ),
            ),
          ),
          Expanded(
            child: GestureDetector(
              onTap: () {
                // _viewModel.pageController.jumpToPage(1);
                Provider.of<VoucherHistoryViewModel>(context, listen: false)
                    .updateTabController(1);
              },
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(),
                  Container(
                    // color: Colors.red,
                    alignment: Alignment.center,
                    child: Text('Đã sử dụng'),
                  ),
                  Container(
                    height: 2,
                    color: context
                                .watch<VoucherHistoryViewModel>()
                                .curentPageIndex !=
                            0
                        ? Colors.deepOrange
                        : Colors.grey.shade400,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildTabContent(BuildContext context) {
    return PageView(
      physics: NeverScrollableScrollPhysics(),
      scrollDirection: Axis.horizontal,
      onPageChanged: (value) {
        // _viewModel.updateTabController(value);

        Provider.of<VoucherHistoryViewModel>(context, listen: false)
            .updateTabController(value);
      },
      controller: _viewModel.pageController,
      children: <Widget>[
        SingleChildScrollView(child: _buildContainer()),
        SingleChildScrollView(child: _buildContainer()),
      ],
    );
  }

  _buildContainer() {
    return Container(
      padding: EdgeInsets.all(20),
      color: Colors.white,
      child: Column(
        children: [
          _buildCardItem(),
          _buildCardItem(),
          _buildCardItem(),
          _buildCardItem(),
          _buildCardItem(),
          _buildCardItem(),
        ],
      ),
    );
  }

  Widget _buildCardItem() {
    return GestureDetector(
      onTap: () {},
      child: Container(
        padding: EdgeInsets.only(top: 10),
        child: Column(
          children: [
            Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      width: 67,
                      height: 67,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        border: Border.all(
                          color: Colors.grey,
                        ),
                        image: DecorationImage(
                            image: AssetImage(
                              AppImages.imgVoucherActive,
                            ),
                            fit: BoxFit.cover),
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Expanded(
                        child: Column(
                      // crossAxisAlignment: CrossAxisAlignment.start,
                      // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Expanded(
                              child: Text(
                                  'Giảm giá tới 50% cho đơn hàng 1 triệu đ. Tối đa 500.000 đ'),
                            ),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text(
                                  _viewModel.curentPageIndex == 0
                                      ? 'Hết hiệu lực'
                                      : 'Đã sử dụng',
                                  style: TextStyle(color: Colors.red),
                                )
                              ],
                            )
                          ],
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: Text('HSD: 20/8/2021'),
                            ),
                            GestureDetector(
                              child: Text(
                                'Điều kiện',
                                textAlign: TextAlign.end,
                                style: TextStyle(color: Colors.blue.shade800),
                              ),
                            )
                          ],
                        )
                      ],
                    ))
                  ],
                ),
              ],
            ),
            SizedBox(height: 10),
            LineSpace(),
          ],
        ),
      ),
    );
  }
}
