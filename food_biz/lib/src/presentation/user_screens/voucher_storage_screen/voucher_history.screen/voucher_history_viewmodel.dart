import '../../../presentation.dart';
import 'package:get/get.dart';

class VoucherHistoryViewModel extends BaseViewModel {
  late PageController pageController;
  int curentPageIndex = 0;

  init() async {
    pageController = PageController(
      initialPage: curentPageIndex,
      viewportFraction: 1,
      keepPage: true,
    );
  }

  updateTabController(int tab) {
    pageController.jumpToPage(tab);
    curentPageIndex = tab;
    notifyListeners();
  }
}
