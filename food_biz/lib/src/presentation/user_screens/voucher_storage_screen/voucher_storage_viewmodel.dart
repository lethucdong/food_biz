import '../../presentation.dart';
import 'package:get/get.dart';

class VoucherStorageViewModel extends BaseViewModel {
  init() async {}
  int menuSelectedIndex = 0;
  int typeSelectedIndex = 0;
  void onTapVoucherStorage() {
    return null;
  }

  onTapMenu(int tabIndex) {
    menuSelectedIndex = tabIndex;
    notifyListeners();
  }

  onTapMenuType(int tabIndex) {
    typeSelectedIndex = tabIndex;
    notifyListeners();
  }

  void onTabHistory() {
    Get.toNamed(Routers.voucher_history);
  }
}
