import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/configs.dart';
import 'package:flutter_app/src/utils/utils.dart';
import 'package:get/get.dart';
import 'package:provider/src/provider.dart';

import '../../presentation.dart';

class VoucherStorageScreen extends StatefulWidget {
  @override
  _VoucherStorageScreenState createState() => _VoucherStorageScreenState();
}

class _VoucherStorageScreenState extends State<VoucherStorageScreen>
    with ResponsiveWidget {
  late VoucherStorageViewModel _viewModel;
  @override
  Widget build(BuildContext context) {
    return BaseWidget<VoucherStorageViewModel>(
        viewModel: VoucherStorageViewModel(),
        onViewModelReady: (viewModel) {
          _viewModel = viewModel..init();
        },
        builder: (context, viewModel, appbar) {
          return Scaffold(
              appBar: buildAppBar(
                title: Text(
                  'Kho voucher',
                  style: TextStyle(color: Colors.black),
                  textAlign: TextAlign.center,
                ),
                action: GestureDetector(
                  onTap: () {
                    _viewModel.onTabHistory();
                  },
                  child: Text(
                    'Lịch sử',
                    style: TextStyle(color: Colors.black),
                  ),
                ),
              ),
              backgroundColor: Colors.grey.shade300,
              body: buildUi(context));
        });
  }

  @override
  Widget buildDesktop(BuildContext context) {
    return _buildUi(context);
  }

  @override
  Widget buildMobile(BuildContext context) {
    return _buildUi(context);
  }

  @override
  Widget buildTablet(BuildContext context) {
    return _buildUi(context);
  }

  Widget _buildUi(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 5),
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      child: Column(
        children: [
          _buildMenu(context),
          _buildToolButton(),
          Expanded(child: _buildListVoucher(context)),
        ],
      ),
    );
  }

  Widget _buildMenu(BuildContext context) {
    List<String> listItemMenu = [
      'Tất cả',
      'Miễn phí vận chuyển',
      'Giảm giá đơn hàng',
      'Hoàn fobiXu'
    ];
    List<String> listItemType = [
      'Mới nhất',
      'Phổ biến',
      'Sắp hết hạn',
    ];
    return Column(
      children: [
        Container(
          // padding: EdgeInsets.symmetric(horizontal: 10),
          color: Colors.white,
          height: 40,
          // width: double.infinity,
          child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: listItemMenu.length,
              itemBuilder: (BuildContext context, int index) {
                return GestureDetector(
                  onTap: () {
                    Provider.of<VoucherStorageViewModel>(context, listen: false)
                        .onTapMenu(index);
                  },
                  child: Container(
                    decoration: BoxDecoration(
                      border: Border(
                        bottom: index ==
                                context
                                    .watch<VoucherStorageViewModel>()
                                    .menuSelectedIndex
                            ? BorderSide(width: 2.0, color: Colors.deepOrange)
                            : BorderSide(
                                width: 2.0, color: Colors.grey.shade300),
                      ),
                    ),
                    child: FittedBox(
                      fit: BoxFit.scaleDown,
                      child: Container(
                        padding: EdgeInsets.symmetric(
                            horizontal:
                                index == 0 || index == listItemMenu.length - 1
                                    ? 20
                                    : 10),
                        child: Text(
                          listItemMenu[index],
                          style: TextStyle(fontSize: 14),
                        ),
                      ),
                    ),
                  ),
                );
              }),
        ),
        Container(
          color: Colors.white,
          height: 60,
          padding: EdgeInsets.symmetric(horizontal: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              for (int i = 0; i < 3; i++)
                Expanded(
                  child: GestureDetector(
                    onTap: () {
                      Provider.of<VoucherStorageViewModel>(context,
                              listen: false)
                          .onTapMenuType(i);
                    },
                    child: Container(
                      margin: EdgeInsets.symmetric(horizontal: 10),
                      alignment: Alignment.center,
                      child: Stack(
                        children: [
                          Container(
                            height: 42,
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              color: Colors.grey.shade200,
                              border: i ==
                                      context
                                          .watch<VoucherStorageViewModel>()
                                          .typeSelectedIndex
                                  ? Border.all(color: Colors.deepOrange)
                                  : null,
                            ),
                            child: Text(
                              listItemType[i],
                              softWrap: true,
                              overflow: TextOverflow.clip,
                            ),
                          ),
                          i ==
                                  context
                                      .watch<VoucherStorageViewModel>()
                                      .typeSelectedIndex
                              ? Positioned(
                                  child: Image.asset(AppImages.iconCheckRec),
                                  top: 0,
                                  left: 0,
                                )
                              : Positioned(
                                  top: 0,
                                  left: 0,
                                  child: Container(),
                                )
                        ],
                      ),
                    ),
                  ),
                ),
            ],
          ),
        ),
      ],
    );
  }

  Widget _buildToolButton() {
    return Container(
      margin: EdgeInsets.all(20),
      height: 45,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(5),
      ),
      child: Row(
        children: [
          Expanded(
            child: GestureDetector(
              onTap: () => {
                showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return Scaffold(
                        appBar: buildAppBar(
                            title: Text(
                              'Kho voucher',
                              style: TextStyle(color: Colors.black),
                              textAlign: TextAlign.center,
                            ),
                            action: GestureDetector(
                              child: Text(
                                'Lịch sử',
                                style: TextStyle(color: Colors.black),
                              ),
                            ),
                            elevation: 4),
                        backgroundColor: Colors.transparent,
                        body: Container(
                          margin: EdgeInsets.only(top: 5),
                          width: MediaQuery.of(context).size.width,
                          height: MediaQuery.of(context).size.height,
                          color: Colors.transparent,
                          child: Column(
                            children: [
                              Container(
                                height: 75,
                                color: Colors.white,
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 20),
                                child: _buildInputCode(),
                              ),
                              Expanded(
                                child: GestureDetector(
                                    onTap: () {
                                      Get.back();
                                    },
                                    child: Container(
                                      color: Colors.transparent,
                                    )),
                              ),
                            ],
                          ),
                        ),
                      );
                    }),
              },
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Image.asset(AppImages.iconTextInput),
                    SizedBox(
                      width: 5,
                    ),
                    Expanded(
                      child: Text(
                        'Nhập mã voucher',
                        softWrap: true,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Container(
            width: 1,
            color: Colors.grey.shade300,
          ),
          Expanded(
            child: GestureDetector(
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Image.asset(
                      AppImages.iconTicket,
                      scale: 4,
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Expanded(
                        child: Text(
                      'Tìm thêm voucher',
                      softWrap: true,
                    )),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildListVoucher(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(20),
      color: Colors.white,
      child: Column(
        children: [
          _buildCardItem(context),
        ],
      ),
    );
  }

  Widget _buildCardItem(BuildContext context) {
    return GestureDetector(
      onTap: () {},
      child: Container(
        padding: EdgeInsets.only(top: 10),
        child: Column(
          children: [
            Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      width: 67,
                      height: 67,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        border: Border.all(
                          color: Colors.grey,
                        ),
                        image: DecorationImage(
                            image: AssetImage(
                              AppImages.imgVoucherActive,
                            ),
                            fit: BoxFit.cover),
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Expanded(
                        child: Column(
                      // crossAxisAlignment: CrossAxisAlignment.start,
                      // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Expanded(
                              child: Text(
                                  'Giảm giá tới 50% cho đơn hàng 1 triệu đ. Tối đa 500.000 đ'),
                            ),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text(
                                  'Dùng ngay ',
                                  style: TextStyle(color: Colors.deepOrange),
                                ),
                                Image.asset(
                                  AppImages.iconRightArrow,
                                  color: Colors.deepOrange,
                                )
                              ],
                            )
                          ],
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Row(
                          children: [
                            Expanded(
                              // child: Text(context
                              //             .watch<VoucherStorageViewModel>()
                              //             .typeSelectedIndex ==
                              //         2
                              //     ? 'Sắp hết hạn:   Còn 1 ngày'
                              //     : 'HSD:   20/8/2021'),
                              child: context
                                          .watch<VoucherStorageViewModel>()
                                          .typeSelectedIndex ==
                                      2
                                  ? Text(
                                      'Sắp hết hạn:   Còn 1 ngày',
                                      style: TextStyle(color: Colors.red),
                                    )
                                  : Text('HSD:   20/8/2021'),
                            ),
                            GestureDetector(
                              child: Text(
                                'Điều kiện',
                                textAlign: TextAlign.end,
                                style: TextStyle(color: Colors.blue.shade800),
                              ),
                            )
                          ],
                        )
                      ],
                    ))
                  ],
                ),
              ],
            ),
            SizedBox(height: 5),
            LineSpace(),
          ],
        ),
      ),
    );
  }

  Widget _buildInputCode() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Expanded(
          child: Container(
            decoration: BoxDecoration(
              border: Border.all(color: Colors.grey.shade300),
              color: Colors.grey.shade200,
              borderRadius: BorderRadius.circular(10),
            ),
            child: TextField(
                decoration: InputDecoration(
                  isDense: true,
                  hintText: 'Nhập mã Fobi voucher',
                  border: InputBorder.none,
                  contentPadding:
                      EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                ),
                keyboardType: TextInputType.number),
          ),
        ),
        SizedBox(
          width: 10,
        ),
        GestureDetector(
          onTap: () {
            Get.back();
          },
          child: Container(
              width: 79,
              height: 48,
              decoration: BoxDecoration(
                color: Colors.grey.shade200,
                borderRadius: BorderRadius.circular(10),
                border: Border.all(color: Colors.grey.shade300),
              ),
              alignment: Alignment.center,
              child: Text('Áp dụng')),
        )
      ],
    );
  }
}
