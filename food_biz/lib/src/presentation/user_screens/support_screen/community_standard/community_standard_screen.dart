import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/configs.dart';

import '../../../presentation.dart';

class CommunityStandardScreen extends StatefulWidget {
  @override
  _CommunityStandardScreenState createState() =>
      _CommunityStandardScreenState();
}

class _CommunityStandardScreenState extends State<CommunityStandardScreen>
    with ResponsiveWidget {
  late CommunityStandardViewModel _viewModel;
  @override
  Widget build(BuildContext context) {
    return BaseWidget<CommunityStandardViewModel>(
        viewModel: CommunityStandardViewModel(),
        onViewModelReady: (viewModel) {
          _viewModel = viewModel..init();
        },
        builder: (context, viewModel, appbar) {
          return Scaffold(
              appBar: buildAppBar(
                  title: Text(
                    'Tiêu chuẩn cộng đồng',
                    style: TextStyle(color: Colors.black),
                    textAlign: TextAlign.center,
                  ),
                  elevation: 2),
              backgroundColor: Colors.white,
              body: buildUi(context));
        });
  }

  @override
  Widget buildDesktop(BuildContext context) {
    return _buildUi(context);
  }

  @override
  Widget buildMobile(BuildContext context) {
    return _buildUi(context);
  }

  @override
  Widget buildTablet(BuildContext context) {
    return _buildUi(context);
  }

  Widget _buildUi(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        color: Colors.white,
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
        child: Column(
          children: [
            _buildListStep(),
          ],
        ),
      ),
    );
  }

  Widget _buildStep(
      {required int step, required String title, required String content}) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 10),
      child: Column(
        children: [
          Row(
            children: [
              Container(
                width: 15,
                height: 15,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.deepOrange,
                ),
                alignment: Alignment.center,
                child: Text(
                  '$step',
                  style: TextStyle(color: Colors.white, fontSize: 10),
                ),
              ),
              SizedBox(
                width: 10,
              ),
              Text(
                title,
                style: TextStyle(fontSize: 16),
              ),
            ],
          ),
          SizedBox(
            height: 10,
          ),
          Text(content)
        ],
      ),
    );
  }

  Widget _buildListStep() {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 10),
      child: Column(
        children: [
          _buildStep(
            step: 1,
            title: 'An toàn',
            content:
                "Chúng tôi thực sự quan tâm đến sự an toàn của những người sử dụng các ứng dụng mà chúng tôi tạo ra. Chúng tôi thường hỏi ý kiến của các chuyên gia về hành vi tự tử và tự gây thương tích để từ đó xây dựng chính sách và đưa ra các biện pháp thực thi, đồng thời hợp tác với các tổ chức trên toàn cầu để hỗ trợ những người đang lâm vào cảnh đau buồn.",
          ),
          _buildStep(
            step: 2,
            title: 'Tính toàn vẹn và tính xác thực',
            content:
                "Tính xác thực là nền tảng trong cộng đồng của chúng tôi. Chúng tôi tin rằng tính xác thực góp phần tạo ra một cộng đồng nơi mọi người chịu trách nhiệm với nhau và với Fb theo những cách có ý nghĩa.",
          ),
          _buildStep(
              step: 3,
              title: 'Yêu cầu của người dùng',
              content:
                  "Chúng tôi tuân thủ:\nYêu cầu của người dùng về việc xóa tài khoản của chính họ\nYêu cầu của thành viên gia đình trực hệ đã được xác minh hoặc người thi hành di chúc về việc xóa tài khoản của người dùng đã mất\nYêu cầu của đại diện được ủy quyền về việc xóa tài khoản của người dùng không đủ tư cách"),
        ],
      ),
    );
  }

  Widget _buildFooter() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 40, vertical: 15),
      height: 65,
      child: Column(
        children: [
          Flexible(
              child: RichText(
            textAlign: TextAlign.left,
            text: TextSpan(
              children: <TextSpan>[
                TextSpan(
                  text: 'Nhấn đặt hàng đồng nghĩa bạn đã đồng ý tuân theo ',
                  style: TextStyle(color: Colors.black),
                ),
                TextSpan(
                  text: 'điều khoản Foodbiz',
                  style: TextStyle(color: Colors.deepOrange),
                ),
              ],
            ),
          ))
        ],
      ),
    );
  }
}
