import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/configs.dart';

import '../../presentation.dart';

class SupportScreen extends StatefulWidget {
  @override
  _SupportScreenState createState() => _SupportScreenState();
}

class _SupportScreenState extends State<SupportScreen> with ResponsiveWidget {
  late SupportViewModel _viewModel;
  @override
  Widget build(BuildContext context) {
    return BaseWidget<SupportViewModel>(
        viewModel: SupportViewModel(),
        onViewModelReady: (viewModel) {
          _viewModel = viewModel..init();
        },
        builder: (context, viewModel, appbar) {
          return Scaffold(
              appBar: buildAppBar(
                  title: Text(
                    'Hỗ trợ',
                    style: TextStyle(color: Colors.black),
                    textAlign: TextAlign.center,
                  ),
                  elevation: 2),
              backgroundColor: Colors.grey.shade300,
              body: buildUi(context));
        });
  }

  @override
  Widget buildDesktop(BuildContext context) {
    return _buildUi(context);
  }

  @override
  Widget buildMobile(BuildContext context) {
    return _buildUi(context);
  }

  @override
  Widget buildTablet(BuildContext context) {
    return _buildUi(context);
  }

  Widget _buildUi(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(20),
      child: Column(
        children: [
          _buildToolCard(
              imgUrl: AppImages.iconHeadphones, title: 'Trung tâm hỗ trợ'),
          _buildToolCard(
              imgUrl: AppImages.iconUsers,
              title: 'Tiêu chuẩn cộng đồng',
              onTap: () {
                _viewModel.onTapCommunityStandard();
              }),
          _buildToolCard(
              imgUrl: AppImages.iconInfoSp,
              title: 'Điều khoản và chính sách',
              onTap: () {
                _viewModel.onTapPolicy();
              }),
          _buildToolCard(
              imgUrl: AppImages.iconInterrogation,
              title: 'Góp ý',
              onTap: () {
                _viewModel.onTapFeedback();
              }),
          _buildToolCard(
              imgUrl: AppImages.iconTrash, title: 'Yêu cầu huỷ tài khoản'),
        ],
      ),
    );
  }

  Widget _buildToolCard(
      {required String imgUrl, required String title, Function? onTap}) {
    return GestureDetector(
      onTap: () {
        onTap!();
      },
      child: Container(
        margin: EdgeInsets.only(bottom: 10),
        padding: EdgeInsets.symmetric(horizontal: 10),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10), color: Colors.white),
        height: 67,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                Image.asset(
                  imgUrl,
                  color: Colors.deepOrange,
                ),
                SizedBox(
                  width: 15,
                ),
                Text(
                  title,
                  style: TextStyle(fontSize: 16),
                ),
              ],
            ),
            Image.asset(AppImages.iconRightArrow),
          ],
        ),
      ),
    );
  }
}
