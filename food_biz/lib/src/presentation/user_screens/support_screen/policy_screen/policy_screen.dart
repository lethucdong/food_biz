import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/configs.dart';

import '../../../presentation.dart';

class PolicyScreen extends StatefulWidget {
  @override
  _PolicyScreenState createState() => _PolicyScreenState();
}

class _PolicyScreenState extends State<PolicyScreen> with ResponsiveWidget {
  late PolicyViewModel _viewModel;
  @override
  Widget build(BuildContext context) {
    return BaseWidget<PolicyViewModel>(
        viewModel: PolicyViewModel(),
        onViewModelReady: (viewModel) {
          _viewModel = viewModel..init();
        },
        builder: (context, viewModel, appbar) {
          return Scaffold(
              appBar: buildAppBar(
                  title: Text(
                    'Điều khoản',
                    style: TextStyle(color: Colors.black),
                    textAlign: TextAlign.center,
                  ),
                  elevation: 2),
              backgroundColor: Colors.grey.shade300,
              body: buildUi(context));
        });
  }

  @override
  Widget buildDesktop(BuildContext context) {
    return _buildUi(context);
  }

  @override
  Widget buildMobile(BuildContext context) {
    return _buildUi(context);
  }

  @override
  Widget buildTablet(BuildContext context) {
    return _buildUi(context);
  }

  Widget _buildUi(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        color: Colors.white,
        // padding: EdgeInsets.all(20),
        child: Column(
          children: [
            _buildHeader(),
            _buildContent(),
            LineSpace(),
            _buildFooter(),
            Container(
              height: 45,
              color: Colors.deepOrange,
              alignment: Alignment.center,
              child: Text(
                'Đồng ý',
                style: TextStyle(color: Colors.white, fontSize: 16),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _buildHeader() {
    return Container(
      height: 122,
      // color: Colors.red,
      decoration: BoxDecoration(
        image: DecorationImage(
            image: AssetImage(AppImages.imgBgOrange), fit: BoxFit.cover),
      ),
      child: Stack(
        children: [
          Container(
            // color: Colors.blue,
            margin: EdgeInsets.all(20),
            decoration: BoxDecoration(
              border: Border.all(color: Colors.white),
            ),
            alignment: Alignment.center,
            child: Text(
              'Điều khoản',
              style: TextStyle(fontSize: 18, color: Colors.white),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildContent() {
    return Container(
      padding: EdgeInsets.all(20),
      child: Column(
        children: [
          Text(
            "Khi quý khách truy cập vào app của chúng tôi có nghĩa là quý khách đồng ý với các điều khoản này. App có quyền thay đổi, chỉnh sửa, thêm hoặc lược bỏ bất kỳ phần nào trong Quy định và Điều kiện sử dụng, vào bất cứ lúc nào. Các thay đổi có hiệu lực ngay khi được đăng trên app mà không cần thông báo trước. Và khi quý khách tiếp tục sử dụng app, sau khi các thay đổi về quy định và điều kiện được đăng tải, có nghĩa là quý khách chấp nhận với những thay đổi đó.Quý khách vui lòng kiểm tra thường xuyên để cập nhật những thay đổi của chúng tôi",
            textAlign: TextAlign.left,
          ),
          _buildListStep(),
        ],
      ),
    );
  }

  Widget _buildStep(
      {required int step, required String title, required String content}) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 10),
      child: Column(
        children: [
          Row(
            children: [
              Container(
                width: 15,
                height: 15,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.deepOrange,
                ),
                alignment: Alignment.center,
                child: Text(
                  '$step',
                  style: TextStyle(color: Colors.white, fontSize: 10),
                ),
              ),
              SizedBox(
                width: 10,
              ),
              Text(
                title,
                style: TextStyle(fontSize: 16),
              ),
            ],
          ),
          SizedBox(
            height: 10,
          ),
          Text(content)
        ],
      ),
    );
  }

  Widget _buildListStep() {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 10),
      child: Column(
        children: [
          _buildStep(
            step: 1,
            title: 'Hướng dẫn sử dụng app',
            content:
                "- Khi vào app của chúng tôi, người dùng tối thiểu phải 18 tuổi hoặc truy cập dưới sự giám sát của cha mẹ hay người giám hộ hợp pháp.\n- Chúng tôi cấp giấy phép sử dụng để bạn có thể mua sắm trên app trong khuôn khổ điều khoản và điều kiện sử dụng đã đề ra.\n- Nghiêm cấm sử dụng bất kỳ phần nào của app này với mục đích thương mại hoặc nhân danh bất kỳ đối tác thứ ba nào nếu không được chúng tôi cho phép bằng văn bản. Nếu vi phạm bất cứ điều nào trong đây, chúng tôi sẽ hủy giấy phép của bạn mà không cần báo trước.\n- Quý khách phải đăng ký tài khoản với thông tin xác thực về bản thân và phải cập nhật nếu có bất kỳ thay đổi nào. Mỗi người truy cập phải có trách nhiệm với mật khẩu, tài khoản và hoạt động của mình trên app. Hơn nữa, quý khách phải thông báo cho chúng tôi biết khi tài khoản bị truy cập trái phép. Chúng tôi không chịu bất kỳ trách nhiệm nào, dù trực tiếp hay gián tiếp, đối với những thiệt hại hoặc mất mát gây ra do quý khách không tuân thủ quy định.\n- Trong suốt quá trình đăng ký, quý khách đồng ý nhận email quảng cáo từ app. Sau đó, nếu không muốn tiếp tục nhận mail, quý khách có thể từ chối bằng cách nhấp vào đường link ở dưới cùng trong mọi email quảng cáo.",
          ),
          _buildStep(
              step: 2,
              title: 'Chấp nhận đơn hàng và giá cả',
              content:
                  "- Chúng tôi có quyền từ chối hoặc hủy đơn hàng của quý khách vì bất kỳ lý do gì vào bất kỳ lúc nào. Chúng tôi có thể hỏi thêm về số điện thoại và địa chỉ trước khi nhận đơn hàng.\n- Chúng tôi cam kết sẽ cung cấp thông tin giá cả chính xác nhất cho người tiêu dùng. Tuy nhiên, đôi lúc vẫn có sai sót xảy ra, ví dụ như trường hợp giá sản phẩm không hiển thị chính xác trên  app hoặc sai giá, tùy theo từng trường hợp chúng tôi sẽ liên hệ hướng dẫn hoặc thông báo hủy đơn hàng đó cho quý khách. Chúng tôi cũng có quyền từ chối hoặc hủy bỏ bất kỳ đơn hàng nào dù đơn hàng đó đã hay chưa được xác nhận hoặc đã bị thanh toán."),
          _buildStep(
              step: 3,
              title: 'Thương hiệu và bản quyền',
              content:
                  '- Mọi quyền sở hữu trí tuệ (đã đăng ký hoặc chưa đăng ký), nội dung thông tin và tất cả các thiết kế, văn bản, đồ họa, phần mềm, hình ảnh, video, âm nhạc, âm thanh, biên dịch phần mềm, mã nguồn và phần mềm cơ bản đều là tài sản của chúng tôi. Toàn bộ nội dung của app được bảo vệ bởi luật bản quyền của Việt Nam và các công ước quốc tế. Bản quyền đã được bảo lưu.'),
          _buildStep(
              step: 4,
              title: 'Quyền pháp lý',
              content:
                  '- Các điều kiện, điều khoản và nội dung của app này được điều chỉnh bởi luật pháp Việt Nam và Tòa án có thẩm quyền tại Việt Nam sẽ giải quyết bất kỳ tranh chấp nào phát sinh từ việc sử dụng trái phép app này.'),
          _buildStep(
              step: 5,
              title: 'Quy định về bảo mật',
              content:
                  "- App của chúng tôi coi trọng việc bảo mật thông tin và sử dụng các biện pháp tốt nhất bảo vệ thông tin và việc thanh toán của quý khách. Thông tin của quý khách trong quá trình thanh toán sẽ được mã hóa để đảm bảo an toàn. Sau khi quý khách hoàn thành quá trình đặt hàng, quý khách sẽ thoát khỏi chế độ an toàn.\n- Quý khách không được sử dụng bất kỳ chương trình, công cụ hay hình thức nào khác để can thiệp vào hệ thống hay làm thay đổi cấu trúc dữ liệu. App cũng nghiêm cấm việc phát tán, truyền bá hay cổ vũ cho bất kỳ hoạt động nào nhằm can thiệp, phá hoại hay xâm nhập vào dữ liệu của hệ thống. Cá nhân hay tổ chức vi phạm sẽ bị tước bỏ mọi quyền lợi cũng như sẽ bị truy tố trước pháp luật nếu cần thiết.\n- Mọi thông tin giao dịch sẽ được bảo mật nhưng trong trường hợp cơ quan pháp luật yêu cầu, chúng tôi sẽ buộc phải cung cấp những thông tin này cho các cơ quan pháp luật."),
          _buildStep(
            step: 6,
            title: 'Thay đổi, hủy bỏ giao dịch tại app',
            content:
                "Trong mọi trường hợp, khách hàng đều có quyền chấm dứt giao dịch nếu đã thực hiện các biện pháp sau đây:\n- Thông báo cho chúng tôi về việc hủy giao dịch qua đường dây nóng .......\n- Trả lại hàng hoá đã nhận nhưng chưa sử dụng hoặc hưởng bất kỳ lợi ích nào từ hàng hóa đó (theo quy định của chính sách đổi trả hàng).",
          )
        ],
      ),
    );
  }

  Widget _buildFooter() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 40, vertical: 15),
      height: 65,
      child: Column(
        children: [
          Flexible(
              child: RichText(
            textAlign: TextAlign.left,
            text: TextSpan(
              children: <TextSpan>[
                TextSpan(
                  text: 'Nhấn đặt hàng đồng nghĩa bạn đã đồng ý tuân theo ',
                  style: TextStyle(color: Colors.black),
                ),
                TextSpan(
                  text: 'điều khoản Foodbiz',
                  style: TextStyle(color: Colors.deepOrange),
                ),
              ],
            ),
          ))
        ],
      ),
    );
  }
}
