import '../../presentation.dart';
import 'package:get/get.dart';

class SupportViewModel extends BaseViewModel {
  init() async {}

  void onTapPolicy() {
    Get.toNamed(Routers.policy);
  }

  void onTapCommunityStandard() {
    Get.toNamed(Routers.community_standard);
  }

  void onTapFeedback() {
    Get.toNamed(Routers.feedback);
  }
}
