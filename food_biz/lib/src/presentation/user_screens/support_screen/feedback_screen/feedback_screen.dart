import 'package:flutter/material.dart';

import '../../../presentation.dart';

class FeedbackScreen extends StatefulWidget {
  const FeedbackScreen({Key? key}) : super(key: key);

  @override
  _FeedbackScreenState createState() => _FeedbackScreenState();
}

class _FeedbackScreenState extends State<FeedbackScreen> with ResponsiveWidget {
  late FeedbackViewModel _viewModel;
  @override
  Widget build(BuildContext context) {
    return BaseWidget<FeedbackViewModel>(
        viewModel: FeedbackViewModel(),
        onViewModelReady: (viewModel) {
          _viewModel = viewModel..init();
        },
        builder: (context, viewModel, appbar) {
          return Scaffold(
              appBar: buildAppBar(
                title: Text(
                  'Góp ý',
                  style: TextStyle(color: Colors.black),
                  textAlign: TextAlign.center,
                ),
                elevation: 2,
              ),
              backgroundColor: Colors.grey.shade300,
              body: buildUi(context));
        });
  }

  @override
  Widget buildDesktop(BuildContext context) {
    return _buildUi(context);
  }

  @override
  Widget buildMobile(BuildContext context) {
    return _buildUi(context);
  }

  @override
  Widget buildTablet(BuildContext context) {
    return _buildUi(context);
  }

  Widget _buildUi(BuildContext context) {
    return Container(
      color: Colors.white,
      height: MediaQuery.of(context).size.height - 80,
      width: MediaQuery.of(context).size.width,
      child: Column(
        children: [
          Expanded(
            child: Container(
              padding: EdgeInsets.all(20),
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    // SizedBox(height: 10),
                    buildTextInput(
                        null, 'Họ và tên', 'Vui lòng nhập', TextInputType.text),
                    SizedBox(height: 20),
                    buildTextInput(null, 'Số điện thoại', 'Vui lòng nhập',
                        TextInputType.text),
                    SizedBox(height: 20),
                    Container(
                      height: 190,
                      padding: EdgeInsets.all(10),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        border: Border.all(
                          color: Colors.grey,
                        ),
                        color: Colors.white,
                      ),
                      child: Container(
                        constraints: BoxConstraints(maxHeight: 190),
                        child: SingleChildScrollView(
                          physics: NeverScrollableScrollPhysics(),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text('Nội dung góp ý'),
                              TextField(
                                maxLines: null,
                                decoration: const InputDecoration(
                                  isDense: true,
                                  border: InputBorder.none,
                                  hintText: 'Nhập đánh giá của bạn...',
                                ),
                                style: TextStyle(color: Colors.grey),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Container(
            height: 45,
            color: Colors.deepOrange,
            alignment: Alignment.center,
            child: Text(
              'Gửi góp ý',
              style: TextStyle(color: Colors.white, fontSize: 16),
            ),
          )
        ],
      ),
    );
  }

  Widget buildTextInput(TextEditingController? textEditingController,
      String label, String? hintText, TextInputType? textInputType) {
    return Container(
      padding: EdgeInsets.all(10),
      height: 75,
      width: MediaQuery.of(context).size.width,
      alignment: Alignment.topLeft,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          border: Border.all(color: Colors.grey)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(label),
          TextField(
              decoration: InputDecoration(
                  isDense: true,
                  hintText: hintText,
                  disabledBorder: InputBorder.none,
                  border: InputBorder.none),
              controller: textEditingController,
              keyboardType:
                  textInputType == null ? TextInputType.text : textInputType)
        ],
      ),
    );
  }
}
