import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/configs.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';

import '../../presentation.dart';

class MonthPickerWidget<T> extends StatefulWidget {
  late int yearSelected;
  late int monthSelected;
  final Function onChange;
  // final Function onChange;
  MonthPickerWidget({
    Key? key,
    required this.yearSelected,
    required this.monthSelected,
    required this.onChange,
    // required this.onChange,
  }) : super(key: key);

  @override
  _MonthPickerWidgetState<T> createState() => _MonthPickerWidgetState<T>();
}

class _MonthPickerWidgetState<T> extends State<MonthPickerWidget> {
  late int yearTemp;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    yearTemp = widget.yearSelected;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Container(
            color: Colors.white,
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 20),
              height: 50,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  GestureDetector(
                      onTap: () {
                        setState(() {
                          yearTemp--;
                        });
                      },
                      child: Image.asset(AppImages.iconLeftBlackAround)),
                  Text('Năm ${yearTemp}'),
                  GestureDetector(
                      onTap: () {
                        setState(() {
                          yearTemp++;
                        });
                      },
                      child: Image.asset(AppImages.iconRightBlackAround)),
                ],
              ),
            ),
          ),
          Container(
            height: 5,
            color: Colors.grey.shade200,
          ),
          Container(
            padding: EdgeInsets.all(10),
            color: Colors.white,
            width: MediaQuery.of(context).size.width,
            child: Container(
              child: Wrap(
                children: [
                  for (int i = 1; i <= 12; i++)
                    if (yearTemp <= DateTime.now().year &&
                        i <= DateTime.now().month)
                      GestureDetector(
                        onTap: () {
                          setState(() {
                            widget.monthSelected = i;
                            widget.yearSelected = yearTemp;
                            widget.onChange(
                                widget.monthSelected, widget.yearSelected);
                          });
                        },
                        child: Container(
                          width: MediaQuery.of(context).size.width / 3.2,
                          padding: EdgeInsets.all(6),
                          child: Stack(
                            children: [
                              Container(
                                height: 42,
                                alignment: Alignment.center,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                  color: Colors.grey.shade200,
                                  border: i == widget.monthSelected &&
                                          widget.yearSelected == yearTemp
                                      ? Border.all(color: Colors.deepOrange)
                                      : null,
                                ),
                                child: Text('Tháng ${i}'),
                              ),
                              i == widget.monthSelected &&
                                      widget.yearSelected == yearTemp
                                  ? Positioned(
                                      child:
                                          Image.asset(AppImages.iconCheckRec),
                                      top: 0,
                                      left: 0,
                                    )
                                  : Container(),
                            ],
                          ),
                        ),
                      )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
