import '../../presentation.dart';
import 'package:get/get.dart';

class ReconciliationViewModel extends BaseViewModel {
  late PageController pageController;
  int curentPageIndex = 0;

  bool isShowPopup = false;
  bool isShowAll = true;

  int monthSelected = DateTime.now().month;
  int yearSelected = DateTime.now().year;

  onPickupMonth({required int monthSelected, required int yearSelected}) {
    this.monthSelected = monthSelected;
    this.yearSelected = yearSelected;
    isShowAll = false;
    onTapTriggerPopup();
    notifyListeners();
  }

  init() async {
    pageController = PageController(
      initialPage: curentPageIndex,
      viewportFraction: 1,
      keepPage: true,
    );
  }

  updateTabController(int tab) {
    pageController.jumpToPage(tab);
    curentPageIndex = tab;
    notifyListeners();
  }

  onTapWithdrawal() {
    Get.toNamed(Routers.withdrawal);
  }

  void onTapTriggerPopup() {
    isShowPopup = !isShowPopup;
    notifyListeners();
  }
}
