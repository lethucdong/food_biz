import '../../../presentation.dart';
import 'package:get/get.dart';

class WithdrawalSuccessViewModel extends BaseViewModel {
  init() async {}
  onTapGoToHome() {
    Get.offAndToNamed(Routers.home);
  }
}
