import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/constanst/constants.dart';
import 'package:flutter_app/src/utils/app_utils.dart';

import '../../../presentation.dart';

class WithdrawalSuccessScreen extends StatefulWidget {
  const WithdrawalSuccessScreen({Key? key}) : super(key: key);

  @override
  _WithdrawalSuccessScreenState createState() =>
      _WithdrawalSuccessScreenState();
}

class _WithdrawalSuccessScreenState extends State<WithdrawalSuccessScreen>
    with ResponsiveWidget {
  late WithdrawalSuccessViewModel _viewModel;
  @override
  Widget build(BuildContext context) {
    return BaseWidget<WithdrawalSuccessViewModel>(
        viewModel: WithdrawalSuccessViewModel(),
        onViewModelReady: (viewModel) {
          _viewModel = viewModel..init();
        },
        builder: (context, viewModel, appbar) {
          return Scaffold(
              appBar: buildAppBar(
                title: Text(
                  'Rút tiền thành công',
                  style: TextStyle(color: Colors.black),
                  textAlign: TextAlign.center,
                ),
                elevation: 2,
              ),
              backgroundColor: Colors.grey.shade300,
              body: buildUi(context));
        });
  }

  @override
  Widget buildDesktop(BuildContext context) {
    return _buildUi(context);
  }

  @override
  Widget buildMobile(BuildContext context) {
    return _buildUi(context);
  }

  @override
  Widget buildTablet(BuildContext context) {
    return _buildUi(context);
  }

  Widget _buildUi(BuildContext context) {
    return Container(
      color: Colors.white,
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              height: 40,
            ),
            Image.asset(AppImages.imgSuccess),
            SizedBox(
              height: 30,
            ),
            Text(
              'Rút tiền thành công',
              style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(
              height: 10,
            ),
            RichText(
              softWrap: true,
              overflow: TextOverflow.clip,
              text: TextSpan(
                style: TextStyle(fontSize: 16, color: Colors.black),
                children: <TextSpan>[
                  TextSpan(
                    text: 'Số tiền: ',
                  ),
                  TextSpan(
                    text: AppUtils.numToCurencyString(200000),
                    style: TextStyle(
                      color: Colors.deepOrange,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
              child: Text(
                'Số tiền đã được chuyển đến số tài khoản của bạn. Mọi thắc mắc xin vui lòng gọi tới Hotline: 1900 1900',
                style: TextStyle(fontSize: 16),
                textAlign: TextAlign.center,
              ),
            ),
            SizedBox(
              height: 20,
            ),
            GestureDetector(
              onTap: () {
                _viewModel.onTapGoToHome();
              },
              child: Container(
                height: 50,
                margin: EdgeInsets.symmetric(horizontal: 60),
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  color: Colors.deepOrange,
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Text(
                  'Về trang chủ',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 16,
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
