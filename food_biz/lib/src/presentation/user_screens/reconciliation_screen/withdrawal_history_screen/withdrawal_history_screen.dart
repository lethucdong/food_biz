import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/configs.dart';
import 'package:flutter_app/src/utils/app_utils.dart';

import '../../../presentation.dart';

class WithdrawalHistoryScreen extends StatefulWidget {
  const WithdrawalHistoryScreen({Key? key}) : super(key: key);

  @override
  _WithdrawalHistoryScreenState createState() =>
      _WithdrawalHistoryScreenState();
}

class _WithdrawalHistoryScreenState extends State<WithdrawalHistoryScreen>
    with ResponsiveWidget {
  late WithdrawalHistoryViewModel _viewModel;
  @override
  Widget build(BuildContext context) {
    return BaseWidget<WithdrawalHistoryViewModel>(
        viewModel: WithdrawalHistoryViewModel(),
        onViewModelReady: (viewModel) {
          _viewModel = viewModel..init();
        },
        builder: (context, viewModel, appbar) {
          return Scaffold(
              appBar: buildAppBar(
                title: Text(
                  'Lịch sử rút tiền',
                  style: TextStyle(color: Colors.black),
                  textAlign: TextAlign.center,
                ),
                elevation: 2,
              ),
              backgroundColor: Colors.grey.shade300,
              body: buildUi(context));
        });
  }

  @override
  Widget buildDesktop(BuildContext context) {
    return _buildUi(context);
  }

  @override
  Widget buildMobile(BuildContext context) {
    return _buildUi(context);
  }

  @override
  Widget buildTablet(BuildContext context) {
    return _buildUi(context);
  }

  Widget _buildUi(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(20),
      child: SingleChildScrollView(
        child: Column(
          children: [
            _buildHistoryItem(),
            _buildHistoryItem(),
            _buildHistoryItem(),
          ],
        ),
      ),
    );
  }

  Widget _buildHistoryItem() {
    return Container(
      padding: EdgeInsets.all(10),
      margin: EdgeInsets.only(bottom: 20),
      height: 95,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Image.asset(AppImages.iconCoinAround),
          SizedBox(
            width: 10,
          ),
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Mã giao dịch: 12345678',
                      style: TextStyle(fontSize: 16),
                    ),
                    Text(
                      AppUtils.numToCurencyString(300000),
                      style: TextStyle(color: Colors.deepOrange, fontSize: 16),
                    ),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                Text('Thanh toán công nợ cho hệ thống'),
                SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      '08:00 - 20/10/2010',
                      style: TextStyle(color: Colors.grey),
                    ),
                    Text(
                      'Hoàn thành',
                      style: TextStyle(color: Colors.deepOrange),
                    ),
                  ],
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
