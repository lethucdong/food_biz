import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/configs.dart';
import 'package:flutter_app/src/presentation/user_screens/user_widget/month_picker_widget.dart';
import 'package:flutter_app/src/utils/app_utils.dart';
import 'package:provider/src/provider.dart';

import '../../presentation.dart';

class ReconciliationScreen extends StatefulWidget {
  const ReconciliationScreen({Key? key}) : super(key: key);

  @override
  _ReconciliationScreenState createState() => _ReconciliationScreenState();
}

class _ReconciliationScreenState extends State<ReconciliationScreen>
    with ResponsiveWidget {
  late ReconciliationViewModel _viewModel;
  late Size _screenSize;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _screenSize = MediaQuery.of(context).size;
    return BaseWidget<ReconciliationViewModel>(
        viewModel: ReconciliationViewModel(),
        onViewModelReady: (viewModel) {
          _viewModel = viewModel..init();
        },
        builder: (context, viewModel, appbar) {
          return Scaffold(
              appBar: buildAppBar(
                title: Text(
                  'Đối soát',
                  style: TextStyle(color: Colors.black),
                  textAlign: TextAlign.center,
                ),
                elevation: 2,
              ),
              backgroundColor: Colors.grey.shade300,
              body: buildUi(context));
        });
  }

  @override
  Widget buildDesktop(BuildContext context) {
    return _buildUi(context);
  }

  @override
  Widget buildMobile(BuildContext context) {
    return _buildUi(context);
  }

  @override
  Widget buildTablet(BuildContext context) {
    return _buildUi(context);
  }

  Widget _buildUi(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        height: _screenSize.height - 80,
        child: Column(
          children: [
            Container(
              color: Colors.white,
              child: Column(
                children: [
                  _buildHeader(),
                ],
              ),
            ),
            Expanded(
              child: Stack(
                children: [
                  Column(
                    children: [
                      _buildTabControl(context),
                      Expanded(
                        child: Container(
                            padding: EdgeInsets.all(20),
                            child: _buildTabContent(context)),
                      ),
                    ],
                  ),
                  Visibility(
                    visible:
                        context.watch<ReconciliationViewModel>().isShowPopup,
                    child: Stack(
                      children: [
                        Container(
                          color: Colors.black.withOpacity(0.5),
                        ),
                        Positioned(
                          child: SingleChildScrollView(
                            child: MonthPickerWidget(
                              monthSelected: _viewModel.monthSelected,
                              yearSelected: _viewModel.yearSelected,
                              onChange: (int monthSelected, int yearSelected) {
                                Provider.of<ReconciliationViewModel>(context,
                                        listen: false)
                                    .onPickupMonth(
                                        monthSelected: monthSelected,
                                        yearSelected: yearSelected);
                              },
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            GestureDetector(
              onTap: () {
                _viewModel.onTapWithdrawal();
              },
              child: Container(
                height: 45,
                color: Colors.deepOrange,
                alignment: Alignment.center,
                child: Text(
                  'Rút tiền',
                  style: TextStyle(color: Colors.white, fontSize: 16),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _buildTabControl(BuildContext context) {
    return Container(
      height: 40,
      color: Colors.white,
      child: Row(
        children: [
          Expanded(
            child: GestureDetector(
              onTap: () {
                // _viewModel.pageController.jumpToPage(0);

                Provider.of<ReconciliationViewModel>(context, listen: false)
                    .updateTabController(0);
              },
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(),
                  Container(
                    // color: Colors.red,
                    alignment: Alignment.center,
                    child: Text('Thông tin đối soát'),
                  ),
                  Container(
                    height: 2,
                    color: context
                                .watch<ReconciliationViewModel>()
                                .curentPageIndex ==
                            0
                        ? Colors.deepOrange
                        : Colors.grey,
                  ),
                ],
              ),
            ),
          ),
          Expanded(
            child: GestureDetector(
              onTap: () {
                // _viewModel.pageController.jumpToPage(1);
                Provider.of<ReconciliationViewModel>(context, listen: false)
                    .updateTabController(1);
              },
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(),
                  Container(
                    // color: Colors.red,
                    alignment: Alignment.center,
                    child: Text('Lịch sử đối soát'),
                  ),
                  Container(
                    height: 2,
                    color: context
                                .watch<ReconciliationViewModel>()
                                .curentPageIndex ==
                            1
                        ? Colors.deepOrange
                        : Colors.grey,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildTabContent(BuildContext context) {
    return PageView(
      physics: NeverScrollableScrollPhysics(),
      scrollDirection: Axis.horizontal,
      onPageChanged: (value) {
        // _viewModel.updateTabController(value);

        Provider.of<ReconciliationViewModel>(context, listen: false)
            .updateTabController(value);
      },
      controller: _viewModel.pageController,
      children: <Widget>[
        SingleChildScrollView(child: _buildInfo()),
        SingleChildScrollView(child: _buildHistories()),
      ],
    );
  }

  Widget _buildHeader() {
    return Container(
      height: 122,
      // color: Colors.red,
      decoration: BoxDecoration(
        image: DecorationImage(
            image: AssetImage(AppImages.imgBgOrange), fit: BoxFit.cover),
      ),
      child: Stack(
        children: [
          Container(
            // color: Colors.blue,\
            padding: EdgeInsets.all(10),
            margin: EdgeInsets.all(20),
            decoration: BoxDecoration(
              border: Border.all(color: Colors.white),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'NỢ ĐỐI SOÁT',
                  style: TextStyle(
                    fontSize: 12,
                    color: Colors.white,
                  ),
                  textAlign: TextAlign.left,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      '0 đ',
                      style: TextStyle(fontSize: 26, color: Colors.white),
                    ),
                    GestureDetector(
                      onTap: () {
                        _viewModel.onTapTriggerPopup();
                      },
                      child: Row(
                        children: [
                          Text(
                            _viewModel.isShowAll
                                ? 'Tất cả'
                                : 'Tháng ${_viewModel.monthSelected}/ ${_viewModel.yearSelected}',
                            style: TextStyle(fontSize: 18, color: Colors.white),
                          ),
                          Image.asset(
                            AppImages.iconRightArrow,
                            color: Colors.white,
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  _buildInfo() {
    return Container(
      // padding: EdgeInsets.symmetric(horizontal: 20),
      child: false
          ? Column(
              children: [
                SizedBox(
                  height: 100,
                ),
                Image.asset(AppImages.imgBox),
                SizedBox(
                  height: 20,
                ),
                Text('Bạn chưa có thông tin đối soát nào.')
              ],
            )
          : Column(
              children: [
                _buildInfoItem(),
                _buildInfoItem(),
                _buildInfoItem(),
                _buildInfoItem(),
                _buildInfoItem(),
              ],
            ),
    );
  }

  Widget _buildHistories() {
    return Container(
      child: Column(
        children: [
          _buildHistoryItem(),
        ],
      ),
    );
  }

  Widget _buildInfoItem() {
    return Container(
      padding: EdgeInsets.all(15),
      margin: EdgeInsets.only(bottom: 20),
      // height: 135,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
      ),
      child: Column(
        children: [
          Row(
            children: [
              Expanded(
                child: Text(
                  'Đơn hàng đang được xử lý',
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(color: Colors.deepOrange),
                ),
              ),
              SizedBox(
                width: 15,
              ),
              Container(
                width: 5,
                height: 5,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.deepOrange,
                ),
              ),
              SizedBox(
                width: 7,
              ),
              Text(
                '28/7/2021',
                style: TextStyle(color: Colors.grey),
              ),
            ],
          ),
          SizedBox(height: 10),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                'Mã đơn hàng: A123456',
                style: TextStyle(fontSize: 16),
              ),
              Image.asset(AppImages.iconRightAround)
            ],
          ),
          SizedBox(height: 10),
          Row(
            children: [
              Text(
                AppUtils.numToCurencyString(340000),
                style: TextStyle(color: Colors.blueAccent),
              ),
              SizedBox(
                width: 15,
              ),
              Container(
                width: 5,
                height: 5,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.blueAccent,
                ),
              ),
              SizedBox(
                width: 7,
              ),
              Text(
                '4 món',
                style: TextStyle(color: Colors.grey),
              ),
            ],
          ),
          SizedBox(height: 10),
          Row(
            children: [
              Expanded(
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 5, vertical: 5),
                  // height: 25,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Color.fromRGBO(206, 232, 204, 1),
                  ),
                  alignment: Alignment.center,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        child: Text(
                          'Thanh toán:',
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(color: Colors.grey),
                        ),
                      ),
                      Text(
                        AppUtils.numToCurencyString(340000),
                        style: TextStyle(color: Colors.green.shade800),
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                width: 20,
              ),
              Expanded(
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 5, vertical: 5),
                  // height: 25,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Color.fromRGBO(255, 204, 204, 1),
                  ),
                  alignment: Alignment.center,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        child: Text(
                          'Nợ còn lại:',
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(color: Colors.grey),
                        ),
                      ),
                      Text(
                        AppUtils.numToCurencyString(400000),
                        style: TextStyle(color: Colors.red),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }

  Widget _buildHistoryItem() {
    return Container(
      padding: EdgeInsets.all(15),
      margin: EdgeInsets.only(bottom: 20),
      // height: 95,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Image.asset(AppImages.iconCoinAround),
          SizedBox(
            width: 10,
          ),
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      child: Text(
                        'Mã giao dịch: 12345678 ',
                        softWrap: true,
                        style: TextStyle(fontSize: 16),
                      ),
                    ),
                    Text(
                      AppUtils.numToCurencyString(300000),
                      style: TextStyle(color: Colors.deepOrange, fontSize: 16),
                    ),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                Text('Thanh toán công nợ cho hệ thống'),
                SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      '08:00 - 20/10/2010',
                      style: TextStyle(color: Colors.grey),
                    ),
                    Text(
                      'Hoàn thành',
                      style: TextStyle(color: Colors.deepOrange),
                    ),
                  ],
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
