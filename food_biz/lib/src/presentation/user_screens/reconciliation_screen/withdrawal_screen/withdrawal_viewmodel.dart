import '../../../presentation.dart';
import 'package:get/get.dart';

class WithdrawalViewModel extends BaseViewModel {
  init() async {}

  void onTapConfirm() {
    Get.toNamed(Routers.withdrawal_success);
  }

  void onTapHisory() {
    Get.toNamed(Routers.withdrawal_history);
  }
}
