import 'package:flutter_app/src/presentation/login_screens/register_screen/register_viewmodel.dart';
import 'package:flutter_app/src/resource/model/model.dart';
import 'package:flutter_app/src/resource/repo/repo.dart';

import '../../presentation.dart';
import 'package:get/get.dart';

class EditProfileViewModel extends BaseViewModel {
  List<ProvinceModel> listProvince = List<ProvinceModel>.empty(growable: true);
  final List<GenderModel> genderModelList = [
    GenderModel(1, "Nam"),
    GenderModel(2, "Nữ"),
    GenderModel(3, "Giới tính khác")
  ];
  init() async {
    initListProvince();
  }

  initListProvince() async {
    try {
      await ProvinceRepository().getProvince().then((value) {
        if (value.isSuccess) {
          if (value.message == 'OK') {
            listProvince = value.data!;
          } else {
            print(value.message);
          }
        }
      });
      notifyListeners();
    } catch (e) {
      print("Exception: $e");
    }
  }
}
