import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/configs.dart';
import 'package:flutter_app/src/presentation/login_screens/register_screen/register_viewmodel.dart';
import 'package:flutter_app/src/resource/model/model.dart';

import '../../presentation.dart';

class EditProfileScreen extends StatefulWidget {
  final bool isEdit;
  const EditProfileScreen({Key? key, this.isEdit = false}) : super(key: key);

  @override
  _EditProfileScreenState createState() => _EditProfileScreenState();
}

class _EditProfileScreenState extends State<EditProfileScreen>
    with ResponsiveWidget {
  late EditProfileViewModel _viewModel;
  @override
  Widget build(BuildContext context) {
    return BaseWidget<EditProfileViewModel>(
        viewModel: EditProfileViewModel(),
        onViewModelReady: (viewModel) {
          _viewModel = viewModel..init();
        },
        builder: (context, viewModel, appbar) {
          return Scaffold(
              appBar: buildAppBar(
                  title: Text(
                'Thông tin cá nhân',
                style: TextStyle(color: Colors.black),
                textAlign: TextAlign.center,
              )),
              backgroundColor: Colors.grey.shade300,
              body: buildUi(context));
        });
  }

  @override
  Widget buildDesktop(BuildContext context) {
    return _buildUi(context);
  }

  @override
  Widget buildMobile(BuildContext context) {
    return _buildUi(context);
  }

  @override
  Widget buildTablet(BuildContext context) {
    return _buildUi(context);
  }

  Widget _buildUi(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 5),
      color: Colors.white,
      child: Column(
        children: [
          Expanded(
            child: SingleChildScrollView(
              child: Container(
                padding: EdgeInsets.all(20),
                child: Column(
                  children: [
                    buildTextInput(
                        null, 'Họ và tên', 'Vui lòng nhập', TextInputType.text),
                    SizedBox(height: 10),
                    buildTextInput(null, 'Số điện thoại', 'Vui lòng nhập',
                        TextInputType.text),
                    SizedBox(height: 10),
                    buildTextInput(
                        null, 'Email', 'Vui lòng nhập', TextInputType.text),
                    SizedBox(height: 10),
                    buildDroplistGender(),
                    SizedBox(height: 10),
                    buildDroplistProvince('Ngày tháng năm sinh'),
                    SizedBox(height: 10),
                    buildDroplistProvince('Tỉnh/ thành phố'),
                    SizedBox(height: 10),
                    buildDroplistProvince('Quận/ Huyện'),
                    SizedBox(height: 10),
                    buildDroplistProvince('Địa chỉ số nhà củ thể'),
                    SizedBox(height: 10),
                    Container(
                      decoration: BoxDecoration(
                        color: Colors.deepOrange,
                        borderRadius: BorderRadius.circular(10),
                      ),
                      height: 45,
                      alignment: Alignment.center,
                      child: Text(
                        'Cập nhật',
                        style: TextStyle(color: Colors.white, fontSize: 16),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget buildTextInput(TextEditingController? textEditingController,
      String label, String? hintText, TextInputType? textInputType) {
    return Container(
      padding: EdgeInsets.all(10),
      height: 75,
      width: MediaQuery.of(context).size.width,
      alignment: Alignment.topLeft,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          border: Border.all(color: Colors.grey)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(label),
          TextField(
              decoration: InputDecoration(
                  isDense: true,
                  hintText: hintText,
                  disabledBorder: InputBorder.none,
                  border: InputBorder.none),
              controller: textEditingController,
              keyboardType:
                  textInputType == null ? TextInputType.text : textInputType)
        ],
      ),
    );
  }

  Widget buildDroplistGender() {
    return Container(
        padding: EdgeInsets.all(10),
        height: 90,
        width: MediaQuery.of(context).size.width,
        alignment: Alignment.topLeft,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          border: Border.all(color: Colors.grey),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text('Giới tính'),
            Container(
              width: MediaQuery.of(context).size.width,
              child: DropdownButton<int>(
                underline: SizedBox(),
                hint: Text('Vui lòng chọn'),
                isExpanded: true,
                icon: Image.asset(AppImages.iconDropDown),
                items: _viewModel.genderModelList.map((GenderModel map) {
                  return new DropdownMenuItem<int>(
                    value: map.id,
                    child: new Text(map.name),
                  );
                }).toList(),
                onChanged: (_) {},
              ),
            )
          ],
        ));
  }

  Widget buildDroplistProvince(String title) {
    return Container(
        padding: EdgeInsets.all(10),
        height: 90,
        width: MediaQuery.of(context).size.width,
        alignment: Alignment.topLeft,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          border: Border.all(color: Colors.grey),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(title),
            Container(
              width: MediaQuery.of(context).size.width,
              child: DropdownButton<int>(
                underline: SizedBox(),
                hint: Text('Vui lòng chọn'),
                isExpanded: true,
                icon: Image.asset(AppImages.iconDropDown),
                items: [],
                // items: _viewModel
                //     .listProvince //_viewModel.registerController.listProvince.value
                //     .map((ProvinceModel value) {
                //   return DropdownMenuItem<int>(
                //     value: value.code,
                //     child: Text('${value.name}'),
                //   );
                // }).toList(),
                onChanged: (_) {},
              ),
            )
          ],
        ));
  }
}
