import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/constanst/constants.dart';
import 'package:flutter_app/src/utils/app_utils.dart';

class AddToCartWidget extends StatefulWidget {
  List<String>? listSize; // = ['Nhỏ', 'Vừa', 'To'];
  List<String>? listWeight; // = ['1 Kg', '2 Kg', '3 Kg', '4 Kg', '5 Kg'];
  int? quantity;
  bool? isEdit;
  final Function onTap;
  late int weightSelect = 0;
  late int sizeSelect = 0;
  AddToCartWidget(
      {Key? key,
      this.isEdit = false,
      this.listSize,
      this.listWeight,
      this.quantity,
      required this.onTap})
      : super(key: key);

  @override
  _AddToCartWidgetState createState() => _AddToCartWidgetState();
}

class _AddToCartWidgetState extends State<AddToCartWidget> {
  @override
  void initState() {
    // TODO: implement initState
    widget.quantity = 1;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    widget.listSize = ['Nhỏ', 'Vừa', 'To'];
    widget.listWeight = ['1 Kg', '2 Kg', '3 Kg', '4 Kg', '5 Kg'];

    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Container(
        // color: Colors.transparent,
        child: Column(
          children: [
            Expanded(
              child: GestureDetector(onTap: () {
                Navigator.pop(context);
              }
                  // color: Colors.transparent,
                  ),
            ),
            Column(
              children: [
                Container(
                  width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  height: 460,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10),
                      topRight: Radius.circular(10),
                    ),
                    // border: Border.all(color: Colors.grey, width: 2),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 20),
                        child: Text(
                          'TÔM NÕN HẤP HẠ LONG',
                          style: TextStyle(fontSize: 16),
                        ),
                      ),
                      Text('Khối lượng', style: TextStyle(fontSize: 14)),
                      SizedBox(height: 15),
                      SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            for (int i = 0; i < widget.listWeight!.length; i++)
                              _buildWeight(
                                  id: i,
                                  title: widget.listWeight![i],
                                  isSeleted: i == widget.weightSelect),
                          ],
                        ),
                      ),
                      SizedBox(height: 15),
                      Text('Kích thước'),
                      SizedBox(height: 15),
                      SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            for (int i = 0; i < widget.listSize!.length; i++)
                              _buildSize(
                                  id: i,
                                  title: widget.listSize![i],
                                  isSeleted: i == widget.sizeSelect,
                                  width: 108),
                          ],
                        ),
                      ),
                      SizedBox(height: 15),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text('Số lượng', style: TextStyle(fontSize: 14)),
                          Container(
                            width: 173,
                            height: 34,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(10),
                              border: Border.all(
                                  color: Colors.deepOrange, width: 1),
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                GestureDetector(
                                  onTap: () {
                                    if (widget.quantity! > 1)
                                      setState(() {
                                        widget.quantity = widget.quantity! - 1;
                                      });
                                  },
                                  child: Container(
                                    margin: EdgeInsets.only(left: 10),
                                    width: 20,
                                    height: 20,
                                    alignment: Alignment.center,
                                    decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        color: Colors.grey.shade600),
                                    child: Text(
                                      '-',
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 17,
                                      ),
                                    ),
                                  ),
                                ),
                                Text('${widget.quantity}'),
                                GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      widget.quantity = widget.quantity! + 1;
                                    });
                                  },
                                  child: Container(
                                    margin: EdgeInsets.only(right: 10),
                                    width: 20,
                                    height: 20,
                                    alignment: Alignment.center,
                                    decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        color: Colors.grey.shade600),
                                    child: Text(
                                      '+',
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 17,
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: 15),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            children: [
                              Text('Giá bán của bạn',
                                  style: TextStyle(fontSize: 14)),
                              SizedBox(
                                width: 20,
                              ),
                              Image.asset(
                                AppImages.iconInfo,
                              ),
                            ],
                          ),
                          SizedBox(width: 10),
                          Expanded(
                            child: Container(
                                // width: 173,
                                height: 34,
                                alignment: Alignment.centerRight,
                                padding: EdgeInsets.only(right: 10),
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(10),
                                  border: Border.all(
                                      color: Colors.deepOrange, width: 1),
                                ),
                                child: Text(AppUtils.numToCurencyString(35000),
                                    style: TextStyle(fontSize: 14))),
                          ),
                        ],
                      ),
                      SizedBox(height: 15),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text('Tổng giá bán của bạn'),
                          Text(
                              AppUtils.numToCurencyString(
                                  35000.0 * widget.quantity!),
                              style: TextStyle(fontSize: 14)),
                        ],
                      ),
                      SizedBox(height: 15),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            children: [
                              Text('Thưởng hạng'),
                              SizedBox(
                                width: 20,
                              ),
                              Image.asset(
                                AppImages.iconRank2,
                                scale: 2.5,
                              ),
                            ],
                          ),
                          Text(AppUtils.numToCurencyString(20000),
                              style: TextStyle(fontSize: 14)),
                        ],
                      ),
                      SizedBox(height: 15),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text('Lợi nhuận của bạn'),
                          Text(
                            AppUtils.numToCurencyString(0),
                            style: TextStyle(
                                color: Colors.blueAccent, fontSize: 14),
                          ),
                        ],
                      ),
                      SizedBox(height: 18),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Image.asset(AppImages.iconInfo),
                          Container(
                            // height: 60,
                            width: 200,
                            child: Text(
                                '  Giá bán tối thiểu: 40.000 đ, tối đa: 35.000 đ',
                                style:
                                    TextStyle(color: Colors.grey, fontSize: 14),
                                softWrap: true,
                                maxLines: 2),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
                widget.isEdit == false
                    ? GestureDetector(
                        onTap: () {
                          widget.onTap();
                        },
                        child: Container(
                          width: MediaQuery.of(context).size.width,
                          height: 45,
                          color: Colors.deepOrange,
                          alignment: Alignment.center,
                          child: Text(
                            'Thêm vào giỏ hàng',
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.normal,
                                fontSize: 16),
                          ),
                        ),
                      )
                    : Row(
                        children: [
                          Expanded(
                            flex: 1,
                            child: GestureDetector(
                              child: Container(
                                // width: MediaQuery.of(context).size.width,
                                height: 45,
                                color: Colors.white,
                                alignment: Alignment.center,
                                child: Text(
                                  'Hủy bỏ',
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16),
                                ),
                              ),
                            ),
                          ),
                          Expanded(
                            flex: 1,
                            child: GestureDetector(
                              child: Container(
                                // width: MediaQuery.of(context).size.width,
                                height: 45,
                                color: Colors.deepOrange,
                                alignment: Alignment.center,
                                child: Text(
                                  'Lưu lại',
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16),
                                ),
                              ),
                            ),
                          )
                        ],
                      )
              ],
            )
          ],
        ),
      ),
    );
  }

  Widget _buildWeight(
      {required int id,
      required String title,
      required bool isSeleted,
      double? width}) {
    return GestureDetector(
      onTap: () {
        setState(() {
          widget.weightSelect = id;
        });
      },
      child: Container(
        alignment: Alignment.center,
        width: width == null ? 61 : width,
        height: 34,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          border: Border.all(color: isSeleted ? Colors.white : Colors.grey),
          color: isSeleted ? Colors.deepOrange : Colors.white,
        ),
        child: Text(
          title,
          style: TextStyle(color: isSeleted ? Colors.white : Colors.black),
        ),
      ),
    );
  }

  Widget _buildSize(
      {required int id,
      required String title,
      required bool isSeleted,
      double? width}) {
    return GestureDetector(
      onTap: () {
        setState(() {
          widget.sizeSelect = id;
        });
      },
      child: Container(
        alignment: Alignment.center,
        width: width == null ? 61 : width,
        height: 34,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          border: Border.all(color: isSeleted ? Colors.white : Colors.grey),
          color: isSeleted ? Colors.deepOrange : Colors.white,
        ),
        child: Text(
          title,
          style: TextStyle(color: isSeleted ? Colors.white : Colors.black),
        ),
      ),
    );
  }
}
