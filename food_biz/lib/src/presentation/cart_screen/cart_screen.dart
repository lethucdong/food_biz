import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/configs.dart';
import 'package:flutter_app/src/presentation/presentation.dart';
import 'package:flutter_app/src/utils/app_utils.dart';
import 'package:provider/provider.dart';

import 'widget/add_to_cart_widget.dart';

class CartScreen extends StatefulWidget {
  @override
  _CartScreenState createState() => _CartScreenState();
}

class _CartScreenState extends State<CartScreen> with ResponsiveWidget {
  late CartViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<CartViewModel>(
        viewModel: CartViewModel(),
        onViewModelReady: (viewModel) {
          _viewModel = viewModel..init();
        },
        builder: (context, viewModel, appbar) {
          return Scaffold(
              appBar: buildAppBar(
                  title: Text(
                'Giỏ hàng',
                style: TextStyle(color: Colors.black),
                textAlign: TextAlign.center,
              )),
              backgroundColor: Colors.grey.shade200,
              body: buildUi(context));
        });
  }

  @override
  Widget buildDesktop(BuildContext context) {
    return _buildBody(context);
  }

  @override
  Widget buildMobile(BuildContext context) {
    return _buildBody(context);
  }

  @override
  Widget buildTablet(BuildContext context) {
    return _buildBody(context);
  }

  Widget _buildBody(BuildContext context) {
    Color getColor(Set<MaterialState> states) {
      const Set<MaterialState> interactiveStates = <MaterialState>{
        MaterialState.pressed,
        MaterialState.hovered,
        MaterialState.focused,
      };
      if (states.any(interactiveStates.contains)) {
        return Colors.blue;
      }
      return Colors.red;
    }

    // Future.delayed(
    //     Duration.zero,
    //     () => {
    //           showDialog(
    //               context: context,
    //               builder: (BuildContext context) {
    //                 return AddToCartWidget();
    //               }),
    //         });

    return SingleChildScrollView(
      child: Container(
        // padding: EdgeInsets.symmetric(horizontal: 20),
        // color: Colors.white,
        child: Column(
          children: [
            _buildListCartItem(context),
            _buildDeliveryNote(),
            _buildBillInfo(context),
            _buildFooter(context),
          ],
        ),
      ),
    );
  }

  Widget _lineSpace({required double height}) {
    return Container(
      height: height,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20), color: Colors.grey.shade300),
    );
  }

  Widget _buildCartItem(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 10),
      child: Column(
        children: [
          Row(
            children: [
              Container(
                margin: EdgeInsets.only(bottom: 41),
                child: WidgetCheckButton(
                  onChange: () {},
                  // // isChecked: true,
                ),
              ),
              SizedBox(
                width: 10,
              ),
              Expanded(
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          width: 60,
                          height: 60,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: Colors.grey,
                            ),
                            image: DecorationImage(
                                image: AssetImage(
                                  AppImages.imgproductHaLong1,
                                ),
                                fit: BoxFit.cover),
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Text(
                                'Tôm nõn Hạ Long',
                                style: TextStyle(fontSize: 16),
                              ),
                              SizedBox(height: 5),
                              RichText(
                                softWrap: true,
                                text: TextSpan(
                                  text: 'Giá bán nhà cung cấp:     ',
                                  style: TextStyle(color: Colors.black),
                                  children: <TextSpan>[
                                    TextSpan(
                                        text: AppUtils.numToCurencyString(
                                            5500000)),
                                  ],
                                ),
                              ),
                              SizedBox(height: 5),
                              RichText(
                                text: TextSpan(
                                  text: 'Giá bán của bạn:     ',
                                  style: TextStyle(color: Colors.black),
                                  children: <TextSpan>[
                                    TextSpan(
                                      text:
                                          AppUtils.numToCurencyString(6000000),
                                      style:
                                          TextStyle(color: Colors.deepOrange),
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(height: 5),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Expanded(
                                      flex: 3, child: buildDroplistGender()),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Expanded(
                                    flex: 2,
                                    child: Container(
                                      child: Row(
                                        children: [
                                          GestureDetector(
                                            onTap: () {
                                              Provider.of<CartViewModel>(
                                                      context,
                                                      listen: false)
                                                  .decreaseProduct();
                                            },
                                            child: Container(
                                              width: 20,
                                              height: 20,
                                              decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(5),
                                                  color: Colors.grey.shade300),
                                              child: Text(
                                                '-',
                                                style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 17,
                                                ),
                                              ),
                                              alignment: Alignment.center,
                                            ),
                                          ),
                                          // SizedBox(
                                          //   width: 20,
                                          // ),
                                          Expanded(child: Container()),
                                          Text(
                                              '${context.watch<CartViewModel>().quantity}'),
                                          // SizedBox(
                                          //   width: 20,
                                          // ),
                                          Expanded(child: Container()),
                                          GestureDetector(
                                            onTap: () {
                                              Provider.of<CartViewModel>(
                                                      context,
                                                      listen: false)
                                                  .increaseProduct();
                                            },
                                            child: Container(
                                              width: 20,
                                              height: 20,
                                              alignment: Alignment.center,
                                              decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(5),
                                                  color: Colors.grey.shade400),
                                              child: Text(
                                                '+',
                                                style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 17,
                                                ),
                                              ),
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              )
            ],
          ),
          // SizedBox(height: 5),
          // _lineSpace(height: 2),
        ],
      ),
    );
  }

  Widget _buildListCartItem(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 5),
      padding: EdgeInsets.symmetric(
        horizontal: 20,
      ),
      color: Colors.white,
      child: Column(
        children: [
          _buildGroupCartItem(1, context),
          _buildGroupCartItem(2, context),
        ],
      ),
    );
  }

  Widget _buildDeliveryNote() {
    return Container(
      color: Colors.grey.shade200,
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Image.asset(
                AppImages.iconDeliveryTruck,
                scale: 1.5,
                color: Colors.black,
              ),
              SizedBox(width: 10),
              Flexible(
                child: Text(
                  'Miễn phí vận chuyển cho đơn hàng từ 2.000.000 (tối đa 50.000đ).  Miễn phí vận chuyển cho đơn hàng từ 6.000.000 đ (tối đa 100.000đ)',
                  softWrap: true,
                  textAlign: TextAlign.left,
                  style: TextStyle(fontSize: 14),
                ),
              ),
            ],
          ),
          SizedBox(height: 5),
          _lineSpace(height: 2),
        ],
      ),
    );
  }

  Widget _buildBillInfo(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Thông tin đơn hàng',
            style: TextStyle(fontSize: 16),
          ),
          SizedBox(height: 20),
          Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10), color: Colors.white),
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: Column(
              children: [
                _buildLineInfo(
                  leftContent: Text(
                    'Tổng tiền hàng: ',
                    softWrap: true,
                  ),
                  rightContent: Text(
                    AppUtils.numToCurencyString(170000),
                    style: TextStyle(
                      color: Colors.blueAccent,
                    ),
                  ),
                ),
                _lineSpace(height: 2),
                _buildLineInfo(
                  leftContent: Text('Tổng giá bán của bạn:', softWrap: true),
                  rightContent: Text(
                    AppUtils.numToCurencyString(130000),
                  ),
                ),
                _lineSpace(height: 2),
                _buildLineInfo(
                    leftContent: Row(
                      children: [
                        Text('Tổng lợi nhuận: ', softWrap: true),
                        Image.asset(AppImages.iconInfo),
                      ],
                    ),
                    rightContent: GestureDetector(
                      onTap: () {
                        Provider.of<CartViewModel>(context, listen: false)
                            .onTapTotalProfit();
                      },
                      child: Row(
                        children: [
                          Text(
                            '${AppUtils.numToCurencyString(130000)}  ',
                          ),
                          Image.asset(AppImages.iconRightArrow),
                        ],
                      ),
                    )),
                Visibility(
                  visible: context.watch<CartViewModel>().isShowtotalProfit,
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: Column(
                      children: [
                        _lineSpace(height: 2),
                        _buildLineInfo(
                          leftContent:
                              Text('Lợi nhuận sản phẩm: ', softWrap: true),
                          rightContent: Text(
                            AppUtils.numToCurencyString(20000),
                          ),
                        ),
                        _lineSpace(height: 2),
                        _buildLineInfo(
                          leftContent: Row(
                            children: [
                              Expanded(
                                  child: Text('Tổng thưởng hạng:',
                                      softWrap: true)),
                              Image.asset(AppImages.iconRank2, scale: 2),
                            ],
                          ),
                          rightContent: Text(
                            AppUtils.numToCurencyString(130000),
                          ),
                        ),
                        _lineSpace(height: 2),
                        _buildLineInfo(
                          leftContent: Text('Voucher', softWrap: true),
                          rightContent: Text(
                            AppUtils.numToCurencyString(20000),
                          ),
                        ),
                        _lineSpace(height: 2),
                        _buildLineInfo(
                          leftContent: Text('15 Fobi Xu: ', softWrap: true),
                          rightContent: Text(
                            AppUtils.numToCurencyString(15000),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildLineInfo(
      {required Widget leftContent, required Widget rightContent}) {
    return Container(
      height: 50,
      alignment: Alignment.centerLeft,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [Expanded(child: leftContent), rightContent],
      ),
    );
  }

  Widget _buildFooter(BuildContext context) {
    return Container(
      color: Colors.white,
      padding: EdgeInsets.only(top: 20),
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Image.asset(
                      AppImages.iconNavCoupon,
                      scale: 2,
                      color: Colors.deepOrange,
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Text('Voucher')
                  ],
                ),
                Visibility(
                  visible: context.watch<CartViewModel>().isShowVoucher,
                  child: Expanded(
                    child: Container(
                      alignment: Alignment.topRight,
                      child: Wrap(
                        direction: Axis.horizontal,
                        children: [
                          Stack(
                            children: [
                              Container(
                                child: Image.asset(
                                  AppImages.imgUnionFrame1,
                                  scale: 2,
                                ),
                              ),
                              Positioned(
                                child: Padding(
                                  padding: const EdgeInsets.only(top: 2),
                                  child: Text(
                                    '+ 15 xu',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontSize: 12, color: Colors.blueAccent),
                                  ),
                                ),
                                left: 0,
                                right: 0,
                              )
                            ],
                          ),
                          Stack(
                            children: [
                              Container(
                                child: Image.asset(
                                  AppImages.imgUnionFrame3,
                                  scale: 2,
                                ),
                              ),
                              Positioned(
                                child: Padding(
                                  padding: const EdgeInsets.only(top: 2),
                                  child: Text(
                                    '-20k',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(fontSize: 12),
                                  ),
                                ),
                                left: 0,
                                right: 0,
                              )
                            ],
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 10),
                            child: Stack(
                              children: [
                                Container(
                                  child: Image.asset(
                                    AppImages.imgUnionFrame2,
                                    scale: 2,
                                  ),
                                ),
                                Positioned(
                                  child: Padding(
                                    padding: const EdgeInsets.only(top: 2),
                                    child: Text(
                                      'Miễn phí vận chuyển',
                                      textAlign: TextAlign.center,
                                      style: TextStyle(fontSize: 12),
                                    ),
                                  ),
                                  left: 0,
                                  right: 0,
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    Provider.of<CartViewModel>(context, listen: false)
                        .onTapShowVoucher();
                  },
                  child: Row(
                    children: [
                      Visibility(
                        visible: !context.watch<CartViewModel>().isShowVoucher,
                        child: Text(
                          'Chọn voucher ',
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 3.0),
                        child: Image.asset(
                          AppImages.iconRightArrow,
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 10,
          ),
          _lineSpace(height: 2),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    Image.asset(
                      AppImages.iconCoin,
                      color: Colors.deepOrange,
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Text(' Dùng 15 Fobi xu')
                  ],
                ),
                Container(
                  child: Row(
                    children: [
                      Text('(-15.000đ)'),
                      SwitchButton(),
                    ],
                  ),
                ),
              ],
            ),
          ),
          _lineSpace(height: 2),
          Padding(
            padding: const EdgeInsets.only(left: 20),
            child: Row(
              children: [
                Row(
                  children: [
                    WidgetCheckButton(onChange: () {}),
                    SizedBox(
                      width: 5,
                    ),
                    Text(
                      ' Tất cả',
                      style: TextStyle(color: Colors.black),
                    )
                  ],
                ),
                Expanded(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Column(
                        children: [
                          RichText(
                            text: TextSpan(
                              text: 'Tổng:     ',
                              style: TextStyle(color: Colors.black),
                              children: <TextSpan>[
                                TextSpan(
                                  text: AppUtils.numToCurencyString(135000),
                                  style: TextStyle(color: Colors.deepOrange),
                                ),
                              ],
                            ),
                          ),
                          Text(
                            'Nhận 135 Fobi Xu',
                            style: TextStyle(color: Colors.grey),
                          ),
                        ],
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      GestureDetector(
                        onTap: () => {_viewModel.onTapCreateOrder()},
                        child: Container(
                          width: 95,
                          height: 45,
                          color: Colors.deepOrange,
                          alignment: Alignment.center,
                          child: Text(
                            'Tạo đơn',
                            style: TextStyle(
                              color: Colors.white,
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget buildDroplistGender() {
    final List<GenderModel> optionProduct = [GenderModel(1, "5kg - Size vừa")];
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 2),
      height: 24,
      width: 125,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        color: Colors.grey.shade300,
      ),
      alignment: Alignment.center,
      child: DropdownButton<int>(
        value: optionProduct[0].id,
        underline: SizedBox(),
        isExpanded: true,
        icon: Image.asset(AppImages.iconDropDown),
        items: optionProduct.map((GenderModel map) {
          return new DropdownMenuItem<int>(
            value: map.id,
            child: new Text(map.name),
          );
        }).toList(),
        onChanged: (_) {},
      ),
    );
  }

  _buildGroupCartItem(int quantity, BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 20),
      child: Column(
        children: [
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                WidgetCheckButton(
                  onChange: () {},
                ),
                SizedBox(
                  width: 20,
                ),
                Text(
                  'Elina Med',
                  style: TextStyle(
                    fontSize: 16,
                  ),
                ),
              ],
            ),
          ),
          // SizedBox(
          //   height: 10,
          // ),
          for (int i = 0; i < quantity; i++) _buildCartItem(context),
          // SizedBox(
          //   height: 20,
          // ),
          LineSpace(),
        ],
      ),
    );
  }
}
