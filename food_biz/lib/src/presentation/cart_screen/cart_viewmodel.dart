import 'package:flutter_app/src/presentation/presentation.dart';
import 'package:get/get.dart';

class CartViewModel extends BaseViewModel {
  init() async {}
  bool isShowtotalProfit = false;
  bool isShowVoucher = false;
  int quantity = 1;
  onTapTotalProfit() {
    isShowtotalProfit = !isShowtotalProfit;
    notifyListeners();
  }

  onTapShowVoucher() {
    isShowVoucher = !isShowVoucher;
    notifyListeners();
  }

  onTapCreateOrder() {
    Get.toNamed(Routers.create_order);
  }

  increaseProduct() {
    quantity++;
    notifyListeners();
  }

  decreaseProduct() {
    if (quantity > 1) {
      quantity--;
      notifyListeners();
    }
  }
}
