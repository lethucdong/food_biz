import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/src/configs/configs.dart';
import 'package:flutter_app/src/resource/resource.dart';
import 'package:get/get.dart';
import '../../presentation.dart';
import 'register_viewmodel.dart';
import 'dart:convert';
import 'package:provider/provider.dart';

class RegisterScreen extends StatefulWidget {
  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> with ResponsiveWidget {
  late RegisterViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<RegisterViewModel>(
        viewModel: RegisterViewModel(),
        onViewModelReady: (viewModel) {
          _viewModel = viewModel..init();
        },
        builder: (context, viewModel, appbar) {
          return Scaffold(
              appBar: buildAppBar(title: null), body: buildUi(context));
        });
  }

  @override
  Widget buildDesktop(BuildContext context) {
    return _buildBody(context);
  }

  @override
  Widget buildMobile(BuildContext context) {
    return _buildBody(context);
  }

  @override
  Widget buildTablet(BuildContext context) {
    return _buildBody(context);
  }

  Widget _buildBody(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 20),
        child: Column(
          children: [
            // Text('${context.watch<RegisterViewModel>().a}'),
            Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Đăng ký tài khoản mới",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                      'Bạn cần cung cấp một số thông tin của bạn để đăng ký tài khoản mới'),
                  SizedBox(
                    height: 20,
                  ),
                  buildTextInput(
                      null, 'Họ và tên', 'Vui lòng nhập', TextInputType.text),
                  SizedBox(
                    height: 10,
                  ),
                  buildTextInput(null, 'Số điện thoại', 'Vui lòng nhập',
                      TextInputType.number),
                  SizedBox(
                    height: 10,
                  ),
                  buildTextInput(
                      null, 'Email', 'Vui lòng nhập', TextInputType.text),
                  SizedBox(
                    height: 10,
                  ),
                ],
              ),
            ),
            buildDroplistProvince(),
            SizedBox(
              height: 10,
            ),
            buildDroplistGender(),
            SizedBox(
              height: 10,
            ),
            buildPolicy(context),
            SizedBox(
              height: 30,
            ),
            GestureDetector(
              onTap: () => {_viewModel.onTapRegister()},
              child: Container(
                height: 45,
                width: MediaQuery.of(context).size.width,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.deepOrange),
                child: Text('Đăng nhập'),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget buildTextInput(TextEditingController? textEditingController,
      String label, String? hintText, TextInputType? textInputType) {
    return Container(
      padding: EdgeInsets.all(10),
      height: 75,
      width: MediaQuery.of(context).size.width,
      alignment: Alignment.topLeft,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          border: Border.all(color: Colors.grey)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(label),
          TextField(
              decoration: InputDecoration(
                  isDense: true,
                  hintText: hintText,
                  disabledBorder: InputBorder.none,
                  border: InputBorder.none),
              controller: textEditingController,
              keyboardType:
                  textInputType == null ? TextInputType.text : textInputType)
        ],
      ),
    );
  }

  Widget buildDroplistProvince() {
    return Container(
        padding: EdgeInsets.only(
          top: 7,
          left: 10,
          right: 10,
        ),
        height: 75,
        width: MediaQuery.of(context).size.width,
        alignment: Alignment.topLeft,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          border: Border.all(color: Colors.grey),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text('Tỉnh/thành phố'),
            Container(
              width: MediaQuery.of(context).size.width,
              child: DropdownButton<int>(
                underline: SizedBox(),
                hint: Text('Vui lòng chọn'),
                isExpanded: true,
                icon: Image.asset(AppImages.iconDropDown),
                items: _viewModel
                    .listProvince //_viewModel.registerController.listProvince.value
                    .map((ProvinceModel value) {
                  return DropdownMenuItem<int>(
                    value: value.code,
                    child: Text('${value.name}'),
                  );
                }).toList(),
                onChanged: (_) {},
              ),
            )
          ],
        ));
  }

  Widget buildDroplistGender() {
    return Container(
        padding: EdgeInsets.only(
          top: 7,
          left: 10,
          right: 10,
        ),
        height: 75,
        width: MediaQuery.of(context).size.width,
        alignment: Alignment.topLeft,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          border: Border.all(color: Colors.grey),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text('Giới tính'),
            Container(
              width: MediaQuery.of(context).size.width,
              child: DropdownButton<int>(
                itemHeight: 48.0,
                underline: SizedBox(),
                hint: Text('Vui lòng chọn'),
                isExpanded: true,
                icon: Image.asset(AppImages.iconDropDown),
                items: _viewModel.genderModelList.map((GenderModel map) {
                  return new DropdownMenuItem<int>(
                    value: map.id,
                    child: new Text(map.name),
                  );
                }).toList(),
                onChanged: (_) {},
              ),
            )
          ],
        ));
  }

  Widget buildCheckBox(BuildContext context) {
    return Checkbox(
      checkColor: Colors.white,
      fillColor: MaterialStateProperty.all(Colors.grey),
      value: context.watch<RegisterViewModel>().isAccept,
      shape: CircleBorder(),
      onChanged: (value) {
        Provider.of<RegisterViewModel>(context, listen: false).onTapAccept();
      },
    );
  }

  Widget buildPolicy(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        buildCheckBox(context),
        Flexible(
            child: RichText(
          text: TextSpan(
            // style: defaultStyle,
            children: <TextSpan>[
              TextSpan(
                  text: 'Tôi đồng ý với ',
                  style: TextStyle(color: Colors.black)),
              TextSpan(
                  text: ' điều khoản sử dụng',
                  style: TextStyle(color: Colors.deepOrange),
                  recognizer: TapGestureRecognizer()..onTap = () {}),
              TextSpan(text: ' & ', style: TextStyle(color: Colors.black)),
              TextSpan(
                  text: 'chính sách bảo mật',
                  style: TextStyle(color: Colors.deepOrange),
                  recognizer: TapGestureRecognizer()..onTap = () {}),
            ],
          ),
        ))
      ],
    );
  }
}
