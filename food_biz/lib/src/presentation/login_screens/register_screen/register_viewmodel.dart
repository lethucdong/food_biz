import 'package:dio/dio.dart';
import 'package:flutter_app/src/resource/model/province_model.dart';
import 'package:flutter_app/src/resource/repo/province_repository.dart';

import '../../presentation.dart';
import 'package:get/get.dart';

class RegisterViewModel extends BaseViewModel {
  final List<GenderModel> genderModelList = [
    GenderModel(1, "Nam"),
    GenderModel(2, "Nữ"),
    GenderModel(3, "Giới tính khác")
  ];
  List<ProvinceModel> listProvince = List<ProvinceModel>.empty(growable: true);
  bool isAccept = false;

  init() async {
    initListProvince();
  }

  onTapRegister() {
    Get.toNamed(Routers.navigation);
  }

  onTapAccept() {
    isAccept = !isAccept;
    notifyListeners();
  }

  initListProvince() async {
    try {
      await ProvinceRepository().getProvince().then((value) {
        if (value.isSuccess) {
          if (value.message == 'OK') {
            listProvince = value.data!;
          } else {
            print(value.message);
          }
        }
      });
      notifyListeners();
    } catch (e) {
      print("Exception: $e");
    }
  }
}

class GenderModel {
  int id;
  String name;
  @override
  String toString() {
    return '$id $name';
  }

  GenderModel(this.id, this.name);
}
