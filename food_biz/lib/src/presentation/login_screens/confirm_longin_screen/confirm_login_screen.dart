import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../presentation.dart';
import 'confirm_login_viewmodel.dart';

class ConfrimLoginScreen extends StatefulWidget {
  @override
  _ConfrimLoginScreenState createState() => _ConfrimLoginScreenState();
}

class _ConfrimLoginScreenState extends State<ConfrimLoginScreen>
    with ResponsiveWidget {
  late ConfirmLoginViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<ConfirmLoginViewModel>(
        viewModel: ConfirmLoginViewModel(),
        onViewModelReady: (viewModel) {
          _viewModel = viewModel..init();
        },
        builder: (context, viewModel, appbar) {
          return Scaffold(
              appBar: buildAppBar(title: null), body: buildUi(context));
        });
  }

  @override
  Widget buildDesktop(BuildContext context) {
    return _buildBody(context);
  }

  @override
  Widget buildMobile(BuildContext context) {
    return _buildBody(context);
  }

  @override
  Widget buildTablet(BuildContext context) {
    return _buildBody(context);
  }

  Widget _buildBody(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20),
      height: MediaQuery.of(context).size.height,
      child: Column(
        children: [
          Expanded(
            child: SingleChildScrollView(
              child: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Nhập mã xác nhận",
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                    ),
                    SizedBox(
                      height: 42,
                    ),
                    Text(
                        'Mã xác nhận đã được gửi qua tin nhắn SMS của số điện thoại +84912345678'),
                    SizedBox(
                      height: 30,
                    ),
                    TextField(
                        decoration: InputDecoration(
                          isDense: true,
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                          ),
                          contentPadding: EdgeInsets.all(15),
                        ),
                        keyboardType: TextInputType.number),
                    SizedBox(
                      height: 30,
                    ),
                    Center(
                      child: Column(
                        children: [
                          Text('Chưa nhận được mã OTP?'),
                          SizedBox(
                            height: 30,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              GestureDetector(
                                child: Text('Gửi lại ',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      decoration: TextDecoration.underline,
                                    )),
                              ),
                              Text(
                                '(0:59)',
                                style: TextStyle(color: Colors.deepOrange),
                              )
                            ],
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          // Expanded(child: Container()),

          GestureDetector(
            onTap: () => {_viewModel.onTapRegister()},
            child: Container(
              height: 45,
              width: MediaQuery.of(context).size.width,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Colors.deepOrange),
              child: Text('Đăng nhập'),
            ),
          ),
        ],
      ),
    );
  }
}
