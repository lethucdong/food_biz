import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/main.dart';
import 'package:flutter_app/src/configs/configs.dart';
import 'package:flutter_app/src/configs/constanst/app_images.dart';
import 'package:get/get.dart';

import '../../presentation.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> with ResponsiveWidget {
  late LoginViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<LoginViewModel>(
        viewModel: LoginViewModel(),
        onViewModelReady: (viewModel) {
          _viewModel = viewModel..init();
        },
        builder: (context, viewModel, appbar) {
          return Scaffold(
            appBar: buildAppBar(title: null),
            body: Column(
              children: [
                appbar ?? SizedBox(),
                Expanded(child: Center(child: buildUi(context)))
              ],
            ),
          );
        });
  }

  @override
  Widget buildDesktop(BuildContext context) {
    return _buildBody(context);
  }

  @override
  Widget buildMobile(BuildContext context) {
    return _buildBody(context);
  }

  @override
  Widget buildTablet(BuildContext context) {
    return _buildBody(context);
  }

  Widget _buildBody(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 20),
        color: Colors.white,
        alignment: Alignment.center,
        child: Column(
          children: [
            Image.asset(
              AppImages.imgLogo,
              scale: 1.5,
            ),
            SizedBox(
              height: 42,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15),
              child: Text(
                'Nhập số điện thoại của bạn vào ô dưới đây để đăng nhập',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 16,
                  color: Color(0xFF6D6D6D),
                  wordSpacing: 0,
                  // letterSpacing: 10,
                  height: 1.3,
                ),
              ),
            ),
            SizedBox(
              height: 30,
            ),
            TextField(
                decoration: InputDecoration(
                  isDense: true,
                  hintText: "Nhập số điện thoại",
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                  contentPadding: EdgeInsets.all(15),
                ),
                keyboardType: TextInputType.number),
            SizedBox(
              height: 30,
            ),
            GestureDetector(
              onTap: () => (_viewModel.onTapLogin()),
              child: Container(
                height: 45,
                width: MediaQuery.of(context).size.width,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.deepOrange),
                child: Text('Đăng nhập'),
              ),
            ),
            SizedBox(
              height: 100,
            ),
            Text('Hoặc đăng nhập bằng'),
            SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                iconButton(Image.asset(AppImages.iconGmail), Colors.white,
                    'Email', 35, () {}),
                iconButton(Image.asset(AppImages.iconFacebook),
                    Color.fromRGBO(59, 89, 152, 1), 'Facebook', 35, () {}),
                iconButton(Image.asset(AppImages.iconGoogle), Colors.white,
                    'Google', 35, () {
                  _viewModel.onTapLoginGoogle();
                }),
                iconButton(Image.asset(AppImages.iconApple), Colors.black,
                    'Apple', 35, () {}),
              ],
            )
          ],
        ),
      ),
    );
  }

  Widget iconButton(Image image, Color bgColor, String title, double size,
      Function function) {
    return Column(
      children: [
        GestureDetector(
          onTap: () {
            function();
          },
          child: Container(
            alignment: Alignment.center,
            width: size,
            height: size,
            child: image,
            decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: bgColor,
                border: Border.all(color: Colors.grey.shade300, width: 1)),
          ),
        ),
        Text(title)
      ],
    );
  }
}
