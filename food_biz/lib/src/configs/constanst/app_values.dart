import 'package:flutter/material.dart';

class AppValues {
  AppValues._();

  static const String APP_NAME = "FLUTTER APPLICATION";
  static const double INPUT_FORM_HEIGHT = 55;
  static List<Widget> LIST_RANK_0_INFOR = [
    RichText(
      softWrap: true,
      overflow: TextOverflow.clip,
      text: TextSpan(
        text: 'Nhận ',
        style: TextStyle(color: Colors.black),
        children: <TextSpan>[
          TextSpan(
            text: '1',
            style: TextStyle(
              color: Colors.blueAccent,
            ),
          ),
          TextSpan(
            text: 'điểm thưởng',
            style: TextStyle(
              color: Colors.black,
            ),
          ),
        ],
      ),
    ),
    RichText(
      softWrap: true,
      overflow: TextOverflow.clip,
      text: TextSpan(
        text: 'Thưởng ',
        style: TextStyle(color: Colors.black),
        children: <TextSpan>[
          TextSpan(
            text: '500 ',
            style: TextStyle(
              color: Colors.blueAccent,
            ),
          ),
          TextSpan(
            text: ' điểm khi lên hạng',
            style: TextStyle(
              color: Colors.black,
            ),
          ),
        ],
      ),
    ),
    Text('Giá ưu đãi cho bán sản phẩm mới'),
    Text('Hỗ trợ các giải pháp marketing'),
    Text('Hỗ trợ tư vấn kịch bản bán hàng'),
  ];
  static List<Widget> LIST_RANK_2_INFOR = [
    RichText(
      softWrap: true,
      overflow: TextOverflow.clip,
      text: TextSpan(
        text: 'Nhận ',
        style: TextStyle(color: Colors.black),
        children: <TextSpan>[
          TextSpan(
            text: '3',
            style: TextStyle(
              color: Colors.blueAccent,
            ),
          ),
          TextSpan(
            text: 'điểm thưởng',
            style: TextStyle(
              color: Colors.black,
            ),
          ),
        ],
      ),
    ),
    RichText(
      softWrap: true,
      overflow: TextOverflow.clip,
      text: TextSpan(
        text: 'Thưởng ',
        style: TextStyle(color: Colors.black),
        children: <TextSpan>[
          TextSpan(
            text: '5.000 ',
            style: TextStyle(
              color: Colors.blueAccent,
            ),
          ),
          TextSpan(
            text: ' điểm khi lên hạng',
            style: TextStyle(
              color: Colors.black,
            ),
          ),
        ],
      ),
    ),
    RichText(
      softWrap: true,
      overflow: TextOverflow.clip,
      text: TextSpan(
        children: <TextSpan>[
          TextSpan(
            text: '10',
            style: TextStyle(
              color: Colors.blueAccent,
            ),
          ),
          TextSpan(
            text:
                ' đơn Freeship đầu tiên (không giới hạn giá trị đơn hàng) khi lên rank',
            style: TextStyle(
              color: Colors.black,
            ),
          ),
        ],
      ),
    ),
    RichText(
      softWrap: true,
      overflow: TextOverflow.clip,
      text: TextSpan(
        children: <TextSpan>[
          TextSpan(
            text: '5',
            style: TextStyle(
              color: Colors.blueAccent,
            ),
          ),
          TextSpan(
            text: ' đơn Freeship tặng hàng tháng (có giới hạn giá trị đơn)',
            style: TextStyle(
              color: Colors.black,
            ),
          ),
        ],
      ),
    ),
    Text(
      'Giá ưu đãi cho bán sản phẩm mới',
      softWrap: true,
    ),
    Text(
      'Hỗ trợ các giải pháp marketing',
      softWrap: true,
    ),
    Text(
      'Hỗ trợ tư vấn kịch bản bán hàng',
      softWrap: true,
    ),
    Text(
      'Ưu tiên Sản phẩm Best-Sellers',
      softWrap: true,
    ),
  ];
}
