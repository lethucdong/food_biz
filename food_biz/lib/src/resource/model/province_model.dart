class ProvinceModel {
  String? name;
  int? code;
  String? divisionType;
  String? codename;
  int? phoneCode;
  List<Districts>? districts;

  ProvinceModel(
      {this.name,
      this.code,
      this.divisionType,
      this.codename,
      this.phoneCode,
      this.districts});

  ProvinceModel.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    code = json['code'];
    divisionType = json['division_type'];
    codename = json['codename'];
    phoneCode = json['phone_code'];
    if (json['districts'] != null) {
      districts = new List<Districts>.empty(growable: true);
      json['districts'].forEach((v) {
        districts!.add(new Districts.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['code'] = this.code;
    data['division_type'] = this.divisionType;
    data['codename'] = this.codename;
    data['phone_code'] = this.phoneCode;
    if (this.districts != null) {
      data['districts'] = this.districts!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Districts {
  String? name;
  int? code;
  String? divisionType;
  String? codename;
  int? provinceCode;

  Districts({
    this.name,
    this.code,
    this.divisionType,
    this.codename,
    this.provinceCode,
  });

  Districts.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    code = json['code'];
    divisionType = json['division_type'];
    codename = json['codename'];
    provinceCode = json['province_code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['code'] = this.code;
    data['division_type'] = this.divisionType;
    data['codename'] = this.codename;
    data['province_code'] = this.provinceCode;
    return data;
  }
}

class ListProvinceModel {
  ListProvinceModel({
    this.provinces,
  });

  List<ProvinceModel>? provinces;

  factory ListProvinceModel.fromJson(Map<String, dynamic> json) =>
      ListProvinceModel(
          provinces: List<ProvinceModel>.from(
              json["provinces"].map((x) => ProvinceModel.fromJson(x))));

  Map<String, dynamic> toJson() => {
        "provinces": List<dynamic>.from(provinces!.map((x) => x.toJson())),
      };
}
