class BannerModel {
  late int id;
  late int typeId;
  late int serviceItemId;
  late String link;
  late int fixed;
  late String fileName;

  BannerModel(
      {required this.id,
      required this.typeId,
      required this.serviceItemId,
      required this.link,
      required this.fixed,
      required this.fileName});

  BannerModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    typeId = json['type_id'];
    serviceItemId = json['service_item_id'];
    link = json['link'];
    fixed = json['fixed'];
    fileName = json['file_name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['type_id'] = this.typeId;
    data['service_item_id'] = this.serviceItemId;
    data['link'] = this.link;
    data['fixed'] = this.fixed;
    data['file_name'] = this.fileName;
    return data;
  }
}

class ListBannerModel {
  ListBannerModel({
    this.banners,
  });

  List<BannerModel>? banners;

  factory ListBannerModel.fromJson(Map<String, dynamic> json) =>
      ListBannerModel(
          banners: List<BannerModel>.from(
              json["banners"].map((x) => BannerModel.fromJson(x))));

  Map<String, dynamic> toJson() => {
        "banners": List<dynamic>.from(banners!.map((x) => x.toJson())),
      };
}
