import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter_app/src/configs/configs.dart';
import 'package:flutter_app/src/utils/utils.dart';

import '../resource.dart';

class ProvinceRepository {
  ProvinceRepository._();

  static ProvinceRepository? _instance;

  factory ProvinceRepository() {
    if (_instance == null) _instance = ProvinceRepository._();
    return _instance!;
  }

  Future<NetworkState<List<ProvinceModel>>> getProvince() async {
    bool isDisconnect = await WifiService.isDisconnect();
    if (isDisconnect) return NetworkState.withDisconnect();

    try {
      Response response =
          await Dio().get('https://provinces.open-api.vn/api/?depth=2',
              options: Options(headers: {
                HttpHeaders.contentTypeHeader: "application/json",
              }));

      NetworkState<List<ProvinceModel>> result = NetworkState(
          status: response.statusCode,
          data: ListProvinceModel.fromJson(
            {'provinces': response.data},
          ).provinces,
          message: 'OK');

      if (result.isSuccess && !result.isError) {
        return result;
      } else
        throw DioErrorType.cancel;

      // return NetworkState(
      //     status: response.statusCode, response: NetworkResponse.fromJson(response.data, converter: (data) => CategoryModel.fromJson(data)));
    } on DioError catch (e) {
      return NetworkState.withError(e);
    }
  }
}
