import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter_app/src/configs/configs.dart';

import '../resource.dart';

class BannerRepository {
  BannerRepository._();

  static BannerRepository? _instance;

  factory BannerRepository() {
    if (_instance == null) _instance = BannerRepository._();
    return _instance!;
  }

  Future<NetworkState<List<BannerModel>>> getListBanner() async {
    bool isDisconnect = await WifiService.isDisconnect();
    if (isDisconnect) return NetworkState.withDisconnect();

    try {
      Response response =
          await Dio().get(AppEndpoint.BASE_URL + AppEndpoint.GET_LIST_BANNER,
              options: Options(headers: {
                HttpHeaders.contentTypeHeader: "application/json",
              }));

      NetworkState<List<BannerModel>> result = NetworkState(
          status: response.statusCode,
          data: ListBannerModel.fromJson(
            {'banners': response.data['data']},
          ).banners,
          message: response.data['message']);

      if (result.isSuccess && !result.isError) {
        return result;
      } else
        throw DioErrorType.cancel;

      // return NetworkState(
      //     status: response.statusCode, response: NetworkResponse.fromJson(response.data, converter: (data) => CategoryModel.fromJson(data)));
    } on DioError catch (e) {
      return NetworkState.withError(e);
    }
  }
}
